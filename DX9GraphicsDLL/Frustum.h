#pragma once
#include "MathLibrary.h"

struct D3D9Vertex_Colored
{
    D3DXVECTOR3 position; // The position
    D3DCOLOR    color;    // The color

	static const DWORD FVF;
};

class FPlane : public D3DXPLANE
{
public:

	inline void Normalize();

	inline void Init(const DXMathLibrary::Vector3 &p0, 
					 const DXMathLibrary::Vector3 &p1, 
					 const DXMathLibrary::Vector3 &p2);

	bool Inside(const DXMathLibrary::Vector3 &point, const float radius) const;
	bool Inside(const DXMathLibrary::Vector3 &point) const;
};

class Frustum
{
public:

	enum Side {Near, Far, Top, Right, Bottom, Left, NumPlanes};
	
	FPlane m_Planes[NumPlanes];
	DXMathLibrary::Vector3 m_NearClip[4];
	DXMathLibrary::Vector3 m_FarClip[4];

	float m_Fov;
	float m_Aspect;
	float m_Near;
	float m_Far;

public:
	Frustum();

	bool Inside(const DXMathLibrary::Vector3 &point) const;
	bool Inside(const DXMathLibrary::Vector3 &point, const float radius) const;

	const FPlane &Get(Side side)				{ return m_Planes[side]; }

	void		 SetFOV(float fov)				{ m_Fov = fov; Init(m_Fov, m_Aspect, m_Near, m_Far); }
	void		 SetAspect(float aspect)		{ m_Aspect = aspect; Init(m_Fov, m_Aspect, m_Near, m_Far); }
	
	void		 Init(const float fov, 
					  const float aspect, 
					  const float nearZ, 
					  const float farZ);

	void		 Render();

};