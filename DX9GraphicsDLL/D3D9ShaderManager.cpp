#include "D3D9ShaderManager.h"
#include "JSONParser.h"
#include <vector>
#include <windowsx.h>

using namespace std;
using namespace Utility;

D3D9ShaderManager::D3D9ShaderManager()
{

}

D3D9ShaderManager::~D3D9ShaderManager()
{
	for(auto FX : m_Effects)
	{
		delete FX.second;
	}
}

//Usage Functions
//=================================
D3D9ShaderEffect* D3D9ShaderManager::GetShaderEffect(string ShaderName)
{
	return m_Effects.at(ShaderName);
}
//*********************************

//Automated effect creation
//=================================
void D3D9ShaderManager::LoadEffectsFromFile(LPDIRECT3DDEVICE9 device)
{
	vector<char> path(MAX_PATH);
	DWORD result = GetModuleFileNameA(nullptr, &path[0], static_cast<DWORD>(path.size()));
	string BasePath = string(path.begin(), path.begin() + result);

	int position = 0;
	for(auto it = BasePath.rbegin();
		it != BasePath.rend();
		it++)
	{
		if(*it == '\\')
			position++;

		if(position == 2)
		{
			BasePath = string(BasePath.begin(), it.base());
			break;
		}
	}

	vector<string> ShaderLocations;

	WIN32_FIND_DATA ffd;
	HANDLE hFind = INVALID_HANDLE_VALUE;
	string spec = BasePath + "Mantis\\Assets\\Shaders\\Saved\\*";

	hFind = FindFirstFile(spec.c_str(), &ffd);

	do {
		if((string(ffd.cFileName) != ".") && (string(ffd.cFileName) != "..")){
			if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
				//ignore files
				ShaderLocations.push_back("Assets/Shaders/Saved/" + string(ffd.cFileName));
			}
			else {
				ShaderLocations.push_back("Assets/Shaders/Saved/" + string(ffd.cFileName)); 
			}
		}
	} while (FindNextFile(hFind, &ffd) != 0);

	FindClose(hFind);

	JSONParser parser;
	JSONObject shader;

	for(auto location : ShaderLocations)
	{
		parser.OpenFile(location);
		shader = parser.GetParsedObject();

		D3D9ShaderEffect *newShader = new D3D9ShaderEffect(shader.m_variables["FileName"], device);

		for(auto technique : shader.m_arrays["Techniques"].m_values)
		{
			newShader->CreateNewTechnique(technique.m_variables["TechniqueName"]);
		}

		for(auto paramater : shader.m_arrays["Paramaters"].m_values)
		{
			newShader->CreateNewParamater(paramater.m_variables["ParamaterName"]);
		}

		m_Effects.insert(make_pair(shader.m_variables["Name"], newShader));
	}
}
//*********************************

//Manually Create Effect
//=================================
void D3D9ShaderManager::CreateNewEffect(string EffectName, string ShaderFileName, LPDIRECT3DDEVICE9 device)
{
	m_Effects.insert(make_pair(EffectName, new D3D9ShaderEffect(ShaderFileName, device)));
}
//*********************************