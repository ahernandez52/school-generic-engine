#include "Scene.h"


using namespace std;

Scene::Scene(LPDIRECT3DDEVICE9 device)
{
	m_Device = device;
	
	m_Root = new RootNode();

	D3DXCreateMatrixStack(0, &m_MatrixStack);
}


Scene::~Scene()
{
	m_MatrixStack->Release();
}

void Scene::OnLostDevice()
{
	if(m_Root) m_Root->OnLostDevice(this);
}

void Scene::OnResetDevice()
{
	if(m_Root) m_Root->OnResetDevice(this);
}

void Scene::Update(float dt)
{
	if(!m_Root) return; //No Scene

	m_Root->Update(this, dt);
}

void Scene::Render()
{
	if(m_Root && m_Camera)
	{
		m_Camera->SetViewTransform(this);

		m_Root->PreRender(this);

		m_Root->Render(this);

		m_Root->RenderChildren(this);

		m_Root->PostRender(this);
	}
}

ISceneNode* Scene::FindNode(int id)
{
	auto result = m_NodeMap.find(id);

	if(result == m_NodeMap.end())
		return nullptr;

	return m_NodeMap.at(id);
}

bool Scene::AddChild(int id, ISceneNode* kid)
{
	if(id != 0)
	{
		m_NodeMap.insert(make_pair(id, kid));
	}

	return m_Root->AddChild(kid);
}

bool Scene::RemoveChild(int id)
{
	if(id == 0) return false;

	m_NodeMap.erase(id);

	return m_Root->RemoveChild(id);
}