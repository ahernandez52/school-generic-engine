#pragma once
#include "SceneNodeComponents.h"

//
//Clearly defines what a Scene node base class is
//and what is expected to carry out in its lifetime
//

//Foward Declarations
class Scene;

class ISceneNode
{
public:

	NodeType Type;

	virtual const SceneNodeComponent* const Component() const = 0; //Access to a Node's properties

	//Transforms
	virtual void SetTransform(const Matrix4 *toWorld, const Matrix4 *fromWorld = nullptr) = 0;

	//Device lost/reset functions
	virtual void OnLostDevice(Scene *scene) = 0;
	virtual void OnResetDevice(Scene *scene) = 0;

	//Update Functions
	virtual void Update(Scene *scene, float dt) = 0;

	//Render functions
	virtual void PreRender(Scene *scene) = 0;
	virtual void Render(Scene *scene) = 0;
	virtual void RenderChildren(Scene *scene) = 0;
	virtual void PostRender(Scene *scene) = 0;

	//Child Management
	virtual bool AddChild(ISceneNode *child) = 0;
	virtual bool RemoveChild(int id) = 0;

	//Utility
	virtual bool IsVisible(Scene *scene) const = 0;
	virtual bool Pick(Scene *scene, Ray *RayCast) = 0;

	virtual ~ISceneNode() {};
};

//Typedefines
typedef std::vector<ISceneNode*>	SceneNodeList;	
typedef std::map<int, ISceneNode*>	SceneNodeMap;	//ID, Node