#include "GraphicsCore.h"
#include "DX9GraphicsPipeline.h"

namespace DX9GraphicsCore
{
	GraphicsCore::GraphicsCore()
	{

	}

	GraphicsCore::~GraphicsCore()
	{

	}
	
	void GraphicsCore::Startup(LPDIRECT3DDEVICE9 device)
	{
		core = new DX9GraphicsPipeline(device);
	}

	void GraphicsCore::Shutdown()
	{
		delete core;
	}

	void GraphicsCore::OnResetDevice()
	{
		core->OnResetDevice();
	}

	void GraphicsCore::OnLostDevice()
	{
		core->OnLostDevice();
	}

	void GraphicsCore::Update(float dt)
	{
		core->Update(dt);
	}

	void GraphicsCore::Render()
	{
		core->Render();
	}

	void GraphicsCore::NewSphere(int id, D3DXVECTOR3 pos, float radius)
	{
		core->NewSphere(id, pos, radius);
	}

	void GraphicsCore::NewBox(int id, D3DXVECTOR3 pos, D3DXVECTOR3 halfLengths, D3DXQUATERNION orin)
	{
		core->NewBox(id, pos, halfLengths, orin);
	}

	void GraphicsCore::SyncBox(int id, D3DXVECTOR3 pos, D3DXQUATERNION orin)
	{
		core->SyncComponent(id, pos, orin);
	}

	//Core API
	//==============================

	//Updates
	void GraphicsCore::UpdateCamera(Vector3 pos, float pitch, float yaw, float speed)
	{
		core->camera->ApplyInput(pos, pitch, yaw, speed);
	}

	//Setters
	void GraphicsCore::SetCameraTarget(Vector3 target)
	{

	}

	//bool GraphicsCore::HandleMessage(int value, DWORD value1, void* extra)
	//{
	//	return true;
	//}
}