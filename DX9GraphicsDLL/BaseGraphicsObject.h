#pragma once
#include "D3D9GraphicsUtility.h"

//enum ObjectType
//{
//	MESH = 0,
//	PARTICLE,
//};
//
//class BaseGraphicsObject
//{
//friend class GraphicsFactory;
//protected:
//
//	int							m_ID;
//	int							m_OwningID;
//	int							m_Type;
//
//	float						m_Radius;
//
//	Transform					m_Transform;
//
//public:
//
//	BaseGraphicsObject() {}
//	BaseGraphicsObject(DXMathLibrary::Vector3 position, DXMathLibrary::Vector3 scale, DXMathLibrary::Quaternion orientation)
//	{
//		m_Transform.Position		= position;
//		m_Transform.Scale			= scale;
//		m_Transform.Orientation	= orientation;
//	}
//
//	virtual ~BaseGraphicsObject() {}
//
//	int							ID()			{ return m_ID; }
//	int							OwnerID()		{ return m_OwningID; }
//
//	float						Radius()		{ return m_Radius; }
//
//	DXMathLibrary::Vector3&		Position()		{ return m_Transform.Position; }
//	DXMathLibrary::Vector3&		Scale()			{ return m_Transform.Scale; }
//	DXMathLibrary::Quaternion&	Orientation()	{ return m_Transform.Orientation; }
//	Transform&					Transform()		{ return m_Transform; }
//};