#include "SceneNodes.h"
#include "Scene.h"
#include "SceneNodeComponents.h"

//SceneNodeProperties::SceneNodeProperties()
//{
//	m_NodeID		= 0;
//	m_Radius		= 0.0f;
//	m_RenderPass	= RenderPass_0;
//	//m_AlphaType		= AlphaOpaque;
//}
//
//void SceneNodeProperties::BuildTransform()
//{
//
//}
//
//void SceneNodeProperties::Transform(Matrix4 *toWorld, Matrix4 *fromWorld) const
//{
//	if(toWorld)		*toWorld = m_ToWorld;
//	if(fromWorld)	*fromWorld = m_FromWorld;
//}






SceneNode::SceneNode()
{
	m_Parent					= nullptr;
	m_Component					= nullptr;

	SetTransform(&Matrix4(), nullptr);
	SetRadius(0);
}

SceneNode::SceneNode(SceneNodeComponent *component,
					 const Matrix4 *to,
					 const Matrix4 *from)
{
	m_Parent					= nullptr;
	m_Component					= component;

	//m_Component->m_NodeID		= id;
	//m_Component->m_RenderPass	= renderPass;
	//m_Properties.m_AlphaType	= AlphaOpaque;

	SetTransform(to, from);
	SetRadius(0);
}

SceneNode::~SceneNode()
{

}

void SceneNode::SetTransform(const Matrix4 *toWorld, const Matrix4 *fromWorld)
{
	m_Component->m_ToWorld = *toWorld;
	if(!fromWorld)
	{
		m_Component->m_FromWorld = m_Component->m_ToWorld.Inverse();
	}
	else
	{
		m_Component->m_FromWorld = *fromWorld;
	}
}

void SceneNode::OnLostDevice(Scene *scene)
{
	for(auto node : m_Children)
		node->OnLostDevice(scene);
}

void SceneNode::OnResetDevice(Scene *scene)
{
	for(auto node : m_Children)
		node->OnResetDevice(scene);
}

void SceneNode::Update(Scene *scene, float dt)
{
	for(auto node : m_Children)
		node->Update(scene, dt);
}

void SceneNode::PreRender(Scene *scene)
{
	//check if component position, orientation needs to be syncd

	scene->PushAndSetMatrix(m_Component->m_ToWorld);
}

//void SceneNode::Render(Scene *scene)
//{
//
//}

void SceneNode::RenderChildren(Scene *scene)
{
	for(auto child : m_Children)
	{
		child->PreRender(scene);

		if(child->IsVisible(scene))
		{
			child->Render(scene);

			child->RenderChildren(scene);
		}

		child->PostRender(scene);
	}
}

void SceneNode::PostRender(Scene *scene)
{
	scene->PopMatrix();
}

bool SceneNode::AddChild(ISceneNode *kid)
{
	m_Children.push_back(kid);

	SceneNode* child = static_cast<SceneNode*>(kid);

	child->m_Parent = this;

	Vector3 childPosition = child->Component()->Position();

	float radius = childPosition.Length() + child->Component()->Radius();

	if(radius > m_Component->m_Radius)
		m_Component->m_Radius = radius;

	return true;
}

bool SceneNode::RemoveChild(int id)
{
	for(auto it = m_Children.begin();
		it != m_Children.end();
		++it)
	{
		int childID = (*it)->Component()->m_NodeID;
		if((childID == id) && (childID != 0))
		{
			m_Children.erase(it);
			return true;
		}
	}
}

bool SceneNode::IsVisible(Scene *scene) const
{
	Matrix4 toWorld, fromWorld;
	scene->GetCamera()->Component()->Transform(&toWorld, &fromWorld);

	Vector3 position =  WorldPosition();

	Vector3 fromWorldPosition = fromWorld.Xform(position);

	Frustum const &frustum = scene->GetCamera()->GetFrustum();

	return frustum.Inside(fromWorldPosition, Component()->Radius());
}

bool SceneNode::Pick(Scene *scene, Ray *RayCast)
{
	for(auto node : m_Children)
	{
		return node->Pick(scene, RayCast); 
	}

	return false;
}

const Vector3 SceneNode::WorldPosition() const
{
	Vector3 pos = Position();
	if(m_Parent)
	{
		pos += m_Parent->WorldPosition();
	}

	return pos;
}






void D3D9SceneNode::Render(Scene *scene)
{
	//m_Component->GetMaterial().UseMaterial9();

	//Check for alpha render state changes
}






RootNode::RootNode() : SceneNode(new NodeComponent(RenderPass_0, 0), &Matrix4())
{
	m_Children.reserve(RenderPass_Last);

	//m_Children.push_back(new SceneNode(0, RenderPass_static, &Matrix4()));
	//m_Children.push_back(new SceneNode(0, RenderPass_Actor, &Matrix4()));
	//m_Children.push_back(new SceneNode(0, RenderPass_Sky, &Matrix4()));
	//m_Children.push_back(new SceneNode(0, RenderPass_NotRendered, &Matrix4()));

	m_Children.push_back(new SceneNode(new NodeComponent(RenderPass_static, 0), &Matrix4()));
	m_Children.push_back(new SceneNode(new NodeComponent(RenderPass_Actor, 0), &Matrix4()));
	m_Children.push_back(new SceneNode(new NodeComponent(RenderPass_Sky, 0), &Matrix4()));
	m_Children.push_back(new SceneNode(new NodeComponent(RenderPass_NotRendered, 0), &Matrix4()));
}

bool RootNode::AddChild(ISceneNode *kid)
{
	RenderPass pass = kid->Component()->Pass();
	if(pass >= m_Children.size() || !m_Children.at(pass))
	{
		return false; //Was unable to add
	}

	return m_Children.at(pass)->AddChild(kid);
}

// Could have multiple nodes beloning to an Actor
// Do not immidiately quit on removing a node
bool RootNode::RemoveChild(int id)
{
	bool removed = false;

	for(auto pass : m_Children)
	{
		if(pass->RemoveChild(id))
			removed = true;
	}

	return removed;
}

//Renders the different passes
void RootNode::RenderChildren(Scene *scene)
{
	for(int pass = RenderPass_0; pass < RenderPass_Last; ++pass)
	{
		switch(pass)
		{
			case RenderPass_static:
			case RenderPass_Actor:
				m_Children.at(pass)->RenderChildren(scene);
				break;

			case RenderPass_Sky:
			{
				//prepare device for skybox render
				//m_Children.at(pass)->RenderChildren(scene);
				break;
			}
		}
	}
}






void CameraNode::Render(Scene *scene)
{
	if(m_DebugCamera)
	{
		scene->PopMatrix();

		m_Frustum.Render();

		scene->PushAndSetMatrix(m_Component->ToWorld());
	}
}

void CameraNode::OnResetDevice(Scene *scene)
{
	D3DVIEWPORT9 viewport;
	g_d3dDevice->GetViewport(&viewport);

	m_Frustum.SetAspect((float)viewport.Width / (float)viewport.Height);
	D3DXMatrixPerspectiveFovLH(&m_Projection, m_Frustum.m_Fov, m_Frustum.m_Aspect, m_Frustum.m_Near, m_Frustum.m_Far);
	
	//scene->GetRenderer()->SetProjectionTransform(&m_Projection);
	//g_d3dDevice->SetTransform(D3DTS_PROJECTION, &m_Projection);
}

void CameraNode::SetViewTransform(Scene *scene)
{
	if(m_Target)
	{
		Matrix4 matrix = m_Target->Component()->ToWorld();
		Vector4 at = m_OffsetVector;
		Vector4 atWorld = matrix.Xform(at);
		Vector3 pos = matrix.GetPosition() + Vector3(atWorld);
		matrix.SetPosition(pos);
		SetTransform(&matrix);
	}

	m_View = Component()->FromWorld();

	//g_d3dDevice->SetTransform(D3DTS_VIEW, &m_View);
	//scene->GetRenderer()->SetViewTransform(&m_View);
}

void CameraNode::BuildView()
{
	m_Forward.Normalize();

	m_Up = m_Forward.Cross(m_Right);
	m_Up.Normalize();

	m_Right = m_Up.Cross(m_Forward);
	m_Right.Normalize();

	float x = -Component()->ToWorld().GetPosition().Dot(m_Right);
	float y = -Component()->ToWorld().GetPosition().Dot(m_Up);
	float z = -Component()->ToWorld().GetPosition().Dot(m_Forward);

	m_View(0, 0) = m_Right.x;
	m_View(1, 0) = m_Right.y;
	m_View(2, 0) = m_Right.z;
	m_View(3, 0) = x;

	m_View(0, 1) = m_Up.x;
	m_View(1, 1) = m_Up.y;
	m_View(2, 1) = m_Up.z;
	m_View(3, 1) = y;

	m_View(0, 2) = m_Forward.x;
	m_View(1, 2) = m_Forward.y;
	m_View(2, 2) = m_Forward.z;
	m_View(3, 2) = z;

	m_View(0, 3) = 0.0f;
	m_View(1, 3) = 0.0f;
	m_View(2, 3) = 0.0f;
	m_View(3, 3) = 1.0f;
}

Matrix4 CameraNode::GetWVP(Scene *scene)
{
	Matrix4 world = scene->GetTopMatrix();
	
	BuildView();

	Matrix4 worldView = world * m_View;

	return worldView * m_Projection;
}