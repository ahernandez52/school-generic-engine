#pragma once

#include <d3dx9.h>

class D3D9SkyBox
{
public:
	D3D9SkyBox(char* filename, float radius, LPDIRECT3DDEVICE9 device);
	~D3D9SkyBox();

	void onLostDevice();
	void onResetDevice();

	void Render(D3DXMATRIX ViewProjection, D3DXVECTOR3 CameraPosition);

private:
	float m_radius;
	ID3DXMesh* m_sphere;
	IDirect3DCubeTexture9* m_cubeMap;
	ID3DXEffect* m_FX;
	D3DXHANDLE m_hTech;
	D3DXHANDLE m_hcube;
	D3DXHANDLE m_hWVP;

};

