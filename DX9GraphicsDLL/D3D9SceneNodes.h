#pragma once
//#include "BaseGraphicsObject.h"
#include "SceneNodes.h"

//OLD
//class D3D9Mesh : public BaseGraphicsObject
//{
//public:
//	Material		m_material;
//
//	ID3DXMesh		   *m_mesh;
//	MeshMap				m_maps[5];
//
//public:
//
//	D3D9Mesh();
//	D3D9Mesh(GraphicSchematic schem, int ownerID);
//};
//==========================

class D3D9MeshNode : public D3D9SceneNode
{
public: //protected

	//ID3DXMesh *m_Mesh;
	//LPDIRECT3DTEXTURE9 m_Tex;

public:

	D3D9MeshNode(Mesh* component,
				 const Matrix4 *t);

	virtual ~D3D9MeshNode() { }

	void Render(Scene *scene);

	//virtual void OnResetDevice(Scene *scene);
	//virtual void OnLostDevice(Scene *scene);

	virtual bool Pick(Scene *scene, Ray *rayCast);

	float CalculateBVSphere();
};






//class D3D9ShaderMeshNode : public D3D9MeshNode
//{
//protected:
//
//	ID3DXEffect *m_FX;
//
//public:
//
//	D3D9ShaderMeshNode(const int id,
//					   ID3DXMesh *mesh,
//					   ID3DXEffect *fx,
//					   RenderPass pass,
//					   const Matrix4 *t);
//
//	virtual ~D3D9ShaderMeshNode() { }
//
//	virtual void Render(Scene *scene);
//
//	virtual void OnResetDevice(Scene *scene);
//	virtual void OnLostDevice(Scene *scene);
//};