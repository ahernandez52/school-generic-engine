#include "D3D9SceneNodes.h"

#include "Scene.h"

// OLD
//D3D9Mesh::D3D9Mesh()
//{
//	m_Transform.Position		= Vector3(0.0f, 0.0f, 0.0f);
//	m_Transform.Scale			= Vector3(1.0f, 1.0f, 1.0f);
//	m_Transform.Orientation		= Quaternion();
//
//	m_material.SetAmbient(g_White);
//	m_material.SetDiffuse(g_Gray40);
//	m_material.SetSpecular(g_Gray25, 45.0f);
//
//	m_Type = MESH;
//}
//
//D3D9Mesh::D3D9Mesh(GraphicSchematic schematic, int ownerID)
//{
//	m_Transform.Position		= Vector3(0.0f, 0.0f, 0.0f);
//	m_Transform.Scale			= Vector3(1.0f, 1.0f, 1.0f);
//	m_Transform.Orientation		= Quaternion();
//
//	m_material.SetAmbient(g_White);
//	m_material.SetDiffuse(g_Gray40);
//	m_material.SetSpecular(g_Gray25, 45.0f);
//
//	m_Type = MESH;
//
//	//m_maps = schematic.Maps;
//	m_maps[0] = schematic.Maps[0];
//	m_maps[1] = schematic.Maps[1];
//	m_maps[2] = schematic.Maps[2];
//	m_maps[3] = schematic.Maps[3];
//	m_maps[4] = schematic.Maps[4];
//
//	m_mesh = schematic.Mesh;
//
//	m_OwningID = ownerID;
//	m_ID = 0;
//}
//=====================================





D3D9MeshNode::D3D9MeshNode(Mesh* component,
						   const Matrix4 *t) : D3D9SceneNode(component, t)
{
	Type = NodeType_D3D9_Mesh;
	
	SetRadius(CalculateBVSphere());
}

float D3D9MeshNode::CalculateBVSphere()
{
	float radius;
	D3DXVECTOR3* data;
	D3DXVECTOR3 center;

	ID3DXMesh *mesh = static_cast<Mesh*>(m_Component)->GetMesh();
	
	mesh->LockVertexBuffer(0, (LPVOID*)&data);
	D3DXComputeBoundingSphere(data, mesh->GetNumVertices(), D3DXGetFVFVertexSize(mesh->GetFVF()), &center, &radius);
	mesh->UnlockVertexBuffer();

	return radius;
}

void D3D9MeshNode::Render(Scene *scene)
{
	//m_Mesh->DrawSubset(0);

	//m_Component->ToWorld() = scene->GetTopMatrix();

	static_cast<Mesh*>(m_Component)->m_ToWorld = scene->GetTopMatrix();

	static_cast<Mesh*>(m_Component)->m_Options |= ComponentOption_Render;
}

//void D3D9MeshNode::OnResetDevice(Scene *scene)
//{
//	
//}
//
//void D3D9MeshNode::OnLostDevice(Scene *scene)
//{
//
//}

bool D3D9MeshNode::Pick(Scene *scene, Ray *rayCast)
{
	if(!SceneNode::Pick(scene, rayCast))
		return false;

	scene->PushAndSetMatrix(m_Component->ToWorld());
	//bool result = rayCast->Pick(scene, m_Properties.NodeID(), m_Mesh);

	scene->PopMatrix();

	//return result;
	return false;
}





//D3D9ShaderMeshNode::D3D9ShaderMeshNode(const int id,
//									   ID3DXMesh *mesh,
//									   ID3DXEffect *fx,
//									   RenderPass pass,
//									   const Matrix4 *t) : D3D9MeshNode(id, mesh, pass, t)
//{
//	m_FX = fx;
//}
//
//void D3D9ShaderMeshNode::OnLostDevice(Scene *scene)
//{
//
//}
//
//void D3D9ShaderMeshNode::OnResetDevice(Scene *scene)
//{
//
//}
//
//void D3D9ShaderMeshNode::Render(Scene *scene)
//{
//	Matrix4 wvp = scene->GetCamera()->GetWVP(scene);
//	Matrix4 world = scene->GetTopMatrix();
//	Matrix4 wi;
//
//	//world.SetPosition(Vector3(0.0f, 0.0f, 100.0f));
//
//	DirectionalLight light;
//
//	light.ambient		= Color(1.0f, 1.0f, 1.0f, 1.0f);
//	light.diffuse		= Color(1.0f, 1.0f, 1.0f, 1.0f);
//	light.specular		= Color(1.0f, 0.0f, 0.0f, 1.0f);
//	light.directionWorld= Vector3(-0.577f, -0.577f, -0.577f);
//	light.directionWorld.Normalize();
//
//	Material mat;
//	mat.SetAmbient(g_White);
//	mat.SetDiffuse(g_Gray40);
//	mat.SetSpecular(g_Gray25, 45.0f);
//	
//	m_FX->SetValue("gMtrl", &mat.GetD3D9(), sizeof(D3DMATERIAL9));
//	m_FX->SetValue("gLight", &light, sizeof(DirectionalLight));
//
//	m_FX->SetValue("gEyePos", &scene->GetCamera()->Position(), sizeof(D3DXVECTOR3));
//
//	m_FX->SetMatrix("gWVP", &wvp);
//	m_FX->SetMatrix("gWorld", &world);
//
//	wi = world.Inverse();
//	D3DXMatrixTranspose(&wi, &wi);
//
//	m_FX->SetMatrix("gWorldInv", &wi);
//
//	m_FX->SetTexture("gTex", m_Tex);
//
//	UINT numPasses = 0;
//	m_FX->Begin(&numPasses, 0);
//	m_FX->BeginPass(0);
//
//	m_Mesh->DrawSubset(0);
//
//	m_FX->EndPass();
//	m_FX->End();
//}