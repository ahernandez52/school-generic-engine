#pragma once
#include "ISceneNode.h"
#include "Frustum.h"


//	Scene Node Properties			
//	holds unique values per node	
//////////////////////////////////////
//class SceneNodeProperties
//{
//friend class SceneNode;
//protected:
//
//	int		m_NodeID;
//
//	Matrix4		m_ToWorld;
//	Matrix4		m_FromWorld;
//
//	float		m_Radius;
//
//	RenderPass	m_RenderPass;
//	Material	m_Material;
//
//public:
//
//	SceneNodeProperties();
//
//	const int		&NodeID() const		{ return m_NodeID; }
//	Matrix4 const	&ToWorld() const	{ return m_ToWorld; }
//	Matrix4 const	&FromWorld() const	{ return m_FromWorld; }
//
//	Vector3 const	&Position() const	{ return m_Position; }
//
//	void			BuildTransform();
//	void			Transform(Matrix4 *toWorld, Matrix4 *fromWorld) const;
//
//	bool			HasAlpha() const	{ return m_Material.HasAlpha(); }
//	float			Alpha() const		{ return m_Material.GetAlpha(); }
//
//	RenderPass		RenderPass() const	{ return m_RenderPass; }
//	float			Radius() const		{ return m_Radius; }
//
//	Material		GetMaterial() const	{ return m_Material; }
//};





class SceneNode : public ISceneNode
{
friend class Scene;
protected:

	SceneNodeList			m_Children;
	SceneNode			   *m_Parent;
	SceneNodeComponent	   *m_Component;

public:

	SceneNode();
	SceneNode(SceneNodeComponent *component,
			  const Matrix4 *to,
			  const Matrix4 *from = nullptr);

	virtual ~SceneNode();

	virtual const SceneNodeComponent* const Component() const { return m_Component; }
	 
	virtual void SetTransform(const Matrix4 *toWorld, const Matrix4 *fromWorld = nullptr);

	virtual void OnLostDevice(Scene *scene);
	virtual void OnResetDevice(Scene *scene);

	virtual void Update(Scene *scene, float dt);

	virtual void PreRender(Scene *scene);
	virtual void Render(Scene *scene) { }
	virtual void RenderChildren(Scene *scene);
	virtual void PostRender(Scene *scene);

	virtual bool AddChild(ISceneNode *kid);
	virtual bool RemoveChild(int id);

	virtual bool IsVisible(Scene *scene) const;
	virtual bool Pick(Scene *scene, Ray *RayCast);

	//Scene Node Specific functions

	Vector3			Position() const						{ return m_Component->m_ToWorld.GetPosition(); }
	void			SetPosition(const Vector3 &pos)			{ m_Component->m_ToWorld.SetPosition(pos); }

	const Vector3	WorldPosition() const;

	Vector3			GetDirection() 							{ return m_Component->m_ToWorld.GetDirection(); }

	void			SetRadius(const float radius)			{ m_Component->m_Radius = radius; }
	//void			SetMaterial(const Material &mat)		{ m_Component->m_Material = mat; }
};






class D3D9SceneNode : public SceneNode
{
public:
	D3D9SceneNode(SceneNodeComponent *component,
				  const Matrix4 *t) : SceneNode(component, t) 
	{ 
		Type = NodeType_D3D9;
	}

	virtual void Render(Scene *scene);
};






class RootNode : public SceneNode
{
public:
	RootNode();

	virtual void RenderChildren(Scene *scene);

	virtual bool AddChild(ISceneNode* child);
	virtual bool RemoveChild(int id);

	virtual bool IsVisible(Scene *scene) const { return true; }
};






class CameraNode : public SceneNode
{
public:

	CameraNode(Matrix4 const *t, Frustum const &frustum) 
		: SceneNode(new NodeComponent(RenderPass_0, 0), t)
	{
		m_Frustum		= frustum;
		m_Active		= true;
		m_DebugCamera	= false;
		m_Target		= nullptr;
		m_OffsetVector	= Vector4(0.0f, 1.0f, -10.0f, 0.0f);

		m_Right			= Vector3(1.0f, 0.0f, 0.0f);
		m_Up			= Vector3(0.0f, 1.0f, 0.0f);
		m_Forward		= Vector3(0.0f, 0.0f, 1.0f);

		Type = NodeType_Camera;
	}

	virtual void	OnResetDevice(Scene *scene);

	virtual void	Render(Scene *scene);

	virtual bool	IsVisible(Scene *scene) const					{ return m_Active; }

	const Frustum	&GetFrustum()									{ return m_Frustum; }
	void			SetTarget(SceneNode *target)					{ m_Target = target; }
	void			ClearTarget()									{ m_Target = nullptr; }
	SceneNode*		GetTarget()										{ return m_Target; }

	void			BuildView();
	Matrix4			GetWVP(Scene *scene);
	void			SetViewTransform(Scene *scene);

	Matrix4			GetProjection()									{ return m_Projection; }
	Matrix4			GetView()										{ return m_View; }

	void			SetCameraOffset(const Vector4 &cameraOffset)	{ m_OffsetVector = cameraOffset; }
														
protected:
	
	Frustum		m_Frustum;
	Matrix4		m_Projection;
	Matrix4		m_View;
	bool		m_Active;
	bool		m_DebugCamera;
	SceneNode*	m_Target;
	Vector4		m_OffsetVector;

	Vector3		m_Forward;
	Vector3		m_Up;
	Vector3		m_Right;
};