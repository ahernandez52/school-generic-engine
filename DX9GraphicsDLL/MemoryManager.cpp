#include "MemoryManager.h"
#include "D3D9Vertex.h"

using namespace std;

MemoryManager::MemoryManager()
{
	LoadDefaults();
}

MemoryManager::~MemoryManager()
{
	CleanUp();

	m_DefaultMesh->Release();
	m_DefaultTexture->Release();
}

//Private Functions
//====================================
ID3DXMesh* MemoryManager::CalculateTBN(ID3DXMesh* mesh)
{
	D3DVERTEXELEMENT9 elems[MAX_FVF_DECL_SIZE];
	UINT numElements = 0;
	VertexNormalMap::Decl->GetDeclaration(elems, &numElements);

	ID3DXMesh* cloneTempMesh = 0;
	mesh->CloneMesh(D3DXMESH_MANAGED, elems, g_d3dDevice, &cloneTempMesh);

	mesh->Release();
	mesh = 0;

	D3DXComputeTangentFrameEx(
			cloneTempMesh,			  // Input mesh
			D3DDECLUSAGE_TEXCOORD, 0, // Vertex element of input tex-coords.  
			D3DDECLUSAGE_BINORMAL, 0, // Vertex element to output binormal.
			D3DDECLUSAGE_TANGENT, 0,  // Vertex element to output tangent.
			D3DDECLUSAGE_NORMAL, 0,   // Vertex element to output normal.
			0,						  // Options
			0,						  // Adjacency
			0.01f, 0.25f, 0.01f,	  // Thresholds for handling errors
			&mesh,				  // Output mesh
			0);

	cloneTempMesh->Release();

	return mesh;
}

MeshMap MemoryManager::GetMeshTexture(string filename, string extension)
{
	MeshMap result;

	result.second = AllocateTexture(filename + extension, "Assets/Meshes/" + filename + "/" + filename + extension);

	if(result.second == nullptr)
	{
		result.first = "";
	}
	else
	{
		result.first = filename + extension;
	}
	
	return result;
}

bool MemoryManager::ContentsDoMatch(GraphicSchematic Incoming, GraphicSchematic Orginal)
{
	//compare for same mesh
	if(Incoming.Mesh != Orginal.Mesh)		return false;

	//compre map details
	if(Incoming.Maps[0] != Orginal.Maps[0]) return false;
	if(Incoming.Maps[1] != Orginal.Maps[1])	return false;
	if(Incoming.Maps[2] != Orginal.Maps[2])	return false;
	if(Incoming.Maps[3] != Orginal.Maps[3]) return false;
	if(Incoming.Maps[4] != Orginal.Maps[4]) return false;

	return true;
}

void MemoryManager::DoesExist(GraphicSchematic &Schematic)
{
	//unique identifer
	static int unique = 0;

	//Check if a schematic by that name exists
	auto result = m_Schematics.find(Schematic.Identifier);

	//if it does not then check all other schematics for content duplicates
	//if duplicate is found then return the original schematic and ignore the new one
	if(result == m_Schematics.end())
	{
		for(auto original : m_Schematics)
		{
			if(ContentsDoMatch(Schematic, original.second))
			{
				Schematic = original.second;
				return;
			}
		}

		return;
	}

	//if a duplicate name is found check for duplicate contents
	//if contents MATCH then return original schematic ignore the new one
	//if contents DO NOT MATCH generate new unique name and check for duplicates against other schematics
	if(ContentsDoMatch(Schematic, result->second))
	{
		Schematic = result->second;
		return;
	}
	else
	{
		//char buffer[10];
		//Incoming.name = Incoming.name + "_" + itoa(unique, buffer, 10);
		//unique++;

		for(auto original : m_Schematics)
		{
			if(ContentsDoMatch(Schematic, original.second))
			{
				Schematic = original.second;
				return;
			}
		}

		return;
	}
}

ID3DXMesh* MemoryManager::AllocateMesh(string identifier, string filename)
{
	ID3DXMesh* returnMesh;

	auto result = m_Meshes.find(identifier);

	if(result == m_Meshes.end())
	{
		// Step 1: Load the .x file from file into a system memory mesh.
		ID3DXBuffer* adjBuffer = 0;
		ID3DXBuffer* mtrlBuffer = 0;
		DWORD numMtrls = 0;

		D3DXLoadMeshFromX(filename.c_str(), D3DXMESH_SYSTEMMEM, g_d3dDevice, &adjBuffer, &mtrlBuffer, NULL, &numMtrls, &returnMesh);

		//Resource<ID3DXMesh*> *source = new Resource<ID3DXMesh*>(returnMesh); //Testing out resource reference counting system

		// Step 2: Find out if the mesh already has normal info?
		D3DVERTEXELEMENT9 elems[MAX_FVF_DECL_SIZE];
		returnMesh->GetDeclaration(elems);

		bool hasNormals = false;
		D3DVERTEXELEMENT9 term = D3DDECL_END();
		
		for(int i = 0; i < MAX_FVF_DECL_SIZE; ++i)
		{
			// Did we reach D3DDECL_END() {0xFF,0,D3DDECLTYPE_UNUSED, 0,0,0}?
			if(elems[i].Stream == 0xff )
				break;

			if( elems[i].Type == D3DDECLTYPE_FLOAT3 &&
				elems[i].Usage == D3DDECLUSAGE_NORMAL &&
				elems[i].UsageIndex == 0 )
			{
				hasNormals = true;
				break;
			}
		}
		
		// Step 3: Change vertex format to VertexPNT.
		D3DVERTEXELEMENT9 elements[64];
		UINT numElements = 0;
		VertexPNT::Decl->GetDeclaration(elements, &numElements);

		ID3DXMesh* temp = 0;
		returnMesh->CloneMesh(D3DXMESH_SYSTEMMEM, elements, g_d3dDevice, &temp);
		returnMesh->Release();
		returnMesh = temp;

		// Step 4: If the mesh did not have normals, generate them.
		if( hasNormals == false)
			D3DXComputeNormals(returnMesh, 0);
		
		// Step 5: Optimize the mesh.
		returnMesh->Optimize(D3DXMESH_MANAGED | 
			D3DXMESHOPT_COMPACT | D3DXMESHOPT_ATTRSORT | D3DXMESHOPT_VERTEXCACHE, 
			(DWORD*)adjBuffer->GetBufferPointer(), 0, 0, 0, &returnMesh);
		//meshSys->release(); // Done w/ system mesh.
		adjBuffer->Release(); // Done with buffer.
		
		
		
		//cout << "Allocated new Mesh: " << endl;
		//cout << "\tName: " << identifier << endl;
		//cout << "\tPath: " << resourcePath << endl << endl;

		//assert((returnMesh != NULL) && "MESH NOT FOUND");

		//returnMesh = CalculateTBN(returnMesh);

		m_Meshes.insert(make_pair(identifier, returnMesh));

		return returnMesh;
	}
	else
	{
		return result->second;
	}
}

LPDIRECT3DTEXTURE9 MemoryManager::AllocateTexture(string identifier, string filename)
{
	if (identifier == "NULL") return nullptr;

	LPDIRECT3DTEXTURE9 returnTexture = nullptr;

	auto result = m_Textures.find(identifier);

	if(result == m_Textures.end())
	{
		D3DXCreateTextureFromFile(g_d3dDevice, filename.c_str(), &returnTexture);

		//assert((returnTexture != NULL) && "TEXTURE NOT FOUND");

		if(returnTexture == nullptr)
			return nullptr;

		//cout << "Allocated new Texture: " << endl;
		//cout << "\tName: " << resourceName << endl;
		//cout << "\tPath: " << resourcePath << endl << endl;

		m_Textures.insert(make_pair(identifier, returnTexture));

		return returnTexture;
	}
	else
	{
		return result->second;
	}
}

void MemoryManager::AllocateGraphicsSchematic(string identifier, string filename)
{
	GraphicSchematic newSchem;

	newSchem.Identifier = identifier;
	newSchem.FolderName = filename;

	newSchem.Mesh = AllocateMesh(filename,  "Assets/Meshes/" + filename + "/" + filename +".X");

	VertexNormalMap *pVerts = 0;

	newSchem.Mesh->LockVertexBuffer(0, (void**)&pVerts);

	D3DXComputeBoundingSphere(&pVerts[0].pos,
							  newSchem.Mesh->GetNumVertices(),
							  sizeof(VertexNormalMap),
							  0,
							  &newSchem.Radius);

	newSchem.Mesh->UnlockVertexBuffer();
	 
	//Assign Detail Maps
	//==================================

	string extentions[5] = {"_DIF.jpg", "_BMP.jpg", "_NRM.jpg", "_SPC.jpg", "_ILL.jpg"};

	for(int i = 0; i < MAXIMUM_MAPS; i++)
		newSchem.Maps[i] = GetMeshTexture(filename, extentions[i]);

	//**********************************

	DoesExist(newSchem);

	m_Schematics.insert(make_pair(identifier, newSchem));
}
//************************************

//Public Functions
//====================================
void MemoryManager::LoadDefaults()
{
	//load defaults
	D3DXLoadMeshFromX("Assets/Meshes/Default.X", D3DXMESH_SYSTEMMEM, g_d3dDevice, NULL, NULL, NULL, NULL, &m_DefaultMesh);

	D3DXCreateTextureFromFile(g_d3dDevice, "Assets/Textures/Default.jpg", &m_DefaultTexture);

	D3DXCreateTextureFromFile(g_d3dDevice, "Assets/Textures/perlin512.jpg", &m_SphereTexture);

	D3DXCreateSphere(g_d3dDevice, 5.0f, 20, 12, &m_Sphere, nullptr);
	D3DXCreateBox(g_d3dDevice, 10.0f, 10.0f, 10.0f, &m_Box, nullptr);

	//IDirect3DSurface9 *surface;
	//m_DefaultTexture->GetSurfaceLevel(0, &surface);
	//g_d3dDevice->ColorFill(surface, NULL, 0xff000000);
	//surface->Release();
}

void MemoryManager::CleanUp()
{
	for(auto mesh : m_Meshes)
		mesh.second->Release();

	for(auto texture : m_Textures)
		texture.second->Release();

	m_Meshes.clear();
	m_Textures.clear();
}

void MemoryManager::RegisterNewSchematic(string identifier, string folderName)
{
	//Check if the Schematic exisits
	auto result = m_Schematics.find(identifier);

	if(result == m_Schematics.end())
	{
		AllocateGraphicsSchematic(identifier, folderName);
	}
}

//D3D9Mesh* MemoryManager::BuildObject(string schematicName, int ownerID)
//{
//	m_MeshObjects.push_back(new D3D9Mesh(m_Schematics.find(schematicName)->second, ownerID));
//
//	return m_MeshObjects.back();
//}

Mesh* MemoryManager::BuildMesh(string meshName, RenderPass pass, int ownerID, int id)
{
	Mesh *newComponent = nullptr;

	if(meshName == "SPHERE")
	{
		newComponent = new Mesh(m_Sphere, m_SphereTexture, pass, ownerID);
	}

	if(meshName == "BOX")
	{
		newComponent = new Mesh(m_Box, m_DefaultTexture, pass, ownerID);
	}

	m_MeshComponents.push_back(newComponent);
	m_MeshComponentLookUp.insert(make_pair(id, newComponent));

	return newComponent;
	

	//GraphicSchematic schem = m_Schematics.at(meshName);

	//newComponent = new Mesh(schem.Mesh, schem.Maps[0].second, pass, ownerID);

	//m_MeshComponents.push_back(newComponent);

	//return newComponent;
}

vector<Mesh*> MemoryManager::GetMeshs()
{
	return m_MeshComponents;
}

Mesh* MemoryManager::GetMesh(int id)
{
	return m_MeshComponentLookUp.at(id);
}
//************************************