#include "Frustum.h"
#include "D3D9GraphicsUtility.h"

using namespace DXMathLibrary;

const DWORD D3D9Vertex_Colored::FVF = (D3DFVF_XYZ|D3DFVF_DIFFUSE);

/*
	Plane class implementation
*/
inline void FPlane::Normalize()
{
	float mag;
	mag = sqrt(a * a + b * b + c * c);
	a /= mag;
	b /= mag;
	c /= mag;
	d /= mag;
}

inline void FPlane::Init(const Vector3 &p0, const Vector3 &p1, const Vector3 &p2)
{
	D3DXPlaneFromPoints(this, &p0, &p1, &p2);
	Normalize();
}

bool FPlane::Inside(const Vector3 &point) const
{
	float result = D3DXPlaneDotCoord(this, &point);

	return (result >= 0.0f);
}

bool FPlane::Inside(const Vector3 &point, const float radius) const
{
	float Distance;

	Distance = D3DXPlaneDotCoord(this, &point);

	return (Distance >= -radius);
}

/*
	Frustum class implementation
*/
Frustum::Frustum()
{
	m_Fov		= D3DX_PI / 4.0f;
	m_Aspect	= 1.0f;
	m_Near		= 1.0f;
	m_Far		= 1000.0f;
}

bool Frustum::Inside(const Vector3 &point) const
{
	for(auto plane : m_Planes)
		if(!plane.Inside(point))
			return false;
	
	return true;
}

bool Frustum::Inside(const Vector3 &point, const float radius) const
{
	for(auto plane : m_Planes)
		if(!plane.Inside(point, radius))
			return false;
	
	return true;
}

void Frustum::Init(const float fov, const float aspect, const float nearClip, const float farClip)
{
	m_Fov		= fov;
	m_Aspect	= aspect;
	m_Near		= nearClip;
	m_Far		= farClip;

	Vector3 RRight(1.0f, 0.0f, 0.0f);
	Vector3 Up(0.0f, 1.0f, 0.0f);
	Vector3 Forward(0.0f, 0.0f, 1.0f);

	float tanFovOver2	= (float)tan(m_Fov/2.0f);
	Vector3 nearRight	= (m_Near * tanFovOver2) * m_Aspect * RRight;
	Vector3 farRight	= (m_Far * tanFovOver2) * m_Aspect * RRight;
	Vector3 nearUp		= (m_Near * tanFovOver2 ) * Up;
	Vector3 farUp		= (m_Far * tanFovOver2)  * Up;

	// points start in the upper right and go around clockwise
	m_NearClip[0] = (m_Near * Forward) - nearRight + nearUp;
	m_NearClip[1] = (m_Near * Forward) + nearRight + nearUp;
	m_NearClip[2] = (m_Near * Forward) + nearRight - nearUp;
	m_NearClip[3] = (m_Near * Forward) - nearRight - nearUp;

	m_FarClip[0] = (m_Far * Forward) - farRight + farUp;
	m_FarClip[1] = (m_Far * Forward) + farRight + farUp;
	m_FarClip[2] = (m_Far * Forward) + farRight - farUp;
	m_FarClip[3] = (m_Far * Forward) - farRight - farUp;

	// now we have all eight points. Time to construct 6 planes.
	// the normals point away from you if you use counter clockwise verts.

	Vector3 origin(0.0f, 0.0f, 0.0f);
	m_Planes[Near].Init(m_NearClip[2], m_NearClip[1], m_NearClip[0]);
	m_Planes[Far].Init(m_FarClip[0], m_FarClip[1], m_FarClip[2]);
	m_Planes[Right].Init(m_FarClip[2], m_FarClip[1], origin);
	m_Planes[Top].Init(m_FarClip[1], m_FarClip[0], origin);
	m_Planes[Left].Init(m_FarClip[0], m_FarClip[3], origin);
	m_Planes[Bottom].Init(m_FarClip[3], m_FarClip[2], origin);
}

void Frustum::Render()
{
	D3D9Vertex_Colored verts[24];
	for (int i=0; i<8; ++i)
	{
		verts[i].color = g_White;
	}

	for (int i=0; i<8; ++i)
	{
		verts[i+8].color = g_Red;
	}

	for (int i=0; i<8; ++i)
	{
		verts[i+16].color = g_Blue;
	}

	// Draw the near clip plane
	verts[0].position = m_NearClip[0];	verts[1].position = m_NearClip[1];
	verts[2].position = m_NearClip[1];	verts[3].position = m_NearClip[2];
	verts[4].position = m_NearClip[2];	verts[5].position = m_NearClip[3];
	verts[6].position = m_NearClip[3];	verts[7].position = m_NearClip[0];

	// Draw the far clip plane
	verts[8].position = m_FarClip[0];	verts[9].position = m_FarClip[1];
	verts[10].position = m_FarClip[1];	verts[11].position = m_FarClip[2];
	verts[12].position = m_FarClip[2];	verts[13].position = m_FarClip[3];
	verts[14].position = m_FarClip[3];	verts[15].position = m_FarClip[0];

	// Draw the edges between the near and far clip plane
	verts[16].position = m_NearClip[0];	verts[17].position = m_FarClip[0];
	verts[18].position = m_NearClip[1];	verts[19].position = m_FarClip[1];
	verts[20].position = m_NearClip[2];	verts[21].position = m_FarClip[2];
	verts[22].position = m_NearClip[3];	verts[23].position = m_FarClip[3];

	DWORD oldLightMode;
	g_d3dDevice->GetRenderState( D3DRS_LIGHTING, &oldLightMode );
	g_d3dDevice->SetRenderState( D3DRS_LIGHTING, FALSE );

    g_d3dDevice->SetFVF( D3D9Vertex_Colored::FVF );
	g_d3dDevice->DrawPrimitiveUP( D3DPT_LINELIST, 12, verts, sizeof(D3D9Vertex_Colored) );

	g_d3dDevice->SetRenderState( D3DRS_LIGHTING, oldLightMode );
}