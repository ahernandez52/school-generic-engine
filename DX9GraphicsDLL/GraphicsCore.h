#pragma once
#include "../MathLibrary/MathLibrary.h"

namespace DX9GraphicsCore
{
	class DX9GraphicsPipeline;

	class __declspec(dllexport) GraphicsCore
	{
	private:

		DX9GraphicsPipeline *core;

	public:

		GraphicsCore();
		~GraphicsCore();

		void Startup(LPDIRECT3DDEVICE9 device);
		void Shutdown();

		void OnResetDevice();
		void OnLostDevice();

		void Update(float dt);
		void Render();

		//Core API
		//==============================
		//Creation
		void NewSphere(int id, D3DXVECTOR3 pos, float radius);
		void NewBox(int id, D3DXVECTOR3 pos, D3DXVECTOR3 halfLengths, D3DXQUATERNION orin);

		void SyncBox(int id, D3DXVECTOR3 pos, D3DXQUATERNION orin);

		//Updates
		void UpdateCamera(DXMathLibrary::Vector3 pos, float pitch, float yaw, float speed);

		//Setters
		void SetCameraTarget(DXMathLibrary::Vector3 target);

		//bool HandleMessage(int value, DWORD value1, void* extra);

	};
}