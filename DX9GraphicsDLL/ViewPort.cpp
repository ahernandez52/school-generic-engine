#include "ViewPort.h"
#include "D3D9GraphicsUtility.h"

ViewPort::ViewPort(UINT width, 
				   UINT height, 
				   UINT mips, 
				   D3DFORMAT textureFormat, 
				   bool renderDepth, 
				   D3DFORMAT depthFormat, 
				   D3DVIEWPORT9& viewport)
{
	m_Width			= width;
	m_Height		= height;
	m_Mips			= mips;
	m_TextureFormat	= textureFormat;
	m_RenderDepth	= renderDepth;
	m_DepthFormat	= depthFormat;
	m_ViewPort		= viewport;
}

ViewPort::~ViewPort()
{
	m_RTT->Release();
	m_RTS->Release();
	m_TextureSurface->Release();
}
/// ^ V
void ViewPort::OnLostDevice()
{
	m_RTT->Release();
	m_RTS->Release();
	m_TextureSurface->Release();
}

void ViewPort::OnResetDevice()
{
	//Recreate Texture
	D3DXCreateTexture(g_d3dDevice, 
					  m_Width, 
					  m_Height, 
					  m_Mips, 
					  D3DUSAGE_RENDERTARGET, 
					  m_TextureFormat, 
					  D3DPOOL_DEFAULT, 
					  &m_RTT);

	//Recreate Render Surface
	D3DXCreateRenderToSurface(g_d3dDevice, 
							  m_Width, 
							  m_Height, 
							  m_TextureFormat, 
							  m_RenderDepth, 
							  m_DepthFormat, 
							  &m_RTS);

	//get Texture render surface
	m_RTT->GetSurfaceLevel(0, &m_TextureSurface);

}

void ViewPort::beginScene()
{
	m_RTS->BeginScene(m_TextureSurface, &m_ViewPort);
}

void ViewPort::endScene()
{
	//End scene with no mip filtering
	m_RTS->EndScene(D3DX_FILTER_NONE);
}