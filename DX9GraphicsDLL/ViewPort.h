#pragma once
#include "D3D9GraphicsUtility.h"

class ViewPort
{
private:

	//Render Areas
	IDirect3DTexture9*		m_RTT;
	ID3DXRenderToSurface*	m_RTS;
	IDirect3DSurface9*		m_TextureSurface;

	//Render settings
	UINT			m_Width;
	UINT			m_Height;
	UINT			m_Mips;
	D3DFORMAT		m_TextureFormat;
	bool			m_RenderDepth;
	D3DFORMAT		m_DepthFormat;
	D3DVIEWPORT9	m_ViewPort;


public:
	//Similar constructor as a texture
	ViewPort(UINT width, 
			 UINT height, 
			 UINT mips, 
			 D3DFORMAT textureFormat, 
			 bool renderDepth, 
			 D3DFORMAT depthFormat, 
			 D3DVIEWPORT9& viewport);
	~ViewPort();

	void OnLostDevice();
	void OnResetDevice();

	//use same names as hardware draws
	void beginScene();
	void endScene();

	IDirect3DTexture9* getTexture() { return m_RTT; }
};