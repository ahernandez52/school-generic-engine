#include "D3D9ShaderEffect.h"

using namespace std;

D3D9ShaderEffect::D3D9ShaderEffect(string FileName, LPDIRECT3DDEVICE9 device)
{
	LPD3DXBUFFER XLog = 0;
	HRESULT winError;
	winError = D3DXCreateEffectFromFile(device, string("Assets\\Shaders\\" + FileName + ".fx").c_str(), 0, 0, D3DXSHADER_DEBUG, 0, &m_FX, &XLog);
	if(XLog)
		 MessageBox(0, (char*)XLog->GetBufferPointer(), 0, 0);
	if (FAILED(winError))
	{
		MessageBox(0, "Shader Registration Failed", 0, 0);
	}

}

D3D9ShaderEffect::~D3D9ShaderEffect()
{
	m_FX->Release();
}

//Creation Functions
//=====================================
void D3D9ShaderEffect::CreateNewParamater(string ParamaterName)
{
	m_Handles.insert(make_pair(ParamaterName, m_FX->GetParameterByName(0, ParamaterName.c_str())));
}

void D3D9ShaderEffect::CreateNewTechnique(string TecniqueName)
{
	m_Handles.insert(make_pair(TecniqueName, m_FX->GetTechniqueByName(TecniqueName.c_str())));
}
//*************************************

//Setter Functions
//=====================================
void D3D9ShaderEffect::SetTechnique(string TechniqueName)
{
	m_FX->SetTechnique(m_Handles.at(TechniqueName));
}

void D3D9ShaderEffect::SetParamaterValue(string ParamaterName, void* Value, int ValueSize)
{
	m_FX->SetValue(m_Handles.at(ParamaterName), Value, ValueSize);
}

void D3D9ShaderEffect::SetBool(string ParamaterName, bool Value)
{
	m_FX->SetBool(m_Handles.at(ParamaterName), Value);
}

//*************************************

//Rendering Functions
//=====================================
void D3D9ShaderEffect::Begin(UINT numPasses)
{
	m_FX->Begin(&numPasses, 0);
}

void D3D9ShaderEffect::End()
{
	m_FX->End();
}

void D3D9ShaderEffect::BeginPass(UINT pass)
{
	m_FX->BeginPass(pass);
}

void D3D9ShaderEffect::EndPass()
{
	m_FX->EndPass();
}

void D3D9ShaderEffect::CommitChanges()
{
	m_FX->CommitChanges();
}
//*************************************

//Onlost onReset
//=====================================
void D3D9ShaderEffect::OnLostDevice()
{
	m_FX->OnLostDevice();
}

void D3D9ShaderEffect::OnResetDevice()
{
	m_FX->OnResetDevice();
}
//*************************************