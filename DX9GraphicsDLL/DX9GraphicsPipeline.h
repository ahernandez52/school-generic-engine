#pragma once
#include "D3D9ShaderEffect.h"
#include "D3D9GraphicsUtility.h"
#include "D3D9SkyBox.h"

#include "MathLibrary.h"
#include "Camera.h"

#include "MemoryManager.h"

#include "BaseGraphicsObject.h"

#include "Scene.h"

#include <vector>
#include <map>
#include <list>

#include "GfxStats.h"

namespace DX9GraphicsCore
{
	class DX9GraphicsPipeline
	{
	friend class GraphicsCore;
	private:

		LPDIRECT3DDEVICE9 m_Device;

		MemoryManager* m_Memory;

		//std::vector<BaseGraphicsObject*> m_Objects;

		Scene	*m_Scene;

	private:

		Camera *camera;

		CameraNode *m_Camera;

		PointLight			m_PointLight;
		DirectionalLight	m_GlobalLight;
		Material			m_Material;

		Matrix4				m_CameraTransform;

		GfxStats			*m_Stats;

		//D3D9ShaderEffect *m_FX;
		D3D9SkyBox *SkyBox;
		
		//Effect Handles
		ID3DXEffect *m_FX;

		D3DXHANDLE m_tech;

		D3DXHANDLE m_WVP;
		D3DXHANDLE m_World;
		D3DXHANDLE m_WorldInv;

		D3DXHANDLE m_EyePos;

		D3DXHANDLE m_Mtrl;
		D3DXHANDLE m_Light;

		D3DXHANDLE m_Tex;
		D3DXHANDLE m_Specular;

		//D3D9Mesh *mesh;
		//D3D9Mesh *sun;

	private:

		void BuildFX();

	public:

		DX9GraphicsPipeline(LPDIRECT3DDEVICE9 device);
		~DX9GraphicsPipeline();

		void OnResetDevice();
		void OnLostDevice();

		void Update(float dt);
		void Render();

		//Additional API
		void NewSphere(int id, D3DXVECTOR3 pos, float radius);
		void NewBox(int id, D3DXVECTOR3 pos, D3DXVECTOR3 halfLengths, D3DXQUATERNION orin);

		void SyncComponent(int id, D3DXVECTOR3 pos, D3DXQUATERNION orin);
	};
}