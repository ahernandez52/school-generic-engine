#pragma once
#include "D3D9GraphicsUtility.h"

#include "D3D9SceneNodes.h"


// ========== Enumerations ==========
enum MapNames 
{
	DIFFUSE_MAP,
	BUMP_MAP,
	NORMAL_MAP,
	SPECULAR_MAP,
	ILLUMINATION_MAP,
	MAXIMUM_MAPS,
};

enum DetailFlags
{
	DIF = 0x00001,
	BMP = 0x00010,
	NRM = 0x00100,
	SPC = 0x01000,
	ILL = 0x10000,
};
// ==================================

class MemoryManager
{
private:

	//Default Resources
	LPDIRECT3DTEXTURE9							m_DefaultTexture;
	LPDIRECT3DTEXTURE9							m_SphereTexture;
	ID3DXMesh*									m_DefaultMesh;

	ID3DXMesh*									m_Box;
	ID3DXMesh*									m_Sphere;

	//Objects
	std::map<int, NodeComponent*>					m_ObjectLookUp;

	//std::vector<D3D9Mesh*>						m_MeshObjects;
	std::vector<MeshNodeComponent*>				m_MeshComponents;
	std::map<int, MeshNodeComponent*>			m_MeshComponentLookUp;
	//std::vector<D3D9Particle*>					m_ParticleObjects;

	//Resources
	std::map<std::string, LPDIRECT3DTEXTURE9>	m_Textures;
	std::map<std::string, ID3DXMesh*>			m_Meshes;

	//Resource Containers
	std::map<std::string ,GraphicSchematic>		m_Schematics;

private:

	//Utility Functions
	ID3DXMesh*			CalculateTBN(ID3DXMesh* mesh);

	MeshMap				GetMeshTexture(std::string filename, std::string extension);

	bool				ContentsDoMatch(GraphicSchematic Incoming, GraphicSchematic Orginal);
	void				DoesExist(GraphicSchematic &Schematic);		
	//======================================

	//Resource Allocation
	ID3DXMesh*			AllocateMesh(std::string identifier, std::string filename);
	LPDIRECT3DTEXTURE9	AllocateTexture(std::string identifier, std::string filename);

	void				AllocateGraphicsSchematic(std::string identifier, std::string filename);
	//======================================

public:

	MemoryManager();
	~MemoryManager();

	void				LoadDefaults();
	void				CleanUp();
	
	void				RegisterNewSchematic(std::string identifier, std::string folderName);

	//D3D9Mesh*			BuildObject(std::string schematicName, int ownerID);
	Mesh*				BuildMesh(std::string meshName, RenderPass pass, int ownerID, int id);
	Particle*			BuildParticle(std::string particleName, int ownerID);

	std::vector<Mesh*>	GetMeshs();
	Mesh* GetMesh(int id);

};