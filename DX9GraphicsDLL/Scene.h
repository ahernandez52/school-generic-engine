#pragma once
#include "SceneNodes.h"



class Scene
{
protected:

	LPDIRECT3DDEVICE9			m_Device;

	SceneNode					*m_Root;
	CameraNode					*m_Camera;

	ID3DXMatrixStack			*m_MatrixStack;

	std::map<int, ISceneNode*>	m_NodeMap;

	Matrix4						m_WorldTransform;

public:

	Scene(LPDIRECT3DDEVICE9 device);
	virtual ~Scene();

	void OnLostDevice();
	void OnResetDevice();

	void Update(float dt);
	void Render();

	ISceneNode* FindNode(int id);

	bool AddChild(int id, ISceneNode* kid);
	bool RemoveChild(int id);

	//Utility
	bool Pick(Ray *rayCast)					{ return m_Root->Pick(this, rayCast); }

	//Camera
	void SetCamera(CameraNode *camera)		{ m_Camera = camera; }
	CameraNode* GetCamera() const			{ return m_Camera; }

	//Matrix Stack Functions
	//============================================
	void PushAndSetMatrix(const Matrix4 &toWorld)
	{
		m_MatrixStack->Push();
		m_MatrixStack->MultMatrixLocal(&toWorld);
		m_WorldTransform = GetTopMatrix();
	}

	void PopMatrix()
	{
		m_MatrixStack->Pop();
		m_WorldTransform = GetTopMatrix();
	}

	const Matrix4 GetTopMatrix()
	{
		return static_cast<const Matrix4>(*m_MatrixStack->GetTop()); //Daymn
	}
	//********************************************
};

