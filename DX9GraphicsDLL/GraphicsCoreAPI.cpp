#include "GraphicsCoreAPI.h"
#include "GraphicsCore.h"

using namespace DX9GraphicsCore;

void GraphicsCoreAPI::Message(char* API, unsigned long param, void* in, void* out)
{
	static int componentID = 0;

	if(strcmp(API, "INIT") == 0)
	{
		graphics = new DX9GraphicsCore::GraphicsCore();
		graphics->Startup((LPDIRECT3DDEVICE9)in);
		return;
	}

	if(strcmp(API, "SHUTDOWN") == 0)
	{
		graphics->Shutdown();
		delete graphics;
		return;
	}

	if(strcmp(API, "UPDATE") == 0)
	{
		graphics->Update(*reinterpret_cast<float const*>(&param));
	}

	if(strcmp(API, "RENDER") == 0)
	{
		graphics->Render();
		return;
	}

	if(strcmp(API, "ONRESET") == 0)
	{
		graphics->OnResetDevice();
		return;
	}

	if(strcmp(API, "ONLOST") == 0)
	{
		graphics->OnLostDevice();
		return;
	}

	if(strcmp(API, "NEWSPHERE") == 0)
	{
		float *paramList = (float*)in;
		graphics->NewSphere(componentID, D3DXVECTOR3(paramList[0], paramList[1], paramList[2]), paramList[3]);
		paramList[0] = componentID;
		componentID++;
		return;
	}

	if(strcmp(API, "NEWBOX") == 0)
	{
		// x, y, z, l, w, h, qw, qx, qy, qz
		float *paramList = (float*)in;
		graphics->NewBox(componentID, D3DXVECTOR3(paramList[0], paramList[1], paramList[2]), D3DXVECTOR3(paramList[3], paramList[4], paramList[5]), D3DXQUATERNION(paramList[6], paramList[7], paramList[8], paramList[9]));
		paramList[0] = componentID;
		componentID++;
		return;
	}

	if(strcmp(API, "SYNCCOMPONENT") == 0)
	{
		float *paramList = (float*)in;
		graphics->SyncBox(param, D3DXVECTOR3(paramList[0], paramList[1], paramList[2]), D3DXQUATERNION(paramList[3], paramList[4], paramList[5], paramList[6]));
		return;
	}
}

HRESULT CreateGraphicsCore(HINSTANCE hDLL, ISubSystem **Interface)
{
	if(!*Interface)
	{
		*Interface = new GraphicsCoreAPI();

		return S_OK;
	}

	return S_FALSE;
}

HRESULT ReleaseGraphicsCore(ISubSystem **Interface)
{
	if(!*Interface)
	{
		return S_FALSE;
	}

	delete *Interface;
	*Interface = nullptr;

	return S_OK;
}