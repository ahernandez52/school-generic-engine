#pragma once
#include "MathLibrary.h"
#include <map>
#include <string>

class D3D9ShaderEffect
{
private:

	ID3DXEffect							*m_FX;
	std::map<std::string, D3DXHANDLE>	 m_Handles;

public:

	D3D9ShaderEffect(std::string FileName, LPDIRECT3DDEVICE9 device);
	~D3D9ShaderEffect();

	//Creation Functions
	void CreateNewParamater(std::string ParamaterName);
	void CreateNewTechnique(std::string TechniqueName);

	//Setter Functions
	void SetTechnique(std::string TechniqueName);
	void SetParamaterValue(std::string ParamaterName, void* Value, int ValueSize);
	void SetBool(std::string ParamaterName, bool Value);

	//Rendering functions
	void Begin(UINT numPasses);
	void End();

	void BeginPass(UINT pass);
	void EndPass();

	void CommitChanges();

	//Onlost OnReset
	void OnLostDevice();
	void OnResetDevice();

};

