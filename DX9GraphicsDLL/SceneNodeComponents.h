#pragma once
#include "D3D9GraphicsUtility.h"


class SceneNodeComponent
{
friend class SceneNode;
protected:

	//Actor		*m_owner;

	DWORD		m_Options;

	int			m_NodeID;

	Matrix4		m_ToWorld;
	Matrix4		m_FromWorld;

	float		m_Radius;

	RenderPass	m_RenderPass;

	int			m_Type;

public:

	SceneNodeComponent(RenderPass pass, int id)
	{
		m_RenderPass	= pass;
		m_NodeID		= id;
		m_Radius		= 0.0f;
	}

	const int		&NodeID()		const			{ return m_NodeID; }
	Matrix4 const	&ToWorld()		const			{ return m_ToWorld; }
	Matrix4 const	&FromWorld()	const			{ return m_FromWorld; }

	RenderPass		Pass()			const			{ return m_RenderPass; }
	float			Radius()		const			{ return m_Radius; }

	Vector3			Position()		const			{ return m_ToWorld.GetPosition(); }
	void			SetPosition(Vector3 newPos)		{ m_ToWorld.SetPosition(newPos); }

	void			Transform(Matrix4 *toWorld, Matrix4 *fromWorld) const
	{
		if (toWorld)	*toWorld = m_ToWorld;
		if (fromWorld)	*fromWorld = m_FromWorld;
	}

};

class MeshNodeComponent : public SceneNodeComponent
{
friend class SceneNode;
friend class D3D9MeshNode;
protected:

	Material	m_Material;

	ID3DXMesh	*m_Mesh;

	LPDIRECT3DTEXTURE9	m_Diffuse;

public:

	MeshNodeComponent(ID3DXMesh *mesh, 
					  LPDIRECT3DTEXTURE9 dif, 
					  RenderPass pass, 
					  int id) : SceneNodeComponent(pass, id)
	{
		m_Mesh		= mesh;
		RenderPass temp = RenderPass_Actor;

		m_Diffuse	= dif;

		m_Material.SetDiffuse(g_White);
		m_Material.SetAmbient(g_White);
		m_Material.SetSpecular(g_Gray40, 5.0f);
	}

	bool			HasAlpha() const	{ return m_Material.HasAlpha(); }
	float			Alpha() const		{ return m_Material.GetAlpha(); }

	Material		GetMaterial() const	{ return m_Material; }

	ID3DXMesh*		GetMesh()			{ return m_Mesh; }

	LPDIRECT3DTEXTURE9 GetTexture()		{ return m_Diffuse; }

};

class ParticleNodeComponent : public SceneNodeComponent
{
friend class SceneNode;
protected:

public:

};

typedef SceneNodeComponent NodeComponent;
typedef ParticleNodeComponent Particle;
typedef MeshNodeComponent Mesh;