#pragma once
#include "../TaskEngine/ISubSystem.h"
#include "GraphicsCore.h"



class GraphicsCoreAPI : public ISubSystem
{
private:

	DX9GraphicsCore::GraphicsCore* graphics;

public:

	void Message(char* API, unsigned long param, void *in = nullptr, void *out = nullptr);
};


extern "C"
{
	__declspec(dllexport) HRESULT CreateGraphicsCore(HINSTANCE hDLL, ISubSystem **Interface);
	__declspec(dllexport) HRESULT ReleaseGraphicsCore(ISubSystem **Interface);
}

typedef HRESULT (*CREATECOREOBJECT)(HINSTANCE hDLL, ISubSystem **Interface);
typedef HRESULT (*RELEASECOREOBJECT)(ISubSystem **Interface);