#include "D3D9SkyBox.h"

D3D9SkyBox::D3D9SkyBox(char* filename, float radius, LPDIRECT3DDEVICE9 device)
{
	m_radius = radius;

	D3DXCreateSphere(device, radius, 30, 30, &m_sphere, 0);
	D3DXCreateCubeTextureFromFile(device, filename, &m_cubeMap);

	ID3DXBuffer* error = 0;
	D3DXCreateEffectFromFile(device, "Assets/Shaders/SkyBox.fx", 0, 0, 0, 0, &m_FX, &error);

	if(error)
		MessageBox(0, (char*)error->GetBufferPointer(), 0, 0);

	m_hTech   = m_FX->GetTechniqueByName("SkyTech");
	m_hWVP    = m_FX->GetParameterByName(0, "gWVP");
	m_hcube = m_FX->GetParameterByName(0, "gEnvMap");

	//these will not change
	m_FX->SetTechnique(m_hTech);
	m_FX->SetTexture(m_hcube, m_cubeMap);
}


D3D9SkyBox::~D3D9SkyBox()
{
	m_sphere->Release();
	m_cubeMap->Release();
	m_FX->Release();
}

void D3D9SkyBox::onLostDevice()
{
	m_FX->OnLostDevice();
}

void D3D9SkyBox::onResetDevice()
{
	m_FX->OnResetDevice();
}

void D3D9SkyBox::Render(D3DXMATRIX ViewProjection, D3DXVECTOR3 CameraPosition)
{
	D3DXMATRIX W;

	D3DXMatrixTranslation(&W, CameraPosition.x, CameraPosition.y, CameraPosition.z);
	m_FX->SetMatrix(m_hWVP, &(W * ViewProjection));

	UINT numPasses = 0;
	m_FX->Begin(&numPasses, 0);
	m_FX->BeginPass(0);

	m_sphere->DrawSubset(0);

	m_FX->EndPass();
	m_FX->End();
}