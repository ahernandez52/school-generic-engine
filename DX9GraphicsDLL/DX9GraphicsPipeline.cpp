#include "DX9GraphicsPipeline.h"
#include "D3D9Vertex.h"
#include "D3D9SceneNodes.h"

using namespace std;

namespace DX9GraphicsCore
{
	DX9GraphicsPipeline::DX9GraphicsPipeline(LPDIRECT3DDEVICE9 device)
	{
		D3DXCreateBox(device, 20.0f, 20.0f, 20.0f, &g_BoxMesh, 0);

		m_Device = device;
		g_d3dDevice = device;

		InitAllVertexDeclarations(device);

		m_Scene = new Scene(device);

		//Init default Light
		m_GlobalLight.ambient		= Color(1.0f, 1.0f, 1.0f, 1.0f);
		m_GlobalLight.diffuse		= Color(1.0f, 1.0f, 1.0f, 1.0f);
		m_GlobalLight.specular		= g_White;

		//m_GlobalLight.ambient		= Color(1.0f, 1.0f, 1.0f, 1.0f);
		//m_GlobalLight.diffuse		= Color(1.0f, 1.0f, 1.0f, 1.0f);
		//m_GlobalLight.specular		= Color(0.6f, 0.6f, 0.6f, 1.0f);
		m_GlobalLight.directionWorld= Vector3(-0.577f, -0.577f, -0.577f);
		//m_GlobalLight.directionWorld= Vector3(1.0f, 0.0f, 0.0f);
		m_GlobalLight.directionWorld.Normalize();

		//Init default material
		m_Material.SetAmbient(g_White);
		m_Material.SetDiffuse(g_White);
		m_Material.SetSpecular(g_Gray40, 5.0f);

		BuildFX();

		m_Stats = new GfxStats();

		SkyBox = new D3D9SkyBox("Assets/Textures/Skybox/project-sol-purple-blue-1.dds", 20000.0f, device);
	
		m_Memory = new MemoryManager();

		camera = new Camera();
		


		//m_Memory->RegisterNewSchematic("Test", "battleship_tac_2");
		//m_Memory->RegisterNewSchematic("Sun", "sun2");


		//Mesh *meshComp = m_Memory->BuildMesh("Test", RenderPass_Actor, 0);

		//m_Stats->addTriangles(mesh->m_mesh->GetNumFaces());
		//m_Stats->addVertices(mesh->m_mesh->GetNumVertices());



		//Mesh *meshComp2 = m_Memory->BuildMesh("Test", RenderPass_Actor, 1);
		//meshComp2->SetPosition(Vector3(0.0f, 0.0f, -150.0f));

		//m_Stats->addTriangles(mesh->m_mesh->GetNumFaces());
		//m_Stats->addVertices(mesh->m_mesh->GetNumVertices());

	
		//D3D9MeshNode *temp = new D3D9MeshNode(meshComp, &Matrix4());
		//temp->SetPosition(Vector3(50.0f, 0.0f, 0.0f));
		//m_Scene->AddChild(1, temp);




		//temp = new D3D9MeshNode(meshComp2, &Matrix4());
		//temp->SetPosition(Vector3(-50.0f, 0.0f, 0.0f));

		//m_Scene->FindNode(1)->AddChild(temp);



		//m_CameraTransform.SetPosition(Vector3(0.0f, 0.0f, 100.0f));

		m_CameraTransform.BuildRotationLookAt(Vector3(0.0f, 20.0f, 100.0f),
											  Vector3(0.0f, 0.0f, 0.0f),
											  Vector3(0.0f, 1.0f, 0.0f));



		Frustum frustum;
		frustum.Init(D3DX_PI / 4.0f, 1.0f, 1.0f, 20000.0f);
		m_Camera = new CameraNode(&Matrix4(), frustum);

		m_Scene->AddChild(0, m_Camera);
		m_Scene->SetCamera(m_Camera);

		OnResetDevice();

		//D3D9ShaderMeshNode *temp = new D3D9ShaderMeshNode(1, mesh->m_mesh, m_FX, RenderPass_Actor, &m_CameraTransform);
		//m_CameraTransform.SetPosition(Vector3(0.0f, 0.0f, 200.0f));
		//temp->SetPosition(Vector3(0.0f, 0.0f, 0.0f));
		//temp->m_Tex = &mesh->m_maps->second[DIFFUSE_MAP];

		//m_Scene->AddChild(1, temp);

		//temp = new D3D9ShaderMeshNode(2, mesh->m_mesh, m_FX, RenderPass_Actor, &Matrix4());
		//temp->SetPosition(Vector3(-20.0f, 0.0f, 0.0f));
		//temp->m_Tex = &mesh->m_maps->second[DIFFUSE_MAP];
		//m_Scene->FindNode(1)->AddChild(temp);
	}

	DX9GraphicsPipeline::~DX9GraphicsPipeline()
	{
		DestroyAllVertexDeclarations();

		m_FX->Release();

		delete SkyBox;
		delete m_Memory;
		delete m_Scene;
	}

	//Private functions
	//=================================================
	void DX9GraphicsPipeline::BuildFX()
	{
		LPD3DXBUFFER XLog = 0;
		D3DXCreateEffectFromFile(g_d3dDevice, "Assets/Shaders/PhongDirLtTex.fx", 0, 0, 0, 0, &m_FX, &XLog);
		if(XLog)
			 MessageBox(0, (char*)XLog->GetBufferPointer(), 0, 0);

		//technique
		m_tech			= m_FX->GetTechniqueByName("PhongDirLtTexTech");

		//transform and view matracies
		m_WVP			= m_FX->GetParameterByName(0, "gWVP");
		m_World			= m_FX->GetParameterByName(0, "gWorld");
		m_WorldInv		= m_FX->GetParameterByName(0, "gWorldInv");

		//camera eye
		m_EyePos		= m_FX->GetParameterByName(0, "gEyePos");

		//material handles
		m_Mtrl			= m_FX->GetParameterByName(0, "gMtrl");
		m_Light			= m_FX->GetParameterByName(0, "gLight");

		//texture handles
		m_Tex			= m_FX->GetParameterByName(0, "gTex");
		m_Specular		= m_FX->GetParameterByName(0, "gSpecular");

		m_FX->SetTechnique(m_tech);
	}
	//*************************************************

	//Public functions
	//=================================================
	void DX9GraphicsPipeline::OnResetDevice()
	{
		D3DVIEWPORT9 viewport;
		m_Device->GetViewport(&viewport);

		camera->OnResetDevice(viewport.Width, viewport.Height);

		SkyBox->onResetDevice();
		m_FX->OnResetDevice();

		m_Stats->onResetDevice();

		m_Scene->OnResetDevice();
	}

	void DX9GraphicsPipeline::OnLostDevice()
	{
		SkyBox->onLostDevice();
		m_FX->OnLostDevice();

		m_Stats->onLostDevice();

		m_Scene->OnLostDevice();
	}

	void DX9GraphicsPipeline::Update(float dt)
	{
		camera->Update(dt);
		camera->Position().z = -100.0f;

		m_Stats->update(dt);

		m_Scene->Update(dt);

		static float z = 0.0f;
		z += 10.0f * dt;

		m_Camera->SetPosition(Vector3(0.0f, 0.0f, z));

		m_CameraTransform.BuildYawPitchROll(z, 0.0f, 0.0f);

		//((SceneNode*)m_Scene->FindNode(1))->SetTransform(&m_CameraTransform);
		//((SceneNode*)m_Scene->FindNode(1))->SetPosition(Vector3(0.0f, 0.0f, z));

		static float time = 0.0f;
		time += dt;

		m_PointLight.posW.x =  250.0f * cos(time);
		m_PointLight.posW.y =  0.0f;
		m_PointLight.posW.z =  250.0f * sin(time);

		//sun->Position() = m_PointLight.posW;

		//m_GlobalLight.directionWorld.x = cos(time);

		//mesh->Orientation().BuildRotYawPitchRoll(time, 0.0f, 0.0f);
	}

	void DX9GraphicsPipeline::Render()
	{
		m_Device->Clear(0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER,  g_Blue, 1.0f, 0);
		m_Device->BeginScene();

		//m_Scene->Render();

		SkyBox->Render(camera->ViewProj(), camera->Position());

		m_FX->SetValue(m_Light, &m_GlobalLight, sizeof(DirectionalLight));
		m_FX->SetValue(m_EyePos, &camera->m_Position, sizeof(D3DXVECTOR3));
		//
		////Matrix4 W, WI;

		auto GraphicObjects = m_Memory->GetMeshs();

		UINT numPasses = 0;
		m_FX->Begin(&numPasses, 0);
		m_FX->BeginPass(0);

		Matrix4 CameraVP = camera->ViewProj();

		//iterate throuh mesh components and render
		for(auto object : GraphicObjects)
		{
			Matrix4 W, WI;

			//m_FX->SetTexture(m_Tex, object->m_maps[SPECULAR_MAP].second);

			//W *= object->Transform().Build();

			W *= object->ToWorld();

			m_FX->SetMatrix(m_World, &W);
			//Matrix4 wvp = W * CameraVP;
			m_FX->SetMatrix(m_WVP, &(W * CameraVP));

			WI = W.Inverse();
			D3DXMatrixTranspose(&WI, &WI);

			m_FX->SetMatrix(m_WorldInv, &WI);

			
			m_FX->SetTexture(m_Tex, object->GetTexture());
			//m_FX->SetBool(m_FDif, (object->GetTexture()) ? false : true);

			m_FX->SetValue(m_Mtrl, &m_Material.GetD3D9(), sizeof(D3DMATERIAL9));
			m_FX->CommitChanges();

			//object->m_mesh->DrawSubset(0);
			object->GetMesh()->DrawSubset(0);
		}

		m_FX->EndPass();
		m_FX->End();

		

		m_Stats->display();

		m_Device->EndScene();
		m_Device->Present(0, 0, 0, 0);
	}

	void DX9GraphicsPipeline::NewSphere(int id, D3DXVECTOR3 pos, float raidus)
	{
		Mesh *Sphere = m_Memory->BuildMesh("SPHERE", RenderPass_Actor, 1, id);
		D3D9MeshNode *temp = new D3D9MeshNode(Sphere, &Matrix4());
		temp->SetPosition(pos);
		m_Scene->AddChild(id+1, temp);
	}

	void DX9GraphicsPipeline::NewBox(int id, D3DXVECTOR3 pos, D3DXVECTOR3 halfLengths, D3DXQUATERNION orin)
	{
		Mesh *Sphere = m_Memory->BuildMesh("BOX", RenderPass_Actor, 1, id);
		D3D9MeshNode *temp = new D3D9MeshNode(Sphere, &Matrix4());
		temp->SetPosition(pos);
		m_Scene->AddChild(id+1, temp);
	}

	void DX9GraphicsPipeline::SyncComponent(int id, D3DXVECTOR3 pos, D3DXQUATERNION orin)
	{
		Matrix4 transform;

		transform.BuildRotationQuat(orin);

		//assign position to mesh component
		m_Memory->GetMesh(id)->SetPosition(pos);

		//Check mesh components
		auto meshes = m_Memory->GetMeshs();

		//((SceneNode*)m_Scene->FindNode(id+1))->SetPosition(pos);
		//((SceneNode*)m_Scene->FindNode(id+1))->SetTransform(&transform);
	}
	//*************************************************
}