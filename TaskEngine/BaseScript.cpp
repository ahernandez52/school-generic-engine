#include "BaseScript.h"
#include <iostream>

using namespace v8;

BaseScript::BaseScript(std::string file, v8::Handle<Script> script)
{
	m_File = file;
	m_Script.Reset(Isolate::GetCurrent(), script);
}

BaseScript::~BaseScript(void)
{
	m_Script.Dispose();
}

void BaseScript::Run(const v8::Handle<v8::Context>& context)
{
	HandleScope handle_scope;

	Context::Scope context_scope(context);

	//Make local handle for script
	v8::Handle<v8::Script> script = v8::Handle<v8::Script>::New(context->GetIsolate(), m_Script);

	v8::Handle<v8::Value> result = script->Run();

	//Do something with the return value
}