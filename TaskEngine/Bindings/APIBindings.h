#pragma once

#include <v8.h>
#include <string>

using namespace v8;

void CreatePhysicsComponent( const FunctionCallbackInfo<v8::Value>& args )
{
	int id = 0;
	std::string type = "";

	// Validate for function(number, string)
	if( !args[0].IsEmpty() && args[0]->IsNumber() &&
		!args[1].IsEmpty() && args[1]->IsString() ) {
			// Get desired id
			id		= args[0]->Int32Value();

			// Get string
			v8::String::Utf8Value str(args[1]);
			type = *(str);
	}

	// Return Undefied
	args.GetReturnValue().Set( v8::Undefined() );
}