#pragma once

#include <v8.h>

#include "Vector3Bindings.h"

using namespace v8;

void QuatNormalizeCallback(const FunctionCallbackInfo<v8::Value>& args);
void QuatAxisAngleCallback(const FunctionCallbackInfo<v8::Value>& args);
void QuatForwardCallback(const FunctionCallbackInfo<v8::Value>& args);
void QuatUpCallback(const FunctionCallbackInfo<v8::Value>& args);

Handle<ObjectTemplate> CreateJSQuaternionTemplate(){
	// Create a new ObjectTemplete
	// This we will use to define our JS object
	Handle<ObjectTemplate> t = v8::ObjectTemplate::New();

	// Create x and y members with starting values of 0
	t->Set(String::New("x"), Number::New(0));
	t->Set(String::New("y"), Number::New(0));
	t->Set(String::New("z"), Number::New(0));
	t->Set(String::New("w"), Number::New(0));

	// Create methods
	t->Set(String::New("normalize"), FunctionTemplate::New(QuatNormalizeCallback));
	t->Set(String::New("axisangle"), FunctionTemplate::New(QuatAxisAngleCallback));
	t->Set(String::New("forward"), FunctionTemplate::New(QuatForwardCallback));
	t->Set(String::New("up"), FunctionTemplate::New(QuatUpCallback));

	// Return the template
	return t;
}

void QuatConstructor( const FunctionCallbackInfo<v8::Value>& args )
{
    HandleScope scope;

	// Create a new ObjectTemplete
	// This we will use to define our JS object
	Handle<ObjectTemplate> t = CreateJSQuaternionTemplate();

	// If Point(x, y) ctor was passed in values assign them
	if(!args[0].IsEmpty() && args[0]->IsNumber() &&
		!args[1].IsEmpty() && args[1]->IsNumber()) {
			t->Set(String::New("x"), args[0]);
			t->Set(String::New("y"), args[1]);
			t->Set(String::New("z"), args[2]);
			t->Set(String::New("w"), args[2]);
	}

	// Create and Return this newly created object
	args.GetReturnValue().Set(t->NewInstance());
}

void QuatNormalizeCallback(const FunctionCallbackInfo<v8::Value>& args){
	double x, y, z, w, mag;
	Handle<Object> myObject = args.This();
	x = myObject->Get(String::New("x"))->NumberValue();
	y = myObject->Get(String::New("y"))->NumberValue();
	z = myObject->Get(String::New("z"))->NumberValue();
	w = myObject->Get(String::New("w"))->NumberValue();

	mag = sqrt((x*x) + (y*y) + (z*z) + (w*w));
	x /= mag;
	y /= mag;
	z /= mag;

	myObject->Set(String::New("x"), Number::New(x));
	myObject->Set(String::New("y"), Number::New(y));
	myObject->Set(String::New("z"), Number::New(z));
	myObject->Set(String::New("w"), Number::New(z));
}

void QuatAxisAngleCallback(const FunctionCallbackInfo<v8::Value>& args){
	Handle<Object> myObj = args.This();
	// Grab angle
	double theta = args[1]->NumberValue();
	// Grab vector3 object
	Handle<Object> arg1Obj = args[0]->ToObject();

	double x, y, z;//, w;
	x = arg1Obj->Get(String::New("x"))->NumberValue();
	y = arg1Obj->Get(String::New("y"))->NumberValue();
	z = arg1Obj->Get(String::New("z"))->NumberValue();

	//double newX, newY, newZ;
	myObj->Set(String::New("x"), Number::New( x * sin(theta/2) ));
	myObj->Set(String::New("y"), Number::New( y * sin(theta/2) ));
	myObj->Set(String::New("z"), Number::New( z * sin(theta/2) ));
	myObj->Set(String::New("w"), Number::New( cos(theta/2) ));

	//Handle<Object> returnObj = CreateJSVector3Template()->NewInstance();
	//args.GetReturnValue().Set(returnObj);
}

void QuatForwardCallback(const FunctionCallbackInfo<v8::Value>& args){
	// Get all values
	double x, y, z, w;
	Handle<Object> mainObj = args.This();
	x = mainObj->Get(String::New("x"))->NumberValue();
	y = mainObj->Get(String::New("y"))->NumberValue();
	z = mainObj->Get(String::New("z"))->NumberValue();
	w = mainObj->Get(String::New("w"))->NumberValue();

	// Create and assign values to a Vector3
	Handle<Object> returnObj = CreateJSVector3Template()->NewInstance();
	returnObj->Set(String::New("x"),  Number::New(2 * (x * z + w * y)));
	returnObj->Set(String::New("y"),  Number::New(2 * (y * z - w * x)));
	returnObj->Set(String::New("z"),  Number::New(1 - 2 * (x * x + y * y)));

	// Return vector3
	args.GetReturnValue().Set(returnObj);
}

void QuatUpCallback(const FunctionCallbackInfo<v8::Value>& args){
	// Get all values
	double x, y, z, w;
	Handle<Object> mainObj = args.This();
	x = mainObj->Get(String::New("x"))->NumberValue();
	y = mainObj->Get(String::New("y"))->NumberValue();
	z = mainObj->Get(String::New("z"))->NumberValue();
	w = mainObj->Get(String::New("w"))->NumberValue();

	// Create and assign values to a Vector3
	Handle<Object> returnObj = CreateJSVector3Template()->NewInstance();
	returnObj->Set(String::New("x"),  Number::New(2 * (x * y - w * z)));
	returnObj->Set(String::New("y"),  Number::New(1 - 2 * (x * x + z * z)));
	returnObj->Set(String::New("z"),  Number::New(2 * (y * z + w * x)));

	// Return vector3
	args.GetReturnValue().Set(returnObj);
}
