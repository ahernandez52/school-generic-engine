#pragma once

#include <v8.h>
#include <math.h>

using namespace v8;

// Forward declare Vector3 methods
void Vec3NormalizeCallback(const FunctionCallbackInfo<v8::Value>& args);
void Vec3DotCallback(const FunctionCallbackInfo<v8::Value>& args);
void Vec3CrossCallback(const FunctionCallbackInfo<v8::Value>& args);
void Vec3MagnitudeCallback(const FunctionCallbackInfo<v8::Value>& args);

// This defines a JS Vector3 object
Handle<ObjectTemplate> CreateJSVector3Template(){
	// Create a new ObjectTemplete
	// This we will use to define our JS object
	Handle<ObjectTemplate> t = v8::ObjectTemplate::New();

	// Create x and y members with starting values of 0
	t->Set(String::New("x"), Number::New(0));
	t->Set(String::New("y"), Number::New(0));
	t->Set(String::New("z"), Number::New(0));

	// Create methods
	t->Set(String::New("normalize"), FunctionTemplate::New(Vec3NormalizeCallback));
	t->Set(String::New("dot"), FunctionTemplate::New(Vec3DotCallback));
	t->Set(String::New("cross"), FunctionTemplate::New(Vec3CrossCallback));
	t->Set(String::New("length"), FunctionTemplate::New(Vec3MagnitudeCallback));

	// Return the template
	return t;
}

void Vec3DotCallback(const FunctionCallbackInfo<v8::Value>& args){
	double x1, y1, z1, x2, y2, z2, dot = 99;

	// Get all values
	Handle<Object> mainObj = args.This();
	x1 = mainObj->Get(String::New("x"))->NumberValue();
	y1 = mainObj->Get(String::New("y"))->NumberValue();
	z1 = mainObj->Get(String::New("z"))->NumberValue();

	Handle<Object> argObj = args[0]->ToObject();
	x2 = argObj->Get(String::New("x"))->NumberValue();
	y2 = argObj->Get(String::New("y"))->NumberValue();
	z2 = argObj->Get(String::New("z"))->NumberValue();

	dot = (x1*x2) + (y1*y2) + (z1*z2);
	args.GetReturnValue().Set(Number::New(dot));
}

void Vec3CrossCallback(const FunctionCallbackInfo<v8::Value>& args){
	double x1, y1, z1, x2, y2, z2, dot = 99;
	double newX, newY, newZ;

	// Get all values
	Handle<Object> mainObj = args.This();
	x1 = mainObj->Get(String::New("x"))->NumberValue();
	y1 = mainObj->Get(String::New("y"))->NumberValue();
	z1 = mainObj->Get(String::New("z"))->NumberValue();

	Handle<Object> argObj = args[0]->ToObject();
	x2 = argObj->Get(String::New("x"))->NumberValue();
	y2 = argObj->Get(String::New("y"))->NumberValue();
	z2 = argObj->Get(String::New("z"))->NumberValue();

	// Cross product calculations
	newX = (y1*z2) - (z1*y2);
	newY = (z1*x2) - (x1*z2);
	newZ = (x1*y2) - (y1*x2);

	// Create new vector3 to be returned
	Handle<Object> obj = CreateJSVector3Template()->NewInstance();
	obj->Set(String::New("x"), Number::New(newX));
	obj->Set(String::New("y"), Number::New(newY));
	obj->Set(String::New("z"), Number::New(newZ));

	args.GetReturnValue().Set(obj);
}

void Vec3MagnitudeCallback(const FunctionCallbackInfo<v8::Value>& args){
	double x, y, z, mag;
	Handle<Object> myObject = args.This();
	x = myObject->Get(String::New("x"))->NumberValue();
	y = myObject->Get(String::New("y"))->NumberValue();
	z = myObject->Get(String::New("z"))->NumberValue();

	mag = sqrt((x*x) + (y*y) + (z*z));

	args.GetReturnValue().Set(Number::New(mag));
}


void Vec3NormalizeCallback(const FunctionCallbackInfo<v8::Value>& args){
	double x, y, z, mag;
	Handle<Object> myObject = args.This();
	x = myObject->Get(String::New("x"))->NumberValue();
	y = myObject->Get(String::New("y"))->NumberValue();
	z = myObject->Get(String::New("z"))->NumberValue();

	mag = sqrt((x*x) + (y*y) + (z*z));
	x = x / mag;
	y = y / mag;
	z = z / mag;

	myObject->Set(String::New("x"), Number::New(x));
	myObject->Set(String::New("y"), Number::New(y));
	myObject->Set(String::New("z"), Number::New(z));
}


void Vec3Constructor( const FunctionCallbackInfo<v8::Value>& args )
{
    HandleScope scope;

	// Create a new ObjectTemplete
	// This we will use to define our JS object
	Handle<ObjectTemplate> t = CreateJSVector3Template();

	// If Point(x, y) ctor was passed in values assign them
	if(!args[0].IsEmpty() && args[0]->IsNumber() &&
		!args[1].IsEmpty() && args[1]->IsNumber()) {
			t->Set(String::New("x"), args[0]);
			t->Set(String::New("y"), args[1]);
			t->Set(String::New("z"), args[2]);
	}
	// Create and Return this newly created object
	args.GetReturnValue().Set(t->NewInstance());
}