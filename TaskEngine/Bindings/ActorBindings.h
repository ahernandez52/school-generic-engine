#include <v8.h>

#pragma once

//#include "Vector3Bindings.h"
#include "QuaternionBindings.h"
#include "../Actor.h"
#include "../TaskEngine.h"

using namespace v8;

Handle<ObjectTemplate> CreateJSBaseActorTemplate();
void SetPosition(const FunctionCallbackInfo<Value>& args);

void SphereConstructor( const FunctionCallbackInfo<v8::Value>& args )
{
    HandleScope scope;

        // Assign postition
        double x = 0, y = 0, z = 0;
        if( !args[0].IsEmpty() && args[0]->IsNumber() &&
                !args[1].IsEmpty() && args[1]->IsNumber() &&
                !args[2].IsEmpty() && args[2]->IsNumber() ) {
                        x = args[0]->NumberValue();
                        y = args[1]->NumberValue();
                        z = args[2]->NumberValue();
        }


        Actor *act = NULL;

        // Create Actor
        act = TEI->m_Memory->NewActor("SPHERE", DXMathLibrary::Vector3(x, y, z));

        // Create this object uisng base Actor template ctor
        v8::Handle<Object> obj = CreateJSBaseActorTemplate()->NewInstance();

        //// Set act to external
        //obj->SetInternalField( 0, v8::External::New( act ) );

        // Assign id
        obj->Set( v8::String::New("id"), v8::Number::New( act->GetId() ) );

        // Assign position
        v8::Handle<Value> pos = obj->Get(String::New("position"));
		Handle<Object> vecObj = pos->ToObject();

        vecObj->Set( v8::String::New("x"), v8::Number::New ( x ) );
        vecObj->Set( v8::String::New("y"), v8::Number::New ( y ) );
        vecObj->Set( v8::String::New("z"), v8::Number::New ( z ) );

        // Create and Return this newly created object
        args.GetReturnValue().Set(obj);
}


void BoxConstructor( const FunctionCallbackInfo<v8::Value>& args )
{
    HandleScope scope;

        // Assign postition
        double x = 0, y = 0, z = 0;
        if( !args[0].IsEmpty() && args[0]->IsNumber() &&
                !args[1].IsEmpty() && args[1]->IsNumber() &&
                !args[2].IsEmpty() && args[2]->IsNumber() ) {
                        x = args[0]->NumberValue();
                        y = args[1]->NumberValue();
                        z = args[2]->NumberValue();
        }

        Actor *act = NULL;

        // Create Actor
        act = TEI->m_Memory->NewActor("BOX", DXMathLibrary::Vector3(x, y, z));

        // Create this object uisng base Actor template ctor
        v8::Handle<Object> obj = CreateJSBaseActorTemplate()->NewInstance();

        //// Set act to external
        //obj->SetInternalField( 0, v8::External::New( act ) );

        // Assign id
        obj->Set( v8::String::New("id"), v8::Number::New( act->GetId() ) );

        // Assign position
        v8::Handle<Value> pos = obj->Get(String::New("position"));
		Handle<Object> vecObj = pos->ToObject();

        vecObj->Set( v8::String::New("x"), v8::Number::New ( x ) );
        vecObj->Set( v8::String::New("y"), v8::Number::New ( y ) );
        vecObj->Set( v8::String::New("z"), v8::Number::New ( z ) );

        // Create and Return this newly created object
        args.GetReturnValue().Set(obj);
}


// Base Actor Template
Handle<ObjectTemplate> CreateJSBaseActorTemplate(){
        // Create a new ObjectTemplete
        // This we will use to define our JS object
        Handle<ObjectTemplate> t = v8::ObjectTemplate::New();

        // Create x and y members with starting values of 0
        t->Set(String::New("id"), Number::New( 0 ));
        t->Set(String::New("flags"), Number::New(0));
        t->Set(String::New("position"), CreateJSVector3Template()->NewInstance());
        t->Set(String::New("orientation"), CreateJSQuaternionTemplate()->NewInstance());
		t->Set(String::New("setPosition"), FunctionTemplate::New(SetPosition));

		return t;
}


// Function used for converting a JS object into an Actor object
Actor* GetActor(v8::Handle<v8::Object> mainObj){
	Actor *act = NULL;
	v8::Handle<v8::Object> objHolder;
	
	// Create an Actor and assign same id
	// how do I assign the id?!?!?!
	//act = new Actor(mainObj->Get(v8::String::New("id"))->ToNumber());
	
	// Extract Vector3 values
	double x, y, z;
	objHolder = mainObj->Get(v8::String::New("pos"))->ToObject();
	x = objHolder->Get(v8::String::New("x"))->NumberValue();
	y = objHolder->Get(v8::String::New("y"))->NumberValue();
	z = objHolder->Get(v8::String::New("z"))->NumberValue();
	
	// Assign its position values
	act->SetPosition(DXMathLibrary::Vector3(x, y, z));
	
	// Extract flags
	act->AddFlag(
		(UINT)mainObj->Get(v8::String::New("flags"))->Int32Value()
		);
	
	// Extract Orientation
	double w; // continue using x,y,z variables allocated earlier
	objHolder = mainObj->Get(v8::String::New("orientation"))->ToObject();
	x = objHolder->Get(v8::String::New("x"))->NumberValue();
	y = objHolder->Get(v8::String::New("y"))->NumberValue();
	z = objHolder->Get(v8::String::New("z"))->NumberValue();
	w = objHolder->Get(v8::String::New("w"))->NumberValue();

	// Set orientation
	act->SetOrientation(DXMathLibrary::Quaternion(x, y, z, w));
	
	// Return actor
	return act;
}

//Funciton for setting an actors position
void SetPosition(const FunctionCallbackInfo<Value>& args)
{
	HandleScope scope;

	//Get the actor object
	Handle<Object> obj = args.This();

	//Get the actor ID
	int id = obj->Get(String::New("id"))->Int32Value();

	//Pull the actor from memory
	Actor* actor = TEI->m_Memory->GetActor(id);

    // Extract the arguments
    double x = 0, y = 0, z = 0;
    if( !args[0].IsEmpty() && args[0]->IsNumber() &&
        !args[1].IsEmpty() && args[1]->IsNumber() &&
        !args[2].IsEmpty() && args[2]->IsNumber() ) 
	{
        x = args[0]->NumberValue();
        y = args[1]->NumberValue();
        z = args[2]->NumberValue();
    }

	//Call the SetPosition function of the actor
	actor->SetPosition(Vector3(x, y, z));

	//Set the object's positions to this position
	Handle<Value> pos = obj->Get(String::New("position"));
	Handle<Object> vec = pos->ToObject();

	vec->Set(String::New("x"), Number::New(x));
	vec->Set(String::New("y"), Number::New(y));
	vec->Set(String::New("z"), Number::New(z));
}