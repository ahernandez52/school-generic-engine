#include "ScriptingCore.h"
#include "ScriptFunctions.h"
#include "BaseScript.h"
#include "ActorManager.h"

#include "Bindings/ActorBindings.h"
#include "Bindings/Vector3Bindings.h"
#include "Bindings/QuaternionBindings.h"

#include <iostream>
#include <io.h>
#include <string>
#include <Windows.h>
#include <stdio.h>
#include <fcntl.h>
#include <fstream>

using namespace v8;
using namespace std;

static const WORD MAX_CONSOLE_LINES = 500;

/**
 * Demo utilities
 */
static int moveFlags = 0;
static double timeElapsed = 0.0;

void dtGetter(Handle<String> property, const PropertyCallbackInfo<Value>& info)
{
	info.GetReturnValue().Set(Number::New(timeElapsed));
}

void dtSetter(Handle<String> property, Handle<Value> value, const PropertyCallbackInfo<Value>& info)
{
	timeElapsed = value->NumberValue();
}

void FlagGetter(Handle<String> property, const PropertyCallbackInfo<Value>& info)
{
	info.GetReturnValue().Set(Number::New(moveFlags));
}

void FlagSetter(Handle<String> property, Handle<Value> value, const PropertyCallbackInfo<Value>& info)
{
	moveFlags = value->Int32Value();
}

/**
 * Scripting definitions
 */

ScriptingCore::ScriptingCore(void)
{
	m_Isolate = nullptr;
}

ScriptingCore::~ScriptingCore(void)
{
	if ( !m_Associates.empty() || !m_ScriptMap.empty())
		Shutdown();
}

void ScriptingCore::Initialize(bool shell, TaskEngine* engine)
{

	//Store reference to the isolate
	m_Isolate = Isolate::GetCurrent();

	//Create local handle scope
	HandleScope handle_scope(m_Isolate);

	//Create a template for the global object where we set the 
	//built-in global functions
	Handle<ObjectTemplate> global = ObjectTemplate::New();

	//Expose the print function
	global->Set(String::New("print"), FunctionTemplate::New(Print));

	//Expose Vector3
	global->Set(String::New("vec3"), CreateJSVector3Template());

	//Expose Quaternion
	global->Set(String::New("quaternion"), CreateJSQuaternionTemplate());

	//Expose Box constructor
	global->Set(String::New("Box"), FunctionTemplate::New(BoxConstructor));

	//Expose Sphere
	global->Set(String::New("Sphere"), FunctionTemplate::New(SphereConstructor));

	//Expose moveFlags
	global->SetAccessor(String::New("moveFlags"),
		(AccessorGetterCallback)FlagGetter,
		(AccessorSetterCallback)FlagSetter);

	//Expose timeElapsed
	global->SetAccessor(String::New("dt"),
		(AccessorGetterCallback)dtGetter,
		(AccessorSetterCallback)dtSetter);

	//Create the context using global template
	Handle<Context> context = Context::New(m_Isolate, NULL, global);

	//Set our persistent handle to this context
	m_GlobalContext.Reset(GetIsolate(), context);

	if (shell)
	{
		/**
		 * Redirect IO to scripting shell
		 */
		int hConHandle;
		long lStdHandle;

		CONSOLE_SCREEN_BUFFER_INFO console_info;

		FILE* fp;

		//Allocate a console for this app
		AllocConsole();

		//Set the screen buffer to be big enough to let us scroll text
		GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &console_info);

		console_info.dwSize.Y = MAX_CONSOLE_LINES;

		SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE), console_info.dwSize);

		//Redirect unbuffered STDOUT to the console
		lStdHandle = (long)GetStdHandle(STD_OUTPUT_HANDLE);
		hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
		fp = _fdopen(hConHandle, "w");
		*stdout = *fp;
		setvbuf(stdout, NULL, _IONBF, 0);

		//Redirect unbuffered STDIN to the console
		lStdHandle = (long)GetStdHandle(STD_INPUT_HANDLE);
		hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
		fp = _fdopen(hConHandle, "r");
		*stdin = *fp;
		setvbuf(stdin, NULL, _IONBF, 0);

		//Redirect unbuffered STDERR to the console
		lStdHandle = (long)GetStdHandle(STD_ERROR_HANDLE);
		hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
		fp = _fdopen(hConHandle, "w");
		*stderr = *fp;
		setvbuf(stderr, NULL, _IONBF, 0);

		//Make cout, cin, cerr, and clog point to the console as well
		ios::sync_with_stdio();

		std::cout << "Scripting core initialized." << std::endl;

		std::cout << "Spinning shell" << endl;

		SpinThread();

		/**
		 * Expose demo variables
		 */


		SpawnEvent("loadScene.js");

		RegisterContinuous("movement.js");

		CreateAssociate(15, "movement.js");
	}
}

void ScriptingCore::Shutdown(void)
{
	ClearAssociates();
	ClearScripts();

	m_GlobalContext.Dispose();
}

void ScriptingCore::Update(float dt)
{
	Locker lock(GetIsolate());
	HandleScope handle_scope(GetIsolate());

	//Set timeElapsed
	timeElapsed = dt;

	//Create a local copy of the context to run here and enter it
	v8::Handle<v8::Context> context = v8::Handle<v8::Context>::New(GetIsolate(), m_GlobalContext);
	v8::Context::Scope context_scope(context);

	for (auto associate : m_Associates)
	{
		if (associate)
		{
			//Set the global actor postion

			//Set the global movement flags

			//Run the appropriate script
			m_ScriptMap.find(associate->script)->second->Run(context);
		}
	}
}

//void ScriptingCore::BuildActor(float x, float y, float z, string type)
//{
//
//}

bool ScriptingCore::RegisterContinuous(std::string file)
{
	//Locker lock(m_Isolate);
	//Isolate::Scope isoScope(m_Isolate);

	HandleScope handle_scope(m_Isolate);

	//Enter execution environment
	v8::Handle<v8::Context> context = v8::Handle<v8::Context>::New(GetIsolate(), m_GlobalContext);
	Context::Scope context_scope(context);

	//Fetch the file
	Handle<String> source = ReadFile(file.c_str());

	if (source.IsEmpty())
	{
		cout << file << " source code empty. " << endl;
		return false;
	}

	//Compile the source code
	v8::Handle<v8::Script> script = Script::Compile(source);

	if (script.IsEmpty())
	{
		cout << file << " script compile error. " << endl;
		return false;
	}

	BaseScript* s = new BaseScript(file, script);

	cout << file << " loaded successfully. " << endl;

	m_ScriptMap.insert(std::pair<std::string, BaseScript*>(file, s));

	return true;
}

bool ScriptingCore::SpawnEvent(std::string file)
{
	HandleScope handle_scope(m_Isolate);

	//Enter execution environment
	v8::Handle<v8::Context> context = v8::Handle<v8::Context>::New(m_Isolate, m_GlobalContext);
	Context::Scope context_scope(context);

	//Fetch the file
	Handle<String> source = ReadFile(file.c_str());

	if (source.IsEmpty())
	{
		return false;
	}

	//Check compilation
	Handle<Script> script = Script::Compile(source);

	if (script.IsEmpty())
	{
		return false;
	}

	Handle<Value> result = script->Run();	//<ALERT> If value returns handle here

	return true;
}

bool ScriptingCore::ShellEvent(std::string file)
{
	Locker lock(m_Isolate);
	Isolate::Scope isoScope(m_Isolate);

	HandleScope handle_scope(m_Isolate);

	//Enter execution environment
	v8::Handle<v8::Context> context = v8::Handle<v8::Context>::New(m_Isolate, m_GlobalContext);
	Context::Scope context_scope(context);

	//Fetch the file
	Handle<String> source = ReadFile(file.c_str());

	if (source.IsEmpty())
	{
		return false;
	}

	//Check compilation
	Handle<Script> script = Script::Compile(source);

	if (script.IsEmpty())
	{
		return false;
	}

	Handle<Value> result = script->Run();	//<ALERT> If value returns handle here

	return true;
}

bool ScriptingCore::Inject(std::string func)
{
	Locker lock(m_Isolate);
	Isolate::Scope isoScope(m_Isolate);

	HandleScope handle_scope(m_Isolate);

	//Enter execution environment
	v8::Handle<v8::Context> context = v8::Handle<v8::Context>::New(Isolate::GetCurrent(), m_GlobalContext);
	Context::Scope context_scope(context);

	v8::Handle<v8::String> source = v8::String::New(func.c_str(), func.size());

	if (source.IsEmpty())
	{
		return false;
	}

	v8::Handle<v8::Script> script = v8::Script::Compile(source);

	if (script.IsEmpty())
	{
		return false;
	}

	Handle<Value> result = script->Run();

	return true;
}

bool ScriptingCore::CreateAssociate(int id, std::string script)
{
	m_Associates.push_back(new Associate(id, script));

	return true;	//<ALERT> Reevaluate this function
}

// <ALERT> This is an expensive call research better solution
bool ScriptingCore::RemoveAssociate(int id, std::string script)
{
	for (std::vector<Associate*>::iterator associate = m_Associates.end() ; 
		 associate != m_Associates.begin() ;
		 associate--)
	{
		if (((*associate)->actorId == id) &&
			((*associate)->script == script))
		{
			m_Associates.erase(associate);
			return true;
		}
	}

	return false;
}

v8::Handle<v8::Value> ScriptingCore::CallScriptedFunction(v8::Handle<v8::Object> global,
										   std::string funcName,
										   v8::Handle<v8::Value> argList[],
										   unsigned int argCount)
{
	Handle<Value> js_result;

	Handle<Value> value = global->Get(String::New(funcName.c_str()));

	Handle<v8::Function> func = Handle<Function>::Cast(value);

	js_result = func->Call(global, argCount, argList);

	return js_result;
}

void ScriptingCore::Move(int flags)
{
	moveFlags = flags;
}

void ScriptingCore::AddStringToArguments(std::string str, 
										 Handle<Value> argList[],
										 unsigned int argPos)
{
	argList[argPos] = String::New(str.c_str());
}

void ScriptingCore::AddNumberToArguments(double num, 
										 Handle<Value> argList[],
										 unsigned int argPos)
{
	argList[argPos] = Number::New(num);
}

void ScriptingCore::AddBoolToArguments(bool value,
									   Handle<Value> argList[],
									   unsigned int argPos)
{
	argList[argPos] = Boolean::New(value);
}

v8::Handle<v8::String> ScriptingCore::ReadFile(const std::string& name)
{
	//Open the file
	FILE* file;
	fopen_s(&file, name.c_str(), "rb");

	//If there is no file, return an empty string
	if (file == NULL) return v8::Handle<v8::String>();

	//Set the pointer to the end of the file
	fseek(file, 0, SEEK_END);

	//Get the size of file
	int size = ftell(file);

	//Rewind the pointer to the beginning of the stream
	rewind(file);

	//Set up and read into the buffer
	char* chars = new char[size + 1];
	chars[size] = '\0';
	for (int i = 0 ; i < size;)
	{
		int read = static_cast<int>(fread(&chars[i], 1, size - i, file));
		i += read;
	}

	//Close file
	fclose(file);

	//Locker lock(m_Isolate);
	//Isolate::Scope isoScope(m_Isolate);

	//HandleScope handle_scope(m_Isolate);

	v8::Handle<v8::String> result = v8::String::New(chars, size);
	delete[] chars;
	return result;
}

//bool ScriptingCore::ExecuteString(Handle<String> source)
//{
//	HandleScope handle_scope(GetIsolate());
//	
//	Handle<Script> script = Script::Compile(source, 
//}

void ScriptingCore::ClearAssociates(void)
{
	for (auto associate : m_Associates)
	{
		Associate* temp = associate;
		delete temp;
	}

	m_Associates.clear();
}

void ScriptingCore::ClearScripts(void)
{
	for (auto script : m_ScriptMap)
	{
		BaseScript* temp = script.second;
		delete temp;
	}

	m_ScriptMap.clear();
}


DWORD WINAPI shellProc(LPVOID lpParam)
{
	ScriptingCore* scripting = static_cast<ScriptingCore*>(lpParam);

	////Testing the cout function

	//cout << "This is a test" << endl;

	//std::string input;

	//std::getline(std::cin, input);

	//cout << "\nWe received this input: " << input << endl;

	cout << "Scripting shell created. \n" << endl;
	cout << "Type -help for available commands \n" << endl;

	std::string input;
	std::string command;
	std::string argument;

	int stringPos = 0;

	while (input != "exit")
	{
		std::getline(std::cin, input);

		cout << endl;

		//Create a substring from the beginning to first space
		stringPos = input.find(" ");
		command = input.substr(0, stringPos);

		//Compare the string literals
		if (strcmp(command.c_str(), "-help") == 0)
		{
			//Display help

			cout << "-help				list commands" << endl;
			cout << "-inject      <JavaScript code>	inject JavaScript to global object" << endl;
			cout << "-loadScene	 <filename.js>	load scene from .js file" << endl;
			cout << "-reloadPhysics   <filename.js>	load physics from .js file" << endl;
			cout << "-reloadGraphics  <filename.js>	load graphics from .js file" << endl;
			cout << "-runScript  <filename.js>  run script from .js file \n" << endl;
		}

		else if (strcmp(command.c_str(), "-inject") == 0)
		{
			//Create the argument string
			argument = input.substr(stringPos + 1);

			scripting->Inject(argument);

			cout << endl;
		}

		else if (strcmp(command.c_str(), "-loadScene") == 0)
		{
			//Create the argument string
			argument = input.substr(stringPos + 1);

			//Print the argument
			cout << argument << endl;

			//Load the scene
			cout << "This option is not yet available\n" << endl;
		}

		else if (strcmp(command.c_str(), "-reloadPhysics") == 0)
		{
			//Create the argument string
			argument = input.substr(stringPos + 1);

			//Print the argument
			cout << argument << endl;

			//Reload physics arguments
			cout << "This option is not yet avilable\n" << endl;
		}

		else if (strcmp(command.c_str(), "-reloadGraphics") == 0)
		{
			//Create the argument string
			argument = input.substr(stringPos + 1);

			//Print the argument
			cout << argument << endl;

			//Reload graphics
			cout << "This option is not yet available\n" << endl;
		}

		else if (strcmp(command.c_str(), "-runScript") == 0)
		{
			//Create the argument string
			argument = input.substr(stringPos + 1);

			//Create source from the argument
			scripting->ShellEvent(argument);
		}

		else if (strcmp(command.c_str(), "exit") == 0)
		{
			cout << "Shutting down the shell\n" << endl;
			continue;
		}

		else
		{
			cout << "Unknown command. \nType -help for list of available commands.\n" << endl;
		}
	}

	std::cin.get();

	FreeConsole();

	return 0;

}

void ScriptingCore::SpinThread(void)
{
	CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE) shellProc, this, 0, NULL);
}

//void ScriptingCore::CreateActor( const FunctionCallbackInfo<v8::Value>& args )
//{
//	Actor *act = NULL;
//
//	// Create Actor
//	act = m_Engine->m_Memory->NewActor("BOX");
//
//	// Grab X, Y, Z values from params
//	double x = 0, y = 0, z = 0;
//	if( !args[0].IsEmpty() && args[0]->IsNumber() &&
//		!args[1].IsEmpty() && args[1]->IsNumber() &&
//		!args[2].IsEmpty() && args[2]->IsNumber() ) {
//			x = args[0]->NumberValue();
//			y = args[1]->NumberValue();
//			z = args[2]->NumberValue();
//	}
//
//	// Assign values to Actor
//	act->SetPosition(DXMathLibrary::Vector3(x, y, z));
//
//	// Return Actor ID to JS environment
//	args.GetReturnValue().Set( v8::Number::New( act->GetId() ));
//}

typedef void (*CREATEACTOR)(const FunctionCallbackInfo<v8::Value>& args);