#include "ActorManager.h"
#include "ISubSystem.h"

using namespace std;

ActorManager::ActorManager(ISubSystem *graphicsCore, ISubSystem *physicsCore)
{
	graphics = graphicsCore;
	physics = physicsCore;
}

ActorManager::~ActorManager()
{

}

void ActorManager::Sync()
{
	//go through and sync values
	for(Actor *actor : m_Actors)
	{
		ComponentList components = actor->GetComponents();

		if(actor->GetFlags() & MC_Gameplay)
		{
			Vector3 pos = actor->GetPosition();
			Quaternion orin = actor->GetOrientation();

			//x, y, z, qw, qx, qy, qz
			float params[] = {pos.x, pos.y, pos.z, orin.w, orin.x, orin.y, orin.z};

			//sync graphics component
			graphics->Message("SYNCCOMPONENT", components.at("GRAPHIC"), params);

			physics->Message("SYNCCOMPONENT", components.at("PHYSICS"), params);
		}
		else //physics control
		{
			//Get data from physic's component
			float params[] = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
			physics->Message("GETCOMPONENTDATA", components.at("PHYSICS"), params);

			//Set the Actor data
			actor->SetPosition(Vector3(params[0], params[1], params[2]));
			actor->SetOrientation(Quaternion(params[3], params[4], params[5], params[6]));
			
			//Vector3 pos = actor->GetPosition();
			//Quaternion orin = actor->GetOrientation();

			//x, y, z, qw, qx, qy, qz
			//float graphicsParams[] = {pos.x, pos.y, pos.z, orin.w, orin.x, orin.y, orin.z};

			//Set Graphics Component to physics data
			graphics->Message("SYNCCOMPONENT", components.at("GRAPHIC"), params);
		}
	}
}

void ActorManager::AddGraphicToActor(int actorID, char* graphicType)
{
	if(strcmp(graphicType, "SPHERE") == 0)
	{
		auto pos = m_Actors.at(actorID)->GetPosition();

		//x, y, z, r
		float params[] = {pos.x, pos.y, pos.z, 10.0f};
		graphics->Message("NEWSPHERE", actorID, params);
		m_Actors.at(actorID)->AddComponent("GRAPHIC", (int)params[0]);
		return;
	}

	if(strcmp(graphicType, "BOX") == 0)
	{
		auto pos = m_Actors.at(actorID)->GetPosition();

		//x, y, z, l, w, h
		float params[] = {pos.x, pos.y, pos.z, 10.0f, 10.0f, 10.0f};
		graphics->Message("NEWBOX", actorID, params);
		m_Actors.at(actorID)->AddComponent("GRAPHIC", (int)params[0]);
		return;
	}
}

void ActorManager::AddPhysicsToActor(int actorID, char* physicsType)
{
	if(strcmp(physicsType, "SPHERE") == 0)
	{
		auto pos = m_Actors.at(actorID)->GetPosition();

		//x, y, z, mass
		float params[] = {pos.x, pos.y, pos.z, 15.0f};
		physics->Message("CREATESPHERE", actorID, params);
		m_Actors.at(actorID)->AddComponent("PHYSICS", (int)params[0]);
		m_Actors.at(actorID)->AddFlag(MC_Gameplay);
		return;
	}

	if(strcmp(physicsType, "BOX") == 0)
	{
		auto pos = m_Actors.at(actorID)->GetPosition();

		//x, y, z, mass
		float params[] = {pos.x, pos.y, pos.z, 15.0f};
		physics->Message("CREATEBOX", actorID, params);
		m_Actors.at(actorID)->AddComponent("PHYSICS", (int)params[0]);
		m_Actors.at(actorID)->AddFlag(MC_Physics);
		return;
	}
}

Actor* ActorManager::NewActor(char* type, D3DXVECTOR3 pos)
{
	Actor *newActor = new Actor();

	newActor->SetPosition(pos);

	m_Actors.push_back(newActor);
	m_ActorMap.insert(make_pair(newActor->GetId(), newActor));

	AddGraphicToActor(newActor->GetId(), type);
	AddPhysicsToActor(newActor->GetId(), type);

	return newActor;
}

Actor* ActorManager::GetActor(int actorID)
{
	return m_ActorMap.at(actorID);
}

vector<Actor*> ActorManager::GetActors()
{
	return m_Actors;
}