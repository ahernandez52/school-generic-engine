#include "Actor.h"


int Actor::m_NextId = 0;

Actor::Actor(void) 
	: m_Position(0.0f, 0.0f, 0.0f),
	  m_Orientation(Quaternion(0, 0, 0, 1)),
	  m_Flags(0)
{
	SetId();
}

Actor::Actor(Vector3 pos)
	: m_Position(pos), 
	  m_Orientation(Quaternion(0, 0, 0, 1)),
	  m_Flags(0)
{
	SetId();
}

Actor::Actor(Vector3 pos, Quaternion q)
	: m_Position(pos), 
	  m_Orientation(q),
	  m_Flags(0)
{
	SetId();
}

Actor::Actor(float x, float y, float z)
	: m_Position(Vector3(x, y, z)),
	  m_Orientation(Quaternion(0, 0, 0, 1)),
	  m_Flags(0)
{
	SetId();
}

Actor::Actor(float x, float y, float z, Quaternion q)
	: m_Position(Vector3(x, y, z)),
	  m_Orientation(Quaternion(0, 0, 0, 1)),
	  m_Flags(0)
{
	SetId();
}

Vector3 Actor::GetPosition(void)
{
	return m_Position;
}

Quaternion Actor::GetOrientation(void)
{
	return m_Orientation;
}

int Actor::GetId(void)
{
	return m_Id;
}

DWORD Actor::GetFlags(void)
{
	return m_Flags;
}

void Actor::SetPosition(Vector3 pos)
{
	ScopedCriticalSection(this->m_CriticalSection);
	m_Position = pos;
}

void Actor::SetOrientation(Quaternion q)
{
	ScopedCriticalSection(this->m_CriticalSection);
	m_Orientation = q;
}

void Actor::AddFlag(DWORD flag)
{
	ScopedCriticalSection(this->m_CriticalSection);
	m_Flags = m_Flags | flag;
}

void Actor::RemoveFlag(DWORD flag)
{
	ScopedCriticalSection(this->m_CriticalSection);
	m_Flags = m_Flags &~ flag;
}

void Actor::AddComponent(char* type, int id)
{
	m_Components.insert(std::make_pair(type, id));
}

ComponentList Actor::GetComponents()
{
	return m_Components;
}

void Actor::SetId(void)
{
	m_Id = m_NextId;
	m_NextId++;
}