#pragma once
#include <Windows.h>

#include "ISubSystem.h"

extern "C"
{
	HRESULT CreateCoreObject(HINSTANCE hDLL, ISubSystem **Interface);
	HRESULT ReleaseCoreObject(ISubSystem **Interface);
}

typedef HRESULT (*CREATECOREOBJECT)(HINSTANCE hDLL, ISubSystem **Interface);
typedef HRESULT (*RELEASECOREOBJECT)(ISubSystem **Interface);

class SystemDLL
{
private:

	HMODULE				dll;
	CREATECOREOBJECT	createProc;
	RELEASECOREOBJECT	releaseProc;

public:

	ISubSystem		   *api;

public:

	SystemDLL(char* dllName, char* createCall, char* releaseCall)
	{
		api = nullptr;

		dll = LoadLibraryA(dllName);

		if(dll)
		{
			createProc = (CREATECOREOBJECT)GetProcAddress(dll, createCall);

			releaseProc = (RELEASECOREOBJECT)GetProcAddress(dll, releaseCall);
		}
	}

	HRESULT Load()
	{
		if(!dll) return S_FALSE;

		HRESULT hr;

		hr = createProc(dll, &api);

		if(FAILED(hr)) return S_FALSE;

		return S_OK;
	}

	HRESULT Release()
	{
		if(!dll) return S_FALSE;

		HRESULT hr;

		hr = releaseProc(&api);

		if(FAILED(hr)) api = nullptr;

		return S_OK;
	}
};