#pragma once

class ISubSystem
{
public:

	virtual void Message(char* API, unsigned long param, void *in = nullptr, void *out = nullptr) = 0;

};