#pragma once
#include "Actor.h"
#include <vector>
#include <map>
#include <unordered_map>

class ISubSystem;

class ActorManager
{
private:

	std::vector<Actor*>		m_Actors;
	std::map<int, Actor*>	m_ActorMap;

	ISubSystem *graphics;
	ISubSystem *physics;

public:
	ActorManager();
	ActorManager(ISubSystem *graphicsCore, ISubSystem *physicsCore);
	~ActorManager();

	void Sync();

	void AddGraphicToActor(int actorID, char* graphicType);
	void AddPhysicsToActor(int actorID, char* physicsType);

	Actor* NewActor(char* type, D3DXVECTOR3 pos);

	Actor* GetActor(int actorID);
	std::vector<Actor*> GetActors();
};