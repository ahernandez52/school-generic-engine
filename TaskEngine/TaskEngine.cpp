#include "TaskEngine.h"

#include <iostream>

#include <ppl.h>
#include <concrt.h>
#include <concrtrm.h>
#include <stdio.h>
#include <Windows.h>

#include "ActorManager.h"

#include "../NetworkingCore/NetworkingStructs.h"

#define GRAPHICS_HRZ (1.0 / 60.0)

#define CONNECTION_IP "127.0.0.1"
#define CONNECTION_PORT "6000"

enum CoreIDs
{
	C_GRAPHICS = 0,
	C_PHYSICS,
	C_NETWORKING,
	C_INPUT,
};


	using namespace Concurrency;
	using namespace std;

	TaskEngine::TaskEngine() : m_scripting(nullptr)
	{
	}

	TaskEngine* TaskEngine::Instance(void)
	{
		static TaskEngine ptr = TaskEngine();
		return &ptr;
	}

	TaskEngine::~TaskEngine()
	{
	}

	void TaskEngine::Initialize(LPDIRECT3DDEVICE9 device)
	{
		CreateSystems();

		m_Systems.at(C_GRAPHICS).api->Message("INIT", 0, device);
		m_Systems.at(C_NETWORKING).api->Message("INIT", 0);
		m_Systems.at(C_INPUT).api->Message("STARTUP", 0);
		m_Systems.at(C_PHYSICS).api->Message("INIT", 0);

		m_Memory = new ActorManager(m_Systems.at(C_GRAPHICS).api, m_Systems.at(C_PHYSICS).api);

		m_scripting = new ScriptingCore();
		m_scripting->Initialize(true, this);

		/////////////////////////////////////////
		//primaryActorPtr = m_Memory->NewActor("SPHERE");
		//primaryActorPtr->AddFlag(MC_Physics);
		/////////////////////////////////////////

		int newActor;
		Actor* newActorPtr;
		/////////////////////////////////////////
		//primaryActorPtr = m_Memory->NewActor("SPHERE");
		//primaryActorPtr->AddFlag(MC_Gameplay);
		/////////////////////////////////////////

		/////////////////////////////////////////
		//newActorPtr = m_Memory->NewActor("BOX");
		//newActorPtr->AddFlag(MC_Physics);
		/////////////////////////////////////////
		primaryActorPtr = m_Memory->GetActor(15);

		connected = false;
		cout << "--!-- Demo controls --!--" << endl;
		cout << "\tW, A, S, D to move the sphere." << endl;
		cout << "\tC, V to connect and disconnect." << endl;
	}

	void TaskEngine::Shutdown()
	{
		m_Systems.at(C_GRAPHICS).api->Message("SHUTDOWN", 0);
		m_Systems.at(C_NETWORKING).api->Message("SHUTDOWN", 0);
		m_Systems.at(C_INPUT).api->Message("SHUTDOWN", 0);
		m_Systems.at(C_PHYSICS).api->Message("SHUTDOWN", 0);

		if (m_scripting)
		{
			m_scripting->Shutdown();
		}

		ReleaseSystems();
	}

	void TaskEngine::Tic(float dt)
	{
		static float graphicsTE = 0.0f;
		static bool drawing = false;

		graphicsTE += dt;

		static task_group tg;	
		if(graphicsTE > GRAPHICS_HRZ)
		{
			if(!drawing)
			{	
				drawing = true;
				tg.run([&](){

					m_Memory->Sync();

					DEMO_CheckInput();

					DEMO_CheckPackets();

					//scripting
					m_scripting->Update(graphicsTE);

					m_Systems.at(C_PHYSICS).api->Message("UPDATE", *reinterpret_cast<DWORD const*>(&graphicsTE));

					m_Systems.at(C_GRAPHICS).api->Message("UPDATE", *reinterpret_cast<DWORD const*>(&graphicsTE));

					m_Systems.at(C_GRAPHICS).api->Message("RENDER", 0);

					drawing = false;
					graphicsTE = 0.0f;
				});
			}
		}

			
	}
	void TaskEngine::Input(LPARAM lparam)
	{
		//m_Systems.at(C_INPUT).second->Message("UPDATE", lparam, nullptr, nullptr);
		m_Systems.at(C_INPUT).api->Message("UPDATE", lparam);
	}

	void TaskEngine::onLostDevice()
	{
		//m_Systems.at(C_GRAPHICS).second->Message("ONLOST", 0, nullptr, nullptr);
		m_Systems.at(C_GRAPHICS).api->Message("ONLOST", 0);
	}

	void TaskEngine::onResetDevice()
	{
		//m_Systems.at(C_GRAPHICS).second->Message("ONRESET", 0, nullptr, nullptr);
		m_Systems.at(C_GRAPHICS).api->Message("ONRESET", 0);
	}

	void TaskEngine::CreateSystems()
	{
		m_Systems.insert(make_pair(C_GRAPHICS, SystemDLL("DX9GraphicsDLL.dll", "CreateGraphicsCore", "ReleaseGraphicsCore")));
		m_Systems.insert(make_pair(C_NETWORKING, SystemDLL("NetworkingCOre.dll", "CreateNetworkCore", "ReleaseNetworkCore")));
		m_Systems.insert(make_pair(C_INPUT, SystemDLL("Input Core.dll", "CreateInputCore", "ReleaseInputCore")));
		m_Systems.insert(make_pair(C_PHYSICS, SystemDLL("DragonPhysicsEngineDLL.dll", "CreatePhysicsCore", "ReleasePhysicsCore")));
	
		for(auto &system : m_Systems)
			system.second.Load();
	}

	void TaskEngine::ReleaseSystems()
	{
		for(auto &system : m_Systems)
			system.second.Release();

		m_Systems.clear();
	}

	void TaskEngine::DEMO_CheckInput()
	{
		bool keyDown;
		bool W, S, A, D;

		if(!connected)
		{
			m_Systems.at(C_INPUT).api->Message("KEYDOWN", 'C', 0, &keyDown);

			if(keyDown)
			{
				NETWORK::ConnectionInfo info;
				info.ip = CONNECTION_IP;
				info.port = CONNECTION_PORT;

				cout << "Attempting to connect to " << info.ip << ":" << info.port << "...\t";
				m_Systems.at(C_NETWORKING).api->Message("NETCONNECT", 0, &info, &connected);
				if(connected)
					cout << "Connected!" << endl;
				else
					cout << "Failed connection." << endl;
			}
		}
		else
		{
			m_Systems.at(C_INPUT).api->Message("KEYDOWN", 'V', 0, &keyDown);

			if(keyDown)
			{
				cout << "Disconnecting...\t";
				m_Systems.at(C_NETWORKING).api->Message("NETDISCONNECT", 0, 0, 0);
				cout << "Disconnected!" << endl;

				connected = false;
			}
		}

		m_Systems.at(C_INPUT).api->Message("KEYDOWN", 'W', 0, &W);
		m_Systems.at(C_INPUT).api->Message("KEYDOWN", 'S', 0, &S);
		m_Systems.at(C_INPUT).api->Message("KEYDOWN", 'A', 0, &A);
		m_Systems.at(C_INPUT).api->Message("KEYDOWN", 'D', 0, &D);
		
		if(W || S || A || D)
		{
			bool sent;
			NETWORK::DemoPacket packet;

			if(W)
			{
				packet.data += 1;
			}
			if(S)
			{
				packet.data += 2;
			}
			if(A)
			{
				packet.data += 4;
			}
			if(D)
			{
				packet.data += 8;
			}

			if(connected)
			{
				m_Systems.at(C_NETWORKING).api->Message("NETSEND", sizeof(packet), &packet, &sent);
			}
			m_scripting->Move(packet.data);
		}
	}

	void TaskEngine::DEMO_CheckPackets()
	{
		if(!connected)
			return;

		bool packetReceived;
		NETWORK::DemoPacket* packet;
		NETWORK::PacketData packetData;

		m_Systems.at(C_NETWORKING).api->Message("NETCHECKPACKET", 1, 0, &packetReceived);

		if(packetReceived)
		{
			m_Systems.at(C_NETWORKING).api->Message("NETGETPACKET", 1, 0, &packetData);

			packet = reinterpret_cast<NETWORK::DemoPacket*>(&packetData.data);

			m_scripting->Move(packet->data);
		}
	}
