#pragma once
#include <list>
#include <Windows.h>
#include "../MathLibrary/MathLibrary.h"
#include "CriticalSection.h"

#include <atomic>
#include <map>
#include <unordered_map>

class BaseComponent;

using namespace DXMathLibrary;

enum MovementController
{
	MC_Physics	= 0x001,
	MC_Gameplay = 0x010,
	MC_AI		= 0x100,
};

typedef std::unordered_map<char*, int> ComponentList;

class Actor
{
private:
	DWORD						m_Flags;

	Vector3						m_Position;
	Quaternion					m_Orientation;
	
	int							m_Id;
	static int					m_NextId;

	CriticalSection				m_CriticalSection;

	ComponentList				m_Components;

public:

	Actor(void);
	Actor(Vector3 pos);
	Actor(Vector3 pos, Quaternion q);

	Actor(float x, float y, float z);
	Actor(float x, float y, float z, Quaternion q);

	~Actor(void){}

	/**
	 * Accessors
	 */
	Vector3		GetPosition(void);
	Quaternion  GetOrientation(void);
	int			GetId(void);
	DWORD		GetFlags(void);

	void		SetPosition(Vector3 pos);
	void		SetOrientation(Quaternion q);
	void		AddFlag(DWORD flag);
	void		RemoveFlag(DWORD flag);

	void		AddComponent(char* type, int id);
	ComponentList GetComponents();
	
private:
	/**
	 * Utility
	 */
	void		SetId(void);
};