/**
 * BaseScript - Contains a persistent pointer to a unique script
 *
 * Author - Jesse Dillon
 */

#pragma once

#include <string> 
#include "../ScriptingCore/v8_include/v8.h"

class BaseScript
{
private:
	
	//File name
	std::string m_File;

	//Cached script
	v8::Persistent<v8::Script> m_Script;

public:

	/**
	 * Script 
	 * /name of file
	 */
	BaseScript(std::string file, v8::Handle<v8::Script> script);
	~BaseScript(void);

	/**
	 * Run
	 * 
	 * Runs the script
	 */
	void Run(const v8::Handle<v8::Context>& context);

private:

	BaseScript(void){}
	BaseScript(const BaseScript&){}
};