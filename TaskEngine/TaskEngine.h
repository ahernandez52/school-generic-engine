#pragma once
#include <d3d9.h>
#include <d3dx9.h>

#include "ScriptingCore.h"
#include "SystemDLL.h"

#include <map>


class ActorManager;
class Actor;

#define TEI TaskEngine::Instance()

class __declspec(dllexport) TaskEngine
{
friend class ScriptingCore;
private:

	std::map<int, SystemDLL> m_Systems;

	void		CreateSystems();
	void		ReleaseSystems();

	SystemDLL	LoadSystemDLL(char* dll, char* loadProc);
	void		ReleaseSystemDLL(SystemDLL dll, char* releaseProc);

	TaskEngine();

	bool		connected;
	void		DEMO_CheckInput();
	void		DEMO_CheckPackets();

protected:
	
	ScriptingCore  *m_scripting;

	int				PrimaryActor;
	Actor		   *primaryActorPtr;

public:
	static TaskEngine* Instance(void);
	~TaskEngine();

	void Initialize(LPDIRECT3DDEVICE9 graphicsDevice);
	void Shutdown();

	void Tic(float dt);

	void Input(LPARAM lparam);
	void onLostDevice();
	void onResetDevice();

	ActorManager   *m_Memory;
};


