/**
 * ScriptFunctions
 *
 * Defines the built-in global functions the scripting core is going to expose
 *
 * Author - Jesse Dillon
 */

#pragma once

#include <v8.h>
#include <iostream>

//#include "../DragonPhysicsEngineDLL/DragonPhysicsEngineDLL/DragonPhysicsAPI.h"

void Print(const v8::FunctionCallbackInfo<v8::Value>& args)
{
	//Print the arguments
	for (int i = 0; i < args.Length(); i++) {
		v8::HandleScope handle_scope(args.GetIsolate());
		v8::String::Utf8Value str(args[i]);
		std::string myStr = *(str);
		const char* cstr = myStr.c_str();
		printf("%s", cstr);
	}
	printf("\n", NULL);
}

//// JS Function: CreateActor(x, y, z)
//void CreateActor(const v8::FunctionCallbackInfo<v8::Value>& args) {
//
//}
//
// JS Function: CreateGraphics(id, meshName)
void CreateGraphics(const v8::FunctionCallbackInfo<v8::Value>& args) {

}

// JS Function: CreateObb(id, width, height, length, name)
void CreateObbObj(const v8::FunctionCallbackInfo<v8::Value>& args) {
	static float mass = 5.0f;
	static bool physicsEnabled = true;
	static bool resolveCollisions = true;

	// Convert string
	v8::HandleScope handle_scope(args.GetIsolate());
	v8::String::Utf8Value str(args[4]);
	std::string myStr = *(str);
	const char* cstr = myStr.c_str();

	//if(CreateObbCollisionObjectSchematic(
	//	D3DXVECTOR3(args[1]->Int32Value(), args[2]->Int32Value(), args[3]->Int32Value()), 
	//	cstr)){
	//		//(int id,D3DXVECTOR3 pos,std::string SchemName,float mass,bool PhysxEnable,bool colResolutionOn,Actor* actr)
	//		CreatePhysicsObject(args[0]->Int32Value(), D3DXVECTOR3(), cstr, mass, physicsEnabled, resolveCollisions, NULL);
	//} else {
	//}
}

//// Calls CreatePlaneCollisionObjectSchematic(DX3DVECTOR3 norm, float dist, string name)
//// JS Function call: CreatePlaneCollisionObjectSchematic(x, y, z, dist, name)
//void CreatePlaneCollObjSch(const v8::FunctionCallbackInfo<v8::Value>& args) {
//	// Convert string
//	v8::HandleScope handle_scope(args.GetIsolate());
//	v8::String::Utf8Value str(args[4]);
//	string myStr = *(str);
//	const char* cstr = myStr.c_str();
//	
//	CreatePlaneCollisionObjectSchematic(
//	D3DXVECTOR3(args[0]->Int32Value(), args[1]->Int32Value(), args[2]->Int32Value()),
//		args[3]->Int32Value(),
//		cstr);
//}
//
//// Calls CreatePhysicsObject(int id, DX3DVECTOR3 pos, string name, float mass, bool pysicsenabled, bool collistion resolution, Actor *actor)
//// JS Function call: 
//// --CreatePhyObj(number, x, y, z, name, mass, phyEnabled, resolveColl, **reserved for actor**)
////                  [0]  [1][2][3]  [4]   [5]      [6]        [7]         [8]
//void CreatePhyObj(const v8::FunctionCallbackInfo<v8::Value>& args) {
//	// Convert string
//	v8::HandleScope handle_scope(args.GetIsolate());
//	v8::String::Utf8Value str(args[4]);
//	string myStr = *(str);
//	const char* cstr = myStr.c_str();
//	
//	CreatePhysicsObject( args[0],
//	D3DXVECTOR3(args[1]->Int32Value(), args[2]->Int32Value(), args[3]->Int32Value()),
//		cstr,					// name
//		args[5]->Int32Value(), //mass
//		args[6]->BooleanValue(), // phyEnabled
//		args[7]->BooleanValue() // resolveColl
//		NULL);					// *Actor
//}
