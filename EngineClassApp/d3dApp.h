#pragma once

#include "d3dUtil.h"
#include <string>

/**
	Primary entry point into framework
	Takes paramaters from WinMain to Initialize the
	D3D App
*/
class D3DApp
{
public:
	D3DApp(HINSTANCE hInstance, std::string winCaption, D3DDEVTYPE devType, DWORD requestedVP);
	virtual ~D3DApp();

	HINSTANCE getAppInst();
	HWND      getMainWnd();


	virtual bool checkDeviceCaps()			{ return true; }
	virtual void onLostDevice()				{}
	virtual void onResetDevice()			{}
	virtual void updateScene(float dt)		{}
	virtual void WindowInput(LPARAM lparam) {}

	virtual void initMainWindow();
	virtual void initDirect3D();
	virtual int run();
	virtual LRESULT msgProc(UINT msg, WPARAM wParam, LPARAM lParam);

	void enableFullScreenMode(BOOL enable);
	bool isDeviceLost();

protected:
 
	std::string				mMainWndCaption;
	D3DDEVTYPE				mDevType;
	DWORD					mRequestedVP;
	
	HINSTANCE				mhAppInst;
	HWND					mhMainWnd;
	IDirect3D9*				md3dObject;
	bool					mAppPaused;
	D3DPRESENT_PARAMETERS	md3dPP;

};

extern D3DApp* gd3dApp;
extern IDirect3DDevice9* gd3dDevice;