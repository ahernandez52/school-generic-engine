//
// Draws Texture on to quad
// Based on Frank Luna's DirectX9 Book
//======================================
uniform extern texture  gTex;

sampler TexS = sampler_state
{
    Texture = <gTex>;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
    MipFilter = LINEAR;
    AddressU  = WRAP;
    AddressV  = WRAP;
};
 
struct OutputVS
{
    float4 posH    : POSITION0;
    float2 tex0    : TEXCOORD0;
};

OutputVS RadarVS(float3 posL : POSITION0, float2 tex0 : TEXCOORD0)
{

    OutputVS outVS = (OutputVS)0;
	

    outVS.posH = float4(posL, 1.0f);
	
    outVS.tex0 = tex0;

    return outVS;
}

float4 RadarPS(float2 tex0 : TEXCOORD0) : COLOR
{
    return tex2D(TexS, tex0);
}

technique RadarTech
{
    pass P0
    {
        // Specify the vertex and pixel shader associated with this pass.
        vertexShader = compile vs_2_0 RadarVS();
        pixelShader  = compile ps_2_0 RadarPS();
    }
}
