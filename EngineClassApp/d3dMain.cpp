#include "d3dMain.h"



int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance, PSTR cmdLine, int showCmd)
{

	/**
	 * Attempting to move console into scripting core 10/6 JD
	 */
	/*
		In a normal circumstance the engine is create, Initialized and started here.
		App helps to link D3D to Window handles. This should be done inside the Graphics
		Core in the future
	*/

	#if defined(DEBUG) | defined(_DEBUG)
		_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
		//_CrtSetBreakAlloc(542);
	#endif

	GameFramework app(hInstance, "Base DX9 Project", D3DDEVTYPE_HAL, D3DCREATE_HARDWARE_VERTEXPROCESSING);
	gd3dApp = &app;

	//Start the App
	return gd3dApp->run();
}

GameFramework::GameFramework(HINSTANCE hInstance, std::string winCaption, D3DDEVTYPE devType, DWORD requestedVP)
: D3DApp(hInstance, winCaption, devType, requestedVP)
{
	srand(time_t(0));

	if(!checkDeviceCaps())
	{
		MessageBox(0, "checkDeviceCaps() Failed", 0, 0);
		PostQuitMessage(0);
	}

	engine = TEI;

	engine->Initialize(gd3dDevice);
}

GameFramework::~GameFramework()
{
	engine->Shutdown();
}

bool GameFramework::checkDeviceCaps()
{
	// Nothing to check.
	return true;
}

void GameFramework::onLostDevice()
{
	engine->onLostDevice();
}

void GameFramework::onResetDevice()
{
	engine->onResetDevice();
}

void GameFramework::updateScene(float dt)
{
	
		engine->Tic(dt);
}

void GameFramework::WindowInput(LPARAM lparam)
{
	engine->Input(lparam);
}