var temp;

var tempPos = [-20, 0, 0];    //Actor position

var width  = 10;
var height = 10;
var length = 10;

//Create a bunch of boxes
for (var cols = 0 ; cols < 5 ; cols++) {
    for (var rows = 0 ; rows < 3 ; rows++) {
        //Create the bottom box of this column
        temp = new Box(tempPos[0], tempPos[1], tempPos[2]);
        tempPos[1] += 15;   //Move up one position
    }
    
    //Move the Y position back to origin
    tempPos[1] = 10;

    //Move the X position over one box space
    tempPos[0] += 10;
}

//Create the player sphere
var player = new Sphere(0, 5, -50);