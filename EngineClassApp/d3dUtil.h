#pragma once

//Console Headers
#include <io.h>
#include <fcntl.h>
#include <stdio.h>
#include <iostream>

//DirectX Includes
#include <d3d9.h>
#include <d3dx9.h>
#include <DxErr.h>

//Containers
#include <vector>
#include <map>
#include <list>
#include <string>

//Project Includes


// Globals for convenient access.
//===============================================================

//Forward Declarations
class D3DApp;

extern D3DApp* gd3dApp;
extern IDirect3DDevice9* gd3dDevice;

//***************************************************************

//Debugging
//===============================================================
//Safe Release
#define ReleaseCOM(x)  if(x) x->Release(); x = 0;

//HR
#if defined(DEBUG) | defined(_DEBUG)
	#ifndef HR
	#define HR(x)                                      \
	{                                                  \
		HRESULT hr = x;                                \
		if(FAILED(hr))                                 \
		{                                              \
			DXTrace(__FILE__, __LINE__, hr, #x, TRUE); \
		}                                              \
	}
	#endif

#else
	#ifndef HR
	#define HR(x) x;
	#endif
#endif 
//***************************************************************