//Defines movement for the sphere object

var forward  = 1;
var backward = 2;
var left     = 4;
var right    = 8;

var speed = 10.0;

var x = player.position.x;
var y = player.position.y;
var z = player.position.z;

if (backward & moveFlags) {
    z -= dt * speed;
    //print("Backward : " + z);
}

if (forward & moveFlags) {
    z += dt * speed;
    //print("Forward : " + z);
}

if (left & moveFlags) {
    x -= dt * speed;
    //print("Left : " + x);
}

if (right & moveFlags) {
    x += dt * speed;
   //print("Right : " + x);
}

//print("Player moved: ");
//print("x : " + x);
//print("y : " + y);
//print("z : " + z);

player.setPosition(x, y, z);