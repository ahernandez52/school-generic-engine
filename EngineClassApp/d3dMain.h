#pragma once
#include "d3dApp.h"
#include <tchar.h>
#include <crtdbg.h>

#include "TaskEngine.h"

class GameFramework : public D3DApp
{
public:
	GameFramework(HINSTANCE hInstance, std::string winCaption, D3DDEVTYPE devType, DWORD requestedVP);
	~GameFramework();

	bool checkDeviceCaps();
	
	void onLostDevice();
	void onResetDevice();
	void updateScene(float dt);
	void WindowInput(LPARAM lparam);

	POINT GetScreenSize();

private:

	//TaskEngine *engine;
	TaskEngine *engine;
};