//Create the actors
var boxes = [];

boxes[0] = new Box(-20, 0, 0);
boxes[1] = new Box(-10, 0, 0);
boxes[2] = new Box(0, 0, 0);
boxes[3] = new Box(10, 0, 0);
boxes[4] = new Box(20, 0, 0);

boxes[5] = new Box(-20, 10, 0);
boxes[6] = new Box(-10, 10, 0);
boxes[7] = new Box(0, 10, 0);
boxes[8] = new Box(10, 10, 0);
boxes[9] = new Box(20, 10, 0);

boxes[10] = new Box(-20, 20, 0);
boxes[11] = new Box(-10, 20, 0);
boxes[12] = new Box(0, 20, 0);
boxes[13] = new Box(10, 20, 0);
boxes[14] = new Box(20, 20, 0);

var sphere;
var sphere = new Sphere(0, 0, 0);
