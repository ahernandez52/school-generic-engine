#include "INetworkingCore.h"
#include "NetworkingCore.h"

namespace NETWORK
{
	//INetworkingCore::INetworkingCore()
	//{
	//	net = new NetworkingCore();
	//}
	//
	//INetworkingCore::~INetworkingCore()
	//{
	//	delete net;
	//}

	void INetworkingCore::Message(char* API, unsigned long param, void* in, void* out)
	{
		if(strcmp(API, "NETCONNECT") == 0)
		{
			*(bool*)out = net->Connect(((ConnectionInfo*)in)->ip, ((ConnectionInfo*)in)->port);
		}
		else if(strcmp(API, "NETSEND") == 0)
		{
			*(bool*)out = net->Send((char*)in, (unsigned int)param);
		}
		else if(strcmp(API, "NETDISCONNECT") == 0)
		{
			net->Disconnect();
		}
		else if(strcmp(API, "NETCHECKPACKET") == 0)
		{
			*(bool*)out = net->CheckPacket((unsigned int)param);
		}
		else if(strcmp(API, "NETGETPACKET") == 0)
		{
			*(PacketData*)out = net->GetLastPacket((unsigned int)param);
		}
		else if(strcmp(API, "INIT") == 0)
		{
			net = new NETWORK::NetworkingCore();
		}
		else if(strcmp(API, "SHUTDOWN") == 0)
		{
			delete net;
		}
	}
}

HRESULT CreateNetworkCore(HINSTANCE hDLL, ISubSystem **Interface)
{
	if(!*Interface)
	{
		*Interface = new NETWORK::INetworkingCore();

		return S_OK;
	}

	return S_FALSE;
}

HRESULT ReleaseNetworkCore(ISubSystem **Interface)
{
	if(!*Interface)
	{
		return S_FALSE;
	}

	delete *Interface;
	*Interface = nullptr;

	return S_OK;
}