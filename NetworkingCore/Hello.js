
var net = require('net');
//var binary = require('binary');

var DataTypeLE = {UInt8: 0, Int8:1, UInt16:2, Int16:3, UInt32:4, Int32:5, Float:6, Double:7, dString:8, Other:9}
var DataTypeBE = {UInt8: 10, Int8:11, UInt16:12, Int16:13, UInt32:14, Int32:15, Float:16, Double:17}

function ControlType(Type, Read, Write)
{
    if (typeof(Type) === 'undefined') {
        //code
        Type = DataTypeLE.Other;
        
    }
    this.type = Type;
    
    this.setSize = function(customSize){
        if (this.type === DataTypeLE.UInt8 || this.type === DataTypeBE.UInt8 || this.type === DataTypeLE.Int8 || this.type === DataTypeBE.Int8) {
            return 1;
        }
        if (this.type === DataTypeLE.UInt16 || this.type === DataTypeLE.Int16 || this.type === DataTypeBE.UInt16 || this.type === DataTypeBE.Int16 ) {
            return 2;
        }
        if (this.type === DataTypeLE.UInt32 || this.type === DataTypeLE.Int32 || this.type === DataTypeBE.UInt32 || this.type === DataTypeBE.Int32
        ||  this.type === DataTypeLE.Float || this.type === DataTypeLE.Double || this.type === DataTypeBE.Float || this.type === DataTypeBE.Double) {
            return 4;
        }
        else
            return customSize;
    }
    
    this.size = this.setSize();
    
    this.buf = new Buffer(this.size);
    this.buf.readData = new ReadData(this.buf, Type,Read);
    
    this.buf.writeData = new WriteData(this.buf, Type,Write);
    this.buf.writeData(0,0);
    
    
    
    this.read = function(buffer,offset)
    {
        return buffer.readData(offset);
    }
    
    this.write = function(value, offset)
    {
        this.buf.writeData(value,offset);
    }
    return this;

}


    function ReadData(buf, Type, Read, size)
    {
        if (typeof(Read)!== 'undefined') {
            buf = new Buffer(size)
            return Read;
        }
        switch(Type)
        {
            case DataTypeLE.UInt8:
                buf = new Buffer(1);
                return buf.readUInt8

            case DataTypeLE.Int8:
                buf = new Buffer(1);
                return buf.readInt8
            
            case DataTypeLE.UInt16:
                buf = new Buffer(2);
                return buf.readUInt16LE
            
            case DataTypeLE.Int16:
                buf = new Buffer(2);
                return buf.readInt16LE
            
            case DataTypeLE.UInt32:
                buf = new Buffer(4);
                return buf.readUInt32LE
            
            case DataTypeLE.Int32:
                buf = new Buffer(4);
                return buf.readInt32LE
            
            case DataTypeLE.Float:
                buf = new Buffer(4);
                return buf.readFloatLE
            
            case DataTypeLE.Double:
                buf = new Buffer(4);
                return buf.readDoubleLE
            
            case DataTypeLE.dString:
                buf = new Buffer(1);
                return buf.toString
            
            case DataTypeBE.UInt8:
                buf = new Buffer(1);
                return buf.readUInt8
            
            case DataTypeBE.Int8:
                buf = new Buffer(1);
                return buf.readInt8
            
            case DataTypeBE.UInt16:
                  buf = new Buffer(2);
              return buf.readUInt16BE
            
            case DataTypeBE.Int16:
                buf = new Buffer(2);
                return buf.readInt16BE
            
            case DataTypeBE.UInt32:
                buf = new Buffer(4);
                return buf.readUInt32BE
            
            case DataTypeBE.Int32:
                buf = new Buffer(2);
                return buf.readInt32BE
            
            case DataTypeBE.Float:
                buf = new Buffer(4);
                return buf.readFloatBE
            
            case DataTypeBE.Double:
                buf = new Buffer(4);
                return buf.readDoubleBE
            
            default:
                return null;
        }
        
    }
    
    function WriteData(buf, Type, Write,size){
        if (typeof(Write)!== 'undefined') {
             buf = new Buffer(size);

            return Write;
        }
        switch(Type)
        {
            case DataTypeLE.UInt8:
                buf = new Buffer(1);
                return buf.writeUInt8
            
            case DataTypeLE.Int8:
                buf = new Buffer(1);
                return buf.writeInt8
            
            case DataTypeLE.UInt16:
                  buf = new Buffer(2);
              return buf.writeUInt16LE
            
            case DataTypeLE.Int16:
                buf = new Buffer(2);
                return buf.writeInt16LE
            
            case DataTypeLE.UInt32:
                buf = new Buffer(4);
                return buf.writeUInt32LE
            
            case DataTypeLE.Int32:
                buf = new Buffer(4);
                return buf.writeInt32LE
            
            case DataTypeLE.Float:
                buf = new Buffer(4);
                return buf.writeFloatLE
            
            case DataTypeLE.Double:
                buf = new Buffer(4);
                return buf.writeDoubleLE
            
            case DataTypeLE.dString:
                buf = new Buffer(1);
                return buf.write
            
            case DataTypeBE.UInt8:
                buf = new Buffer(1);
                return buf.writeUInt8
            
            case DataTypeBE.Int8:
                buf = new Buffer(1);
                return buf.writeInt8
            
            case DataTypeBE.UInt16:
                buf = new Buffer(2);
                return buf.writeUInt16BE
            
            case DataTypeBE.Int16:
                buf = new Buffer(2);
                return buf.writeInt16BE
            
            case DataTypeBE.UInt32:
                buf = new Buffer(4);
                return buf.writeUInt32BE
            
            case DataTypeBE.Int32:
                buf = new Buffer(4);
                return buf.writeInt32BE
            
            case DataTypeBE.Float:
                buf = new Buffer(4);
                return buf.writeFloatBE
            
            case DataTypeBE.Double:
                buf = new Buffer(4);
                return buf.writeDoubleBE
            
            default:
                return null;
        }
    }
    
function PacketType (DTArray)
{
    var dataTypes = new Array();
    dataTypes.push(new ControlType(DataTypeLE.UInt32));
    
    for(var i = 0; i<DTArray.length; i++){
        
        dataTypes.push(new ControlType(DTArray[i]));
    }
    
    this.AddType = new function(DataTypes, size){
        for(var i = 0; i<size; i++)
        {
            dataTypes.push(DataTypes[i]);
        }
    }
    
    this.Write = function(position,data,offset){
        
        dataTypes[position].write(data,offset);
    }
    
    this.Read = function(buffer)
    {
        var index = 0;
        var returnBuf = new Array();
        for(var i = 0; i<dataTypes.length; i++)
        {
            buffer.readData = dataTypes[i].buf.readData
            buffer.writeData = dataTypes[i].buf.writeData
            dataTypes[i].buf = buffer;
            returnBuf.push(dataTypes[i].read(dataTypes[i].buf,index));
            index += dataTypes[i].size;
        }
        return returnBuf;
    }
    
    this.buffer = function()
    {
        var size = 0;
        var index = 0
        for (var i = 0; i<dataTypes.length; i++) {
            size += dataTypes[i].size;
        }
        var returnBuf = new Buffer(size);
        for(var i = 0; i<dataTypes.length; i++)
        {
            returnBuf.writeData = dataTypes[i].buf.writeData;
            returnBuf.writeData(dataTypes[i].buf.readData(0),index);
            index += dataTypes[i].size;
        }
        return returnBuf;
    }
}

/*
PacketType.prototype.AddType = function(){
    this.dataTypes.push()
}
*/

function PacketHolder ()
{
    var m_nextID = 0;
    var PacketList = new Array();
    var OnRecieve = new Array();
    var Sockets = new Array();
    
    
    
    this.GetID = function(packet)
    {
        return packet.readUInt32LE(0);
    }
    
    this.DefineNewPacket = function(DataTypeArray, OnRecieveFunc){
        m_nextID++;
        OnRecieve.push(OnRecieveFunc);
        return PacketList.push(new PacketType(DataTypeArray));
    }
    
    this.GetPacket = function(index){
        return PacketList[index];
    }
    
    this.Unpack = function(ID, packet){
        return PacketList[ID].Read(packet);
    }
    
    this.RecievedPacket = function(data,socket){
        var buffer = new Buffer(data.length)
         
         for (var i = data.length-1; i>=0; i--)
         {
            buffer[i] = data.charCodeAt(i);
         }
         
        /*var parsed = binary.parse(buffer)
        .word8lu('id')
        .vars;*/

        var packetID = holder.GetID(buffer);
        
        console.log("Got Data: " + data.length + " bytes with ID: "+packetID+"\n");
        
        var Packet = holder.Unpack(packetID,buffer);
        
        if (typeof(OnRecieve[Packet[0]]) !== 'undefined') {
            OnRecieve[Packet[0]](Packet,socket);
        }
    }
    
    this.AddSocket = function(socket)
    {
        Sockets.push(socket);
    }
    
    this.GetSocket = function(ID)
    {
        return Sockets[ID];
    }
    
    this.RemoveSocket = function(Socket)
    {
        var s = new Array();
        for(var i = 0; i<Sockets.length; i++)
        {
            if (Sockets[i] === Socket) {
                Sockets.splice(i,1);
            }
        }
    }
    
    this.Echo = function(buffer)
    {
        for (var i = 0; i<Sockets.length; i++) {
            if (Sockets[i] !== undefined || Sockets[i] !== null) {
                 Sockets[i].write(buffer);
            }
        }
    }
    
    this.exEcho = function(Socket,buffer)
    {
        for (var i = 0; i<Sockets.length; i++) {
            if (Sockets[i] !== Socket) {
                try{
                    Sockets[i].write(buffer);
                }
                catch(e){
                    console.log(e);
                }
            }
        }
    }
    
    //Adding default packet at ID 0 so all packets will start with 1, 0 is reserved
    var Temp = new Array();
    Temp.push(DataTypeLE.UInt32);
    this.DefineNewPacket(Temp,null);
}

holder = new PacketHolder();

function JoinGame(packet,socket)
{
    console.log("Got ID: " + packet[0] + " from client ID: "+packet[1]+" to game ID: " +packet[2]+"\n");
    
    var newPack = holder.GetPacket(2);
    newPack.Write(0,6,0);
    newPack.Write(1,1,0);
    newPack.Write(2,17,0);
    newPack.Write(3,2,0);
    var toWrite = newPack.buffer();
    holder.Echo(newPack.buffer());
    //socket.write(toWrite);
}

function EchoAll(packet) {
    holder.Echo(packet);
    //code
}

function GotJoinServer(packet){
    console.log("Got Packet: "+packet[0]+" With ClientID: "+packet[1]);
    var temp = holder.GetPacket(packet[0]);
    temp.Write(0,packet[0],0);
    temp.Write(1,packet[1],0);
    EchoAll(temp.buffer());
}

function GotJoinGame(packet){
    console.log("Got Packet: "+packet[0]+" With ClientID: "+packet[1]+" and GameID: "+packet[2]);
    var temp = holder.GetPacket(packet[0]);
    temp.Write(0,packet[0],0);
    temp.Write(1,packet[1],0);
    temp.Write(2,packet[2],0);
    EchoAll(temp.buffer());
}

function GotFire(packet){
    console.log("Got Packet: "+packet[0]+" With ClientID: "+packet[1]+" Target: "+packet[2]+ " GameID: "+packet[3]);
    var temp = holder.GetPacket(packet[0]);
    temp.Write(0,packet[0],0);
    temp.Write(1,packet[1],0);
    temp.Write(2,packet[2],0);
    temp.Write(3,packet[3],0);
    EchoAll(temp.buffer());
}

function BoxMover(packet,Socket) {
    var echo = holder.GetPacket(1);
    for (var i = 0; i< packet.length; i++) {
        echo.Write(i,packet[i],0);
    }
    holder.exEcho(Socket,echo.buffer());
}
var server = net.createServer(function(socket){
    socket.name = socket.remoteAddress + ":" + socket.remotePort;
    console.log("Connection from: " + socket.name);

    //Set socket to binary
    socket.setEncoding('binary');
    console.log('Hello World');
    
    holder.AddSocket(socket);
    
    var moveBox = new Array();
    moveBox.push(DataTypeLE.Int32);
    holder.DefineNewPacket(moveBox,BoxMover);
    // Set Packet Definitions
    /*
    //      Demo Packets
    var pJoinServer = new Array();
    pJoinServer.push(DataTypeLE.Int8);
    holder.DefineNewPacket(pJoinServer,GotJoinServer)   // ID: 0
    
    /*
    var pJoinGame = new Array();
        pJoinGame.push(DataTypeLE.UInt32);
        pJoinGame.push(DataTypeLE.UInt32);
        holder.DefineNewPacket(pJoinGame,GotJoinGame);   // ID: 1
        
    var pMove = pJoinGame;
        holder.DefineNewPacket(pMove,GotJoinGame);   // ID: 2
        
    var pFire = new Array();
        pFire.push(DataTypeLE.UInt32)
        pFire.push(DataTypeLE.UInt32)
        pFire.push(DataTypeLE.UInt32)
        holder.DefineNewPacket(pFire,GotFire);   // ID: 3
        
    var pQuitGame = pMove;
        holder.DefineNewPacket(pQuitGame,GotJoinGame);   // ID: 4
        
    var pQuitServer = pMove;
        holder.DefineNewPacket(pQuitServer,GotJoinGame);   // ID: 5
        
    var pSyncLobby = new Array();
        pSyncLobby.push(DataTypeLE.Int8);
        pSyncLobby.push(DataTypeLE.UInt16);
        pSyncLobby.push(DataTypeLE.Int8);
        holder.DefineNewPacket(pSyncLobby);   // ID: 6*/
        
        
        

    socket.on('data', function(data){
        
        holder.RecievedPacket(data,socket);
        
        // all of this works up until this point. Next we need to confirm the writing of packets and add the onReceived()//
        
        
     /*   var newPacket = new Array(5);
        newPacket[0] = new ControlType(DataTypeLE.Int8);
        newPacket[1] = new ControlType(DataTypeLE.Int8);
        newPacket[2] = new ControlType(DataTypeLE.UInt16);
        newPacket[3] = new ControlType(DataTypeLE.Int8);
        newPacket[0].write(6,0);
        newPacket[1].write(1,0);
        newPacket[2].write(17,0);
        newPacket[3].write(0,0);
        totalBuffer = new Buffer(5);
        
        totalBuffer[0] = newPacket[0].buf[0];
        totalBuffer[1] = newPacket[1].buf[0];
        totalBuffer[2] = newPacket[2].buf[0];
        totalBuffer[3] = newPacket[2].buf[1];
        totalBuffer[4] = newPacket[3].buf[0];
        
        
        var ct = new ControlType(DataTypeLE.Int8);
        ct.buf = totalBuffer;
        socket.write(ct.buf);
        console.log("Sent Data: " + ct.buf.length + " bytes with ID: "+ct.buf[0]+"\n");*/

    });
    
    socket.on('end', function(){
        console.log("End Connection from: " + socket.name);
    });
    socket.on('close', function(){
        console.log("Closed Connection from: " + socket.name);
        holder.RemoveSocket(socket);
        
    });
    socket.on('error',function(){
        console.log("Client Rage Quit");
        holder.RemoveSocket(socket);
    })
});
server.listen(6000);