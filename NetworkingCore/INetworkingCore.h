#pragma once

#include "../TaskEngine/ISubSystem.h"

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

namespace NETWORK
{
	class NetworkingCore;

	class INetworkingCore : public ISubSystem
	{
	private:

		NetworkingCore *net;

	public:

		/*INetworkingCore();
		~INetworkingCore();*/

		/**	Message() function that provides the engine access to the core.
		 *	
		 *	The Message() function is the derived function from the ISubSystem interface that provides the core access to the NetworkingCore.
		 *	Here are the possible API calls that the function accepts: NETCONNECT, NETSEND, NETDISCONNECT, NETCHECKPACKET, NETGETPACKET
		 *
		 *	NETCONNECT
		 *	param	-	Not used.
		 *	in		-	Expects a void pointer to a ConnectionInfo struct.
		 *	out		-	Expects a void pointer to a bool value.
		 *	
		 *	NETSEND
		 *	param	-	Expects the size of the data being sent.
		 *	in		-	Expects a void pointer to the data the user wishes to send.
		 *	out		-	Expects a void pointer to a bool value.
		 *	
		 *	NETDISCONNECT
		 *	param	-	Not used.
		 *	in		-	Not used.
		 *	out		-	Not used.
		 *	
		 *	NETCHECKPACKET
		 *	param	-	Expects the packetID of the packet the user wants to get.
		 *	in		-	Not used.
		 *	out		-	Expects a void pointer to a bool value.
		 *	
		 *	NETGETPACKET
		 *	param	-	Expects the packetID of the packet the user wants to get.
		 *	in		-	Not used.
		 *	out		-	Expects a void pointer to a PacketData struct. **This is given by value, expect it as such.**
		 */
		void Message(char* API, unsigned long param, void* in = nullptr, void* out = nullptr);
	};
}

extern "C"
{
	__declspec(dllexport) HRESULT CreateNetworkCore(HINSTANCE hDLL, ISubSystem **Interface);
	__declspec(dllexport) HRESULT ReleaseNetworkCore(ISubSystem **Interface);
}

typedef HRESULT (*CREATECOREOBJECT)(HINSTANCE hDLL, ISubSystem **Interface);
typedef HRESULT (*RELEASECOREOBJECT)(ISubSystem **Interface);