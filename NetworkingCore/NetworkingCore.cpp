#include "NetworkingCore.h"

namespace NETWORK
{
	NetworkingCore::NetworkingCore()
	{
		m_PacketReporter = new PacketReporter();
		m_Client = new Client(m_PacketReporter);
	}

	NetworkingCore::~NetworkingCore()
	{
		delete m_Client;
		delete m_PacketReporter;
	}

	bool NetworkingCore::Connect(std::string ipaddr, std::string port)
	{
		return m_Client->Connect(ipaddr, port);
	}

	bool NetworkingCore::Send(char* data, unsigned int size)
	{
		return m_Client->Send(data, size);
	}

	void NetworkingCore::Disconnect()
	{
		m_Client->Disconnect();
	}

	bool NetworkingCore::CheckPacket(unsigned int ID)
	{
		return m_PacketReporter->CheckPacket(ID);
	}

	PacketData NetworkingCore::GetLastPacket(unsigned int ID)
	{
		return m_PacketReporter->GetLastPacket(ID);
	}
}