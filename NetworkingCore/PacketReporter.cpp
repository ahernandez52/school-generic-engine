#include "PacketReporter.h"

namespace NETWORK
{
	PacketReporter::PacketReporter(){}
	
	PacketReporter::~PacketReporter()
	{
		m_Registry.clear();
	}
	
	bool PacketReporter::CheckPacket(unsigned int ID)
	{
		std::map<unsigned int, PacketAlert>::iterator it;

		it = m_Registry.find(ID);

		if(it == m_Registry.end())
		{
			return false;
		}
		else
		{
			if(it->second.dirty)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}

	PacketData PacketReporter::GetLastPacket(unsigned int ID)
	{
		PacketData zero;
		zero.packetID = 0;
		std::map<unsigned int, PacketAlert>::iterator it;

		it = m_Registry.find(ID);

		if(it == m_Registry.end())
		{
			// Not found in the map
			return zero;
		}
		else
		{
			// ID exists in the map, check if dirty
			if(it->second.dirty)
			{
				it->second.dirty = false;
				zero.packetID = it->second.packetID;
				memcpy(zero.data, it->second.data, sizeof(it->second.data));
				return zero;
			}
			else
			{
				return zero;
			}
		}
	}

	void PacketReporter::TrackPacket(PacketAlert p)
	{
		std::map<unsigned int, PacketAlert>::iterator it;

		it = m_Registry.find(p.packetID);

		if(it == m_Registry.end())
		{
			// Not found in the map
			m_Registry.insert(std::pair<unsigned int, PacketAlert>(p.packetID, p));
		}
		else
		{
			// ID exists in the map, overwrite data and flag dirty
			memcpy(m_Registry[p.packetID].data, p.data, sizeof(p.data));
			m_Registry[p.packetID].dirty = true;
		}
	}
}