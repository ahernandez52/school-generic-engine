#include "Input.h"

namespace RawInputCore
{
	Input::Input(void)
	{
		int count = 0;

		USHORT zero = 0x30;
		for(int i = 0; i<10; i++)
		{
			KeyState number;
			number.id = count;
			number.state = false;
			number.value = zero+i;
			m_KeyStates.push_back(number);

			count++;
		}

		USHORT num0 = VK_NUMPAD0;
		for(int i = 0; i<15;i++)
		{
			KeyState numpad;
			numpad.id = count;
			numpad.state = false;
			numpad.value = num0+i;
			m_KeyStates.push_back(numpad);

			count++;
		}

		USHORT a = 0x41;
		for(int i = 0; i<26; i++)
		{
			KeyState letter;
			letter.id = count;
			letter.state = false;
			letter.value = i+a;
			m_KeyStates.push_back(letter);

			count++;
		}

		USHORT f1 = VK_F1;
		for(int i = 0; i<24; i++)
		{
			KeyState function;
			function.id = count;
			function.state = false;
			function.value = f1+i;
			m_KeyStates.push_back(function);

			count++;
		}

		USHORT space = VK_SPACE;
		for(int i = 0; i<16; i++)
		{
			KeyState special;
			special.id = count;
			special.state = false;
			special.value = space+i;
			m_KeyStates.push_back(special);
			count++;
		}

		USHORT backSpace = VK_BACK;
		for(int i = 0; i<11; i++)
		{
			if(i!=2 && i!=5 && i!=8)
			{
				KeyState general;
				general.id = count;
				general.state = false;
				general.value = backSpace+i;
				m_KeyStates.push_back(general);
				count++;
			}

		}

		KeyState numlock;
		numlock.id = count;
		numlock.state = false;
		numlock.value = VK_NUMLOCK;
		m_KeyStates.push_back(numlock);
		count++;

		KeyState scrollLock;
		scrollLock.id = count;
		scrollLock.state = false;
		scrollLock.value = VK_SCROLL;
		m_KeyStates.push_back(scrollLock);
		count++;

		KeyState LWin;
		LWin.id = count;
		LWin.state = false;
		LWin.value = VK_LWIN;
		m_KeyStates.push_back(LWin);
		count++;

		KeyState RWin;
		RWin.id = count;
		RWin.state = false;
		RWin.value = VK_RWIN;
		m_KeyStates.push_back(RWin);
		count++;

		KeyState LShift;
		LShift.id = count;
		LShift.state = false;
		LShift.value = VK_LSHIFT;
		m_KeyStates.push_back(LShift);
		count++;

		KeyState RShift;
		RShift.id = count;
		RShift.state = false;
		RShift.value = VK_RSHIFT;
		m_KeyStates.push_back(RShift);
		count++;

		KeyState RControl;
		RControl.id = count;
		RControl.state = false;
		RControl.value = VK_RCONTROL;
		m_KeyStates.push_back(RControl);
		count++;

		KeyState LControl;
		LControl.id = count;
		LControl.state = false;
		LControl.value = VK_LCONTROL;
		m_KeyStates.push_back(LControl);
		count++;

		KeyState RMenu;
		RMenu.id = count;
		RMenu.state = false;
		RMenu.value = VK_RMENU;
		m_KeyStates.push_back(RMenu);
		count++;

		KeyState LMenu;
		LMenu.id = count;
		LMenu.state = false;
		LMenu.value = VK_LMENU;
		m_KeyStates.push_back(LMenu);
		count++;

		KeyState Question;
		Question.id = count;
		Question.state = false;
		Question.value = VK_OEM_2;
		m_KeyStates.push_back(Question);
		count;

		KeyState Tilde;
		Tilde.id = count;
		Tilde.state = false;
		Tilde.value = VK_OEM_3;
		m_KeyStates.push_back(Tilde);
		count;

		KeyState OBracket;
		OBracket.id = count;
		OBracket.state = false;
		OBracket.value = VK_OEM_4;
		m_KeyStates.push_back(OBracket);
		count;

		KeyState BackSlash;
		BackSlash.id = count;
		BackSlash.state = false;
		BackSlash.value = VK_OEM_5;
		m_KeyStates.push_back(BackSlash);
		count;

		KeyState CBracket;
		CBracket.id = count;
		CBracket.state = false;
		CBracket.value = VK_OEM_6;
		m_KeyStates.push_back(CBracket);
		count;

		KeyState Quote;
		Quote.id = count;
		Quote.state = false;
		Quote.value = VK_OEM_7;
		m_KeyStates.push_back(Quote);
		count;

		KeyState Escape;
		Escape.id = count;
		Escape.state = false;
		Escape.value = VK_ESCAPE;
		m_KeyStates.push_back(Escape);
		count++;

		for(unsigned int i = 0; i<m_KeyStates.size(); i++)
		{
			if(GetAsyncKeyState(m_KeyStates[i].value) & 0x8000)
			{
				m_KeyStates[i].state = true;
			}
			m_KeyStates[i].held = false;
		}

	}



	Input::~Input(void)
	{
	}


	void Input::Init()
	{
			//Initialize the keyboard
		Device[0].usUsagePage = 1;
		Device[0].usUsage	   = 6;
		Device[0].dwFlags     = 0;
		Device[0].hwndTarget  = NULL;

		//Initialize the mouse
		Device[1].usUsagePage = 1;
		Device[1].usUsage	   = 2;
		Device[1].dwFlags	   = 0;
		Device[1].hwndTarget  = NULL;

		if(RegisterRawInputDevices(Device,2,sizeof(RAWINPUTDEVICE))==false)
		{
			return;
		}
	}

	void Input::GetInput(LPARAM param)
	{
		XINPUT_STATE temp[4];
		for(int i = 0; i<4; i++)
		{
			temp[i] = m_controller[i];
		}
		for(int i = 0; i<4; i++)
		{
			ZeroMemory(&m_controller[i], sizeof(XINPUT_STATE));

			XInputGetState(i,&m_controller[i]);
		
		}

		UINT bufferSize;

		GetRawInputData((HRAWINPUT) param,RID_INPUT,NULL,&bufferSize,sizeof(RAWINPUTHEADER));

		m_buffer = new BYTE[bufferSize];
	
		GetRawInputData((HRAWINPUT)param, RID_INPUT, (LPVOID)m_buffer, &bufferSize, sizeof (RAWINPUTHEADER));

		RAWINPUT *raw = (RAWINPUT*) m_buffer;

		if (raw->header.dwType == RIM_TYPEMOUSE)
		{
			m_MouseX = raw->data.mouse.lLastX;
			m_MouseY = raw->data.mouse.lLastY;
			m_MouseWheel = raw->data.mouse.usButtonData;

			if(raw->data.mouse.usButtonFlags & RI_MOUSE_BUTTON_1_DOWN)
			{
				m_MouseButton[0] = true;
				if(!m_MouseButtonHeld[0])
				{
					DispatchLeftClicked();
					m_MouseButtonHeld[0] = true;
				}
			}
			if(raw->data.mouse.usButtonFlags & RI_MOUSE_BUTTON_1_UP)
			{
				m_MouseButton[0] = false;
				DispatchLeftReleased();
				m_MouseButtonHeld[0] = false;
			}
			if(raw->data.mouse.usButtonFlags & RI_MOUSE_BUTTON_2_DOWN)
			{
				m_MouseButton[1] = true;
				if(!m_MouseButtonHeld[1])
				{
					DispatchRightClicked();
					m_MouseButtonHeld[1] = true;
				}
			}
			if(raw->data.mouse.usButtonFlags & RI_MOUSE_BUTTON_2_UP)
			{
				m_MouseButton[1] = false;
				DispatchRightReleased();
				m_MouseButtonHeld[1] = false;
			}
			if(raw->data.mouse.usButtonFlags & RI_MOUSE_BUTTON_3_DOWN)
			{
				m_MouseButton[2] = true;
				if(!m_MouseButtonHeld[2])
				{
					DispatchMidClicked();
					m_MouseButtonHeld[2] = true;
				}
			}
			if(raw->data.mouse.usButtonFlags & RI_MOUSE_BUTTON_3_UP)
			{
				m_MouseButton[2] = false;
				DispatchMidReleased();
				m_MouseButtonHeld[2] = false;
			}
		}

		if (raw->header.dwType == RIM_TYPEKEYBOARD)
		{
			USHORT code = raw->data.keyboard.VKey;
		
			for(UINT i = 0; i<m_KeyStates.size(); i++)
			{
				if(code == m_KeyStates[i].value)
				{
					KeyPressed(i,raw->data.keyboard.Message);
				}
			}
		}
	}

	void Input::KeyPressed(UINT key, USHORT message)
	{
		if(message ==  WM_KEYDOWN || message == WM_SYSKEYDOWN)
		{
			if(m_KeyStates[key].state == false)
			{
				//Send message that the m_KeyStates[key] is pressed
				changed = true;
				m_KeyStates[key].state = true;
			}
			else
			{
				if(m_KeyStates[key].held == false)
				{
					//Send message that the m_KeyStates[key] is being held
					changed = true;
					m_KeyStates[key].held = true;
				}
			}
		}

		if(message == WM_KEYUP || message == WM_SYSKEYUP)
		{
			//Send message that the key is released
			changed = true;
			m_KeyStates[key].held = false;
			m_KeyStates[key].state = false;
		
		}
	}

	bool Input::KeyDown(USHORT keyCode)
	{
		for(UINT i = 0; i<m_KeyStates.size(); i++)
		{
			if(m_KeyStates[i].value == keyCode)
			{
				if(m_KeyStates[i].state == true)
					return true;
				return false;
			}
		}
		return false;
	}

	vector<Input::KeyState> Input::GetKeyStates()
	{
		return m_KeyStates;
	}


	bool Input::HasChanged()
	{
		return changed;
	}
	void Input::ResetChange()
	{
		changed = false;
	}
}