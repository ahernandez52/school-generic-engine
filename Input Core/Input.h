#pragma once
#include <dinput.h>
#include <Xinput.h>
#include <vector>

#pragma comment(lib, "XINPUT9_1_0.LIB")
using namespace std;


namespace RawInputCore
{
	class __declspec(dllexport) Input
	{
	public:
		struct KeyState{
			int id;
			bool state;
			bool held;
			USHORT value;
		};

	private:
		bool changed;
	
		LPBYTE m_buffer;
		long m_MouseX;
		long m_MouseY;
		USHORT m_MouseWheel;
	
		bool m_MouseButton[3]; //0 - left, 1 - right, 2 - middle
		bool m_MouseButtonHeld[3];
		vector<KeyState> m_KeyStates;
		XINPUT_STATE m_controller[4];


		//Message Senders
		void DispatchLeftClicked(){};
		void DispatchLeftReleased(){};
		void DispatchRightClicked(){};
		void DispatchRightReleased(){};
		void DispatchMidClicked(){};
		void DispatchMidReleased(){};
		void DispatchKeyPressed(USHORT keyCode){}

		void KeyPressed(UINT keyIndex, USHORT message);
	public:

		Input(void);
		~Input(void);

		void Init();
		void GetInput(LPARAM param);
		vector<KeyState> GetKeyStates();
		bool HasChanged();
		void ResetChange();

		//OutSideAccess keyboard/mouse
		bool KeyDown(USHORT keyCode);
		long GetMouseX(){return m_MouseX;}
		long GetMouseY(){return m_MouseY;}
		USHORT GetMouseWheel(){return m_MouseWheel;}
		bool GetMouseButton(int button){return m_MouseButton[button];}

			//XBox DPAD
		XINPUT_STATE GetXboxController(int id){return m_controller[id];}
		bool XdpadUp(int ID){return m_controller[ID].Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP;}  
		bool XdpadDown(int ID){return m_controller[ID].Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN;}
		bool XdpadLeft(int ID){return m_controller[ID].Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT;} 
		bool XdpadRight(int ID){return m_controller[ID].Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT;} 

		bool Xstart(int ID){return m_controller[ID].Gamepad.wButtons & XINPUT_GAMEPAD_START;}// XBOX Start
		bool Xback(int ID){return m_controller[ID].Gamepad.wButtons & XINPUT_GAMEPAD_BACK;}// XBox Back
		bool X_A(int ID){return m_controller[ID].Gamepad.wButtons & XINPUT_GAMEPAD_A;}
		bool X_B(int ID){return m_controller[ID].Gamepad.wButtons & XINPUT_GAMEPAD_B;}
		bool X_X(int ID){return m_controller[ID].Gamepad.wButtons & XINPUT_GAMEPAD_X;}
		bool X_Y(int ID){return m_controller[ID].Gamepad.wButtons & XINPUT_GAMEPAD_Y;}
		bool lXthumb(int ID){return m_controller[ID].Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_THUMB;} // XBox left thumb
		bool rXthumb(int ID){return m_controller[ID].Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_THUMB;} // XBox right thumb
		bool rXshoulder(int ID){return m_controller[ID].Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER;} // Xbox Right Shoulder
		bool lXshoulder(int ID){return m_controller[ID].Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER;} // Xbox Left Shoulder
		BYTE rXTrigger(int ID){return m_controller[ID].Gamepad.bRightTrigger;} // Xbox Right Trigger
		BYTE lXTrigger(int ID){return m_controller[ID].Gamepad.bLeftTrigger;}// Xbox Left Trigger
		SHORT rXthumbX(int ID){return m_controller[ID].Gamepad.sThumbRX;}// Xbox Right thumb x
		SHORT rXthumbY(int ID){return m_controller[ID].Gamepad.sThumbRY;} // Xbox Right thumb y
		SHORT lXthumbX(int ID){return m_controller[ID].Gamepad.sThumbLX;} // XBox Left Thumb X
		SHORT lXthumbY(int ID){return m_controller[ID].Gamepad.sThumbLY;} // XBox Left Thumb Y


		RAWINPUTDEVICE Device[2];
	};

}