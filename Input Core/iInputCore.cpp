#include "iInputCore.h"

void iInputCore::Message(char* API, unsigned long param, void *in, void *out)
{
	if(strcmp(API, "STARTUP") == 0)
	{
		input = new Input();
		input->Init();
	}

	else if(strcmp(API, "SHUTDOWN") == 0)
	{
		delete input;
	}

	else if(strcmp(API, "UPDATE") == 0)
	{
		input->GetInput((USHORT)param);
	}

	else if(strcmp(API, "GETKEYBOARD") == 0)
	{
		*(vector<Input::KeyState>*)out = input->GetKeyStates();
	}

	else if(strcmp(API, "GETXBOXCONTROLLER") == 0)
	{
		*(XINPUT_STATE*)out = input->GetXboxController((int)param);
	}

	else if(strcmp(API, "KEYDOWN") == 0)
	{
		*(bool*)out = input->KeyDown((USHORT)param);
	}

	else if (strcmp(API, "GETMOUSE") == 0)
	{
		*(pair<float,float>*)out = pair<float,float>(input->GetMouseX(),input->GetMouseY());
	}

	else if (strcmp(API, "GETMOUSEBUTTON") == 0)
	{
		*(bool*)out = input->GetMouseButton((int)param);
	}

	else if (strcmp(API, "GETMOUSEWHEEL") == 0)
	{
		*(USHORT*)out = input->GetMouseWheel();
	}

	else if (strcmp(API, "XDPADUP") == 0)
	{
		*(bool*)out = input->XdpadUp((int)param);
	}

	else if (strcmp(API, "XDPADDOWN") == 0)
	{
		*(bool*)out = input->XdpadDown((int)param);
	}

	else if (strcmp(API, "XDPADRIGHT") == 0)
	{
		*(bool*)out = input->XdpadRight((int)param);
	}

	else if (strcmp(API, "XDPADLEFT") == 0)
	{
		*(bool*)out = input->XdpadLeft((int)param);
	}

	else if (strcmp(API, "XSTART") == 0)
	{
		*(bool*)out = input->Xstart((int)param);
	}

	else if (strcmp(API, "XBACK") == 0)
	{
		*(bool*)out = input->Xback((int)param);
	}

	else if (strcmp(API, "X_A") == 0)
	{
		*(bool*)out = input->X_A((int)param);
	}

	else if (strcmp(API, "X_B") == 0)
	{
		*(bool*)out = input->X_B((int)param);
	}

	else if (strcmp(API, "X_X") == 0)
	{
		*(bool*)out = input->X_X((int)param);
	}

	else if (strcmp(API, "X_Y") == 0)
	{
		*(bool*)out = input->X_Y((int)param);
	}

	else if (strcmp(API, "XLTHUMB") == 0)
	{
		*(bool*)out = input->lXthumb((int)param);
	}

	else if (strcmp(API, "XRTHUMB") == 0)
	{
		*(bool*)out = input->rXthumb((int)param);
	}

	else if (strcmp(API, "XRSHOULDER") == 0)
	{
		*(bool*)out = input->rXshoulder((int)param);
	}

	else if (strcmp(API, "XLSHOULDER") == 0)
	{
		*(bool*)out = input->lXshoulder((int)param);
	}

	else if (strcmp(API, "XLTRIGGER") == 0)
	{
		*(BYTE*)out = input->lXTrigger((int)param);
	}

	else if (strcmp(API, "XRTRIGGER") == 0)
	{
		*(BYTE*)out = input->rXTrigger((int)param);
	}

	else if (strcmp(API, "XRTHUMBX") == 0)
	{
		*(SHORT*)out = input->rXthumbX((int)param);
	}

	else if (strcmp(API, "XRTHUMBY") == 0)
	{
		*(SHORT*)out = input->rXthumbY((int)param);
	}

	else if (strcmp(API, "XLTHUMBX") == 0)
	{
		*(SHORT*)out = input->lXthumbX((int)param);
	}

	else if (strcmp(API, "XLTHUMBY") == 0)
	{
		*(SHORT*)out = input->lXthumbY((int)param);
	}
}


HRESULT CreateInputCore(HINSTANCE hDLL, ISubSystem **Interface)
{
	if(!*Interface)
	{
		*Interface = new iInputCore();

		return S_OK;
	}

	return S_FALSE;
}

HRESULT ReleaseInputCore(ISubSystem **Interface)
{
	if(!*Interface)
	{
		return S_FALSE;
	}

	delete *Interface;
	*Interface = nullptr;

	return S_OK;
}