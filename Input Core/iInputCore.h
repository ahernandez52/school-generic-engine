#pragma once
#include "../TaskEngine/ISubSystem.h"
#include <Windows.h>
#include "Input.h"
using namespace RawInputCore;

class iInputCore : public ISubSystem
{
private:

	Input* input;

public:

	void Message(char* API, unsigned long param, void *in = nullptr, void *out = nullptr);

/*
	virtual bool KeyDown(USHORT keyCode) = 0;
	virtual long GetMouseX() = 0;
	virtual long GetMouseY() = 0;
	virtual long GetMouseWheel() = 0;
	virtual bool GetMouseButton(int button) = 0;

	virtual bool XdpadUp(int ID) = 0; 
	virtual bool XdpadDown(int ID) = 0;
	virtual bool XdpadLeft(int ID)= 0; 
	virtual bool XdpadRight(int ID)= 0; 

	virtual bool Xstart(int ID)= 0;// XBOX Start
	virtual bool Xback(int ID)= 0;// XBox Back
	virtual bool X_A(int ID)= 0;
	virtual bool X_B(int ID)= 0;
	virtual bool X_X(int ID)= 0;
	virtual bool X_Y(int ID)= 0;
	virtual bool lXthumb(int ID)= 0; // XBox left thumb
	virtual bool rXthumb(int ID)= 0; // XBox right thumb
	virtual bool rXshoulder(int ID)= 0; // Xbox Right Shoulder
	virtual bool lXshoulder(int ID)= 0; // Xbox Left Shoulder
	virtual BYTE rXTrigger(int ID)= 0; // Xbox Right Trigger
	virtual BYTE lXTrigger(int ID)= 0;// Xbox Left Trigger
	virtual SHORT rXthumbX(int ID)= 0;// Xbox Right thumb x
	virtual SHORT rXthumbY(int ID)= 0; // Xbox Right thumb y
	virtual SHORT lXthumbX(int ID)= 0; // XBox Left Thumb X
	virtual SHORT lXthumbY(int ID)= 0; // XBox Left Thumb Y*/
};

extern "C"
{
	__declspec(dllexport) HRESULT CreateInputCore(HINSTANCE hDLL, ISubSystem **Interface);
	__declspec(dllexport) HRESULT ReleaseInputCore(ISubSystem **Interface);
}

typedef HRESULT (*CREATECOREOBJECT)(HINSTANCE hDLL, ISubSystem **Interface);
typedef HRESULT (*RELEASECOREOBJECT)(ISubSystem **Interface);