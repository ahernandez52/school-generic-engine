#include "DragonPhysicsOBB.h"
#include "DragonEnums.h"

DragonPhysicsCollisionOBB::DragonPhysicsCollisionOBB()
{
	m_PrimTypeID=PT_OBB;
	halfExtents=DragonXVector3(5.0f,5.0f,5.0f);
	mRadius=halfExtents.GetMagnitude();
}
DragonPhysicsCollisionOBB::~DragonPhysicsCollisionOBB()
{
}