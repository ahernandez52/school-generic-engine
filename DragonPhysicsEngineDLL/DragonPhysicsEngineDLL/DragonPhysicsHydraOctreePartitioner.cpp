#include "DragonPhysicsHydraOctreePartitioner.h"
#include "DragonPhysicsCollisionEngine.h"
#include "DragonContactRegistry.h"
#include "DragonCollisionDetector.h"
#include "DragonContactGenerator.h"
#include "DragonContactResolver.h"
#include "DragonConstraintManager.h"
#include "HydraObjectUpdater.h"
#include "BaseDragonPhysicsObject.h"
#include "DragonPhysicsPlane.h"
#include "DragonEnums.h"
#include <thread>

//int DragonPhysicsHydraOctreePartitioner::g_iCurrentNodeLevel = 0;

DragonPhysicsHydraOctreePartitioner::DragonPhysicsHydraOctreePartitioner()
{
	m_vCenter=DragonXVector3(0.0f,0.0f,0.0f);
	m_rDiameter=0.0f;
	for(int i=0; i<8; i++)
	{
		m_pHydraNodes[i]=NULL;
	}
}
DragonPhysicsHydraOctreePartitioner::~DragonPhysicsHydraOctreePartitioner()
{
	ReleaseHydra();
}
//void DragonPhysicsHydraOctreePartitioner::ResetHydra()
//{
//
//}
void DragonPhysicsHydraOctreePartitioner::ReleaseHydra()
{
	if(m_bIsNode)
	{
		//m_vEntities.clear();
	}
	for(int i=0; i<8; i++)
	{
		if(m_pHydraNodes[i])
		{
			m_pHydraNodes[i]->ReleaseHydra();
			delete m_pHydraNodes[i];
			m_pHydraNodes[i]=NULL;
		}
	}
}
void DragonPhysicsHydraOctreePartitioner::Create(std::vector<BaseDragonPhysicsObject*>& entities,HydraObjectUpdater& updater,DragonCollisionEngine& collisionEng,const float dt)
{
	SetData(entities);
	updater.UpdateEntities(entities,dt);
	CreateNode(entities,m_vCenter,m_rDiameter,updater,collisionEng,dt);
}
void DragonPhysicsHydraOctreePartitioner::SetData(const std::vector<BaseDragonPhysicsObject*>& entities)
{
	real numEntities=1.0f;
	// if theres no entities theres no need to make a tree
	if(entities.size()==0)
	{
		return;
	}
	else
	{
		numEntities=(real)entities.size();
	}
	// find the center of the first node
	for(const auto BaseDragonPhysicsObject : entities)
	{
		if(BaseDragonPhysicsObject->m_PhysicsEnabled)
		{
			m_vCenter.x = m_vCenter.x + BaseDragonPhysicsObject->m_Position.x;
			m_vCenter.y = m_vCenter.y + BaseDragonPhysicsObject->m_Position.y;
			m_vCenter.z = m_vCenter.z + BaseDragonPhysicsObject->m_Position.z;
		}
	}
	//center of the tree or um cube well the tree is in a cube shape
	m_vCenter.x=m_vCenter.x/numEntities;
	m_vCenter.y=m_vCenter.y/numEntities;
	m_vCenter.z=m_vCenter.z/numEntities;

	// ok now we need the diameter or i guess sorta distance from the center of the tree cube thing to determine which entities go inside it
	real wMax = 0.0f;//initialize local variable to zero we will use these to figure out our diameter
	real hMax = 0.0f;
	real dMax = 0.0f;
	// now we make some mini variables that we'll be using to help find the afore mentioned diameter
	real lWidth = 0.0f;
	real lHeight = 0.0f;
	real lDepth = 0.0f;

	// now we find the diameter
	for(const auto BaseDragonPhysicsObject : entities)
	{
		if(BaseDragonPhysicsObject->m_PhysicsEnabled)
		{
			lWidth = (real)abs(BaseDragonPhysicsObject->m_Position.x-m_vCenter.x);
			lHeight = (real)abs(BaseDragonPhysicsObject->m_Position.y-m_vCenter.y);
			lDepth = (real)abs(BaseDragonPhysicsObject->m_Position.z-m_vCenter.z);

			if(lWidth>wMax)
			{
				wMax=lWidth;
			}
			if (lHeight>hMax)
			{
				hMax=lHeight;
			}
			if (lDepth>dMax)
			{
				dMax=lDepth;
			}
		}
	}
	//ok we now have the largest radii ? any way radius whatever we need to x by 2 for diameter we all know math
	wMax*=2;
	hMax*=2;
	dMax*=2;
	// ok now lets see which one is biggest which will be our winner for largest diameter
	m_rDiameter=wMax;
	if (hMax>m_rDiameter)
	{
		m_rDiameter=hMax;
	}
	if (dMax>m_rDiameter)
	{
		m_rDiameter=dMax;
	}
}
void DragonPhysicsHydraOctreePartitioner::CreateNode(std::vector<BaseDragonPhysicsObject*>& entities,DragonXVector3 center, real diameter,HydraObjectUpdater& updater,DragonCollisionEngine& collisionEng,const float dt)
{
	m_vCenter=center;
	m_rDiameter=diameter;
	//int iMaxNodes = 50;
	int iMinNumberOfObjectsForStdPartition = 128;//64 gives me a decent frame rate with friction and looks smoothest single threaded
	int iMinNumberOfObjectsForThreading = 1024;//64 gives me a decent frame rate with friction and looks smoothest single threaded

	if((entities.size()<(unsigned)iMinNumberOfObjectsForStdPartition))//||(g_iCurrentNodeLevel >= iMaxNodes))//if its a node
	{
		//SetNode(entities);
		m_bIsNode=true;
		mConPairCont.ResetContainer();
		mConData.ResetContainer();
		//updater.UpdateEntities(entities,dt);
		//std::vector<DragonContactPair> conPairs;
		//std::vector<DragonContact> contacts;
		collisionEng.mCollisionDetector->GenContactPairs(mConPairCont,entities);
		//std::thread colDet = std::thread([&](){conPairs=collisionEng.mCollisionDetector->GenContactPairs(pNode->m_vEntities);});
		//std::thread constraintContactGenThread = std::thread([&](){contacts=collisionEng.mConstraintManager->GenConstraintContacts(pNode->m_vEntities);});
		//colDet.join();
		//constraintContactGenThread.join();
		//std::thread conPairRegThread = std::thread([&](){collisionEng.mContactRegistry->UpdatePairData(mConPairCont.mVecConPairs);});
		collisionEng.mContactGenerator->GenerateContactsForPairs(mConPairCont,mConData);
		collisionEng.mContactRegistry->UpdatePairData(mConPairCont.mVecConPairs);
		//collisionEng.mContactRegistry->UpdatePairData(conPairs);		
		//std::thread conGenThread = std::thread([&](){contacts=collisionEng.mContactGenerator->GenerateContactsForPairs(conPairs,contacts);});
		//conGenThread.join();
		//std::thread contactDataStorageThread = std::thread([&,contacts](){collisionEng.mContactRegistry->UpdateContactData(contacts);});
		//std::thread conResThread = std::thread([&,dt](){collisionEng.mContactResolver->ResolveContacts(contacts,dt);});
		collisionEng.mContactResolver->ResolveContacts(mConData.mVecConData,dt);
		//collisionEng.mContactRegistry->UpdateContactData(contacts);
		//conPairRegThread.join();
	}
	else if ((entities.size()>(unsigned)iMinNumberOfObjectsForThreading))
	{
		m_bIsNode=false;
		real radius = 0.0f;
		vTopFrontLeft.ResetContainer();
		vTopFrontRight.ResetContainer();
		vTopBackLeft.ResetContainer();
		vTopBackRight.ResetContainer();

		vBottomFrontLeft.ResetContainer();
		vBottomFrontRight.ResetContainer();
		vBottomBackLeft.ResetContainer();
		vBottomBackRight.ResetContainer();

		for(const auto BaseDragonPhysicsObject : entities)
		{
			if(BaseDragonPhysicsObject==NULL)
			{
				break;
			}
			if(BaseDragonPhysicsObject->m_PhysicsEnabled)
			{
				radius = BaseDragonPhysicsObject->GetRadius();

				if((BaseDragonPhysicsObject->m_Position.y+radius>=m_vCenter.y)&&(BaseDragonPhysicsObject->m_Position.x-radius<=m_vCenter.x)&&(BaseDragonPhysicsObject->m_Position.z-radius<=m_vCenter.z))
				{
					vTopFrontLeft.Add(BaseDragonPhysicsObject);
				}
				else if((BaseDragonPhysicsObject->m_Position.y+radius>=m_vCenter.y)&&(BaseDragonPhysicsObject->m_Position.x+radius>=m_vCenter.x)&&(BaseDragonPhysicsObject->m_Position.z-radius<=m_vCenter.z))
				{
					vTopFrontRight.Add(BaseDragonPhysicsObject);
				}
				else if((BaseDragonPhysicsObject->m_Position.y+radius>=m_vCenter.y)&&(BaseDragonPhysicsObject->m_Position.x-radius<=m_vCenter.x)&&(BaseDragonPhysicsObject->m_Position.z+radius>=m_vCenter.z))
				{
					vTopBackLeft.Add(BaseDragonPhysicsObject);
				}
				else if((BaseDragonPhysicsObject->m_Position.y+radius>=m_vCenter.y)&&(BaseDragonPhysicsObject->m_Position.x+radius>=m_vCenter.x)&&(BaseDragonPhysicsObject->m_Position.z+radius>=m_vCenter.z))
				{
					vTopBackRight.Add(BaseDragonPhysicsObject);
				}
				else if((BaseDragonPhysicsObject->m_Position.y-radius<=m_vCenter.y)&&(BaseDragonPhysicsObject->m_Position.x-radius<=m_vCenter.x)&&(BaseDragonPhysicsObject->m_Position.z-radius<=m_vCenter.z))
				{
					vBottomFrontLeft.Add(BaseDragonPhysicsObject);
				}
				else if((BaseDragonPhysicsObject->m_Position.y-radius<=m_vCenter.y)&&(BaseDragonPhysicsObject->m_Position.x+radius>=m_vCenter.x)&&(BaseDragonPhysicsObject->m_Position.z-radius<=m_vCenter.z))
				{
					vBottomFrontRight.Add(BaseDragonPhysicsObject);
				}
				else if((BaseDragonPhysicsObject->m_Position.y-radius<=m_vCenter.y)&&(BaseDragonPhysicsObject->m_Position.x-radius<=m_vCenter.x)&&(BaseDragonPhysicsObject->m_Position.z+radius>=m_vCenter.z))
				{
					vBottomBackLeft.Add(BaseDragonPhysicsObject);
				}
				else
				{
					vBottomBackRight.Add(BaseDragonPhysicsObject);
				}
			}
			else
			{
				if(BaseDragonPhysicsObject->mI_PrimitiveType==PT_Plane)
				{
					DragonPhysicsPlane* colObj;
					colObj = static_cast<DragonPhysicsPlane*>(BaseDragonPhysicsObject->m_CollisionObject);
					if((m_vCenter*colObj->mNormalDirection)-m_rDiameter<colObj->mDistFromOrigin)
					{
						vTopFrontLeft.Add(BaseDragonPhysicsObject);
						vTopFrontRight.Add(BaseDragonPhysicsObject);
						vTopBackLeft.Add(BaseDragonPhysicsObject);
						vTopBackRight.Add(BaseDragonPhysicsObject);

						vTopBackRight.Add(BaseDragonPhysicsObject);
						vBottomFrontRight.Add(BaseDragonPhysicsObject);
						vBottomBackLeft.Add(BaseDragonPhysicsObject);
						vBottomBackRight.Add(BaseDragonPhysicsObject);
					}
				}
				else
				{
					radius = BaseDragonPhysicsObject->GetRadius();

					if((BaseDragonPhysicsObject->m_Position.y+radius>=m_vCenter.y)&&(BaseDragonPhysicsObject->m_Position.x-radius<=m_vCenter.x)&&(BaseDragonPhysicsObject->m_Position.z-radius<=m_vCenter.z))
					{
						vTopFrontLeft.Add(BaseDragonPhysicsObject);
					}
					if((BaseDragonPhysicsObject->m_Position.y+radius>=m_vCenter.y)&&(BaseDragonPhysicsObject->m_Position.x+radius>=m_vCenter.x)&&(BaseDragonPhysicsObject->m_Position.z-radius<=m_vCenter.z))
					{
						vTopFrontRight.Add(BaseDragonPhysicsObject);
					}
					if((BaseDragonPhysicsObject->m_Position.y+radius>=m_vCenter.y)&&(BaseDragonPhysicsObject->m_Position.x-radius<=m_vCenter.x)&&(BaseDragonPhysicsObject->m_Position.z+radius>=m_vCenter.z))
					{
						vTopBackLeft.Add(BaseDragonPhysicsObject);
					}
					if((BaseDragonPhysicsObject->m_Position.y+radius>=m_vCenter.y)&&(BaseDragonPhysicsObject->m_Position.x+radius>=m_vCenter.x)&&(BaseDragonPhysicsObject->m_Position.z+radius>=m_vCenter.z))
					{
						vTopBackRight.Add(BaseDragonPhysicsObject);
					}
					if((BaseDragonPhysicsObject->m_Position.y-radius<=m_vCenter.y)&&(BaseDragonPhysicsObject->m_Position.x-radius<=m_vCenter.x)&&(BaseDragonPhysicsObject->m_Position.z-radius<=m_vCenter.z))
					{
						vBottomFrontLeft.Add(BaseDragonPhysicsObject);
					}
					if((BaseDragonPhysicsObject->m_Position.y-radius<=m_vCenter.y)&&(BaseDragonPhysicsObject->m_Position.x+radius>=m_vCenter.x)&&(BaseDragonPhysicsObject->m_Position.z-radius<=m_vCenter.z))
					{
						vBottomFrontRight.Add(BaseDragonPhysicsObject);
					}
					if((BaseDragonPhysicsObject->m_Position.y-radius<=m_vCenter.y)&&(BaseDragonPhysicsObject->m_Position.x-radius<=m_vCenter.x)&&(BaseDragonPhysicsObject->m_Position.z+radius>=m_vCenter.z))
					{
						vBottomBackLeft.Add(BaseDragonPhysicsObject);
					}
					if((BaseDragonPhysicsObject->m_Position.y-radius<=m_vCenter.y)&&(BaseDragonPhysicsObject->m_Position.x+radius>=m_vCenter.x)&&(BaseDragonPhysicsObject->m_Position.z+radius>=m_vCenter.z))
					{
						vBottomBackRight.Add(BaseDragonPhysicsObject);
					}
				}
			}
		}
		// now we make each of the nodes or leafy bits or oh whatever this is where you split up the data
		std::thread HTFLthread = std::thread([&,dt,center,diameter](){CreateNodeEnd(vTopFrontLeft.mVecPhysXobjs,center,diameter,Top_Front_Left,updater,collisionEng,dt);});
		std::thread HTFRthread = std::thread([&,dt,center,diameter](){CreateNodeEnd(vTopFrontRight.mVecPhysXobjs,center,diameter,Top_Front_Right,updater,collisionEng,dt);});
		std::thread HTBLthread = std::thread([&,dt,center,diameter](){CreateNodeEnd(vTopBackLeft.mVecPhysXobjs,center,diameter,Top_Back_Left,updater,collisionEng,dt);});
		std::thread HTBRthread = std::thread([&,dt,center,diameter](){CreateNodeEnd(vTopBackRight.mVecPhysXobjs,center,diameter,Top_Back_Right,updater,collisionEng,dt);});

		std::thread HBFLthread = std::thread([&,dt,center,diameter](){CreateNodeEnd(vBottomFrontLeft.mVecPhysXobjs,center,diameter,Bottom_Front_Left,updater,collisionEng,dt);});
		std::thread HBFRthread = std::thread([&,dt,center,diameter](){CreateNodeEnd(vBottomFrontRight.mVecPhysXobjs,center,diameter,Bottom_Front_Right,updater,collisionEng,dt);});
		std::thread HBBLthread = std::thread([&,dt,center,diameter](){CreateNodeEnd(vBottomBackLeft.mVecPhysXobjs,center,diameter,Bottom_Back_Left,updater,collisionEng,dt);});
		std::thread HBBRthread = std::thread([&,dt,center,diameter](){CreateNodeEnd(vBottomBackRight.mVecPhysXobjs,center,diameter,Bottom_Back_Right,updater,collisionEng,dt);});

		HTFLthread.join();
		HTFRthread.join();
		HTBLthread.join();
		HTBRthread.join();

		HBFLthread.join();
		HBFRthread.join();
		HBBLthread.join();
		HBBRthread.join();
	}
	else
	{
		m_bIsNode=false;
		real radius = 0.0f;
		vTopFrontLeft.ResetContainer();
		vTopFrontRight.ResetContainer();
		vTopBackLeft.ResetContainer();
		vTopBackRight.ResetContainer();

		vBottomFrontLeft.ResetContainer();
		vBottomFrontRight.ResetContainer();
		vBottomBackLeft.ResetContainer();
		vBottomBackRight.ResetContainer();

		for(const auto BaseDragonPhysicsObject : entities)
		{
			if(BaseDragonPhysicsObject==NULL)
			{
				break;
			}
			if(BaseDragonPhysicsObject->m_PhysicsEnabled)
			{
				radius = BaseDragonPhysicsObject->GetRadius();

				if((BaseDragonPhysicsObject->m_Position.y+radius>=m_vCenter.y)&&(BaseDragonPhysicsObject->m_Position.x-radius<=m_vCenter.x)&&(BaseDragonPhysicsObject->m_Position.z-radius<=m_vCenter.z))
				{
					vTopFrontLeft.Add(BaseDragonPhysicsObject);
				}
				if((BaseDragonPhysicsObject->m_Position.y+radius>=m_vCenter.y)&&(BaseDragonPhysicsObject->m_Position.x+radius>=m_vCenter.x)&&(BaseDragonPhysicsObject->m_Position.z-radius<=m_vCenter.z))
				{
					vTopFrontRight.Add(BaseDragonPhysicsObject);
				}
				if((BaseDragonPhysicsObject->m_Position.y+radius>=m_vCenter.y)&&(BaseDragonPhysicsObject->m_Position.x-radius<=m_vCenter.x)&&(BaseDragonPhysicsObject->m_Position.z+radius>=m_vCenter.z))
				{
					vTopBackLeft.Add(BaseDragonPhysicsObject);
				}
				if((BaseDragonPhysicsObject->m_Position.y+radius>=m_vCenter.y)&&(BaseDragonPhysicsObject->m_Position.x+radius>=m_vCenter.x)&&(BaseDragonPhysicsObject->m_Position.z+radius>=m_vCenter.z))
				{
					vTopBackRight.Add(BaseDragonPhysicsObject);
				}
				if((BaseDragonPhysicsObject->m_Position.y-radius<=m_vCenter.y)&&(BaseDragonPhysicsObject->m_Position.x-radius<=m_vCenter.x)&&(BaseDragonPhysicsObject->m_Position.z-radius<=m_vCenter.z))
				{
					vBottomFrontLeft.Add(BaseDragonPhysicsObject);
				}
				if((BaseDragonPhysicsObject->m_Position.y-radius<=m_vCenter.y)&&(BaseDragonPhysicsObject->m_Position.x+radius>=m_vCenter.x)&&(BaseDragonPhysicsObject->m_Position.z-radius<=m_vCenter.z))
				{
					vBottomFrontRight.Add(BaseDragonPhysicsObject);
				}
				if((BaseDragonPhysicsObject->m_Position.y-radius<=m_vCenter.y)&&(BaseDragonPhysicsObject->m_Position.x-radius<=m_vCenter.x)&&(BaseDragonPhysicsObject->m_Position.z+radius>=m_vCenter.z))
				{
					vBottomBackLeft.Add(BaseDragonPhysicsObject);
				}
				if((BaseDragonPhysicsObject->m_Position.y-radius<=m_vCenter.y)&&(BaseDragonPhysicsObject->m_Position.x+radius>=m_vCenter.x)&&(BaseDragonPhysicsObject->m_Position.z+radius>=m_vCenter.z))
				{
					vBottomBackRight.Add(BaseDragonPhysicsObject);
				}
			}
			else
			{
				if(BaseDragonPhysicsObject->mI_PrimitiveType==PT_Plane)
				{
					DragonPhysicsPlane* colObj;
					colObj = static_cast<DragonPhysicsPlane*>(BaseDragonPhysicsObject->m_CollisionObject);
					if((m_vCenter*colObj->mNormalDirection)-m_rDiameter<colObj->mDistFromOrigin)
					{
						vTopFrontLeft.Add(BaseDragonPhysicsObject);
						vTopFrontRight.Add(BaseDragonPhysicsObject);
						vTopBackLeft.Add(BaseDragonPhysicsObject);
						vTopBackRight.Add(BaseDragonPhysicsObject);

						vTopBackRight.Add(BaseDragonPhysicsObject);
						vBottomFrontRight.Add(BaseDragonPhysicsObject);
						vBottomBackLeft.Add(BaseDragonPhysicsObject);
						vBottomBackRight.Add(BaseDragonPhysicsObject);
					}
				}
				else
				{
					radius = BaseDragonPhysicsObject->GetRadius();

					if((BaseDragonPhysicsObject->m_Position.y+radius>=m_vCenter.y)&&(BaseDragonPhysicsObject->m_Position.x-radius<=m_vCenter.x)&&(BaseDragonPhysicsObject->m_Position.z-radius<=m_vCenter.z))
					{
						vTopFrontLeft.Add(BaseDragonPhysicsObject);
					}
					if((BaseDragonPhysicsObject->m_Position.y+radius>=m_vCenter.y)&&(BaseDragonPhysicsObject->m_Position.x+radius>=m_vCenter.x)&&(BaseDragonPhysicsObject->m_Position.z-radius<=m_vCenter.z))
					{
						vTopFrontRight.Add(BaseDragonPhysicsObject);
					}
					if((BaseDragonPhysicsObject->m_Position.y+radius>=m_vCenter.y)&&(BaseDragonPhysicsObject->m_Position.x-radius<=m_vCenter.x)&&(BaseDragonPhysicsObject->m_Position.z+radius>=m_vCenter.z))
					{
						vTopBackLeft.Add(BaseDragonPhysicsObject);
					}
					if((BaseDragonPhysicsObject->m_Position.y+radius>=m_vCenter.y)&&(BaseDragonPhysicsObject->m_Position.x+radius>=m_vCenter.x)&&(BaseDragonPhysicsObject->m_Position.z+radius>=m_vCenter.z))
					{
						vTopBackRight.Add(BaseDragonPhysicsObject);
					}
					if((BaseDragonPhysicsObject->m_Position.y-radius<=m_vCenter.y)&&(BaseDragonPhysicsObject->m_Position.x-radius<=m_vCenter.x)&&(BaseDragonPhysicsObject->m_Position.z-radius<=m_vCenter.z))
					{
						vBottomFrontLeft.Add(BaseDragonPhysicsObject);
					}
					if((BaseDragonPhysicsObject->m_Position.y-radius<=m_vCenter.y)&&(BaseDragonPhysicsObject->m_Position.x+radius>=m_vCenter.x)&&(BaseDragonPhysicsObject->m_Position.z-radius<=m_vCenter.z))
					{
						vBottomFrontRight.Add(BaseDragonPhysicsObject);
					}
					if((BaseDragonPhysicsObject->m_Position.y-radius<=m_vCenter.y)&&(BaseDragonPhysicsObject->m_Position.x-radius<=m_vCenter.x)&&(BaseDragonPhysicsObject->m_Position.z+radius>=m_vCenter.z))
					{
						vBottomBackLeft.Add(BaseDragonPhysicsObject);
					}
					if((BaseDragonPhysicsObject->m_Position.y-radius<=m_vCenter.y)&&(BaseDragonPhysicsObject->m_Position.x+radius>=m_vCenter.x)&&(BaseDragonPhysicsObject->m_Position.z+radius>=m_vCenter.z))
					{
						vBottomBackRight.Add(BaseDragonPhysicsObject);
					}
				}
			}
		}
		// now we make each of the nodes or leafy bits or oh whatever this is where you split up the data
		CreateNodeEnd(vTopFrontLeft.mVecPhysXobjs,center,diameter,Top_Front_Left,updater,collisionEng,dt);
		CreateNodeEnd(vTopFrontRight.mVecPhysXobjs,center,diameter,Top_Front_Right,updater,collisionEng,dt);
		CreateNodeEnd(vTopBackLeft.mVecPhysXobjs,center,diameter,Top_Back_Left,updater,collisionEng,dt);
		CreateNodeEnd(vTopBackRight.mVecPhysXobjs,center,diameter,Top_Back_Right,updater,collisionEng,dt);

		CreateNodeEnd(vBottomFrontLeft.mVecPhysXobjs,center,diameter,Bottom_Front_Left,updater,collisionEng,dt);
		CreateNodeEnd(vBottomFrontRight.mVecPhysXobjs,center,diameter,Bottom_Front_Right,updater,collisionEng,dt);
		CreateNodeEnd(vBottomBackLeft.mVecPhysXobjs,center,diameter,Bottom_Back_Left,updater,collisionEng,dt);
		CreateNodeEnd(vBottomBackRight.mVecPhysXobjs,center,diameter,Bottom_Back_Right,updater,collisionEng,dt);	
	}
}
void DragonPhysicsHydraOctreePartitioner::CreateNodeEnd(std::vector<BaseDragonPhysicsObject*>& entities,DragonXVector3 center,real diameter,int whichNode,HydraObjectUpdater& updater,DragonCollisionEngine& collisionEng,const float dt)
{
	// is there any point in making a node with nothing in it
	if(entities.size()==0)
	{
		return;// no
	}
	else
	{
		if(m_pHydraNodes[whichNode]==NULL)
		{
			m_pHydraNodes[whichNode] = new DragonPhysicsHydraOctreePartitioner();
		}
		// figure out the new center for this new node
		DragonXVector3 vNewCenter = GetNodeCenter(center,diameter,whichNode);
		//g_iCurrentNodeLevel++;
		//make a new node and pass in details
		m_pHydraNodes[whichNode]->CreateNode(entities,vNewCenter,diameter/2,updater,collisionEng,dt);
		//g_iCurrentNodeLevel--;
	}
}
DragonXVector3 DragonPhysicsHydraOctreePartitioner::GetNodeCenter(DragonXVector3 currentCenter, real diameter,int whichNode)
{
	//ok first off we make our new center n set it 2 overs
	DragonXVector3 vNewCenter = DragonXVector3(0.0f,0.0f,0.0f);
	// now based on which part of the tree we are generating the center for we take the current center + or - the diameter over 4 that
	// gives us a center thats well centered in which ever octant? we need a center for
	switch( whichNode )
	{
	case Top_Front_Left:      // 0
		{
			vNewCenter = DragonXVector3( currentCenter.x - diameter/4, currentCenter.y + diameter/4, currentCenter.z + diameter/4 );
			break;
		}
	case Top_Front_Right:     // 1
		{
			vNewCenter = DragonXVector3( currentCenter.x + diameter/4, currentCenter.y + diameter/4, currentCenter.z + diameter/4 );
			break;
		}
	case Top_Back_Left:       // 2
		{
			vNewCenter = DragonXVector3( currentCenter.x - diameter/4, currentCenter.y + diameter/4, currentCenter.z - diameter/4 );
			break;
		}
	case Top_Back_Right:      // 3
		{
			vNewCenter = DragonXVector3( currentCenter.x + diameter/4, currentCenter.y + diameter/4, currentCenter.z - diameter/4 );
			break;
		}
	case Bottom_Front_Left:   // 4
		{
			vNewCenter = DragonXVector3( currentCenter.x - diameter/4, currentCenter.y - diameter/4, currentCenter.z + diameter/4 );
			break;
		}
	case Bottom_Front_Right:  // 5
		{
			vNewCenter = DragonXVector3( currentCenter.x + diameter/4, currentCenter.y - diameter/4, currentCenter.z + diameter/4 );
			break;
		}
	case Bottom_Back_Left:    // 6
		{
			vNewCenter = DragonXVector3( currentCenter.x - diameter/4, currentCenter.y - diameter/4, currentCenter.z - diameter/4 );
			break;
		}
	case Bottom_Back_Right:   // 7
		{
			vNewCenter = DragonXVector3( currentCenter.x + diameter/4, currentCenter.y - diameter/4, currentCenter.z - diameter/4 );
			break;
		}
	default:
		{
			break;
		}
	}
	return vNewCenter;
}
//void DragonPhysicsHydraOctreePartitioner::SetNode(const std::vector<BaseDragonPhysicsObject*>& entities)
//{
//	m_bIsNode = true;
//	m_vEntities.clear();
//	m_vEntities = entities;
//}
//void DragonPhysicsHydraOctreePartitioner::SetNode(std::vector<BaseDragonPhysicsObject>& entities)
//{
//	m_bIsNode = true;
//	m_vEntities.clear();
//	for(auto &BaseDragonPhysicsObject : entities)
//	{
//		m_vEntities.push_back(&BaseDragonPhysicsObject);
//	}
//}
//void DragonPhysicsHydraOctreePartitioner::UseHydra(HydraObjectUpdater& updater,DragonCollisionEngine& collisionEng,DragonPhysicsHydraOctreePartitioner* pNode,const float dt)
//{
//	if (pNode==NULL)
//	{
//		return;
//	}
//	if (pNode->m_bIsNode)
//	{
//		updater.UpdateEntities(pNode->m_vEntities,dt);
//		std::vector<DragonContactPair> conPairs;
//		std::vector<DragonContact> contacts;
//		conPairs=collisionEng.mCollisionDetector->GenContactPairs(pNode->m_vEntities);
//		//std::thread colDet = std::thread([&](){conPairs=collisionEng.mCollisionDetector->GenContactPairs(pNode->m_vEntities);});
//		//std::thread constraintContactGenThread = std::thread([&](){contacts=collisionEng.mConstraintManager->GenConstraintContacts(pNode->m_vEntities);});
//		//colDet.join();
//		//constraintContactGenThread.join();
//		contacts=collisionEng.mContactGenerator->GenerateContactsForPairs(conPairs,contacts);
//		//collisionEng.mContactRegistry->UpdatePairData(conPairs);
//		std::thread conPairRegThread = std::thread([&,conPairs](){collisionEng.mContactRegistry->UpdatePairData(conPairs);});
//		//std::thread conGenThread = std::thread([&](){contacts=collisionEng.mContactGenerator->GenerateContactsForPairs(conPairs,contacts);});
//		//conGenThread.join();
//		//std::thread contactDataStorageThread = std::thread([&,contacts](){collisionEng.mContactRegistry->UpdateContactData(contacts);});
//		//std::thread conResThread = std::thread([&,dt](){collisionEng.mContactResolver->ResolveContacts(contacts,dt);});
//		collisionEng.mContactResolver->ResolveContacts(contacts,dt);
//		//collisionEng.mContactRegistry->UpdateContactData(contacts);
//		conPairRegThread.join();
//		//conResThread.join();
//		//contactDataStorageThread.join();
//	}
//	else
//	{
//		std::thread HTFLthread = std::thread([&,dt](){UseHydra(updater,collisionEng,pNode->m_pHydraNodes[Top_Front_Left],dt);});
//		std::thread HTFRthread = std::thread([&,dt](){UseHydra(updater,collisionEng,pNode->m_pHydraNodes[Top_Front_Right],dt);});
//		std::thread HTBLthread = std::thread([&,dt](){UseHydra(updater,collisionEng,pNode->m_pHydraNodes[Top_Back_Left],dt);});
//		std::thread HTBRthread = std::thread([&,dt](){UseHydra(updater,collisionEng,pNode->m_pHydraNodes[Top_Back_Right],dt);});
//
//		std::thread HBFLthread = std::thread([&,dt](){UseHydra(updater,collisionEng,pNode->m_pHydraNodes[Bottom_Front_Left],dt);});
//		std::thread HBFRthread = std::thread([&,dt](){UseHydra(updater,collisionEng,pNode->m_pHydraNodes[Bottom_Front_Right],dt);});
//		std::thread HBBLthread = std::thread([&,dt](){UseHydra(updater,collisionEng,pNode->m_pHydraNodes[Bottom_Back_Left],dt);});
//		std::thread HBBRthread = std::thread([&,dt](){UseHydra(updater,collisionEng,pNode->m_pHydraNodes[Bottom_Back_Right],dt);});
//
//		HTFLthread.join();
//		HTFRthread.join();
//		HTBLthread.join();
//		HTBRthread.join();
//
//		HBFLthread.join();
//		HBFRthread.join();
//		HBBLthread.join();
//		HBBRthread.join();
//	}
//}