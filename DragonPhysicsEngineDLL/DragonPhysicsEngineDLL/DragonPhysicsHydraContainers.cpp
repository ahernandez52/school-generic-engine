#include "DragonPhysicsHydraContainers.h"
#include "BaseDragonPhysicsObject.h"

DragonPairContainer::DragonPairContainer()
{

}
DragonPairContainer::~DragonPairContainer()
{

}
void DragonPairContainer::Add(DragonContactPair&& nPair)
{
	for(auto &cPair :mVecConPairs)
	{
		if(!cPair.mVailidData)
		{
			cPair=nPair;
			return;
		}
	}
	mVecConPairs.push_back(nPair);
}
void DragonPairContainer::ResetContainer()
{
	for(auto &cPair :mVecConPairs)
	{
		cPair.mVailidData=false;
	}
}

DragonPhysicsObjectContainer::DragonPhysicsObjectContainer()
{

}
DragonPhysicsObjectContainer::~DragonPhysicsObjectContainer()
{

}
void DragonPhysicsObjectContainer::Add(BaseDragonPhysicsObject* obj)
{
	for(auto &Iobj : mVecPhysXobjs)
	{
		if (Iobj==NULL)
		{
			Iobj=obj;
			return;
		}
	}
	mVecPhysXobjs.push_back(obj);
}
void DragonPhysicsObjectContainer::ResetContainer()
{
	for(auto &Iobj : mVecPhysXobjs)
	{
		Iobj=NULL;
	}
}

DragonContactContainer::DragonContactContainer()
{

}
DragonContactContainer::~DragonContactContainer()
{

}
void DragonContactContainer::Add(DragonContact&& nContact)
{
	for(auto &Idc : mVecConData)
	{
		if(!Idc.mValidData)
		{
			Idc=nContact;
			return;
		}
	}
	mVecConData.push_back(nContact);
}
void DragonContactContainer::ResetContainer()
{
	for(auto &Idc : mVecConData)
	{
		Idc.mDesiredDeltaVelocity=-1.0f;
		Idc.mPenitrtionDepth=-1.0f;
		Idc.mValidData=false;
	}
}