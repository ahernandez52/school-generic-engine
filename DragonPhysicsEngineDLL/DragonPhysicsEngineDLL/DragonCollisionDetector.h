#pragma once
#include <vector>
#include "DragonEnums.h"
#include "BaseDragonPhysicsObject.h"
//#include "DragonContactPair.h"
#include "DragonPhysicsHydraContainers.h"
#include "BaseDragonCollisionObject.h"
#include "DragonPhysicsPlane.h"
#include "DragonPhysicsOBB.h"
#include "DragonPhysicsMeshCollisionObject.h"

class DragonCollisionDetector
{
private:
	inline int GetCollisionType(const BaseDragonPhysicsObject& obj1,const BaseDragonPhysicsObject& obj2)const
	{
		if(!obj1.m_PhysicsEnabled&&!obj2.m_PhysicsEnabled)
		{
			return -1;
		}
		else if(!obj1.m_isAwake&&!obj2.m_isAwake)
		{
			return -1;
		}
		else if((obj1.mI_PrimitiveType==PT_Sphere)&&(obj2.mI_PrimitiveType==PT_Sphere))
		{
			return CT_SphereSphere;
		}
		else if((obj1.mI_PrimitiveType==PT_Sphere)&&(obj2.mI_PrimitiveType==PT_Plane)||(obj1.mI_PrimitiveType==PT_Plane)&&(obj2.mI_PrimitiveType==PT_Sphere))
		{
			return CT_SpherePlane;
		}
		else if((obj1.mI_PrimitiveType==PT_Sphere)&&(obj2.mI_PrimitiveType==PT_OBB)||(obj1.mI_PrimitiveType==PT_OBB)&&(obj2.mI_PrimitiveType==PT_Sphere))
		{
			return CT_SphereOBB;
		}
		else if((obj1.mI_PrimitiveType==PT_OBB)&&(obj2.mI_PrimitiveType==PT_Plane)||(obj1.mI_PrimitiveType==PT_Plane)&&(obj2.mI_PrimitiveType==PT_OBB))
		{
			return CT_ObbPlane;
		}
		else if((obj1.mI_PrimitiveType==PT_OBB)&&(obj2.mI_PrimitiveType==PT_OBB))
		{
			return CT_ObbObb;
		}
		else if((obj1.mI_PrimitiveType==PT_Mesh)&&(obj2.mI_PrimitiveType==PT_Sphere)||(obj1.mI_PrimitiveType==PT_Sphere)&&(obj2.mI_PrimitiveType==PT_Mesh))
		{
			return CT_MeshSphere;
		}
		else if((obj1.mI_PrimitiveType==PT_Mesh)&&(obj2.mI_PrimitiveType==PT_OBB)||(obj1.mI_PrimitiveType==PT_OBB)&&(obj2.mI_PrimitiveType==PT_Mesh))
		{
			return CT_MeshOBB;
		}
		else if((obj1.mI_PrimitiveType==PT_Mesh)&&(obj2.mI_PrimitiveType==PT_Mesh))
		{
			return CT_MeshMesh;
		}
		else if((obj1.mI_PrimitiveType==PT_Mesh)&&(obj2.mI_PrimitiveType==PT_Plane)||(obj1.mI_PrimitiveType==PT_Plane)&&(obj2.mI_PrimitiveType==PT_Mesh))
		{
			return CT_MeshPlane;
		}
		else
		{
			return -1;
		}
	}
	inline DragonXVector3 GetTriNormal(const DragonXVector3& a,const DragonXVector3& b, const DragonXVector3& c)const
	{
		DragonXVector3 n = ((b-a)%(c-a));
		n.invert();
		n.Normalize();
		return n;
	}
	inline DragonXVector3 ClosestPtPointTriangle(const DragonXVector3& p,const DragonXVector3& a,const DragonXVector3& b,const DragonXVector3& c)const
	{
		// check if p is in vertex region outside a
		DragonXVector3 ab = b-a;
		DragonXVector3 ac = c-a;
		DragonXVector3 ap = p-a;
		real d1 = ab*ap;
		real d2 = ac*ap;
		if(d1<=0.0f&&d2<=0.0f)
		{
			return a;
		}
		// check b
		DragonXVector3 bp = p-b;
		real d3 = ab*bp;
		real d4=ac*bp;
		if(d3>=0.0f&&d4<=d3)
		{
			return b;
		}
		// check edge region ab
		real vc = d1*d4-d3*d2;
		if(vc<=0.0f&&d1>=0.0f&&d3<=0.0f)
		{
			real v = d1/(d1-d3);
			return a+v*ab;
		}
		// check c
		DragonXVector3 cp = p-c;
		real d5=ab*cp;
		real d6=ac*cp;
		if(d6>=0.0f&&d5<=d6)
		{
			return c;
		}
		// check p on ac
		real vb = d5*d2-d1*d6;
		if(vb<=0.0f&&d2>=0.0f&&d6<=0.0f)
		{
			real w = d2/(d2-d6);
			return a+w*ac;
		}
		// check p on bc
		real va = d3*d6-d5*d4;
		if(va<=0.0f&&(d4-d3)>=0.0f&&(d5-d6)>=0.0f)
		{
			real w = (d4-d3)/((d4-d3)+(d5-d6));
			return b + w*(c-b);
		}
		// p is in the face region
		real denom = 1.0f/(va+vb+vc);
		real v = vb*denom;
		real w = vc * denom;
		return a+ab*v+ac*w;
	}
	inline real GetMin(const real first,const real second,const real third)const
	{
		real ret=9999999999.9f;
		if(first<ret)
		{
			ret=first;
		}
		if(second<ret)
		{
			ret = second;
		}
		if(third<ret)
		{
			ret = third;
		}
		return ret;
	}
	inline real GetMin(const real first,const real second)const
	{
		real ret=9999999999.9f;
		if(first<ret)
		{
			ret=first;
		}
		if(second<ret)
		{
			ret = second;
		}
		return ret;
	}
	inline real GetMax(const real zed,const real ein,const real zwei)const
	{
		real ret=-9999999999.9f;
		if(zed>ret)
		{
			ret=zed;
		}
		if(ein>ret)
		{
			ret = ein;
		}
		if(zwei>ret)
		{
			ret = zwei;
		}
		return ret;
	}
	inline real GetMax(const real zed,const real ein)const
	{
		real ret=-9999999999.9f;
		if(zed>ret)
		{
			ret=zed;
		}
		if(ein>ret)
		{
			ret = ein;
		}
		return ret;
	}
	inline real transformToAxis(const BaseDragonPhysicsObject &box, const DragonXVector3 &axis,const DragonXVector3 &halfSizes)const
	{
		return halfSizes.x*(real)abs(axis * box.TransformMatrix.getAxisVector(0))+halfSizes.y*(real)abs(axis*box.TransformMatrix.getAxisVector(1))+halfSizes.z*(real)abs(axis*box.TransformMatrix.getAxisVector(2));
	}
	inline real DistPointPlane(const DragonXVector3& p, const DragonXVector3& norm,const real dist)const
	{
		return p*norm-dist;
	}
	inline bool TrianglePlaneCollisionTest(const DragonTriangle& tri,const DragonXVector3& planeNorm, const real planeDist)const
	{
		real d0 = DistPointPlane(tri.m_VertA,planeNorm,planeDist);
		real d1 = DistPointPlane(tri.m_VertB,planeNorm,planeDist);
		real d2 = DistPointPlane(tri.m_VertC,planeNorm,planeDist);
		if(d0>0&&d1>0&&d2>0)
		{
			return false;
		}
		else if(d0<0&&d1<0&&d2<0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	inline bool TriangleTriangleCollisionTest(const DragonTriangle& tri1,const DragonXVector3& tri1Norm,const DragonTriangle& tri2,const DragonXVector3& tri2Norm)const
	{
		if (!TrianglePlaneCollisionTest(tri1,tri2Norm,tri2Norm*tri2.m_VertB))
		{
			return false;
		}

		if (!TrianglePlaneCollisionTest(tri2,tri1Norm,tri1Norm*tri1.m_VertB))
		{
			return false;
		}

		// now compute the edges
		DragonXVector3 tri1ba = tri1.m_VertB-tri1.m_VertA;
		DragonXVector3 tri1cb = tri1.m_VertC-tri1.m_VertB;
		DragonXVector3 tri1ac = tri1.m_VertA-tri1.m_VertC;
		//tri2
		DragonXVector3 tri2ba = tri2.m_VertB-tri2.m_VertA;
		DragonXVector3 tri2cb = tri2.m_VertC-tri2.m_VertB;
		DragonXVector3 tri2ac = tri2.m_VertA-tri2.m_VertC;

		real pu0,pu1,pu2,pv0,pv1,pv2;

		// 9 cross product axis tests
		// set 1
		DragonXVector3 testAxis = tri1ba%tri2ba;
		testAxis.Normalize();
		if(testAxis.GetMagSquared()>0.0001)
		{
			pu0=tri1.m_VertA*testAxis;
			pu1=tri1.m_VertB*testAxis;
			pu2=tri1.m_VertC*testAxis;

			if(GetMin(pu0,pu1,pu2)<0.0f)
			{
				testAxis.invert();
				pu0=tri1.m_VertA*testAxis;
				pu1=tri1.m_VertB*testAxis;
				pu2=tri1.m_VertC*testAxis;
			}
			pv0=tri2.m_VertA*testAxis;
			pv1=tri2.m_VertB*testAxis;
			pv2=tri2.m_VertC*testAxis;

			if(GetMax(pv0,pv1,pv2)<GetMin(pu0,pu1,pu2))
			{
				return false;
			}
			if(GetMin(pv0,pv1,pv2)>GetMax(pu0,pu1,pu2))
			{
				return false;
			}
		}

		testAxis = tri1ba%tri2cb;
		testAxis.Normalize();
		if(testAxis.GetMagSquared()>0.0001)
		{
			pu0=tri1.m_VertA*testAxis;
			pu1=tri1.m_VertB*testAxis;
			pu2=tri1.m_VertC*testAxis;

			if(GetMin(pu0,pu1,pu2)<0.0f)
			{
				testAxis.invert();
				pu0=tri1.m_VertA*testAxis;
				pu1=tri1.m_VertB*testAxis;
				pu2=tri1.m_VertC*testAxis;
			}
			pv0=tri2.m_VertA*testAxis;
			pv1=tri2.m_VertB*testAxis;
			pv2=tri2.m_VertC*testAxis;

			if(GetMax(pv0,pv1,pv2)<GetMin(pu0,pu1,pu2))
			{
				return false;
			}
			if(GetMin(pv0,pv1,pv2)>GetMax(pu0,pu1,pu2))
			{
				return false;
			}
		}

		testAxis = tri1ba%tri2ac;
		testAxis.Normalize();
		if(testAxis.GetMagSquared()>0.0001)
		{
			pu0=tri1.m_VertA*testAxis;
			pu1=tri1.m_VertB*testAxis;
			pu2=tri1.m_VertC*testAxis;

			if(GetMin(pu0,pu1,pu2)<0.0f)
			{
				testAxis.invert();
				pu0=tri1.m_VertA*testAxis;
				pu1=tri1.m_VertB*testAxis;
				pu2=tri1.m_VertC*testAxis;
			}
			pv0=tri2.m_VertA*testAxis;
			pv1=tri2.m_VertB*testAxis;
			pv2=tri2.m_VertC*testAxis;

			if(GetMax(pv0,pv1,pv2)<GetMin(pu0,pu1,pu2))
			{
				return false;
			}
			if(GetMin(pv0,pv1,pv2)>GetMax(pu0,pu1,pu2))
			{
				return false;
			}
		}
		//set2
		testAxis = tri1cb%tri2ba;
		testAxis.Normalize();
		if(testAxis.GetMagSquared()>0.0001)
		{
			pu0=tri1.m_VertA*testAxis;
			pu1=tri1.m_VertB*testAxis;
			pu2=tri1.m_VertC*testAxis;

			if(GetMin(pu0,pu1,pu2)<0.0f)
			{
				testAxis.invert();
				pu0=tri1.m_VertA*testAxis;
				pu1=tri1.m_VertB*testAxis;
				pu2=tri1.m_VertC*testAxis;
			}
			pv0=tri2.m_VertA*testAxis;
			pv1=tri2.m_VertB*testAxis;
			pv2=tri2.m_VertC*testAxis;

			if(GetMax(pv0,pv1,pv2)<GetMin(pu0,pu1,pu2))
			{
				return false;
			}
			if(GetMin(pv0,pv1,pv2)>GetMax(pu0,pu1,pu2))
			{
				return false;
			}
		}

		testAxis = tri1cb%tri2cb;
		testAxis.Normalize();
		if(testAxis.GetMagSquared()>0.0001)
		{
			pu0=tri1.m_VertA*testAxis;
			pu1=tri1.m_VertB*testAxis;
			pu2=tri1.m_VertC*testAxis;

			if(GetMin(pu0,pu1,pu2)<0.0f)
			{
				testAxis.invert();
				pu0=tri1.m_VertA*testAxis;
				pu1=tri1.m_VertB*testAxis;
				pu2=tri1.m_VertC*testAxis;
			}
			pv0=tri2.m_VertA*testAxis;
			pv1=tri2.m_VertB*testAxis;
			pv2=tri2.m_VertC*testAxis;

			if(GetMax(pv0,pv1,pv2)<GetMin(pu0,pu1,pu2))
			{
				return false;
			}
			if(GetMin(pv0,pv1,pv2)>GetMax(pu0,pu1,pu2))
			{
				return false;
			}
		}

		testAxis = tri1cb%tri2ac;
		testAxis.Normalize();
		if(testAxis.GetMagSquared()>0.0001)
		{
			pu0=tri1.m_VertA*testAxis;
			pu1=tri1.m_VertB*testAxis;
			pu2=tri1.m_VertC*testAxis;

			if(GetMin(pu0,pu1,pu2)<0.0f)
			{
				testAxis.invert();
				pu0=tri1.m_VertA*testAxis;
				pu1=tri1.m_VertB*testAxis;
				pu2=tri1.m_VertC*testAxis;
			}
			pv0=tri2.m_VertA*testAxis;
			pv1=tri2.m_VertB*testAxis;
			pv2=tri2.m_VertC*testAxis;

			if(GetMax(pv0,pv1,pv2)<GetMin(pu0,pu1,pu2))
			{
				return false;
			}
			if(GetMin(pv0,pv1,pv2)>GetMax(pu0,pu1,pu2))
			{
				return false;
			}
		}
		// set3
		testAxis = tri1ac%tri2ba;
		testAxis.Normalize();
		if(testAxis.GetMagSquared()>0.0001)
		{
			pu0=tri1.m_VertA*testAxis;
			pu1=tri1.m_VertB*testAxis;
			pu2=tri1.m_VertC*testAxis;

			if(GetMin(pu0,pu1,pu2)<0.0f)
			{
				testAxis.invert();
				pu0=tri1.m_VertA*testAxis;
				pu1=tri1.m_VertB*testAxis;
				pu2=tri1.m_VertC*testAxis;
			}
			pv0=tri2.m_VertA*testAxis;
			pv1=tri2.m_VertB*testAxis;
			pv2=tri2.m_VertC*testAxis;

			if(GetMax(pv0,pv1,pv2)<GetMin(pu0,pu1,pu2))
			{
				return false;
			}
			if(GetMin(pv0,pv1,pv2)>GetMax(pu0,pu1,pu2))
			{
				return false;
			}
		}

		testAxis = tri1ac%tri2cb;
		testAxis.Normalize();
		if(testAxis.GetMagSquared()>0.0001)
		{
			pu0=tri1.m_VertA*testAxis;
			pu1=tri1.m_VertB*testAxis;
			pu2=tri1.m_VertC*testAxis;

			if(GetMin(pu0,pu1,pu2)<0.0f)
			{
				testAxis.invert();
				pu0=tri1.m_VertA*testAxis;
				pu1=tri1.m_VertB*testAxis;
				pu2=tri1.m_VertC*testAxis;
			}
			pv0=tri2.m_VertA*testAxis;
			pv1=tri2.m_VertB*testAxis;
			pv2=tri2.m_VertC*testAxis;

			if(GetMax(pv0,pv1,pv2)<GetMin(pu0,pu1,pu2))
			{
				return false;
			}
			if(GetMin(pv0,pv1,pv2)>GetMax(pu0,pu1,pu2))
			{
				return false;
			}
		}

		testAxis = tri1ac%tri2ac;
		testAxis.Normalize();
		if(testAxis.GetMagSquared()>0.0001)
		{
			pu0=tri1.m_VertA*testAxis;
			pu1=tri1.m_VertB*testAxis;
			pu2=tri1.m_VertC*testAxis;

			if(GetMin(pu0,pu1,pu2)<0.0f)
			{
				testAxis.invert();
				pu0=tri1.m_VertA*testAxis;
				pu1=tri1.m_VertB*testAxis;
				pu2=tri1.m_VertC*testAxis;
			}
			pv0=tri2.m_VertA*testAxis;
			pv1=tri2.m_VertB*testAxis;
			pv2=tri2.m_VertC*testAxis;

			if(GetMax(pv0,pv1,pv2)<GetMin(pu0,pu1,pu2))
			{
				return false;
			}
			if(GetMin(pv0,pv1,pv2)>GetMax(pu0,pu1,pu2))
			{
				return false;
			}
		}
		return true;
	}
	inline bool ObbTriangleCollisionTest(const DragonTriangle& tri,const BaseDragonPhysicsObject& obb,const DragonXVector3& obbAxis0,const DragonXVector3& obbAxis1,const DragonXVector3& obbAxis2,const DragonXVector3& halfsizes)const
	{
		DragonXVector3 vertA = tri.m_VertA;
		DragonXVector3 vertB = tri.m_VertB;
		DragonXVector3 vertC = tri.m_VertC;
		DragonXVector3 norm = GetTriNormal(vertA,vertB,vertC);

		DragonXVector3 f0=vertB-vertA;
		DragonXVector3 f1=vertC-vertB;
		DragonXVector3 f2=vertA-vertC;

		// cross product axis tests
		DragonXVector3 axis = obbAxis0%f0;
		real p0 = vertA*axis;
		real p1 = vertB*axis;
		real p2 = vertC*axis;
		if(GetMax(-1.0f*GetMax(p0,p2),GetMin(p0,p2))>transformToAxis(obb,axis,halfsizes))
		{
			return false;
		}

		axis = obbAxis0%f1;
		p0 = vertA*axis;
		p1 = vertB*axis;
		p2 = vertC*axis;
		if(GetMax(-1.0f*GetMax(p0,p2),GetMin(p0,p2))>transformToAxis(obb,axis,halfsizes))
		{
			return false;
		}

		axis = obbAxis0%f2;
		p0 = vertA*axis;
		p1 = vertB*axis;
		p2 = vertC*axis;
		if(GetMax(-1.0f*GetMax(p1,p2),GetMin(p1,p2))>transformToAxis(obb,axis,halfsizes))
		{
			return false;
		}

		axis = obbAxis1%f0;
		p0 = vertA*axis;
		p1 = vertB*axis;
		p2 = vertC*axis;
		if(GetMax(-1.0f*GetMax(p0,p2),GetMin(p0,p2))>transformToAxis(obb,axis,halfsizes))
		{
			return false;
		}

		axis =obbAxis1%f1;
		p0 = vertA*axis;
		p1 = vertB*axis;
		p2 = vertC*axis;
		if(GetMax(-1.0f*GetMax(p0,p2),GetMin(p0,p2))>transformToAxis(obb,axis,halfsizes))
		{
			return false;
		}

		axis =obbAxis1%f2;
		p0 = vertA*axis;
		p1 = vertB*axis;
		p2 = vertC*axis;
		if(GetMax(-1.0f*GetMax(p0,p1),GetMin(p0,p1))>transformToAxis(obb,axis,halfsizes))
		{
			return false;
		}

		axis = obbAxis2%f0;
		p0 = vertA*axis;
		p1 = vertB*axis;
		p2 = vertC*axis;
		if(GetMax(-1.0f*GetMax(p0,p2),GetMin(p0,p2))>transformToAxis(obb,axis,halfsizes))
		{
			return false;
		}

		axis = obbAxis2%f1;
		p0 = vertA*axis;
		p1 = vertB*axis;
		p2 = vertC*axis;
		if(GetMax(-1.0f*GetMax(p0,p1),GetMin(p0,p1))>transformToAxis(obb,axis,halfsizes))
		{
			return false;
		}

		axis = obbAxis2%f2;
		p0 = vertA*axis;
		p1 = vertB*axis;
		p2 = vertC*axis;
		if(GetMax(-1.0f*GetMax(p1,p2),GetMin(p1,p2))>transformToAxis(obb,axis,halfsizes))
		{
			return false;
		}

		if(GetMax(vertA.x,vertB.x,vertC.x)<-1.0f*halfsizes.x||GetMin(vertA.x,vertB.x,vertC.x)>halfsizes.x)
		{
			return false;
		}

		if(GetMax(vertA.y,vertB.y,vertC.y)<-1.0f*halfsizes.y||GetMin(vertA.y,vertB.y,vertC.y)>halfsizes.y)
		{
			return false;
		}

		if(GetMax(vertA.z,vertB.z,vertC.z)<-1.0f*halfsizes.z||GetMin(vertA.z,vertB.z,vertC.z)>halfsizes.z)
		{
			return false;
		}

		if((real)abs(norm*obb.getPointInLocalSpace(obb.m_Position))-(norm*vertA)<=transformToAxis(obb,norm,halfsizes))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	inline bool SphereTriangleCollisionTest(const DragonTriangle& tri,const BaseDragonPhysicsObject& sphere)const
	{
		DragonXVector3 p = ClosestPtPointTriangle(sphere.m_Position,tri.m_VertA,tri.m_VertB,tri.m_VertC);
		p-=sphere.m_Position;
		if(p*p<=sphere.GetRadius()*sphere.GetRadius())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	inline bool overLapOnAxis(const BaseDragonPhysicsObject& obj1,const BaseDragonPhysicsObject& obj2,const DragonXVector3& axis, const DragonXVector3& toCentre,const DragonXVector3& obj1Hs,const DragonXVector3& obj2Hs)const
	{
		// project the half size of one onto axis
		real oneProject = transformToAxis(obj1,axis,obj1Hs);
		real twoProject = transformToAxis(obj2,axis,obj2Hs);
		// project this onto the axis
		real dist = (real)abs(toCentre*axis);
		// check for overlap
		return (dist <= oneProject + twoProject);
	}
	inline bool SpherePlaneCollisionTest(const BaseDragonPhysicsObject& obj1, const BaseDragonPhysicsObject& obj2)const
	{
		if(obj2.mI_PrimitiveType==PT_Plane)
		{
			DragonPhysicsPlane* colObj;
			colObj = static_cast<DragonPhysicsPlane*>(obj2.m_CollisionObject);
			if((colObj->mNormalDirection*obj1.m_Position-obj1.GetRadius())<=colObj->mDistFromOrigin)
			{
				real dist=colObj->mNormalDirection*obj1.m_Position-colObj->mDistFromOrigin;
				if(dist*dist>obj1.GetRadius()*obj1.GetRadius())
				{
					return false;
				}
				else
				{
					return true;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			DragonPhysicsPlane* colObj;
			colObj = static_cast<DragonPhysicsPlane*>(obj1.m_CollisionObject);
			if((colObj->mNormalDirection*obj2.m_Position-obj2.GetRadius())<=colObj->mDistFromOrigin)
			{
				real dist=colObj->mNormalDirection*obj2.m_Position-colObj->mDistFromOrigin;
				if(dist*dist>obj2.GetRadius()*obj2.GetRadius())
				{
					return false;
				}
				else
				{
					return true;
				}
			}
			else
			{
				return false;
			}
		}
	}
	inline bool SphereOBBCollisionTest(const BaseDragonPhysicsObject& obj1,const BaseDragonPhysicsObject& obj2)const
	{
		if(obj1.mI_PrimitiveType==PT_Sphere)//if object 1 is the sphere
		{
			DragonXVector3 center = obj1.m_Position;
			DragonXVector3 relCenter = obj2.TransformMatrix.TransformInverse(center);
			real radius = obj1.GetRadius();
			DragonPhysicsCollisionOBB* colObj;
			colObj = static_cast<DragonPhysicsCollisionOBB*>(obj2.m_CollisionObject);
			if(abs(relCenter.x)-radius>colObj->halfExtents.x||abs(relCenter.y)-radius>colObj->halfExtents.y||abs(relCenter.z)-radius>colObj->halfExtents.z)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
		else //obj2 is the sphere and obj1 is the obb
		{
			DragonXVector3 center = obj2.m_Position;
			DragonXVector3 relCenter = obj1.TransformMatrix.TransformInverse(center);
			real radius = obj2.GetRadius();
			DragonPhysicsCollisionOBB* colObj;
			colObj = static_cast<DragonPhysicsCollisionOBB*>(obj1.m_CollisionObject);
			if(abs(relCenter.x)-radius>colObj->halfExtents.x||abs(relCenter.y)-radius>colObj->halfExtents.y||abs(relCenter.z)-radius>colObj->halfExtents.z)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
	}
	inline bool ObbPlaneCollisionTest(const BaseDragonPhysicsObject& obj1,const BaseDragonPhysicsObject& obj2)const
	{
		if(obj1.mI_PrimitiveType==PT_Plane)
		{
			DragonPhysicsPlane* colObj1;
			colObj1=static_cast<DragonPhysicsPlane*>(obj1.m_CollisionObject);
			DragonPhysicsCollisionOBB* colObj2;
			colObj2=static_cast<DragonPhysicsCollisionOBB*>(obj2.m_CollisionObject);
			real projectedRadius=transformToAxis(obj2,colObj1->mNormalDirection,colObj2->halfExtents);
			real boxDistance=(colObj1->mNormalDirection*obj2.m_Position)-projectedRadius;
			if(boxDistance>=colObj1->mDistFromOrigin)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
		else//else obj2 is the obb and obj1 is the plane
		{
			DragonPhysicsPlane* colObj1;
			colObj1=static_cast<DragonPhysicsPlane*>(obj2.m_CollisionObject);
			DragonPhysicsCollisionOBB* colObj2;
			colObj2=static_cast<DragonPhysicsCollisionOBB*>(obj1.m_CollisionObject);
			real projectedRadius=transformToAxis(obj1,colObj1->mNormalDirection,colObj2->halfExtents);
			real boxDistance=(colObj1->mNormalDirection*obj1.m_Position)-projectedRadius;
			if(boxDistance>=colObj1->mDistFromOrigin)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
	}
	inline bool ObbObbCollisionTest(const BaseDragonPhysicsObject& obj1,const BaseDragonPhysicsObject& obj2)const
	{
		DragonPhysicsCollisionOBB* colObj1;
		colObj1=static_cast<DragonPhysicsCollisionOBB*>(obj1.m_CollisionObject);
		DragonPhysicsCollisionOBB* colObj2;
		colObj2=static_cast<DragonPhysicsCollisionOBB*>(obj2.m_CollisionObject);
		DragonXVector3 toCentre = obj2.m_Position-obj1.m_Position;
		return(
			overLapOnAxis(obj1,obj2,obj1.TransformMatrix.getAxisVector(0),toCentre,colObj1->halfExtents,colObj2->halfExtents)&&
			overLapOnAxis(obj1,obj2,obj1.TransformMatrix.getAxisVector(1),toCentre,colObj1->halfExtents,colObj2->halfExtents)&&
			overLapOnAxis(obj1,obj2,obj1.TransformMatrix.getAxisVector(2),toCentre,colObj1->halfExtents,colObj2->halfExtents)&&
			overLapOnAxis(obj1,obj2,obj2.TransformMatrix.getAxisVector(0),toCentre,colObj1->halfExtents,colObj2->halfExtents)&&
			overLapOnAxis(obj1,obj2,obj2.TransformMatrix.getAxisVector(1),toCentre,colObj1->halfExtents,colObj2->halfExtents)&&
			overLapOnAxis(obj1,obj2,obj2.TransformMatrix.getAxisVector(2),toCentre,colObj1->halfExtents,colObj2->halfExtents)&&
			overLapOnAxis(obj1,obj2,obj1.TransformMatrix.getAxisVector(0)%obj2.TransformMatrix.getAxisVector(0),toCentre,colObj1->halfExtents,colObj2->halfExtents)&&
			overLapOnAxis(obj1,obj2,obj1.TransformMatrix.getAxisVector(0)%obj2.TransformMatrix.getAxisVector(1),toCentre,colObj1->halfExtents,colObj2->halfExtents)&&
			overLapOnAxis(obj1,obj2,obj1.TransformMatrix.getAxisVector(0)%obj2.TransformMatrix.getAxisVector(2),toCentre,colObj1->halfExtents,colObj2->halfExtents)&&
			overLapOnAxis(obj1,obj2,obj1.TransformMatrix.getAxisVector(1)%obj2.TransformMatrix.getAxisVector(0),toCentre,colObj1->halfExtents,colObj2->halfExtents)&&
			overLapOnAxis(obj1,obj2,obj1.TransformMatrix.getAxisVector(1)%obj2.TransformMatrix.getAxisVector(1),toCentre,colObj1->halfExtents,colObj2->halfExtents)&&
			overLapOnAxis(obj1,obj2,obj1.TransformMatrix.getAxisVector(1)%obj2.TransformMatrix.getAxisVector(2),toCentre,colObj1->halfExtents,colObj2->halfExtents)&&
			overLapOnAxis(obj1,obj2,obj1.TransformMatrix.getAxisVector(2)%obj2.TransformMatrix.getAxisVector(0),toCentre,colObj1->halfExtents,colObj2->halfExtents)&&
			overLapOnAxis(obj1,obj2,obj1.TransformMatrix.getAxisVector(2)%obj2.TransformMatrix.getAxisVector(1),toCentre,colObj1->halfExtents,colObj2->halfExtents)&&
			overLapOnAxis(obj1,obj2,obj1.TransformMatrix.getAxisVector(2)%obj2.TransformMatrix.getAxisVector(2),toCentre,colObj1->halfExtents,colObj2->halfExtents)
			);
	}
	inline bool SphereMeshCollisionTest(const BaseDragonPhysicsObject& obj1,const BaseDragonPhysicsObject& obj2)const
	{
		if(obj1.mI_PrimitiveType==PT_Mesh)
		{
			DragonMeshCollisionObject* colObj1;
			colObj1=static_cast<DragonMeshCollisionObject*>(obj1.m_CollisionObject);
			for (auto sector : colObj1->m_DragonSectors)
			{
				float rad = obj2.GetRadius()+sector->m_Radius;
				if (((obj1.TransformMatrix.transform(sector->m_Position)-obj2.m_Position).GetMagSquared())<rad*rad)
				{
					for (auto tri : sector->m_DragonTris)
					{
						DragonTriangle temp(obj1.TransformMatrix.transform(tri->m_VertA),obj1.TransformMatrix.transform(tri->m_VertB),obj1.TransformMatrix.transform(tri->m_VertC));
						if (SphereTriangleCollisionTest(temp,obj2))
						{
							return true;
						}
					}
				}
			}
			return false;
		}
		else
		{
			DragonMeshCollisionObject* colObj1;
			colObj1=static_cast<DragonMeshCollisionObject*>(obj2.m_CollisionObject);
			for (auto sector : colObj1->m_DragonSectors)
			{
				float rad = obj1.GetRadius()+sector->m_Radius;
				if (((obj2.TransformMatrix.transform(sector->m_Position)-obj1.m_Position).GetMagSquared())<rad*rad)
				{
					for (auto tri : sector->m_DragonTris)
					{
						DragonTriangle temp(obj2.TransformMatrix.transform(tri->m_VertA),obj2.TransformMatrix.transform(tri->m_VertB),obj2.TransformMatrix.transform(tri->m_VertC));
						if (SphereTriangleCollisionTest(temp,obj1))
						{
							return true;
						}
					}
				}
			}
			return false;
		}
	}
	inline bool ObbMeshCollisionTest(const BaseDragonPhysicsObject& obj1,const BaseDragonPhysicsObject& obj2)const
	{
		if(obj1.mI_PrimitiveType==PT_Mesh)
		{
			DragonMeshCollisionObject* colObj1;
			colObj1=static_cast<DragonMeshCollisionObject*>(obj1.m_CollisionObject);
			DragonPhysicsCollisionOBB* colObj2;
			colObj2=static_cast<DragonPhysicsCollisionOBB*>(obj2.m_CollisionObject);
			for (auto sector : colObj1->m_DragonSectors)
			{
				float rad = obj2.GetRadius()+sector->m_Radius;
				if (((obj1.TransformMatrix.transform(sector->m_Position)-obj2.m_Position).GetMagSquared())<rad*rad)
				{
					DragonXVector3 obbAxis0 = obj2.TransformMatrix.getAxisVector(0);
					DragonXVector3 obbAxis1 = obj2.TransformMatrix.getAxisVector(1);
					DragonXVector3 obbAxis2 = obj2.TransformMatrix.getAxisVector(2);
					for (auto tri : sector->m_DragonTris)
					{
						DragonTriangle temp(obj1.TransformMatrix.transform(tri->m_VertA),obj1.TransformMatrix.transform(tri->m_VertB),obj1.TransformMatrix.transform(tri->m_VertC));
						if (SphereTriangleCollisionTest(temp,obj2))
						{
							DragonTriangle templ(obj2.getPointInLocalSpace(temp.m_VertA),obj2.getPointInLocalSpace(temp.m_VertB),obj2.getPointInLocalSpace(temp.m_VertC));
							if(ObbTriangleCollisionTest(templ,obj2,obbAxis0,obbAxis1,obbAxis2,colObj2->halfExtents))
							{
								return true;
							}
						}
					}
				}
			}
			return false;
		}
		else
		{
			DragonMeshCollisionObject* colObj1;
			colObj1=static_cast<DragonMeshCollisionObject*>(obj2.m_CollisionObject);
			DragonPhysicsCollisionOBB* colObj2;
			colObj2=static_cast<DragonPhysicsCollisionOBB*>(obj1.m_CollisionObject);
			for (auto sector : colObj1->m_DragonSectors)
			{
				float rad = obj1.GetRadius()+sector->m_Radius;
				if (((obj2.TransformMatrix.transform(sector->m_Position)-obj1.m_Position).GetMagSquared())<rad*rad)
				{
					DragonXVector3 obbAxis0 = obj1.TransformMatrix.getAxisVector(0);
					DragonXVector3 obbAxis1 = obj1.TransformMatrix.getAxisVector(1);
					DragonXVector3 obbAxis2 = obj1.TransformMatrix.getAxisVector(2);
					for (auto tri : sector->m_DragonTris)
					{
						DragonTriangle temp(obj2.TransformMatrix.transform(tri->m_VertA),obj2.TransformMatrix.transform(tri->m_VertB),obj2.TransformMatrix.transform(tri->m_VertC));
						if (SphereTriangleCollisionTest(temp,obj2))
						{
							DragonTriangle templ(obj1.getPointInLocalSpace(temp.m_VertA),obj1.getPointInLocalSpace(temp.m_VertB),obj1.getPointInLocalSpace(temp.m_VertC));
							if(ObbTriangleCollisionTest(templ,obj1,obbAxis0,obbAxis1,obbAxis2,colObj2->halfExtents))
							{
								return true;
							}
						}
					}
				}
			}
			return false;
		}
	}
	inline bool MeshMeshCollisionTest(const BaseDragonPhysicsObject& obj1,const BaseDragonPhysicsObject& obj2)const
	{
		DragonMeshCollisionObject* colObj1;
		colObj1=static_cast<DragonMeshCollisionObject*>(obj1.m_CollisionObject);
		DragonMeshCollisionObject* colObj2;
		colObj2=static_cast<DragonMeshCollisionObject*>(obj2.m_CollisionObject);
		for (auto sector : colObj1->m_DragonSectors)
		{
			float rad = sector->m_Radius;
			DragonXVector3 secWpos = obj1.TransformMatrix.transform(sector->m_Position);
			for(auto sektor : colObj2->m_DragonSectors)
			{
				float rads = rad + sektor->m_Radius;
				if((secWpos-obj2.getPointInWorldSpace(sektor->m_Position)).GetMagSquared()<rads*rads)
				{
					for (auto tris1 : sector->m_DragonTris)
					{
						DragonTriangle temp(obj1.getPointInWorldSpace(tris1->m_VertA),obj1.getPointInWorldSpace(tris1->m_VertB),obj1.getPointInWorldSpace(tris1->m_VertC));
						DragonXVector3 tri1Norm = GetTriNormal(temp.m_VertA,temp.m_VertB,temp.m_VertC);
						for(auto tris2 : sektor->m_DragonTris)
						{
							DragonTriangle temp2(obj2.getPointInWorldSpace(tris2->m_VertA),obj2.getPointInWorldSpace(tris2->m_VertB),obj2.getPointInWorldSpace(tris2->m_VertC));
							DragonXVector3 tri2Norm = GetTriNormal(temp2.m_VertA,temp2.m_VertB,temp2.m_VertC);
							if (TriangleTriangleCollisionTest(temp,tri1Norm,temp2,tri2Norm))
							{
								return true;
							}
						}
					}
				}
			}
		}
		return false;
	}
	inline bool MeshPlaneCollisionTest(const BaseDragonPhysicsObject& obj1,const BaseDragonPhysicsObject& obj2)const
	{
		if(obj1.mI_PrimitiveType==PT_Mesh)
		{
			DragonMeshCollisionObject* colObj1;
			colObj1=static_cast<DragonMeshCollisionObject*>(obj1.m_CollisionObject);
			DragonPhysicsPlane* colObj2;
			colObj2=static_cast<DragonPhysicsPlane*>(obj2.m_CollisionObject);
			for (auto sector : colObj1->m_DragonSectors)
			{
				float rad = sector->m_Radius;
				DragonXVector3 secWpos = obj1.TransformMatrix.transform(sector->m_Position);
				if (colObj2->mNormalDirection*secWpos-rad<=colObj2->mDistFromOrigin)
				{
					for (auto tri : sector->m_DragonTris)
					{
						DragonTriangle temp(obj1.TransformMatrix.transform(tri->m_VertA),obj1.TransformMatrix.transform(tri->m_VertB),obj1.TransformMatrix.transform(tri->m_VertC));
						if (TrianglePlaneCollisionTest(temp,colObj2->mNormalDirection,colObj2->mDistFromOrigin))
						{
							return true;
						}
					}
				}
			}
			return false;
		}
		else
		{
			DragonMeshCollisionObject* colObj1;
			colObj1=static_cast<DragonMeshCollisionObject*>(obj2.m_CollisionObject);
			DragonPhysicsPlane* colObj2;
			colObj2=static_cast<DragonPhysicsPlane*>(obj1.m_CollisionObject);
			for (auto sector : colObj1->m_DragonSectors)
			{
				float rad = sector->m_Radius;
				DragonXVector3 secWpos = obj2.TransformMatrix.transform(sector->m_Position);
				if (colObj2->mNormalDirection*secWpos-rad<=colObj2->mDistFromOrigin)
				{
					for (auto tri : sector->m_DragonTris)
					{
						DragonTriangle temp(obj2.TransformMatrix.transform(tri->m_VertA),obj2.TransformMatrix.transform(tri->m_VertB),obj2.TransformMatrix.transform(tri->m_VertC));
						if (TrianglePlaneCollisionTest(temp,colObj2->mNormalDirection,colObj2->mDistFromOrigin))
						{
							return true;
						}
					}
				}
			}
			return false;
		}
	}
public:
	DragonCollisionDetector();
	~DragonCollisionDetector();
	void GenContactPairs(DragonPairContainer& conPrCont,const std::vector<BaseDragonPhysicsObject*>& entities);
};
