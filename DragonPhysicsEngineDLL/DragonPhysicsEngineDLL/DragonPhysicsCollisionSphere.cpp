#include "DragonPhysicsCollisionSphere.h"
#include "DragonEnums.h"

DragonPhysicsCollisionSphere::DragonPhysicsCollisionSphere()
{
	m_PrimTypeID=PT_Sphere;
	mRadius=1.0f;
}
DragonPhysicsCollisionSphere::~DragonPhysicsCollisionSphere()
{
}