#pragma once
#include "BaseConstraintObject.h"
#include <vector>

class BaseDragonPhysicsObject;

class CustomConstraintObject : public BaseConstraintObject
{
private:

public:
	BaseDragonPhysicsObject* mObj1;
	BaseDragonPhysicsObject* mObj2;
	std::vector<BaseConstraintObject*> mCustomConstraints;
public:
	CustomConstraintObject();
	~CustomConstraintObject();
};