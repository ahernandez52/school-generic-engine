#pragma once
#include <functional>
class BaseDragonPhysicsObject;

class DragonForceGenerator
{
private:

public:
	int m_ForceID;
	std::function<bool (BaseDragonPhysicsObject* obj,float dt)> m_ForceEffect;
public:
	DragonForceGenerator();
	~DragonForceGenerator();
};