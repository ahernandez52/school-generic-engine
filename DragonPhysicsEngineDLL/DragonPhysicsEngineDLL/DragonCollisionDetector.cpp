#include "DragonCollisionDetector.h"

DragonCollisionDetector::DragonCollisionDetector()
{
}
DragonCollisionDetector::~DragonCollisionDetector()
{
}
void DragonCollisionDetector::GenContactPairs(DragonPairContainer& conPrCont,const std::vector<BaseDragonPhysicsObject*>& entities)
{
	//std::vector<DragonContactPair> retConPairs;
	for(int i = 0;(unsigned)i<entities.size()-1;i++)
	{
		for(int j = i+1;(unsigned)j<entities.size();j++)
		{
			if(entities[i]==NULL||entities[j]==NULL)
			{
				return;
			}
			else
			{
				switch(GetCollisionType(*entities[i],*entities[j]))
				{
				case CT_SphereSphere:
					{
						if((entities[i]->m_Position-entities[j]->m_Position).GetMagSquared()<((entities[i]->GetRadius()+entities[j]->GetRadius())*(entities[i]->GetRadius()+entities[j]->GetRadius())))
						{
							DragonContactPair pair= DragonContactPair();
							pair.mContactType=CT_SphereSphere;
							pair.Obj1=entities[i];
							pair.Obj2=entities[j];
							pair.mVailidData=true;
							conPrCont.Add(std::move(pair));
							break;
						}
						break;
					}
				case CT_SpherePlane:
					{
						if(SpherePlaneCollisionTest(*entities[i],*entities[j]))
						{
							DragonContactPair pair= DragonContactPair();
							pair.mContactType=CT_SpherePlane;
							pair.Obj1=entities[i];
							pair.Obj2=entities[j];
							pair.mVailidData=true;
							conPrCont.Add(std::move(pair));
							break;
						}
						break;
					}
				case CT_SphereOBB:
					{
						if((entities[i]->m_Position-entities[j]->m_Position).GetMagSquared()<((entities[i]->GetRadius()+entities[j]->GetRadius())*(entities[i]->GetRadius()+entities[j]->GetRadius())))
						{
							if(SphereOBBCollisionTest(*entities[i],*entities[j]))
							{
								DragonContactPair pair= DragonContactPair();
								pair.mContactType=CT_SphereOBB;
								pair.Obj1=entities[i];
								pair.Obj2=entities[j];
								pair.mVailidData=true;
								conPrCont.Add(std::move(pair));
								break;
							}
							break;
						}
						break;
					}
				case CT_ObbPlane:
					{
						if(SpherePlaneCollisionTest(*entities[i],*entities[j]))
						{
							if(ObbPlaneCollisionTest(*entities[i],*entities[j]))
							{
								DragonContactPair pair= DragonContactPair();
								pair.mContactType=CT_ObbPlane;
								pair.Obj1=entities[i];
								pair.Obj2=entities[j];
								pair.mVailidData=true;
								conPrCont.Add(std::move(pair));
								break;
							}
							break;
						}
						break;
					}
				case CT_ObbObb:
					{
						if((entities[i]->m_Position-entities[j]->m_Position).GetMagSquared()<((entities[i]->GetRadius()+entities[j]->GetRadius())*(entities[i]->GetRadius()+entities[j]->GetRadius())))
						{
							if(ObbObbCollisionTest(*entities[i],*entities[j]))
							{
								DragonContactPair pair= DragonContactPair();
								pair.mContactType=CT_ObbObb;
								pair.Obj1=entities[i];
								pair.Obj2=entities[j];
								pair.mVailidData=true;
								conPrCont.Add(std::move(pair));
								break;
							}
						}
						break;
					}
				case CT_MeshSphere:
					{
						if((entities[i]->m_Position-entities[j]->m_Position).GetMagSquared()<((entities[i]->GetRadius()+entities[j]->GetRadius())*(entities[i]->GetRadius()+entities[j]->GetRadius())))
						{
							if(SphereOBBCollisionTest(*entities[i],*entities[j]))
							{
								if(SphereMeshCollisionTest(*entities[i],*entities[j]))
								{
									DragonContactPair pair= DragonContactPair();
									pair.mContactType=CT_MeshSphere;
									pair.Obj1=entities[i];
									pair.Obj2=entities[j];
									pair.mVailidData=true;
									conPrCont.Add(std::move(pair));
									break;
								}
							}
						}
						break;
					}
				case CT_MeshOBB:
					{
						if((entities[i]->m_Position-entities[j]->m_Position).GetMagSquared()<((entities[i]->GetRadius()+entities[j]->GetRadius())*(entities[i]->GetRadius()+entities[j]->GetRadius())))
						{
							if(ObbObbCollisionTest(*entities[i],*entities[j]))
							{
								if (ObbMeshCollisionTest(*entities[i],*entities[j]))
								{
									DragonContactPair pair= DragonContactPair();
									pair.mContactType=CT_MeshOBB;
									pair.Obj1=entities[i];
									pair.Obj2=entities[j];
									pair.mVailidData=true;
									conPrCont.Add(std::move(pair));
									break;
								}
							}
						}
						break;
					}
				case CT_MeshMesh:
					{
						if((entities[i]->m_Position-entities[j]->m_Position).GetMagSquared()<((entities[i]->GetRadius()+entities[j]->GetRadius())*(entities[i]->GetRadius()+entities[j]->GetRadius())))
						{
							if(ObbObbCollisionTest(*entities[i],*entities[j]))
							{
								if(MeshMeshCollisionTest(*entities[i],*entities[j]))
								{
									DragonContactPair pair= DragonContactPair();
									pair.mContactType=CT_MeshMesh;
									pair.Obj1=entities[i];
									pair.Obj2=entities[j];
									pair.mVailidData=true;
									conPrCont.Add(std::move(pair));
									break;
								}
							}
						}
						break;
					}
				case CT_MeshPlane:
					{
						if(SpherePlaneCollisionTest(*entities[i],*entities[j]))
						{
							if(ObbPlaneCollisionTest(*entities[i],*entities[j]))
							{
								if(MeshPlaneCollisionTest(*entities[i],*entities[j]))
								{
									DragonContactPair pair= DragonContactPair();
									pair.mContactType=CT_MeshPlane;
									pair.Obj1=entities[i];
									pair.Obj2=entities[j];
									pair.mVailidData=true;
									conPrCont.Add(std::move(pair));
									break;
								}
								break;
							}
							break;
						}
						break;
					}			
				default:
					{
						break;
					}
				}
			}
		}
	}
	//return retConPairs;
}