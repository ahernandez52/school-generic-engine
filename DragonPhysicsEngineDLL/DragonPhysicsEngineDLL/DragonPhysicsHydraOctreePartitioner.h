#pragma once
#include <windows.h>
#include <vector>
#include "DragonXMath.h"
#include "DragonPhysicsHydraContainers.h"

class BaseDragonPhysicsObject;
class HydraObjectUpdater;
class DragonCollisionEngine;

class DragonPhysicsHydraOctreePartitioner
{
protected:
	//static int g_iCurrentNodeLevel;
	real m_rDiameter;
public:
	bool m_bIsNode;
	DragonXVector3 m_vCenter;
	DragonPhysicsHydraOctreePartitioner* m_pHydraNodes[8];
	//std::vector<BaseDragonPhysicsObject*> m_vEntities;
	DragonPairContainer mConPairCont;
	DragonContactContainer mConData;
	DragonPhysicsObjectContainer vTopFrontLeft;
	DragonPhysicsObjectContainer vTopFrontRight;
	DragonPhysicsObjectContainer vTopBackLeft;
	DragonPhysicsObjectContainer vTopBackRight;

	DragonPhysicsObjectContainer vBottomFrontLeft;
	DragonPhysicsObjectContainer vBottomFrontRight;
	DragonPhysicsObjectContainer vBottomBackLeft;
	DragonPhysicsObjectContainer vBottomBackRight;
public:
	DragonPhysicsHydraOctreePartitioner();
	~DragonPhysicsHydraOctreePartitioner();
	void ReleaseHydra();
	//void ResetHydra();
	void Create(std::vector<BaseDragonPhysicsObject*>& entities,HydraObjectUpdater& updater,DragonCollisionEngine& collisionEng,const float dt);	
	void SetData(const std::vector<BaseDragonPhysicsObject*>& entities);	
	void CreateNode(std::vector<BaseDragonPhysicsObject*>& entities,DragonXVector3 center, real diameter,HydraObjectUpdater& updater,DragonCollisionEngine& collisionEng,const float dt);	
	void CreateNodeEnd(std::vector<BaseDragonPhysicsObject*>& entities,DragonXVector3 center,real diameter,int whichNode,HydraObjectUpdater& updater,DragonCollisionEngine& collisionEng,const float dt);
	DragonXVector3 GetNodeCenter(DragonXVector3 currentCenter, real diameter,int whichNode);
	//void SetNode(std::vector<BaseDragonPhysicsObject*>& entities,HydraObjectUpdater& updater,DragonCollisionEngine& collisionEng,const float dt);	
};