#pragma once
#include "DragonXMath.h"
#include <functional>
#include <vector>
//#include "Actor.h"
#include "DragonForceGenerator.h"
class BaseDragonCollisionObject;

class BaseDragonPhysicsObject
{
private:

public:
	//Actor* mActor;
	unsigned m_ID;
	int mI_PrimitiveType;
	DragonXVector3 m_Position;
	DragonXVector3 m_LinearVelocity;
	DragonXVector3 m_RotationalVelocity;
	DragonXVector3 m_LastFrameAccel;
	DragonXVector3 m_LinearForceAccum;
	DragonXVector3 m_AngularForceAccum;
	DragonXVector3 m_LinearAccel;
	DragonXMatrix inverseInertiaTensor;
	DragonXMatrix inverseInertiaTensorWorld;
	DragonXMatrix TransformMatrix;
	DragonXQuaternion m_QuatRot;
	BaseDragonCollisionObject* m_CollisionObject;
	real m_InverseMass;
	real m_LinearDampening;
	real m_AngularDampening;
	real m_motion;
	bool m_CanSleep;
	bool m_isAwake;
	bool m_isCollisionResolutionOn;
	bool m_PhysicsEnabled;//if true has non infinite mass if true has infinite mass
	std::vector<DragonForceGenerator> m_VecOfForces;
public:
	BaseDragonPhysicsObject();
	virtual~BaseDragonPhysicsObject();
public:
	inline void _calculateTransformMatrix(DragonXMatrix &TransformMatrix,const DragonXVector3 &position,const DragonXQuaternion &m_QuatRot)
	{
		TransformMatrix._11 = 1-2*m_QuatRot.y*m_QuatRot.y-2*m_QuatRot.z*m_QuatRot.z;
		TransformMatrix._12 = 2*m_QuatRot.x*m_QuatRot.y -2*m_QuatRot.w*m_QuatRot.z;
		TransformMatrix._13 = 2*m_QuatRot.x*m_QuatRot.z +2*m_QuatRot.w*m_QuatRot.y;
		TransformMatrix._14 = position.x;

		TransformMatrix._21 = 2*m_QuatRot.x*m_QuatRot.y +2*m_QuatRot.w*m_QuatRot.z;
		TransformMatrix._22 = 1-2*m_QuatRot.x*m_QuatRot.x-2*m_QuatRot.z*m_QuatRot.z;
		TransformMatrix._23 = 2*m_QuatRot.y*m_QuatRot.z -2*m_QuatRot.w*m_QuatRot.x;
		TransformMatrix._24 = position.y;

		TransformMatrix._31 = 2*m_QuatRot.x*m_QuatRot.z -2*m_QuatRot.w*m_QuatRot.y;
		TransformMatrix._32 = 2*m_QuatRot.y*m_QuatRot.z +2*m_QuatRot.w*m_QuatRot.x;
		TransformMatrix._33 = 1-2*m_QuatRot.x*m_QuatRot.x-2*m_QuatRot.y*m_QuatRot.y;
		TransformMatrix._34 = position.z;
	}
	inline void UpdateMatricies()
	{
		m_QuatRot.normalize();
		_calculateTransformMatrix(TransformMatrix, m_Position, m_QuatRot);
		DragonXMatrix m;
		m.setComponents(TransformMatrix.getAxisVector(0),TransformMatrix.getAxisVector(1),TransformMatrix.getAxisVector(2));
		inverseInertiaTensorWorld= m * inverseInertiaTensor * m.getInverse();
	}
	inline void setMass(const real mass)
	{
		if(mass<=0)
		{
			m_InverseMass=0;
		}
		else
		{
			m_InverseMass=1/mass;
			ReCalcInverseTensor(mass);
		}
	}
	inline real getMass()const
	{
		return 1.0f/m_InverseMass;
	}
	inline void setInverseMass(const real InvMass)
	{
		m_InverseMass=InvMass;
		if(hasFiniteMass())
		{
			ReCalcInverseTensor(1.0f/m_InverseMass);
		}
		else
		{
			m_PhysicsEnabled=false;
		}
	}
	inline bool hasFiniteMass()
	{
		if(m_InverseMass<=0.000000f)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	inline void getInertiaTensor(DragonXMatrix *inertiaTensor)const
	{
		*inertiaTensor=inverseInertiaTensor.getInverse();
	}
	inline DragonXMatrix getInertiaTensor()const
	{
		DragonXMatrix it;
		getInertiaTensor(&it);
		return it;
	}
	inline void getInertiaTensorWorld(DragonXMatrix *m)const
	{
		*m=inverseInertiaTensorWorld.getInverse();
	}
	inline DragonXMatrix getInertiaTensorWorld()const
	{
		DragonXMatrix it;
		getInertiaTensorWorld(&it);
		return it;
	}
	inline void getInverseInertiaTensor(DragonXMatrix *m)const
	{
		*m = inverseInertiaTensor;
	}
	inline void getInverseInertiaTensorWorld(DragonXMatrix *m)const
	{
		*m = inverseInertiaTensorWorld;
	}
	inline DragonXVector3 getPointInLocalSpace(const DragonXVector3 &point) const
	{
		return TransformMatrix.TransformInverse(point);
	}
	inline DragonXVector3 getPointInWorldSpace(const DragonXVector3 &point) const
	{
		return TransformMatrix.transform(point);
	}
	inline DragonXVector3 getDirectionInLocalSpace(const DragonXVector3 &direction) const
	{
		return TransformMatrix.TransformInverseDirection(direction);
	}
	inline DragonXVector3 getDirectionInWorldSpace(const DragonXVector3 &direction) const
	{
		return TransformMatrix.TransformDirection(direction);
	}
	inline void addVelocity(const DragonXVector3 &deltaVelocity)
	{
		m_LinearVelocity += deltaVelocity;
	}
	inline void addRotation(const DragonXVector3 &deltaRotation)
	{
		m_RotationalVelocity += deltaRotation;
	}
	inline void setAwake(const bool awake=true)
	{
		if(awake)
		{
			m_isAwake= true;
			m_motion = sleepEpsilon*2.0f;
		}
		else
		{
			m_isAwake = false;
			m_LinearVelocity.clear();
			m_RotationalVelocity.clear();
		}
	}
	inline void setCanSleep(const bool canSleep=true)
	{
		m_CanSleep = canSleep;
		if (!canSleep && !m_isAwake)
		{
			setAwake();
		}
	}
	inline void clearAccumulators()
	{
		m_LinearForceAccum.clear();
		m_AngularForceAccum.clear();
	}
	inline void addForce(const DragonXVector3 &force)
	{
		m_LinearForceAccum += force;
		setAwake();
	}
	inline void addForceAtPoint(const DragonXVector3 &force, const DragonXVector3 &point)
	{
		DragonXVector3 pt = point;
		pt -= m_Position;
		m_LinearForceAccum += force;
		m_AngularForceAccum += pt % force;
		setAwake();
	}
	inline void addForceAtBodyPoint(const DragonXVector3 &force, const DragonXVector3 &point)
	{
		DragonXVector3 pt = getPointInWorldSpace(point);
		addForceAtPoint(force, pt);
	}
	inline void addTorque(const DragonXVector3 &torque)
	{
		m_AngularForceAccum += torque;
		setAwake();
	}
	void ReCalcInverseTensor(real newMass);
	inline void UpdatePhysX()
	{
		//if(mActor!=nullptr)
		//{
			//m_Position=mActor->GetPosition();
			//m_QuatRot=mActor->GetOrientation();
			//m_QuatRot.normalize();
		//}
	}
	inline void UpdatePhysXAlt()
	{
		UpdatePhysX();
		m_QuatRot.normalize();
		_calculateTransformMatrix(TransformMatrix, m_Position, m_QuatRot);
	}
	void UpdateActor();
	real GetRadius() const;
};