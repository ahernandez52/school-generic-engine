#pragma once
#include <vector>
#include "DragonContact.h"
#include "DragonContactPair.h"

class BaseDragonPhysicsObject;

class DragonPairContainer
{
private:

public:
	std::vector<DragonContactPair> mVecConPairs;
public:
	DragonPairContainer();
	~DragonPairContainer();
	void Add(DragonContactPair&& nPair);
	void ResetContainer();
};

class DragonPhysicsObjectContainer
{
private:

public:
	std::vector<BaseDragonPhysicsObject*> mVecPhysXobjs;
public:
	DragonPhysicsObjectContainer();
	~DragonPhysicsObjectContainer();
	void Add(BaseDragonPhysicsObject* obj);
	void ResetContainer();
};

class DragonContactContainer
{
private:

public:
	std::vector<DragonContact> mVecConData;
public:
	DragonContactContainer();
	~DragonContactContainer();
	void Add(DragonContact&& nContact);
	void ResetContainer();
};