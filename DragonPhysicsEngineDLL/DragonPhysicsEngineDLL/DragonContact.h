#pragma once
#include "DragonXMath.h"
class BaseDragonPhysicsObject;

class DragonContact
{
private:

public:
	bool mValidData;
	BaseDragonPhysicsObject* m_Entities[2];
	DragonXVector3 mRelitiveContactPosition[2];
	DragonXVector3 m_ContactPoint;
	DragonXVector3 m_ContactNormal;
	DragonXVector3 mContactVelocity;
	real mRestitution;
	real mFriction;
	real mPenitrtionDepth;
	real mDesiredDeltaVelocity;
	DragonXMatrix mContactToWorld;
public:
	DragonContact();
	~DragonContact();
};