#pragma once
#include <vector>
#include "DragonPhysicsHydraContainers.h"
#include "BaseDragonPhysicsObject.h"
#include "BaseDragonCollisionObject.h"
#include "DragonPhysicsPlane.h"
#include "DragonPhysicsOBB.h"
#include "DragonPhysicsMeshCollisionObject.h"

class DragonContactGenerator
{
private:
	inline DragonXVector3 contactPoint(const DragonXVector3 &pOne,const DragonXVector3 &dOne,real oneSize,const DragonXVector3 &pTwo,const DragonXVector3 &dTwo,real twoSize,bool useOne)
	{
		DragonXVector3 toSt, cOne, cTwo;
		real dpStaOne, dpStaTwo, dpOneTwo, smOne, smTwo;
		real denom, mua, mub;
		smOne = dOne.GetMagSquared();
		smTwo = dTwo.GetMagSquared();
		dpOneTwo = dTwo * dOne;
		toSt = pOne - pTwo;
		dpStaOne = dOne * toSt;
		dpStaTwo = dTwo * toSt;
		denom = smOne * smTwo - dpOneTwo * dpOneTwo;
		if ((real)abs(denom) < 0.0001f)
		{
			return useOne?pOne:pTwo;
		}		mua = (dpOneTwo * dpStaTwo - smTwo * dpStaOne) / denom;
		mub = (smOne * dpStaTwo - dpOneTwo * dpStaOne) / denom;
		if (mua > oneSize ||mua < -oneSize ||mub > twoSize ||mub < -twoSize)
		{
			return useOne?pOne:pTwo;
		}
		else
		{
			cOne = pOne + dOne * mua;
			cTwo = pTwo + dTwo * mub;
			return cOne * 0.5 + cTwo * 0.5;
		}
	}
	inline real transformToAxis(const DragonXVector3& halfSizes,const DragonXVector3& axis,const DragonXVector3& axisV0,const DragonXVector3& axisV1,const DragonXVector3& axisV2)const
	{
		return halfSizes.x * (real)abs(axis * axisV0) +	halfSizes.y * (real)abs(axis * axisV1) +halfSizes.z * (real)abs(axis * axisV2);
	}
	inline real penetrationOnAxis(const DragonXVector3& hs1,const DragonXVector3& o1av0,const DragonXVector3& o1av1,const DragonXVector3& o1av2,const DragonXVector3& hs2,const DragonXVector3& o2av0,const DragonXVector3& o2av1,const DragonXVector3& o2av2,const DragonXVector3& toCentre,const DragonXVector3& axis)const
	{
		real oneProject = transformToAxis(hs1,axis,o1av0,o1av1,o1av2);
		real twoProject = transformToAxis(hs2,axis,o2av0,o2av1,o2av2);
		real distance = (real)abs(toCentre * axis);
		return oneProject + twoProject - distance;
	}
	inline bool tryAxis(const DragonXVector3& hs1,const DragonXVector3& o1av0,const DragonXVector3& o1av1,const DragonXVector3& o1av2,const DragonXVector3& hs2,const DragonXVector3& o2av0,const DragonXVector3& o2av1,const DragonXVector3& o2av2,const DragonXVector3& toCentre,DragonXVector3& axis,unsigned index,real& smallestPen,unsigned& smallestCase)
	{
		if (axis.GetMagSquared() < 0.0001)
		{
			return true;
		}
		axis.Normalize();
		real penetration = penetrationOnAxis(hs1,o1av0,o1av1,o1av2,hs2,o2av0,o2av1,o2av2,toCentre,axis);
		if (penetration < 0)
		{
			return false;
		}
		if (penetration < smallestPen)
		{
			smallestPen = penetration;
			smallestCase = index;
		}
		return true;
	}
	inline void fillPointFaceBoxBox(BaseDragonPhysicsObject* one,BaseDragonPhysicsObject* two,const DragonXVector3 &toCentre,unsigned best,real pen,const DragonXVector3& obj2Hs,const DragonXVector3& o2av0,const DragonXVector3& o2av1,const DragonXVector3& o2av2,DragonContactContainer& contacts)
	{
		DragonContact DC = DragonContact();
		DragonXVector3 normal = one->TransformMatrix.getAxisVector(best);
		if (one->TransformMatrix.getAxisVector(best) * toCentre > 0)// changing from > to < & foolowing from < to > changed back error found in vel res
		{
			normal.invert();
		}
		DragonXVector3 vertex = obj2Hs;
		if (o2av0 * normal < 0)
		{
			vertex.x = -vertex.x;
		}
		if (o2av1 * normal < 0)
		{
			vertex.y = -vertex.y;
		}
		if (o2av2 * normal < 0)
		{
			vertex.z = -vertex.z;
		}
		// Create the contact data
		DC.m_ContactNormal = normal;
		DC.mValidData=true;
		DC.mPenitrtionDepth = pen;
		DC.m_ContactPoint = two->TransformMatrix * vertex;
		DC.m_Entities[0]=one;
		DC.m_Entities[1]=two;
		DC.mRestitution=m_Restitution;
		contacts.Add(std::move(DC));
	}
private:
	inline bool SphereSphereIntersectTest(const DragonXVector3& p1,real r1,const DragonXVector3& p2,real r2)const
	{
		return (p1-p2).GetMagSquared()<(r1+r2)*(r1+r2);
	}
	inline DragonXVector3 GetTriNormal(const DragonXVector3& a,const DragonXVector3& b, const DragonXVector3& c)const
	{
		DragonXVector3 n = ((b-a)%(c-a));
		n.invert();
		n.Normalize();
		return n;
	}
	inline DragonXVector3 ClosestPtPointTriangle(const DragonXVector3& p,const DragonXVector3& a,const DragonXVector3& b,const DragonXVector3& c)const
	{
		// check if p is in vertex region outside a
		DragonXVector3 ab = b-a;
		DragonXVector3 ac = c-a;
		DragonXVector3 ap = p-a;
		real d1 = ab*ap;
		real d2 = ac*ap;
		if(d1<=0.0f&&d2<=0.0f)
		{
			return a;
		}
		// check b
		DragonXVector3 bp = p-b;
		real d3 = ab*bp;
		real d4=ac*bp;
		if(d3>=0.0f&&d4<=d3)
		{
			return b;
		}
		// check edge region ab
		real vc = d1*d4-d3*d2;
		if(vc<=0.0f&&d1>=0.0f&&d3<=0.0f)
		{
			real v = d1/(d1-d3);
			return a+v*ab;
		}
		// check c
		DragonXVector3 cp = p-c;
		real d5=ab*cp;
		real d6=ac*cp;
		if(d6>=0.0f&&d5<=d6)
		{
			return c;
		}
		// check p on ac
		real vb = d5*d2-d1*d6;
		if(vb<=0.0f&&d2>=0.0f&&d6<=0.0f)
		{
			real w = d2/(d2-d6);
			return a+w*ac;
		}
		// check p on bc
		real va = d3*d6-d5*d4;
		if(va<=0.0f&&(d4-d3)>=0.0f&&(d5-d6)>=0.0f)
		{
			real w = (d4-d3)/((d4-d3)+(d5-d6));
			return b + w*(c-b);
		}
		// p is in the face region
		real denom = 1.0f/(va+vb+vc);
		real v = vb*denom;
		real w = vc * denom;
		return a+ab*v+ac*w;
	}
	inline DragonXVector3 CalcObbEdgeContactpoint(const DragonXVector3& p,const DragonXVector3& d,const DragonXVector3& hs,const real& length)const
	{
		real EPSILON = 0.001f;
		DragonXVector3 retPoint;
		real tmin=-999999999.9f;
		real tmax=length;
		if(abs(d.x)>EPSILON)
		{
			real ood = 1.0f / d.x;
			real t1 = (-hs.x - p.x)*ood;
			real t2 = (hs.x - p.x)*ood;
			if(t1>t2)
			{
				real temp = t1;
				t1=t2;
				t2=temp;
			}
			if(t1>tmin)
			{
				tmin=t1;
			}
			if (t2>tmax)
			{
				tmax=t2;
			}
		}

		if(abs(d.y)>EPSILON)
		{
			real ood = 1.0f / d.y;
			real t1 = (-hs.y - p.y)*ood;
			real t2 = (hs.y - p.y)*ood;
			if(t1>t2)
			{
				real temp = t1;
				t1=t2;
				t2=temp;
			}
			if(t1>tmin)
			{
				tmin=t1;
			}
			if (t2>tmax)
			{
				tmax=t2;
			}
		}

		if(abs(d.z)>EPSILON)
		{
			real ood = 1.0f / d.z;
			real t1 = (-hs.z - p.z)*ood;
			real t2 = (hs.z - p.z)*ood;
			if(t1>t2)
			{
				real temp = t1;
				t1=t2;
				t2=temp;
			}
			if(t1>tmin)
			{
				tmin=t1;
			}
			if (t2>tmax)
			{
				tmax=t2;
			}
		}
		retPoint = p+d*tmin;
		return retPoint;
	}
	inline real GetMin(const real first,const real second,const real third)const
	{
		real ret=9999999999.9f;
		if(first<ret)
		{
			ret=first;
		}
		if(second<ret)
		{
			ret = second;
		}
		if(third<ret)
		{
			ret = third;
		}
		return ret;
	}
	inline real GetMin(const real first,const real second)const
	{
		real ret=9999999999.9f;
		if(first<ret)
		{
			ret=first;
		}
		if(second<ret)
		{
			ret = second;
		}
		return ret;
	}
	inline real GetMax(const real zed,const real ein,const real zwei)const
	{
		real ret=-9999999999.9f;
		if(zed>ret)
		{
			ret=zed;
		}
		if(ein>ret)
		{
			ret = ein;
		}
		if(zwei>ret)
		{
			ret = zwei;
		}
		return ret;
	}
	inline real GetMax(const real zed,const real ein)const
	{
		real ret=-9999999999.9f;
		if(zed>ret)
		{
			ret=zed;
		}
		if(ein>ret)
		{
			ret = ein;
		}
		return ret;
	}
	inline void GenContactSphereTri(BaseDragonPhysicsObject* obj1,BaseDragonPhysicsObject* obj2,const DragonTriangle& tri,DragonContactContainer& contacts)
	{
		DragonXVector3 p = ClosestPtPointTriangle(obj1->m_Position,tri.m_VertA,tri.m_VertB,tri.m_VertC);
		p-=obj1->m_Position;
		if (p*p<=obj1->GetRadius()*obj1->GetRadius())
		{
			DragonContact DC = DragonContact();			
			real pen = obj1->GetRadius()-p.GetMagnitude();
			DC.mPenitrtionDepth=pen;
			p+=obj1->m_Position;
			DC.m_ContactNormal=obj1->m_Position-p;
			DC.m_ContactNormal.Normalize();
			DC.m_ContactPoint=p;
			DC.m_Entities[0]=obj1;
			DC.m_Entities[1]=obj2;
			DC.mValidData=true;
			DC.mRestitution=m_Restitution;
			contacts.Add(std::move(DC));
		}
	}
	inline bool SphereTriangleCollisionTest(const DragonTriangle& tri,const BaseDragonPhysicsObject& sphere)const
	{
		DragonXVector3 p = ClosestPtPointTriangle(sphere.m_Position,tri.m_VertA,tri.m_VertB,tri.m_VertC);
		p-=sphere.m_Position;
		if(p*p<=sphere.GetRadius()*sphere.GetRadius())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	inline DragonXVector3 TriEdgePointOfContact(const DragonXVector3& p,const DragonXVector3&q,const DragonXVector3& a,const DragonXVector3& b,const DragonXVector3& c)const
	{
		DragonXVector3 pq = q-p;
		DragonXVector3 m = pq%p;
		real u = (pq*(c%b))+(m*(c-b));
		real v = (pq*(a%c))+(m*(a-c));
		real w = (pq*(b%a))+(m*(b-a));
		real denom=1.0f/(u+v+w);
		u*=denom;
		v*=denom;
		w*=denom;
		DragonXVector3 ret = u*a+v*b+w*c;
		return ret;
	}
	inline void GenContactObbTri(const DragonTriangle& tri,BaseDragonPhysicsObject* obb,BaseDragonPhysicsObject* mesh,const DragonXVector3& hs,const DragonXVector3& av0,const DragonXVector3& av1,const DragonXVector3& av2,DragonContactContainer& contacts)
	{
		DragonXVector3 norm = GetTriNormal(tri.m_VertA,tri.m_VertB,tri.m_VertC);

		DragonXVector3 f0=tri.m_VertB-tri.m_VertA;

		DragonXVector3 f1=tri.m_VertC-tri.m_VertB;

		DragonXVector3 f2=tri.m_VertA-tri.m_VertC;

		DragonXVector3 tempConPoint;
		int axis = -1;
		DragonXVector3 bestAxis;
		real pen=999999999.0f;

		// one test against the tri face normal   this is the box plane test finish later
		DragonXVector3 taxis = norm;
		bool triNormInverted=false;
		taxis.Normalize();
		real p0 = tri.m_VertA*taxis;
		real p1 = tri.m_VertB*taxis;
		real p2 = tri.m_VertC*taxis;
		if(GetMin(p0,p1,p2)<0.0f)
		{
			triNormInverted=true;
			norm.invert();
			taxis.invert();
			p0 = tri.m_VertA*taxis;
			p1 = tri.m_VertB*taxis;
			p2 = tri.m_VertC*taxis;
		}
		real tpen = transformToAxis(hs,taxis,av0,av1,av2)-GetMin(p0,p1);
		if(GetMax(-1.0f*GetMax(p0,p1,p2),GetMin(p0,p1,p2))<=transformToAxis(hs,taxis,av0,av1,av2))
		{
			if(tpen<pen&&tpen>=0.0f)
			{
				axis=0;
				bestAxis=taxis;
				pen=tpen;
			}
		}
		else
		{
			return;
		}

		// 3 obb face normals
		taxis = av0;
		taxis.Normalize();
		p0 = tri.m_VertA*taxis;
		p1 = tri.m_VertB*taxis;
		p2 = tri.m_VertC*taxis;
		if(GetMin(p0,p1,p2)<0.0f)
		{
			taxis.invert();
			p0 = tri.m_VertA*taxis;
			p1 = tri.m_VertB*taxis;
			p2 = tri.m_VertC*taxis;
		}
		tpen = transformToAxis(hs,taxis,av0,av1,av2)-GetMin(p0,p1,p2);
		if(GetMax(-1.0f*GetMax(p0,p1,p2),GetMin(p0,p1,p2))<=transformToAxis(hs,taxis,av0,av1,av2))
		{
			if(tpen<pen&&tpen>=0.0f)
			{
				if(tpen==transformToAxis(hs,taxis,av0,av1,av2)-p0)
				{
					tempConPoint=tri.m_VertA;
				}
				if(tpen==transformToAxis(hs,taxis,av0,av1,av2)-p1)
				{
					tempConPoint=tri.m_VertB;
				}
				if(tpen==transformToAxis(hs,taxis,av0,av1,av2)-p2)
				{
					tempConPoint=tri.m_VertC;
				}
				axis=1;
				bestAxis=taxis;
				pen=tpen;
			}
		}
		else
		{
			return;
		}

		taxis = av1;
		taxis.Normalize();
		p0 = tri.m_VertA*taxis;
		p1 = tri.m_VertB*taxis;
		p2 = tri.m_VertC*taxis;
		if(GetMin(p0,p1,p2)<0.0f)
		{
			taxis.invert();
			p0 = tri.m_VertA*taxis;
			p1 = tri.m_VertB*taxis;
			p2 = tri.m_VertC*taxis;
		}
		tpen = transformToAxis(hs,taxis,av0,av1,av2)-GetMin(p0,p1,p2);
		if(GetMax(-1.0f*GetMax(p0,p1,p2),GetMin(p0,p1,p2))<=transformToAxis(hs,taxis,av0,av1,av2))
		{
			if(tpen<pen&&tpen>=0.0f)
			{
				if(tpen==transformToAxis(hs,taxis,av0,av1,av2)-p0)
				{
					tempConPoint=tri.m_VertA;
				}
				if(tpen==transformToAxis(hs,taxis,av0,av1,av2)-p1)
				{
					tempConPoint=tri.m_VertB;
				}
				if(tpen==transformToAxis(hs,taxis,av0,av1,av2)-p2)
				{
					tempConPoint=tri.m_VertC;
				}
				axis=2;
				bestAxis=taxis;
				pen=tpen;
			}
		}
		else
		{
			return;
		}

		taxis = av2;
		taxis.Normalize();
		p0 = tri.m_VertA*taxis;
		p1 = tri.m_VertB*taxis;
		p2 = tri.m_VertC*taxis;
		if(GetMin(p0,p1,p2)<0.0f)
		{
			taxis.invert();
			p0 = tri.m_VertA*taxis;
			p1 = tri.m_VertB*taxis;
			p2 = tri.m_VertC*taxis;
		}
		tpen = transformToAxis(hs,taxis,av0,av1,av2)-GetMin(p0,p1,p2);
		if(GetMax(-1.0f*GetMax(p0,p1,p2),GetMin(p0,p1,p2))<=transformToAxis(hs,taxis,av0,av1,av2))
		{
			if(tpen<pen&&tpen>=0.0f)
			{
				if(tpen==transformToAxis(hs,taxis,av0,av1,av2)-p0)
				{
					tempConPoint=tri.m_VertA;
				}
				if(tpen==transformToAxis(hs,taxis,av0,av1,av2)-p1)
				{
					tempConPoint=tri.m_VertB;
				}
				if(tpen==transformToAxis(hs,taxis,av0,av1,av2)-p2)
				{
					tempConPoint=tri.m_VertC;
				}
				axis=3;
				bestAxis=taxis;
				pen=tpen;
			}
		}
		else
		{
			return;
		}

		// cross product axis tests
		taxis = av0%f0;
		taxis.Normalize();
		if(taxis.GetMagSquared()>0.0001)
		{
			p0 = tri.m_VertA*taxis;
			p1 = tri.m_VertB*taxis;
			p2 = tri.m_VertC*taxis;
			if(GetMin(p0,p1,p2)<0.0f)
			{
				taxis.invert();
				p0 = tri.m_VertA*taxis;
				p1 = tri.m_VertB*taxis;
				p2 = tri.m_VertC*taxis;
			}
			tpen = transformToAxis(hs,taxis,av0,av1,av2)-GetMin(p0,p2);
			if(GetMax(-1.0f*GetMax(p0,p2),GetMin(p0,p2))<=transformToAxis(hs,taxis,av0,av1,av2))
			{
				if(tpen<pen&&tpen>=0.0f)
				{
					axis=4;
					bestAxis=taxis;
					pen=tpen;
				}
			}
			else
			{
				return;
			}
		}

		taxis = av0%f1;
		taxis.Normalize();
		if(taxis.GetMagSquared()>0.0001)
		{
			p0 = tri.m_VertA*taxis;
			p1 = tri.m_VertB*taxis;
			p2 = tri.m_VertC*taxis;
			if(GetMin(p0,p1,p2)<0.0f)
			{
				taxis.invert();
				p0 = tri.m_VertA*taxis;
				p1 = tri.m_VertB*taxis;
				p2 = tri.m_VertC*taxis;
			}
			tpen = transformToAxis(hs,taxis,av0,av1,av2)-GetMin(p0,p2);
			if(GetMax(-1.0f*GetMax(p0,p2),GetMin(p0,p2))<=transformToAxis(hs,taxis,av0,av1,av2))
			{
				if(tpen<pen&&tpen>=0.0f)
				{
					axis=5;
					bestAxis=taxis;
					pen=tpen;
				}
			}
			else
			{
				return;
			}
		}

		taxis = av0%f2;
		taxis.Normalize();
		if(taxis.GetMagSquared()>0.0001)
		{
			p0 = tri.m_VertA*taxis;
			p1 = tri.m_VertB*taxis;
			p2 = tri.m_VertC*taxis;
			if(GetMin(p0,p1,p2)<0.0f)
			{
				taxis.invert();
				p0 = tri.m_VertA*taxis;
				p1 = tri.m_VertB*taxis;
				p2 = tri.m_VertC*taxis;
			}
			tpen = transformToAxis(hs,taxis,av0,av1,av2)-GetMin(p1,p2);
			if(GetMax(-1.0f*GetMax(p1,p2),GetMin(p1,p2))<=transformToAxis(hs,taxis,av0,av1,av2))
			{
				if(tpen<pen&&tpen>=0.0f)
				{
					axis=6;
					bestAxis=taxis;
					pen=tpen;
				}
			}
			else
			{
				return;
			}
		}

		taxis = av1%f0;
		taxis.Normalize();
		if(taxis.GetMagSquared()>0.0001)
		{
			p0 = tri.m_VertA*taxis;
			p1 = tri.m_VertB*taxis;
			p2 = tri.m_VertC*taxis;
			if(GetMin(p0,p1,p2)<0.0f)
			{
				taxis.invert();
				p0 = tri.m_VertA*taxis;
				p1 = tri.m_VertB*taxis;
				p2 = tri.m_VertC*taxis;
			}
			tpen=transformToAxis(hs,taxis,av0,av1,av2)-GetMin(p0,p2);
			if(GetMax(-1.0f*GetMax(p0,p2),GetMin(p0,p2))<=transformToAxis(hs,taxis,av0,av1,av2))
			{
				if(tpen<pen&&tpen>=0.0f)
				{
					axis=7;
					bestAxis=taxis;
					pen=tpen;
				}
			}
			else
			{
				return;
			}
		}

		taxis = av1%f1;
		taxis.Normalize();
		if(taxis.GetMagSquared()>0.0001)
		{
			p0 = tri.m_VertA*taxis;
			p1 = tri.m_VertB*taxis;
			p2 = tri.m_VertC*taxis;
			if(GetMin(p0,p1,p2)<0.0f)
			{
				taxis.invert();
				p0 = tri.m_VertA*taxis;
				p1 = tri.m_VertB*taxis;
				p2 = tri.m_VertC*taxis;
			}
			tpen=transformToAxis(hs,taxis,av0,av1,av2)-GetMin(p0,p2);
			if(GetMax(-1.0f*GetMax(p0,p2),GetMin(p0,p2))<=transformToAxis(hs,taxis,av0,av1,av2))
			{
				if(tpen<pen&&tpen>=0.0f)
				{
					axis=8;
					bestAxis=taxis;
					pen=tpen;
				}
			}
			else
			{
				return;
			}
		}

		taxis = av1%f2;
		taxis.Normalize();
		if(taxis.GetMagSquared()>0.0001)
		{
			p0 = tri.m_VertA*taxis;
			p1 = tri.m_VertB*taxis;
			p2 = tri.m_VertC*taxis;
			if(GetMin(p0,p1,p2)<0.0f)
			{
				taxis.invert();
				p0 = tri.m_VertA*taxis;
				p1 = tri.m_VertB*taxis;
				p2 = tri.m_VertC*taxis;
			}
			tpen = transformToAxis(hs,taxis,av0,av1,av2)-GetMin(p0,p1);
			if(GetMax(-1.0f*GetMax(p0,p1),GetMin(p0,p1))<=transformToAxis(hs,taxis,av0,av1,av2))
			{
				if(tpen<pen&&tpen>=0.0f)
				{
					axis=9;
					bestAxis=taxis;
					pen=tpen;
				}
			}
			else
			{
				return;
			}
		}

		taxis = av2%f0;
		taxis.Normalize();
		if(taxis.GetMagSquared()>0.0001)
		{
			p0 = tri.m_VertA*taxis;
			p1 = tri.m_VertB*taxis;
			p2 = tri.m_VertC*taxis;
			if(GetMin(p0,p1,p2)<0.0f)
			{
				taxis.invert();
				p0 = tri.m_VertA*taxis;
				p1 = tri.m_VertB*taxis;
				p2 = tri.m_VertC*taxis;
			}
			tpen=transformToAxis(hs,taxis,av0,av1,av2)-GetMin(p0,p2);
			if(GetMax(-1.0f*GetMax(p0,p2),GetMin(p0,p2))<=transformToAxis(hs,taxis,av0,av1,av2))
			{
				if(tpen<pen&&tpen>=0.0f)
				{
					axis=10;
					bestAxis=taxis;
					pen=tpen;
				}
			}
			else
			{
				return;
			}
		}

		taxis = av2%f1;
		taxis.Normalize();
		if(taxis.GetMagSquared()>0.0001)
		{
			p0 = tri.m_VertA*taxis;
			p1 = tri.m_VertB*taxis;
			p2 = tri.m_VertC*taxis;
			if(GetMin(p0,p1,p2)<0.0f)
			{
				taxis.invert();
				p0 = tri.m_VertA*taxis;
				p1 = tri.m_VertB*taxis;
				p2 = tri.m_VertC*taxis;
			}
			tpen=transformToAxis(hs,taxis,av0,av1,av2)-GetMin(p0,p1);
			if(GetMax(-1.0f*GetMax(p0,p1),GetMin(p0,p1))<=transformToAxis(hs,taxis,av0,av1,av2))
			{
				if(tpen<pen&&tpen>=0.0f)
				{
					axis=11;
					bestAxis=taxis;
					pen=tpen;
				}
			}
			else
			{
				return;
			}
		}

		taxis = av2%f2;
		taxis.Normalize();
		if(taxis.GetMagSquared()>0.0001)
		{
			p0 = tri.m_VertA*taxis;
			p1 = tri.m_VertB*taxis;
			p2 = tri.m_VertC*taxis;
			if(GetMin(p0,p1,p2)<0.0f)
			{
				taxis.invert();
				p0 = tri.m_VertA*taxis;
				p1 = tri.m_VertB*taxis;
				p2 = tri.m_VertC*taxis;
			}
			tpen=transformToAxis(hs,taxis,av0,av1,av2)-GetMin(p1,p2);
			if(GetMax(-1.0f*GetMax(p1,p2),GetMin(p1,p2))<=transformToAxis(hs,taxis,av0,av1,av2))
			{
				if(tpen<pen&&tpen>=0.0f)
				{
					axis=12;
					bestAxis=taxis;
					pen=tpen;
				}
			}
			else
			{
				return;
			}
		}

		if(axis!=-1)
		{
			if(axis==0)
			{
				DragonXVector3 Wnorm = GetTriNormal(obb->getPointInWorldSpace(tri.m_VertA),obb->getPointInWorldSpace(tri.m_VertB),obb->getPointInWorldSpace(tri.m_VertC));
				Wnorm.invert();
				real triPlaneDist = obb->getPointInWorldSpace(tri.m_VertA)*Wnorm;
				// Go through each combination of + and - for each half-size
				real mults[8][3] = {{1,1,1},{-1,1,1},{1,-1,1},{-1,-1,1},{1,1,-1},{-1,1,-1},{1,-1,-1},{-1,-1,-1}};
				for(unsigned i = 0; i < 8; i++)
				{
					DragonXVector3 vertexPos(mults[i][0],mults[i][1],mults[i][2]);
					vertexPos.componentProductUpdate(hs);
					vertexPos = obb->getPointInWorldSpace(vertexPos);
					real dist= vertexPos*Wnorm;
					if(dist<=triPlaneDist)
					{
						DragonContact DC = DragonContact();
						DC.m_ContactPoint=Wnorm;
						DC.m_ContactPoint*=(dist-triPlaneDist);
						DC.m_ContactPoint+=vertexPos;
						DC.m_ContactNormal=Wnorm;
						DC.mPenitrtionDepth=triPlaneDist-dist;
						if(DC.mPenitrtionDepth<0)
						{
							DC.m_ContactNormal.invert();
							DC.mPenitrtionDepth*=-1.0f;
						}
						DC.m_Entities[0]=obb;
						DC.m_Entities[1]=mesh;
						DC.mRestitution=m_Restitution;
						contacts.Add(std::move(DC));
					}
				}
				return;
			}
			if(axis>0&&axis<4)
			{
				DragonContact DC = DragonContact();
				DC.m_ContactPoint=obb->getPointInWorldSpace(tempConPoint);
				DC.m_ContactNormal = obb->getDirectionInWorldSpace(bestAxis);
				DC.m_ContactNormal.Normalize();
				DC.mPenitrtionDepth=pen;
				DC.m_Entities[0]=mesh;
				DC.m_Entities[1]=obb;
				DC.mRestitution=m_Restitution;
				contacts.Add(std::move(DC));
				return;
			}
			DragonXVector3 cp;
			if(axis==4||axis==7||axis==10)
			{
				real len = f0.GetMagnitude();
				f0.Normalize();
				cp = CalcObbEdgeContactpoint(tri.m_VertA,f0,hs,len);
				DragonContact DC = DragonContact();
				DC.m_ContactPoint=obb->getPointInWorldSpace(cp);
				DC.m_ContactNormal = obb->getDirectionInWorldSpace(bestAxis);
				DC.m_ContactNormal.Normalize();
				DC.mPenitrtionDepth=pen;
				DC.m_Entities[0]=mesh;
				DC.m_Entities[1]=obb;
				DC.mRestitution=m_Restitution;
				contacts.Add(std::move(DC));
				return;
			}

			if(axis==5||axis==8||axis==11)
			{
				real len = f1.GetMagnitude();
				f1.Normalize();
				cp = CalcObbEdgeContactpoint(tri.m_VertB,f1,hs,len);
				DragonContact DC = DragonContact();
				DC.m_ContactPoint=obb->getPointInWorldSpace(cp);
				DC.m_ContactNormal = obb->getDirectionInWorldSpace(bestAxis);
				DC.m_ContactNormal.Normalize();
				DC.mPenitrtionDepth=pen;
				DC.m_Entities[0]=mesh;
				DC.m_Entities[1]=obb;
				DC.mRestitution=m_Restitution;
				contacts.Add(std::move(DC));
				return;
			}

			if(axis==6||axis==9||axis==12)
			{
				real len = f2.GetMagnitude();
				f2.Normalize();
				cp = CalcObbEdgeContactpoint(tri.m_VertC,f2,hs,len);
				DragonContact DC = DragonContact();
				DC.m_ContactPoint=obb->getPointInWorldSpace(cp);
				DC.m_ContactNormal = obb->getDirectionInWorldSpace(bestAxis);
				DC.m_ContactNormal.Normalize();
				DC.mPenitrtionDepth=pen;
				DC.m_Entities[0]=mesh;
				DC.m_Entities[1]=obb;
				DC.mRestitution=m_Restitution;
				contacts.Add(std::move(DC));
				return;
			}
			return;
		}
	}
	inline real DistPointPlane(const DragonXVector3& p, const DragonXVector3& norm,const real dist)const
	{
		return p*norm-dist;
	}
	inline bool TrianglePlaneCollisionTest(const DragonTriangle& tri,const DragonXVector3& planeNorm, const real planeDist,real& d0,real& d1, real& d2)
	{
		d0 = DistPointPlane(tri.m_VertA,planeNorm,planeDist);
		d1 = DistPointPlane(tri.m_VertB,planeNorm,planeDist);
		d2 = DistPointPlane(tri.m_VertC,planeNorm,planeDist);
		if(d0>0&&d1>0&&d2>0)
		{
			return false;
		}
		else if(d0<0&&d1<0&&d2<0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	inline void GenContactTriTri(const DragonTriangle& tri1,const DragonXVector3& tri1Norm,const DragonTriangle& tri2,const DragonXVector3& tri2Norm,BaseDragonPhysicsObject* obj1,BaseDragonPhysicsObject* obj2,DragonContactContainer& contacts)
	{
		real d10=0.0f;
		real d11=0.0f;
		real d12=0.0f;
		real d20=0.0f;
		real d21=0.0f;
		real d22=0.0f;
		DragonXVector3 pointOfContact;
		real pen=999999999999.9f;
		DragonXVector3 contactNorm;

		if(!TrianglePlaneCollisionTest(tri1,tri2Norm,tri2Norm*tri2.m_VertA,d10,d11,d12))
		{
			return;
		}

		if(!TrianglePlaneCollisionTest(tri2,tri1Norm,tri1Norm*tri1.m_VertA,d20,d21,d22))
		{
			return;
		}

		// now compute the edges
		DragonXVector3 tri1ba = tri1.m_VertB-tri1.m_VertA;
		DragonXVector3 tri1cb = tri1.m_VertC-tri1.m_VertB;
		DragonXVector3 tri1ac = tri1.m_VertA-tri1.m_VertC;
		//tri2
		DragonXVector3 tri2ba = tri2.m_VertB-tri2.m_VertA;
		DragonXVector3 tri2cb = tri2.m_VertC-tri2.m_VertB;
		DragonXVector3 tri2ac = tri2.m_VertA-tri2.m_VertC;

		real tpen = 99999999999.9f;
		real pu0,pu1,pu2,pv0,pv1,pv2;
		int axis = -1;
		DragonXVector3 cnAxis;
		DragonXVector3 cp;
		if(d10<pen&&d10>0.0f)
		{
			pen = d10;
			axis = 0;
			cnAxis=tri2Norm;
			cp=tri1.m_VertA;
		}
		if(d11<pen&&d11>0.0f)
		{
			pen = d11;
			axis = 1;
			cnAxis=tri2Norm;
			cp=tri1.m_VertB;
		}
		if(d12<pen&&d12>0.0f)
		{
			pen = d12;
			axis = 2;
			cnAxis=tri2Norm;
			cp=tri1.m_VertC;
		}
		// test tri2 verts against tri 1 plane
		if(d20<pen&&d20>0.0f)
		{
			pen = d20;
			axis = 3;
			cnAxis=tri1Norm;
			cp=tri2.m_VertA;
		}
		if(d21<pen&&d21>0.0f)
		{
			pen = d21;
			axis = 4;
			cnAxis=tri1Norm;
			cp=tri2.m_VertB;
		}
		if(d22<pen&&d22>0.0f)
		{
			pen = d22;
			axis = 5;
			cnAxis=tri1Norm;
			cp=tri2.m_VertC;
		}
		// 9 cross product axis tests
		// set 1
		DragonXVector3 testAxis = tri1ba%tri2ba;
		testAxis.Normalize();
		if(testAxis.GetMagSquared()>0.0001)
		{
			pu0=tri1.m_VertA*testAxis;
			pu1=tri1.m_VertB*testAxis;
			pu2=tri1.m_VertC*testAxis;

			if(GetMin(pu0,pu1,pu2)<0.0f)
			{
				testAxis.invert();
				pu0=tri1.m_VertA*testAxis;
				pu1=tri1.m_VertB*testAxis;
				pu2=tri1.m_VertC*testAxis;
			}
			pv0=tri2.m_VertA*testAxis;
			pv1=tri2.m_VertB*testAxis;
			pv2=tri2.m_VertC*testAxis;

			if(GetMax(pv0,pv1,pv2)>GetMin(pu0,pu1,pu2))
			{
				tpen=GetMax(pv0,pv1,pv2)-GetMin(pu0,pu1,pu2);
				if(tpen<pen&&tpen>0.0f)
				{
					pen=tpen;
					axis=6;
					cnAxis=testAxis;
				}
			}
			else
			{
				return;
			}
			if(GetMin(pv0,pv1,pv2)<GetMax(pu0,pu1,pu2))
			{
				tpen=GetMax(pu0,pu1,pu2)-GetMin(pv0,pv1,pv2);
				if(tpen<pen&&tpen>0.0f)
				{
					pen=tpen;
					axis=7;
					cnAxis=testAxis;
				}
			}
			else
			{
				return;
			}
		}

		testAxis = tri1ba%tri2cb;
		testAxis.Normalize();
		if(testAxis.GetMagSquared()>0.0001)
		{
			pu0=tri1.m_VertA*testAxis;
			pu1=tri1.m_VertB*testAxis;
			pu2=tri1.m_VertC*testAxis;

			if(GetMin(pu0,pu1,pu2)<0.0f)
			{
				testAxis.invert();
				pu0=tri1.m_VertA*testAxis;
				pu1=tri1.m_VertB*testAxis;
				pu2=tri1.m_VertC*testAxis;
			}
			pv0=tri2.m_VertA*testAxis;
			pv1=tri2.m_VertB*testAxis;
			pv2=tri2.m_VertC*testAxis;

			if(GetMax(pv0,pv1,pv2)>GetMin(pu0,pu1,pu2))
			{
				tpen=GetMax(pv0,pv1,pv2)-GetMin(pu0,pu1,pu2);
				if(tpen<pen&&tpen>0.0f)
				{
					pen=tpen;
					axis=8;
					cnAxis=testAxis;
				}
			}
			else
			{
				return;
			}
			if(GetMin(pv0,pv1,pv2)<GetMax(pu0,pu1,pu2))
			{
				tpen=GetMax(pu0,pu1,pu2)-GetMin(pv0,pv1,pv2);
				if(tpen<pen&&tpen>0.0f)
				{
					pen=tpen;
					axis=9;
					cnAxis=testAxis;
				}
			}
			else
			{
				return;
			}
		}

		testAxis = tri1ba%tri2ac;
		testAxis.Normalize();
		if(testAxis.GetMagSquared()>0.0001)
		{
			pu0=tri1.m_VertA*testAxis;
			pu1=tri1.m_VertB*testAxis;
			pu2=tri1.m_VertC*testAxis;

			if(GetMin(pu0,pu1,pu2)<0.0f)
			{
				testAxis.invert();
				pu0=tri1.m_VertA*testAxis;
				pu1=tri1.m_VertB*testAxis;
				pu2=tri1.m_VertC*testAxis;
			}
			pv0=tri2.m_VertA*testAxis;
			pv1=tri2.m_VertB*testAxis;
			pv2=tri2.m_VertC*testAxis;

			if(GetMax(pv0,pv1,pv2)>GetMin(pu0,pu1,pu2))
			{
				tpen=GetMax(pv0,pv1,pv2)-GetMin(pu0,pu1,pu2);
				if(tpen<pen&&tpen>0.0f)
				{
					pen=tpen;
					axis=10;
					cnAxis=testAxis;
				}
			}
			else
			{
				return;
			}
			if(GetMin(pv0,pv1,pv2)<GetMax(pu0,pu1,pu2))
			{
				tpen=GetMax(pu0,pu1,pu2)-GetMin(pv0,pv1,pv2);
				if(tpen<pen&&tpen>0.0f)
				{
					pen=tpen;
					axis=11;
					cnAxis=testAxis;
				}
			}
			else
			{
				return;
			}
		}
		//set2
		testAxis = tri1cb%tri2ba;
		testAxis.Normalize();
		if(testAxis.GetMagSquared()>0.0001)
		{
			pu0=tri1.m_VertA*testAxis;
			pu1=tri1.m_VertB*testAxis;
			pu2=tri1.m_VertC*testAxis;

			if(GetMin(pu0,pu1,pu2)<0.0f)
			{
				testAxis.invert();
				pu0=tri1.m_VertA*testAxis;
				pu1=tri1.m_VertB*testAxis;
				pu2=tri1.m_VertC*testAxis;
			}
			pv0=tri2.m_VertA*testAxis;
			pv1=tri2.m_VertB*testAxis;
			pv2=tri2.m_VertC*testAxis;

			if(GetMax(pv0,pv1,pv2)>GetMin(pu0,pu1,pu2))
			{
				tpen=GetMax(pv0,pv1,pv2)-GetMin(pu0,pu1,pu2);
				if(tpen<pen&&tpen>0.0f)
				{
					pen=tpen;
					axis=12;
					cnAxis=testAxis;
				}
			}
			else
			{
				return;
			}
			if(GetMin(pv0,pv1,pv2)<GetMax(pu0,pu1,pu2))
			{
				tpen=GetMax(pu0,pu1,pu2)-GetMin(pv0,pv1,pv2);
				if(tpen<pen&&tpen>0.0f)
				{
					pen=tpen;
					axis=13;
					cnAxis=testAxis;
				}
			}
			else
			{
				return;
			}
		}

		testAxis = tri1cb%tri2cb;
		testAxis.Normalize();
		if(testAxis.GetMagSquared()>0.0001)
		{
			pu0=tri1.m_VertA*testAxis;
			pu1=tri1.m_VertB*testAxis;
			pu2=tri1.m_VertC*testAxis;

			if(GetMin(pu0,pu1,pu2)<0.0f)
			{
				testAxis.invert();
				pu0=tri1.m_VertA*testAxis;
				pu1=tri1.m_VertB*testAxis;
				pu2=tri1.m_VertC*testAxis;
			}
			pv0=tri2.m_VertA*testAxis;
			pv1=tri2.m_VertB*testAxis;
			pv2=tri2.m_VertC*testAxis;

			if(GetMax(pv0,pv1,pv2)>GetMin(pu0,pu1,pu2))
			{
				tpen=GetMax(pv0,pv1,pv2)-GetMin(pu0,pu1,pu2);
				if(tpen<pen&&tpen>0.0f)
				{
					pen=tpen;
					axis=14;
					cnAxis=testAxis;
				}
			}
			else
			{
				return;
			}
			if(GetMin(pv0,pv1,pv2)<GetMax(pu0,pu1,pu2))
			{
				tpen=GetMax(pu0,pu1,pu2)-GetMin(pv0,pv1,pv2);
				if(tpen<pen&&tpen>0.0f)
				{
					pen=tpen;
					axis=15;
					cnAxis=testAxis;
				}
			}
			else
			{
				return;
			}
		}

		testAxis = tri1cb%tri2ac;
		testAxis.Normalize();
		if(testAxis.GetMagSquared()>0.0001)
		{
			pu0=tri1.m_VertA*testAxis;
			pu1=tri1.m_VertB*testAxis;
			pu2=tri1.m_VertC*testAxis;

			if(GetMin(pu0,pu1,pu2)<0.0f)
			{
				testAxis.invert();
				pu0=tri1.m_VertA*testAxis;
				pu1=tri1.m_VertB*testAxis;
				pu2=tri1.m_VertC*testAxis;
			}
			pv0=tri2.m_VertA*testAxis;
			pv1=tri2.m_VertB*testAxis;
			pv2=tri2.m_VertC*testAxis;

			if(GetMax(pv0,pv1,pv2)>GetMin(pu0,pu1,pu2))
			{
				tpen=GetMax(pv0,pv1,pv2)-GetMin(pu0,pu1,pu2);
				if(tpen<pen&&tpen>0.0f)
				{
					pen=tpen;
					axis=16;
					cnAxis=testAxis;
				}
			}
			else
			{
				return;
			}
			if(GetMin(pv0,pv1,pv2)<GetMax(pu0,pu1,pu2))
			{
				tpen=GetMax(pu0,pu1,pu2)-GetMin(pv0,pv1,pv2);
				if(tpen<pen&&tpen>0.0f)
				{
					pen=tpen;
					axis=17;
					cnAxis=testAxis;
				}
			}
			else
			{
				return;
			}
		}
		// set3
		testAxis = tri1ac%tri2ba;
		testAxis.Normalize();
		if(testAxis.GetMagSquared()>0.0001)
		{
			pu0=tri1.m_VertA*testAxis;
			pu1=tri1.m_VertB*testAxis;
			pu2=tri1.m_VertC*testAxis;

			if(GetMin(pu0,pu1,pu2)<0.0f)
			{
				testAxis.invert();
				pu0=tri1.m_VertA*testAxis;
				pu1=tri1.m_VertB*testAxis;
				pu2=tri1.m_VertC*testAxis;
			}
			pv0=tri2.m_VertA*testAxis;
			pv1=tri2.m_VertB*testAxis;
			pv2=tri2.m_VertC*testAxis;

			if(GetMax(pv0,pv1,pv2)>GetMin(pu0,pu1,pu2))
			{
				tpen=GetMax(pv0,pv1,pv2)-GetMin(pu0,pu1,pu2);
				if(tpen<pen&&tpen>0.0f)
				{
					pen=tpen;
					axis=18;
					cnAxis=testAxis;
				}
			}
			else
			{
				return;
			}
			if(GetMin(pv0,pv1,pv2)<GetMax(pu0,pu1,pu2))
			{
				tpen=GetMax(pu0,pu1,pu2)-GetMin(pv0,pv1,pv2);
				if(tpen<pen&&tpen>0.0f)
				{
					pen=tpen;
					axis=19;
					cnAxis=testAxis;
				}
			}
			else
			{
				return;
			}
		}

		testAxis = tri1ac%tri2cb;
		testAxis.Normalize();
		if(testAxis.GetMagSquared()>0.0001)
		{
			pu0=tri1.m_VertA*testAxis;
			pu1=tri1.m_VertB*testAxis;
			pu2=tri1.m_VertC*testAxis;

			if(GetMin(pu0,pu1,pu2)<0.0f)
			{
				testAxis.invert();
				pu0=tri1.m_VertA*testAxis;
				pu1=tri1.m_VertB*testAxis;
				pu2=tri1.m_VertC*testAxis;
			}
			pv0=tri2.m_VertA*testAxis;
			pv1=tri2.m_VertB*testAxis;
			pv2=tri2.m_VertC*testAxis;

			if(GetMax(pv0,pv1,pv2)>GetMin(pu0,pu1,pu2))
			{
				tpen=GetMax(pv0,pv1,pv2)-GetMin(pu0,pu1,pu2);
				if(tpen<pen&&tpen>0.0f)
				{
					pen=tpen;
					axis=20;
					cnAxis=testAxis;
				}
			}
			else
			{
				return;
			}
			if(GetMin(pv0,pv1,pv2)<GetMax(pu0,pu1,pu2))
			{
				tpen=GetMax(pu0,pu1,pu2)-GetMin(pv0,pv1,pv2);
				if(tpen<pen&&tpen>0.0f)
				{
					pen=tpen;
					axis=21;
					cnAxis=testAxis;
				}
			}
			else
			{
				return;
			}
		}

		testAxis = tri1ac%tri2ac;
		testAxis.Normalize();
		if(testAxis.GetMagSquared()>0.0001)
		{
			pu0=tri1.m_VertA*testAxis;
			pu1=tri1.m_VertB*testAxis;
			pu2=tri1.m_VertC*testAxis;

			if(GetMin(pu0,pu1,pu2)<0.0f)
			{
				testAxis.invert();
				pu0=tri1.m_VertA*testAxis;
				pu1=tri1.m_VertB*testAxis;
				pu2=tri1.m_VertC*testAxis;
			}
			pv0=tri2.m_VertA*testAxis;
			pv1=tri2.m_VertB*testAxis;
			pv2=tri2.m_VertC*testAxis;

			if(GetMax(pv0,pv1,pv2)>GetMin(pu0,pu1,pu2))
			{
				tpen=GetMax(pv0,pv1,pv2)-GetMin(pu0,pu1,pu2);
				if(tpen<pen&&tpen>0.0f)
				{
					pen=tpen;
					axis=22;
					cnAxis=testAxis;
				}
			}
			else
			{
				return;
			}
			if(GetMin(pv0,pv1,pv2)<GetMax(pu0,pu1,pu2))
			{
				tpen=GetMax(pu0,pu1,pu2)-GetMin(pv0,pv1,pv2);
				if(tpen<pen&&tpen>0.0f)
				{
					pen=tpen;
					axis=23;
					cnAxis=testAxis;
				}
			}
			else
			{
				return;
			}
		}

		if(axis<6)
		{
			if(axis<3)
			{
				DragonContact DC = DragonContact();
				DC.m_ContactPoint=cp;
				DC.m_ContactNormal = cnAxis;
				DC.mPenitrtionDepth=pen;
				DC.m_Entities[0]=obj2;
				DC.m_Entities[1]=obj1;
				DC.mRestitution=m_Restitution;
				contacts.Add(std::move(DC));
				return;
			}
			else
			{
				DragonContact DC = DragonContact();
				DC.m_ContactPoint=cp;
				DC.m_ContactNormal = cnAxis;
				DC.mPenitrtionDepth=pen;
				DC.m_Entities[0]=obj1;
				DC.m_Entities[1]=obj2;
				DC.mRestitution=m_Restitution;
				contacts.Add(std::move(DC));
				return;
			}
		}
		else if(axis>=6&&axis<=11)
		{
			cp=TriEdgePointOfContact(tri1.m_VertA,tri1.m_VertB,tri1.m_VertC,tri2.m_VertB,tri2.m_VertA);
			DragonContact DC = DragonContact();
			DC.m_ContactPoint=cp;
			DC.m_ContactNormal = cnAxis;
			DC.mPenitrtionDepth=pen;
			if(axis==7||axis==9||axis==11)
			{
				DC.m_Entities[0]=obj2;
				DC.m_Entities[1]=obj1;
				DC.mRestitution=m_Restitution;
			}
			else
			{
				DC.m_Entities[0]=obj1;
				DC.m_Entities[1]=obj2;
				DC.mRestitution=m_Restitution;
			}
			contacts.Add(std::move(DC));
			return;
		}
		else if(axis>=12&&axis<=17)
		{
			cp=TriEdgePointOfContact(tri1.m_VertB,tri1.m_VertC,tri2.m_VertC,tri2.m_VertB,tri2.m_VertA);
			DragonContact DC = DragonContact();
			DC.m_ContactPoint=cp;
			DC.m_ContactNormal = cnAxis;
			DC.mPenitrtionDepth=pen;
			if(axis==13||axis==15||axis==17)
			{
				DC.m_Entities[0]=obj2;
				DC.m_Entities[1]=obj1;
				DC.mRestitution=m_Restitution;
			}
			else
			{
				DC.m_Entities[0]=obj1;
				DC.m_Entities[1]=obj2;
				DC.mRestitution=m_Restitution;
			}
			contacts.Add(std::move(DC));
			return;
		}
		else if(axis>=18&&axis<=23)
		{
			cp=TriEdgePointOfContact(tri1.m_VertC,tri1.m_VertA,tri2.m_VertC,tri2.m_VertB,tri2.m_VertA);
			DragonContact DC = DragonContact();
			DC.m_ContactPoint=cp;
			DC.m_ContactNormal = cnAxis;
			DC.mPenitrtionDepth=pen;
			if(axis==19||axis==21||axis==23)
			{
				DC.m_Entities[0]=obj2;
				DC.m_Entities[1]=obj1;
				DC.mRestitution=m_Restitution;
			}
			else
			{
				DC.m_Entities[0]=obj1;
				DC.m_Entities[1]=obj2;
				DC.mRestitution=m_Restitution;
			}
			contacts.Add(std::move(DC));
			return;
		}
	}
	inline void GenContactTriPlane(const DragonTriangle& tri,const DragonXVector3& plNorm,const real& plDist,BaseDragonPhysicsObject* mesh,BaseDragonPhysicsObject* plane,DragonContactContainer& contacts)
	{
		real vertexDist = tri.m_VertA * plNorm;
		if(vertexDist <= plDist)
		{
			DragonContact DC = DragonContact();
			DC.m_ContactPoint=plNorm;
			DC.m_ContactPoint*=(vertexDist-plDist);
			DC.m_ContactPoint+=tri.m_VertA;
			DC.m_ContactNormal = plNorm;
			DC.mPenitrtionDepth=plDist-vertexDist;
			DC.m_Entities[0]=mesh;
			DC.m_Entities[1]=plane;
			DC.mRestitution=m_Restitution;
			contacts.Add(std::move(DC));
		}
		vertexDist = tri.m_VertB * plNorm;
		if(vertexDist <= plDist)
		{
			DragonContact DC = DragonContact();
			DC.m_ContactPoint=plNorm;
			DC.m_ContactPoint*=(vertexDist-plDist);
			DC.m_ContactPoint+=tri.m_VertB;
			DC.m_ContactNormal = plNorm;
			DC.mPenitrtionDepth=plDist-vertexDist;
			DC.m_Entities[0]=mesh;
			DC.m_Entities[1]=plane;
			DC.mRestitution=m_Restitution;
			contacts.Add(std::move(DC));
		}
		vertexDist = tri.m_VertC * plNorm;
		if(vertexDist <= plDist)
		{
			DragonContact DC = DragonContact();
			DC.m_ContactPoint=plNorm;
			DC.m_ContactPoint*=(vertexDist-plDist);
			DC.m_ContactPoint+=tri.m_VertC;
			DC.m_ContactNormal = plNorm;
			DC.mPenitrtionDepth=plDist-vertexDist;
			DC.m_Entities[0]=mesh;
			DC.m_Entities[1]=plane;
			DC.mRestitution=m_Restitution;
			contacts.Add(std::move(DC));
		}
	}
private:
	inline void GenContactSphereSphere(BaseDragonPhysicsObject* obj1,BaseDragonPhysicsObject* obj2,DragonContactContainer& contacts)
	{
		DragonContact DC = DragonContact();
		DragonXVector3 midLine(obj1->m_Position - obj2->m_Position);
		real size = midLine.GetMagnitude();
		DC.m_ContactPoint=obj1->m_Position +midLine * 0.5f;
		DC.m_ContactNormal= midLine;
		DC.m_ContactNormal.Normalize();
		DC.mPenitrtionDepth=obj1->GetRadius()+obj2->GetRadius() - size;
		DC.m_Entities[0]=obj1;
		DC.m_Entities[1]=obj2;
		DC.mRestitution=m_Restitution;
		contacts.Add(std::move(DC));
	}
	inline void GenContactSpherePlane(BaseDragonPhysicsObject* obj1,BaseDragonPhysicsObject* obj2,DragonContactContainer& contacts)
	{
		if(obj1->mI_PrimitiveType==PT_Sphere)
		{
			DragonPhysicsPlane* colObj;
			colObj = static_cast<DragonPhysicsPlane*>(obj2->m_CollisionObject);
			DragonContact DC = DragonContact();
			real centerDist = colObj->mNormalDirection* obj1->m_Position - colObj->mDistFromOrigin;
			DragonXVector3 normal = colObj->mNormalDirection;
			real penetration = -centerDist;
			if(centerDist < 0)
			{
				normal.invert();
				penetration = -penetration;
			}
			penetration+=obj1->GetRadius();
			// fill out contact data
			DC.m_ContactNormal=normal;
			DC.mPenitrtionDepth=penetration;
			DC.m_ContactPoint = obj1->m_Position - colObj->mNormalDirection * centerDist;
			DC.m_Entities[0]=obj1;
			DC.m_Entities[1]=obj2;
			DC.mRestitution=m_Restitution;
			contacts.Add(std::move(DC));
		}
		else
		{
			DragonPhysicsPlane* colObj;
			colObj = static_cast<DragonPhysicsPlane*>(obj1->m_CollisionObject);
			DragonContact DC = DragonContact();
			real centerDist = colObj->mNormalDirection* obj2->m_Position - colObj->mDistFromOrigin;
			DragonXVector3 normal = colObj->mNormalDirection;
			real penetration = -centerDist;
			if(centerDist < 0)
			{
				normal.invert();
				penetration = -penetration;
			}
			penetration+=obj2->GetRadius();
			// fill out contact data
			DC.m_ContactNormal=normal;
			DC.mPenitrtionDepth=penetration;
			DC.m_ContactPoint = obj2->m_Position - colObj->mNormalDirection * centerDist;
			DC.m_Entities[0]=obj2;
			DC.m_Entities[1]=obj1;
			DC.mRestitution=m_Restitution;
			contacts.Add(std::move(DC));
		}
	}
	inline void GenContactSphereObb(BaseDragonPhysicsObject* obj1,BaseDragonPhysicsObject* obj2,DragonContactContainer& contacts)
	{
		if(obj1->mI_PrimitiveType==PT_Sphere)//if object 1 is the sphere
		{
			DragonPhysicsCollisionOBB* colObj1;
			colObj1=static_cast<DragonPhysicsCollisionOBB*>(obj2->m_CollisionObject);
			DragonXVector3 halfSizes = colObj1->halfExtents;
			DragonXVector3 center = obj1->m_Position;
			DragonXVector3 relCenter = obj2->TransformMatrix.TransformInverse(center);
			DragonXVector3 closetsPt(0.0f,0.0f,0.0f);
			real dist;
			// clamp each coordinate to the box
			dist = relCenter.x;
			if(dist > halfSizes.x)
			{
				dist = halfSizes.x;
			}
			if(dist < -halfSizes.x)
			{
				dist = -halfSizes.x;
			}
			closetsPt.x = dist;

			dist = relCenter.y;
			if(dist > halfSizes.y)
			{
				dist = halfSizes.y;
			}
			if(dist < -halfSizes.y)
			{
				dist = -halfSizes.y;
			}
			closetsPt.y = dist;

			dist = relCenter.z;
			if(dist > halfSizes.z)
			{
				dist = halfSizes.z;
			}
			if(dist < -halfSizes.z)
			{
				dist = -halfSizes.z;
			}
			closetsPt.z = dist;

			//check we are in contact last change for out
			dist = (closetsPt - relCenter).GetMagSquared();

			if(dist > obj1->GetRadius() * obj1->GetRadius())
			{
				return;
			}
			else
			{
				DragonContact DC = DragonContact();
				DragonXVector3 closestPtWorld = obj2->TransformMatrix.transform(closetsPt);
				DC.m_ContactNormal=(closestPtWorld - obj1->m_Position);
				DC.m_ContactNormal.Normalize();
				DC.m_ContactPoint=closestPtWorld;
				DC.mPenitrtionDepth=obj1->GetRadius()-sqrt(dist);
				DC.m_Entities[0]=obj1;
				DC.m_Entities[1]=obj2;
				DC.mRestitution=m_Restitution;
				contacts.Add(std::move(DC));
			}
		}
		else //obj2 is the sphere and obj1 is the obb
		{
			DragonPhysicsCollisionOBB* colObj1;
			colObj1=static_cast<DragonPhysicsCollisionOBB*>(obj1->m_CollisionObject);
			DragonXVector3 halfSizes = colObj1->halfExtents;
			DragonXVector3 center = obj2->m_Position;
			DragonXVector3 relCenter = obj1->TransformMatrix.TransformInverse(center);
			DragonXVector3 closetsPt(0.0f,0.0f,0.0f);
			real dist;
			// clamp each coordinate to the box
			dist = relCenter.x;
			if(dist > halfSizes.x)
			{
				dist = halfSizes.x;
			}
			if(dist < -halfSizes.x)
			{
				dist = -halfSizes.x;
			}
			closetsPt.x = dist;

			dist = relCenter.y;
			if(dist > halfSizes.y)
			{
				dist = halfSizes.y;
			}
			if(dist < -halfSizes.y)
			{
				dist = -halfSizes.y;
			}
			closetsPt.y = dist;

			dist = relCenter.z;
			if(dist > halfSizes.z)
			{
				dist = halfSizes.z;
			}
			if(dist < -halfSizes.z)
			{
				dist = -halfSizes.z;
			}
			closetsPt.z = dist;

			//check we are in contact last change for out
			dist = (closetsPt - relCenter).GetMagSquared();

			if(dist > obj2->GetRadius() * obj2->GetRadius())
			{
				return;
			}
			else
			{
				DragonContact DC = DragonContact();
				DragonXVector3 closestPtWorld = obj1->TransformMatrix.transform(closetsPt);
				DC.m_ContactNormal=(closestPtWorld - obj2->m_Position);
				DC.m_ContactNormal.Normalize();
				DC.m_ContactPoint=closestPtWorld;
				DC.mPenitrtionDepth=obj2->GetRadius()-sqrt(dist);
				DC.m_Entities[0]=obj1;
				DC.m_Entities[1]=obj2;
				DC.mRestitution=m_Restitution;
				contacts.Add(std::move(DC));
			}
		}
	}
	inline void GenContactObbPlane(BaseDragonPhysicsObject* obj1,BaseDragonPhysicsObject* obj2,DragonContactContainer& contacts)
	{
		real mults[8][3] = {{1,1,1},{-1,1,1},{1,-1,1},{-1,-1,1},
		{1,1,-1},{-1,1,-1},{1,-1,-1},{-1,-1,-1}};

		if(obj1->mI_PrimitiveType==PT_OBB)//if obj1 is an obb
		{
			DragonPhysicsCollisionOBB* colObj1;
			colObj1=static_cast<DragonPhysicsCollisionOBB*>(obj1->m_CollisionObject);

			DragonPhysicsPlane* colObj2;
			colObj2 = static_cast<DragonPhysicsPlane*>(obj2->m_CollisionObject);

			for(unsigned i = 0; i < 8; i++)
			{
				DragonXVector3 vertexPos(mults[i][0],mults[i][1],mults[i][2]);
				vertexPos.componentProductUpdate(colObj1->halfExtents);
				vertexPos = obj1->TransformMatrix.transform(vertexPos);

				real vertexDist = vertexPos * colObj2->mNormalDirection;

				if(vertexDist <= colObj2->mDistFromOrigin)
				{
					DragonContact DC = DragonContact();
					DC.m_ContactPoint=colObj2->mNormalDirection;
					DC.m_ContactPoint*=(vertexDist-colObj2->mDistFromOrigin);
					DC.m_ContactPoint+=vertexPos;
					DC.m_ContactNormal = colObj2->mNormalDirection;
					DC.mPenitrtionDepth=colObj2->mDistFromOrigin-vertexDist;
					DC.m_Entities[0]=obj1;
					DC.m_Entities[1]=obj2;
					DC.mRestitution=m_Restitution;
					contacts.Add(std::move(DC));
				}
			}
		}
		else//otherwise obj2 is the obb
		{
			DragonPhysicsCollisionOBB* colObj1;
			colObj1=static_cast<DragonPhysicsCollisionOBB*>(obj2->m_CollisionObject);

			DragonPhysicsPlane* colObj2;
			colObj2 = static_cast<DragonPhysicsPlane*>(obj1->m_CollisionObject);

			for(unsigned i = 0; i < 8; i++)
			{
				DragonXVector3 vertexPos(mults[i][0],mults[i][1],mults[i][2]);
				vertexPos.componentProductUpdate(colObj1->halfExtents);
				vertexPos = obj2->TransformMatrix.transform(vertexPos);

				real vertexDist = vertexPos * colObj2->mNormalDirection;

				if(vertexDist <= colObj2->mDistFromOrigin)
				{
					DragonContact DC = DragonContact();
					DC.m_ContactPoint=colObj2->mNormalDirection;
					DC.m_ContactPoint*=(vertexDist-colObj2->mDistFromOrigin);
					DC.m_ContactPoint+=vertexPos;
					DC.m_ContactNormal = colObj2->mNormalDirection;
					DC.mPenitrtionDepth=colObj2->mDistFromOrigin-vertexDist;
					DC.m_Entities[0]=obj2;
					DC.m_Entities[1]=obj1;
					DC.mRestitution=m_Restitution;
					contacts.Add(std::move(DC));
				}
			}
		}
	}
	inline void GenContactObbObb(BaseDragonPhysicsObject* obj1,BaseDragonPhysicsObject* obj2,DragonContactContainer& contacts)
	{
		DragonXVector3 toCentre = obj2->m_Position-obj1->m_Position;
		real pen = REAL_MAX;
		unsigned best = 0xffffff;
		DragonPhysicsCollisionOBB* colObj1;
		colObj1=static_cast<DragonPhysicsCollisionOBB*>(obj1->m_CollisionObject);
		DragonPhysicsCollisionOBB* colObj2;
		colObj2=static_cast<DragonPhysicsCollisionOBB*>(obj2->m_CollisionObject);
		DragonXVector3 o1av0 = obj1->TransformMatrix.getAxisVector(0);
		DragonXVector3 o1av1 = obj1->TransformMatrix.getAxisVector(1);
		DragonXVector3 o1av2 = obj1->TransformMatrix.getAxisVector(2);
		DragonXVector3 o2av0 = obj2->TransformMatrix.getAxisVector(0);
		DragonXVector3 o2av1 = obj2->TransformMatrix.getAxisVector(1);
		DragonXVector3 o2av2 = obj2->TransformMatrix.getAxisVector(2);

		if(!tryAxis(colObj1->halfExtents,o1av0,o1av1,o1av2,colObj2->halfExtents,o2av0,o2av1,o2av2,toCentre,o1av0,0,pen,best))
		{
			return;
		}
		if(!tryAxis(colObj1->halfExtents,o1av0,o1av1,o1av2,colObj2->halfExtents,o2av0,o2av1,o2av2,toCentre,o1av1,1,pen,best))
		{
			return;
		}
		if(!tryAxis(colObj1->halfExtents,o1av0,o1av1,o1av2,colObj2->halfExtents,o2av0,o2av1,o2av2,toCentre,o1av2,2,pen,best))
		{
			return;
		}

		if(!tryAxis(colObj1->halfExtents,o1av0,o1av1,o1av2,colObj2->halfExtents,o2av0,o2av1,o2av2,toCentre,o2av0,3,pen,best))
		{
			return;
		}
		if(!tryAxis(colObj1->halfExtents,o1av0,o1av1,o1av2,colObj2->halfExtents,o2av0,o2av1,o2av2,toCentre,o2av1,4,pen,best))
		{
			return;
		}
		if(!tryAxis(colObj1->halfExtents,o1av0,o1av1,o1av2,colObj2->halfExtents,o2av0,o2av1,o2av2,toCentre,o2av2,5,pen,best))
		{
			return;
		}
		unsigned bestSingleAxis = best;

		if(!tryAxis(colObj1->halfExtents,o1av0,o1av1,o1av2,colObj2->halfExtents,o2av0,o2av1,o2av2,toCentre,o1av0%o2av0,6,pen,best))
		{
			return;
		}
		if(!tryAxis(colObj1->halfExtents,o1av0,o1av1,o1av2,colObj2->halfExtents,o2av0,o2av1,o2av2,toCentre,o1av0%o2av1,7,pen,best))
		{
			return;
		}
		if(!tryAxis(colObj1->halfExtents,o1av0,o1av1,o1av2,colObj2->halfExtents,o2av0,o2av1,o2av2,toCentre,o1av0%o2av2,8,pen,best))
		{
			return;
		}
		if(!tryAxis(colObj1->halfExtents,o1av0,o1av1,o1av2,colObj2->halfExtents,o2av0,o2av1,o2av2,toCentre,o1av1%o2av0,9,pen,best))
		{
			return;
		}
		if(!tryAxis(colObj1->halfExtents,o1av0,o1av1,o1av2,colObj2->halfExtents,o2av0,o2av1,o2av2,toCentre,o1av1%o2av1,10,pen,best))
		{
			return;
		}
		if(!tryAxis(colObj1->halfExtents,o1av0,o1av1,o1av2,colObj2->halfExtents,o2av0,o2av1,o2av2,toCentre,o1av1%o2av2,11,pen,best))
		{
			return;
		}
		if(!tryAxis(colObj1->halfExtents,o1av0,o1av1,o1av2,colObj2->halfExtents,o2av0,o2av1,o2av2,toCentre,o1av2%o2av0,12,pen,best))
		{
			return;
		}
		if(!tryAxis(colObj1->halfExtents,o1av0,o1av1,o1av2,colObj2->halfExtents,o2av0,o2av1,o2av2,toCentre,o1av2%o2av1,13,pen,best))
		{
			return;
		}
		if(!tryAxis(colObj1->halfExtents,o1av0,o1av1,o1av2,colObj2->halfExtents,o2av0,o2av1,o2av2,toCentre,o1av2%o2av2,14,pen,best))
		{
			return;
		}

		if (best < 3)
		{
			fillPointFaceBoxBox(obj1, obj2, toCentre,best, pen,colObj2->halfExtents,o2av0,o2av1,o2av2,contacts);
		}
		else if (best < 6)
		{
			fillPointFaceBoxBox(obj2,obj1, toCentre*-1.0f, best-3, pen,colObj1->halfExtents,o1av0,o1av1,o1av2,contacts);
		}
		else
		{
			// We've got an edge-edge contact. Find out which axes
			best -= 6;
			unsigned oneAxisIndex = best / 3;
			unsigned twoAxisIndex = best % 3;
			DragonXVector3 oneAxis=obj1->TransformMatrix.getAxisVector(oneAxisIndex);
			DragonXVector3 twoAxis=obj2->TransformMatrix.getAxisVector(twoAxisIndex);
			DragonXVector3 axis = oneAxis % twoAxis;
			axis.Normalize();

			if (axis * toCentre > 0)
			{
				axis = axis * -1.0f;
			}

			DragonXVector3 ptOnOneEdge = colObj1->halfExtents;
			DragonXVector3 ptOnTwoEdge = colObj2->halfExtents;
			for (unsigned i = 0; i < 3; i++)
			{
				if (i == oneAxisIndex)
				{
					ptOnOneEdge[i] = 0;
				}
				else if (obj1->TransformMatrix.getAxisVector(i) * axis > 0)
				{
					ptOnOneEdge[i] = -ptOnOneEdge[i];
				}

				if (i == twoAxisIndex)
				{
					ptOnTwoEdge[i] = 0;
				}
				else if (obj2->TransformMatrix.getAxisVector(i) * axis < 0)
				{
					ptOnTwoEdge[i] = -ptOnTwoEdge[i];
				}
			}

			ptOnOneEdge = obj1->TransformMatrix * ptOnOneEdge;
			ptOnTwoEdge = obj2->TransformMatrix * ptOnTwoEdge;

			DragonXVector3 vertex = contactPoint(ptOnOneEdge, oneAxis, colObj1->halfExtents[oneAxisIndex],ptOnTwoEdge, twoAxis, colObj2->halfExtents[twoAxisIndex],bestSingleAxis > 2);

			// We can fill the contact.
			DragonContact DC = DragonContact();
			DC.mPenitrtionDepth = pen;
			DC.m_ContactNormal = axis;
			DC.m_ContactPoint = vertex;
			DC.m_Entities[0]=obj1;
			DC.m_Entities[1]=obj2;
			DC.mRestitution=m_Restitution;
			contacts.Add(std::move(DC));
		}
	}
	inline void GenContactSphereMesh(BaseDragonPhysicsObject* obj1,BaseDragonPhysicsObject* obj2,DragonContactContainer& contacts)
	{
		if(obj1->mI_PrimitiveType==PT_Mesh)// if obj1 is the mesh
		{
			DragonMeshCollisionObject* colObj1;
			colObj1=static_cast<DragonMeshCollisionObject*>(obj1->m_CollisionObject);
			for(auto sector : colObj1->m_DragonSectors)
			{
				if(SphereSphereIntersectTest(obj1->getPointInWorldSpace(sector->m_Position),sector->m_Radius,obj2->m_Position,obj2->GetRadius()))
				{
					for(auto tri : sector->m_DragonTris)
					{
						DragonTriangle tempW(obj1->getPointInWorldSpace(tri->m_VertA),obj1->getPointInWorldSpace(tri->m_VertB),obj1->getPointInWorldSpace(tri->m_VertC));
						GenContactSphereTri(obj2,obj1,tempW,contacts);
					}
				}
			}
			return;
		}
		else// obj2 is mesh
		{
			DragonMeshCollisionObject* colObj1;
			colObj1=static_cast<DragonMeshCollisionObject*>(obj2->m_CollisionObject);
			for(auto sector : colObj1->m_DragonSectors)
			{
				if(SphereSphereIntersectTest(obj2->getPointInWorldSpace(sector->m_Position),sector->m_Radius,obj1->m_Position,obj1->GetRadius()))
				{
					for(auto tri : sector->m_DragonTris)
					{
						DragonTriangle tempW(obj2->getPointInWorldSpace(tri->m_VertA),obj2->getPointInWorldSpace(tri->m_VertB),obj2->getPointInWorldSpace(tri->m_VertC));
						GenContactSphereTri(obj1,obj2,tempW,contacts);
					}
				}
			}
			return;
		}
	}
	inline void GenContactObbMesh(BaseDragonPhysicsObject* obj1,BaseDragonPhysicsObject* obj2,DragonContactContainer& contacts)
	{
		if(obj1->mI_PrimitiveType==PT_Mesh)
		{
			DragonMeshCollisionObject* colObj1;
			colObj1=static_cast<DragonMeshCollisionObject*>(obj1->m_CollisionObject);
			DragonPhysicsCollisionOBB* colObj2;
			colObj2=static_cast<DragonPhysicsCollisionOBB*>(obj2->m_CollisionObject);
			for (auto sector : colObj1->m_DragonSectors)
			{
				float rad = obj2->GetRadius()+sector->m_Radius;
				if (((obj1->TransformMatrix.transform(sector->m_Position)-obj2->m_Position).GetMagSquared())<rad*rad)
				{
					DragonXVector3 obbAxis0 = obj2->TransformMatrix.getAxisVector(0);
					DragonXVector3 obbAxis1 = obj2->TransformMatrix.getAxisVector(1);
					DragonXVector3 obbAxis2 = obj2->TransformMatrix.getAxisVector(2);
					for (auto tri : sector->m_DragonTris)
					{
						DragonTriangle temp(obj1->TransformMatrix.transform(tri->m_VertA),obj1->TransformMatrix.transform(tri->m_VertB),obj1->TransformMatrix.transform(tri->m_VertC));
						if (SphereTriangleCollisionTest(temp,*obj2))
						{
							DragonTriangle templ(obj2->getPointInLocalSpace(temp.m_VertA),obj2->getPointInLocalSpace(temp.m_VertB),obj2->getPointInLocalSpace(temp.m_VertC));
							GenContactObbTri(templ,obj2,obj1,colObj2->halfExtents,obbAxis0,obbAxis1,obbAxis2,contacts);
						}
					}
				}
			}
			return;
		}
		else
		{
			DragonMeshCollisionObject* colObj1;
			colObj1=static_cast<DragonMeshCollisionObject*>(obj2->m_CollisionObject);
			DragonPhysicsCollisionOBB* colObj2;
			colObj2=static_cast<DragonPhysicsCollisionOBB*>(obj1->m_CollisionObject);
			for (auto sector : colObj1->m_DragonSectors)
			{
				float rad = obj1->GetRadius()+sector->m_Radius;
				if (((obj2->TransformMatrix.transform(sector->m_Position)-obj1->m_Position).GetMagSquared())<rad*rad)
				{
					DragonXVector3 obbAxis0 = obj1->TransformMatrix.getAxisVector(0);
					DragonXVector3 obbAxis1 = obj1->TransformMatrix.getAxisVector(1);
					DragonXVector3 obbAxis2 = obj1->TransformMatrix.getAxisVector(2);
					for (auto tri : sector->m_DragonTris)
					{
						DragonTriangle temp(obj2->TransformMatrix.transform(tri->m_VertA),obj2->TransformMatrix.transform(tri->m_VertB),obj2->TransformMatrix.transform(tri->m_VertC));
						if (SphereTriangleCollisionTest(temp,*obj1))
						{
							DragonTriangle templ(obj1->getPointInLocalSpace(temp.m_VertA),obj1->getPointInLocalSpace(temp.m_VertB),obj1->getPointInLocalSpace(temp.m_VertC));
							GenContactObbTri(templ,obj1,obj2,colObj2->halfExtents,obbAxis0,obbAxis1,obbAxis2,contacts);
						}
					}
				}
			}
			return;
		}
	}
	inline void GenContactMeshMesh(BaseDragonPhysicsObject* obj1,BaseDragonPhysicsObject* obj2,DragonContactContainer& contacts)
	{
		DragonMeshCollisionObject* colObj1;
		colObj1=static_cast<DragonMeshCollisionObject*>(obj1->m_CollisionObject);
		DragonMeshCollisionObject* colObj2;
		colObj2=static_cast<DragonMeshCollisionObject*>(obj2->m_CollisionObject);
		for (auto sector : colObj1->m_DragonSectors)
		{
			float rad = sector->m_Radius;
			DragonXVector3 secWpos = obj1->TransformMatrix.transform(sector->m_Position);
			for(auto sektor : colObj2->m_DragonSectors)
			{
				float rads = rad + sektor->m_Radius;
				if((secWpos-obj2->getPointInWorldSpace(sektor->m_Position)).GetMagSquared()<rads*rads)
				{
					for (auto tris1 : sector->m_DragonTris)
					{
						DragonTriangle temp(obj1->getPointInWorldSpace(tris1->m_VertA),obj1->getPointInWorldSpace(tris1->m_VertB),obj1->getPointInWorldSpace(tris1->m_VertC));
						DragonXVector3 tri1Norm = GetTriNormal(temp.m_VertA,temp.m_VertB,temp.m_VertC);
						for(auto tris2 : sektor->m_DragonTris)
						{
							DragonTriangle temp2(obj2->getPointInWorldSpace(tris2->m_VertA),obj2->getPointInWorldSpace(tris2->m_VertB),obj2->getPointInWorldSpace(tris2->m_VertC));
							DragonXVector3 tri2Norm = GetTriNormal(temp2.m_VertA,temp2.m_VertB,temp2.m_VertC);
							GenContactTriTri(temp,tri1Norm,temp2,tri2Norm,obj1,obj2,contacts);
						}
					}
				}
			}
		}
		return;
	}
	inline void GenContactMeshPlane(BaseDragonPhysicsObject* obj1,BaseDragonPhysicsObject* obj2,DragonContactContainer& contacts)
	{
		if(obj1->mI_PrimitiveType==PT_Mesh)
		{
			DragonMeshCollisionObject* colObj1;
			colObj1=static_cast<DragonMeshCollisionObject*>(obj1->m_CollisionObject);
			DragonPhysicsPlane* colObj2;
			colObj2=static_cast<DragonPhysicsPlane*>(obj2->m_CollisionObject);
			for (auto sector : colObj1->m_DragonSectors)
			{
				float rad = sector->m_Radius;
				DragonXVector3 secWpos = obj1->TransformMatrix.transform(sector->m_Position);
				if (colObj2->mNormalDirection*secWpos-rad<=colObj2->mDistFromOrigin)
				{
					for (auto tri : sector->m_DragonTris)
					{
						DragonTriangle temp(obj1->TransformMatrix.transform(tri->m_VertA),obj1->TransformMatrix.transform(tri->m_VertB),obj1->TransformMatrix.transform(tri->m_VertC));
						GenContactTriPlane(temp,colObj2->mNormalDirection,colObj2->mDistFromOrigin,obj1,obj2,contacts);
					}
				}
			}
			return;
		}
		else
		{
			DragonMeshCollisionObject* colObj1;
			colObj1=static_cast<DragonMeshCollisionObject*>(obj2->m_CollisionObject);
			DragonPhysicsPlane* colObj2;
			colObj2=static_cast<DragonPhysicsPlane*>(obj1->m_CollisionObject);
			for (auto sector : colObj1->m_DragonSectors)
			{
				float rad = sector->m_Radius;
				DragonXVector3 secWpos = obj2->TransformMatrix.transform(sector->m_Position);
				if (colObj2->mNormalDirection*secWpos-rad<=colObj2->mDistFromOrigin)
				{
					for (auto tri : sector->m_DragonTris)
					{
						DragonTriangle temp(obj2->TransformMatrix.transform(tri->m_VertA),obj2->TransformMatrix.transform(tri->m_VertB),obj2->TransformMatrix.transform(tri->m_VertC));
						GenContactTriPlane(temp,colObj2->mNormalDirection,colObj2->mDistFromOrigin,obj2,obj1,contacts);
					}
				}
			}
			return;
		}
	}
public:
	real m_Restitution;
public:
	DragonContactGenerator();
	~DragonContactGenerator();
	void GenerateContactsForPairs(const DragonPairContainer& vecOfPairs,DragonContactContainer& constraintContacts);
};