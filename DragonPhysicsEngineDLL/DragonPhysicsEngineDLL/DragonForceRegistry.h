#pragma once
#include <map>
#include <vector>
#include "DragonForceGenerator.h"

class DragonForceRegistry
{
private:

public:
	int mNxtID;
	std::map<int,DragonForceGenerator> m_ForceGenRegistry;
	std::map<int,std::vector<int>> m_ForceApplicationRegistry;
public:
	DragonForceRegistry();
	~DragonForceRegistry();
};