#include "DragonContactGenerator.h"
#include "DragonEnums.h"

DragonContactGenerator::DragonContactGenerator()
{
	m_Restitution=0.0f;
}
DragonContactGenerator::~DragonContactGenerator()
{
}
void DragonContactGenerator::GenerateContactsForPairs(const DragonPairContainer& vecOfPairs,DragonContactContainer& constraintContacts)
{
	for(auto &cPair : vecOfPairs.mVecConPairs)
	{
		if(!cPair.mVailidData)
		{
			return;
		}
		if(cPair.Obj1->m_isCollisionResolutionOn&&cPair.Obj2->m_isCollisionResolutionOn)
		{
			if(!cPair.Obj1->m_PhysicsEnabled&&!cPair.Obj2->m_PhysicsEnabled)
			{
				break;
			}
			else
			{
				switch(cPair.mContactType)
				{
				case CT_SphereSphere:
					{
						GenContactSphereSphere(cPair.Obj1,cPair.Obj2,constraintContacts);
						break;
					}
				case CT_SpherePlane:
					{
						GenContactSpherePlane(cPair.Obj1,cPair.Obj2,constraintContacts);
						break;
					}
				case CT_SphereOBB:
					{
						GenContactSphereObb(cPair.Obj1,cPair.Obj2,constraintContacts);
						break;
					}
				case CT_ObbPlane:
					{
						GenContactObbPlane(cPair.Obj1,cPair.Obj2,constraintContacts);
						break;
					}
				case CT_ObbObb:
					{
						GenContactObbObb(cPair.Obj1,cPair.Obj2,constraintContacts);
						break;
					}
				case CT_MeshSphere:
					{
						GenContactSphereMesh(cPair.Obj1,cPair.Obj2,constraintContacts);
						break;
					}
				case CT_MeshOBB:
					{
						GenContactObbMesh(cPair.Obj1,cPair.Obj2,constraintContacts);
						break;
					}
				case CT_MeshMesh:
					{
						GenContactMeshMesh(cPair.Obj1,cPair.Obj2,constraintContacts);
						break;
					}
				case CT_MeshPlane:
					{
						GenContactMeshPlane(cPair.Obj1,cPair.Obj2,constraintContacts);
						break;
					}
				default:
					{
						break;
					}
				}
			}
		}
	}
}