#include "DragonContact.h"

DragonContact::DragonContact()
{
	m_Entities[0]=nullptr;
	m_Entities[1]=nullptr;
	mRestitution=0.0f;
	mFriction=1.0f;
	DragonXMatrix m;
	D3DXMatrixIdentity(&m);
	mContactToWorld=m;
	mValidData=true;
}
DragonContact::~DragonContact()
{
}