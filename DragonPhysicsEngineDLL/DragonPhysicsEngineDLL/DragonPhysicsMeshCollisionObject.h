#pragma once
#include "DragonXMath.h"
#include "DragonPhysicsOBB.h"
#include "DragonEnums.h"
#include <vector>

class DragonTriangle
{
private:

public:
	real m_MaxX;
	real m_MinX;
	real m_MaxY;
	real m_MinY;
	real m_MaxZ;
	real m_MinZ;
	DragonXVector3 m_VertA;
	DragonXVector3 m_VertB;
	DragonXVector3 m_VertC;
public:
	DragonTriangle(DragonXVector3& a,DragonXVector3& b,DragonXVector3& c)
	{
		m_VertA=a;
		m_VertB=b;
		m_VertC=c;
		m_MaxX=m_VertA.x;
		if(m_VertB.x>m_MaxX)
		{
			m_MaxX=m_VertB.x;
		}
		if (m_VertC.x>m_MaxX)
		{
			m_MaxX=m_VertC.x;
		}
		// set up min x
		m_MinX=m_VertA.x;
		if(m_VertB.x<m_MinX)
		{
			m_MinX=m_VertB.x;
		}
		if (m_VertC.x<m_MinX)
		{
			m_MinX=m_VertC.x;
		}
		// set up max Y
		m_MaxY=m_VertA.y;
		if(m_VertB.y>m_MaxY)
		{
			m_MaxY=m_VertB.y;
		}
		if (m_VertC.y>m_MaxY)
		{
			m_MaxY=m_VertC.y;
		}
		// set up min y
		m_MinY=m_VertA.y;
		if(m_VertB.y<m_MinY)
		{
			m_MinY=m_VertB.y;
		}
		if (m_VertC.y<m_MinY)
		{
			m_MinY=m_VertC.y;
		}
		// set up max Z
		m_MaxZ=m_VertA.z;
		if(m_VertB.z>m_MaxZ)
		{
			m_MaxZ=m_VertB.z;
		}
		if (m_VertC.z>m_MaxZ)
		{
			m_MaxZ=m_VertC.z;
		}
		// set up min z
		m_MinZ=m_VertA.z;
		if(m_VertB.z<m_MinZ)
		{
			m_MinZ=m_VertB.z;
		}
		if (m_VertC.z<m_MinZ)
		{
			m_MinZ=m_VertC.z;
		}
	}
	~DragonTriangle()
	{
	}
	inline real GetMaxX()
	{
		return m_MaxX;
	}
	inline real GetMinX()
	{
		return m_MinX;
	}
	inline real GetMaxY()
	{
		return m_MaxY;
	}
	inline real GetMinY()
	{
		return m_MinY;
	}
	inline real GetMaxZ()
	{
		return m_MaxZ;
	}
	inline real GetMinZ()
	{
		return m_MinZ;
	}
};

class DragonSector
{
private:

public:
	DragonXVector3 m_Position;
	real m_Radius;
	std::vector<DragonTriangle*> m_DragonTris;
public:
	DragonSector()
	{
	}
	DragonSector(DragonXVector3& pos, real rad, std::vector<DragonTriangle*>& v)
	{
		m_Position=pos;
		m_Radius=rad;
		m_DragonTris=v;
	}
	~DragonSector()
	{
		m_DragonTris.clear();
	}
};

class DragonMeshCollisionObject : public DragonPhysicsCollisionOBB
{
private:

public:
	std::vector<DragonSector*> m_DragonSectors;
	std::vector<DragonTriangle*> m_DragonTris;
public:
	DragonMeshCollisionObject():DragonPhysicsCollisionOBB()
	{
		m_PrimTypeID=PT_Mesh;
	}
	~DragonMeshCollisionObject()
	{
		for (auto DragonSector : m_DragonSectors)
		{
			delete DragonSector;
		}
		m_DragonSectors.clear();

		for (auto DragonTriangle : m_DragonTris)
		{
			delete DragonTriangle;
		}
		m_DragonTris.clear();
	}
	DragonXMatrix ReCalcInvInrTensor(float mass)
	{
		DragonXMatrix m = DragonXMatrix();
		D3DXMatrixIdentity(&m);
		m._11=(real)0.20f*mass*((halfExtents.y*halfExtents.y)+(halfExtents.z*halfExtents.z));
		m._22=(real)0.20f*mass*((halfExtents.x*halfExtents.x)+(halfExtents.z*halfExtents.z));
		m._33=(real)0.20f*mass*((halfExtents.x*halfExtents.x)+(halfExtents.y*halfExtents.y));
		D3DXMatrixInverse(&m,NULL,&m);
		return m;
	}
};
