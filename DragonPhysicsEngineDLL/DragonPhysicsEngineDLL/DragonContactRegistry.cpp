#include "DragonContactRegistry.h"

DragonContactRegistry::DragonContactRegistry()
{
	InitializeCriticalSection(&mContactCS);
	InitializeCriticalSection(&mPairCS);
}
DragonContactRegistry::~DragonContactRegistry()
{
	DeleteCriticalSection(&mContactCS);
	DeleteCriticalSection(&mPairCS);
}
void DragonContactRegistry::ResetData()
{
	for( auto &conRegs : m_VecOfContactRegistrations)
	{
		conRegs.mOverWriteAble=true;
	}
	for( auto &pairRegs : m_VecOfPairRegistrations)
	{
		pairRegs.mOverWriteAble=true;
	}
}
void DragonContactRegistry::UpdatePairData(std::vector<DragonContactPair> pairs)
{
	bool added = false;
	for( auto &cPair : pairs)
	{
		if(!cPair.mVailidData)
		{
			break;
		}
		added=false;
		EnterCriticalSection(&mPairCS);
		for( auto &cPairReg : m_VecOfPairRegistrations)
		{
			if (cPairReg.mOverWriteAble)
			{
				cPairReg.OverWrite(cPair.Obj1->m_ID,cPair.Obj2->m_ID);
				added=true;
			}
		}
		if(added==false)
		{
			DragonPairRegistration dpr = DragonPairRegistration();
			dpr.OverWrite(cPair.Obj1->m_ID,cPair.Obj2->m_ID);
			m_VecOfPairRegistrations.push_back(dpr);
		}
		LeaveCriticalSection(&mPairCS);
	}
}
void DragonContactRegistry::UpdateContactData(std::vector<DragonContact> contacts)
{
	bool added = false;
	for( auto &contact : contacts)
	{
		if(!contact.mValidData)
		{
			break;
		}
		added=false;
		EnterCriticalSection(&mContactCS);
		for( auto &contactReg : m_VecOfContactRegistrations)
		{
			if (contactReg.mOverWriteAble)
			{
				int obj1ID=-1;
				int obj2ID=-1;
				if (contact.m_Entities[0]!=nullptr)
				{
					obj1ID=contact.m_Entities[0]->m_ID;
				}
				if(contact.m_Entities[1]!=nullptr)
				{
					obj2ID=contact.m_Entities[1]->m_ID;
				}
				contactReg.OverWrite(obj1ID,obj2ID,contact.m_ContactPoint,contact.m_ContactNormal,contact.mPenitrtionDepth);
				added=true;
			}
		}
		if(added==false)
		{
			DragonContactRegistration dpr = DragonContactRegistration();
			int obj1ID=-1;
			int obj2ID=-1;
			if (contact.m_Entities[0]!=nullptr)
			{
				obj1ID=contact.m_Entities[0]->m_ID;
			}
			if(contact.m_Entities[1]!=nullptr)
			{
				obj2ID=contact.m_Entities[1]->m_ID;
			}
			dpr.OverWrite(obj1ID,obj2ID,contact.m_ContactPoint,contact.m_ContactNormal,contact.mPenitrtionDepth);
			m_VecOfContactRegistrations.push_back(dpr);
		}
		LeaveCriticalSection(&mContactCS);
	}
}