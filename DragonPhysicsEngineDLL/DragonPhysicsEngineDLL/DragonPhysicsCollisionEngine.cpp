#include "DragonPhysicsCollisionEngine.h"
#include "DragonContactRegistry.h"
#include "DragonCollisionDetector.h"
#include "DragonContactGenerator.h"
#include "DragonContactResolver.h"
#include "DragonConstraintManager.h"

DragonCollisionEngine::DragonCollisionEngine()
{
	mContactRegistry = new DragonContactRegistry();
	mCollisionDetector = new DragonCollisionDetector();
	mContactGenerator = new DragonContactGenerator();
	mContactResolver = new DragonContactResolver();
	mConstraintManager = new DragonConstraintManager();
}
DragonCollisionEngine::~DragonCollisionEngine()
{
	delete mContactRegistry;
	delete mCollisionDetector;
	delete mContactGenerator;
	delete mContactResolver;
	delete mConstraintManager;
}