#pragma once
#include "DragonXMath.h"
#include "BaseDragonCollisionObject.h"

class DragonPhysicsPlane : public BaseDragonCollisionObject
{
private:

public:
	DragonXVector3 mNormalDirection;
	float mDistFromOrigin;
public:
	DragonPhysicsPlane();
	~DragonPhysicsPlane();
	virtual DragonXMatrix ReCalcInvInrTensor(float mass)
	{
		DragonXMatrix m = DragonXMatrix();
		D3DXMatrixIdentity(&m);
		m._11=(real)0.08334*mass*((1000.0f*1000.0f)+(1000.0f*1000.0f));
		m._22=(real)0.08334*mass*((1000.0f*1000.0f)+(1000.0f*1000.0f));
		m._33=(real)0.08334*mass*((1000.0f*1000.0f)+(1000.0f*1000.0f));
		D3DXMatrixInverse(&m,NULL,&m);
		return m;
	}
};