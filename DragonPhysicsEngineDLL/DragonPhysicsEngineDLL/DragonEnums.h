#pragma once

enum primitiveType
{
	PT_Plane,
	PT_Sphere,
	PT_OBB,
	PT_Mesh,
};
enum CollisionType
{
	CT_SphereSphere,
	CT_SpherePlane,
	CT_SphereOBB,
	CT_ObbPlane,
	CT_ObbObb,
	CT_MeshSphere,
	CT_MeshOBB,
	CT_MeshMesh,
	CT_MeshPlane,
};
enum HydraOctreeParts
{
	Top_Front_Left,		//0
	Top_Front_Right,	//1
	Top_Back_Left,		//2
	Top_Back_Right,		//3
	Bottom_Front_Left,	//4
	Bottom_Front_Right,	//5
	Bottom_Back_Left,	//6
	Bottom_Back_Right,	//7
};
enum ConstraintTypes
{
	CnT_Rod,
	CnT_Cable,
	CnT_Custom,
	CnT_cRod,
	CnT_cCable,
};