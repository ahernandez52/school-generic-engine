#include "DragonPhysicsPlane.h"
#include "DragonEnums.h"

DragonPhysicsPlane::DragonPhysicsPlane()
{
	m_PrimTypeID=PT_Plane;
	mNormalDirection = DragonXVector3(0.0f,1.0f,0.0f);
	mDistFromOrigin=1000.0f;
}
DragonPhysicsPlane::~DragonPhysicsPlane()
{
}