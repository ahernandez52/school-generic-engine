#pragma once

#include "BaseDragonPhysicsObject.h"

class DragonContactRegistration
{
private:

public:
	int mObj1ID;
	int mObj2ID;
	float mCpX;
	float mCpY;
	float mCpZ;
	float mCnX;
	float mCnY;
	float mCnZ;
	float Pen;
	bool mOverWriteAble;
public:
	DragonContactRegistration();
	~DragonContactRegistration();
	inline void OverWrite(int nObj1,int nObj2,DragonXVector3 nCP,DragonXVector3 nCN,float nPen)
	{
		mObj1ID=nObj1;
		mObj2ID=nObj2;
		mCpX=nCP.x;
		mCpY=nCP.y;
		mCpZ=nCP.z;
		mCnX=nCN.x;
		mCnY=nCN.y;
		mCnZ=nCN.z;
		Pen=nPen;
		mOverWriteAble=false;
	}
};

class DragonPairRegistration
{
private:

public:
	int mObj1ID;
	int mObj2ID;
	bool mOverWriteAble;
public:
	DragonPairRegistration();
	~DragonPairRegistration();
	inline void OverWrite(int obj1ID,int obj2ID)
	{
		mObj1ID=obj1ID;
		mObj2ID=obj2ID;
		mOverWriteAble=false;
	}
};