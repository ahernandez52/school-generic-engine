#pragma once
#include "BaseConstraintObject.h"
#include "DragonXMath.h"

class BaseDragonPhysicsObject;

class RodConstraintObject : public BaseConstraintObject
{
private:

public:
	BaseDragonPhysicsObject* mObj1;
	BaseDragonPhysicsObject* mObj2;
	DragonXVector3 mAnchorPointOne;
	DragonXVector3 mAnchorPointTwo;
	real mRodLength;
public:
	RodConstraintObject();
	~RodConstraintObject();
};

class CustomRodConstraint : public BaseConstraintObject
{
private:

public:
	DragonXVector3 mAnchorPointOne;
	DragonXVector3 mAnchorPointTwo;
	real mRodLength;
public:
	CustomRodConstraint();
	~CustomRodConstraint();
};