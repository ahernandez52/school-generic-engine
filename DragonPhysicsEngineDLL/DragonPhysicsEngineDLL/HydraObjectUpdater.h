#pragma once
#include <vector>

class BaseDragonPhysicsObject;

class HydraObjectUpdater
{
private:

public:
	HydraObjectUpdater();
	~HydraObjectUpdater();
	void UpdateEntities(std::vector<BaseDragonPhysicsObject*>& entities,const float dt);
};