#pragma once

class DragonContactRegistry;
class DragonCollisionDetector;
class DragonContactGenerator;
class DragonContactResolver;
class DragonConstraintManager;

class DragonCollisionEngine
{
private:

public:
	DragonContactRegistry* mContactRegistry;
	DragonCollisionDetector* mCollisionDetector;
	DragonContactGenerator* mContactGenerator;
	DragonContactResolver* mContactResolver;
	DragonConstraintManager* mConstraintManager;
public:
	DragonCollisionEngine();
	~DragonCollisionEngine();
};