#include "BaseDragonPhysicsObject.h"
#include "DragonPhysicsCollisionSphere.h"
#include "BaseDragonCollisionObject.h"
#include "DragonPhysicsMeshCollisionObject.h"
#include "DragonPhysicsOBB.h"
#include "DragonPhysicsPlane.h"
#include "DragonEnums.h"

BaseDragonPhysicsObject::BaseDragonPhysicsObject()
{
	//mActor=nullptr;
	m_ID=0;
	mI_PrimitiveType=-1;
	m_Position=DragonXVector3(0.0f,0.0f,0.0f);
	m_LinearVelocity=DragonXVector3(0.0f,0.0f,0.0f);
	m_RotationalVelocity=DragonXVector3(0.0f,0.0f,0.0f);
	m_LastFrameAccel=DragonXVector3(0.0f,0.0f,0.0f);
	m_LinearForceAccum=DragonXVector3(0.0f,0.0f,0.0f);
	m_AngularForceAccum=DragonXVector3(0.0f,0.0f,0.0f);
	m_LinearAccel=DragonXVector3(0.0f,0.0f,0.0f);
	inverseInertiaTensor=DragonXMatrix();
	inverseInertiaTensorWorld=DragonXMatrix();
	TransformMatrix=DragonXMatrix();
	m_QuatRot=DragonXQuaternion(0.0f,0.0f,0.0f,1.0f);
	m_CollisionObject=nullptr;
	m_InverseMass=1.0f/10.0f;
	m_LinearDampening=0.95f;
	m_AngularDampening=0.95f;
	m_motion=sleepEpsilon*2.0f;
	m_CanSleep=true;
	m_isAwake=true;
	m_isCollisionResolutionOn=true;
	m_PhysicsEnabled=true;
}
BaseDragonPhysicsObject::~BaseDragonPhysicsObject()
{
}
void BaseDragonPhysicsObject::ReCalcInverseTensor(real newMass)
{
	inverseInertiaTensor=m_CollisionObject->ReCalcInvInrTensor(newMass);
}
void BaseDragonPhysicsObject::UpdateActor()
{
	//if(mActor!=nullptr)
	//{
	//	mActor->SetOrientation(m_QuatRot);
	//	mActor->SetPosition(m_Position);
	//}
}
real BaseDragonPhysicsObject::GetRadius() const
{
	switch(mI_PrimitiveType)
	{
	case PT_Plane:
		{
			return 0.0f;
		}
	case PT_Sphere:
		{
			DragonPhysicsCollisionSphere* colObj;
			colObj = static_cast<DragonPhysicsCollisionSphere*>(m_CollisionObject);
			return colObj->mRadius;
		}
	case PT_OBB:
		{
			DragonPhysicsCollisionOBB* colObj;
			colObj = static_cast<DragonPhysicsCollisionOBB*>(m_CollisionObject);
			return colObj->mRadius;
			break;
		}
	case PT_Mesh:
		{
			DragonMeshCollisionObject* colObj;
			colObj = static_cast<DragonMeshCollisionObject*>(m_CollisionObject);
			return colObj->mRadius;
			break;
		}
	default:
		{
			return 0.0f;
			break;
		}
	}
}