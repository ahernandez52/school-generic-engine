#pragma once
#include <vector>
class BaseDragonPhysicsObject;

class DragonBoneObject
{
private:

public:
	BaseDragonPhysicsObject* m_BonesPhysicsObject;
	int m_BoneID;
	std::vector<DragonBoneObject> m_Children;
public:
	DragonBoneObject();
	~DragonBoneObject();
};