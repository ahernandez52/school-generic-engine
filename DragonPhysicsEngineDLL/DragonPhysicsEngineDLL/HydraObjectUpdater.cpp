#include "HydraObjectUpdater.h"
#include "BaseDragonPhysicsObject.h"

HydraObjectUpdater::HydraObjectUpdater()
{
}
HydraObjectUpdater::~HydraObjectUpdater()
{
}
void HydraObjectUpdater::UpdateEntities(std::vector<BaseDragonPhysicsObject*>& entities,const float dt)
{
	for(auto entitiy : entities)
	{			
		if(entitiy==NULL)
		{
			break;
		}
		if(entitiy->m_PhysicsEnabled&&entitiy->m_isAwake)
		{
			entitiy->UpdatePhysX();	
			if(entitiy->m_VecOfForces.size()>0)
			{
				std::vector<DragonForceGenerator> nVecForces;
				bool nFc=false;
				for(auto &force : entitiy->m_VecOfForces)
				{
					if (force.m_ForceEffect(entitiy,dt))
					{	
						nFc=true;
						for(auto &cfcs : entitiy->m_VecOfForces)
						{
							if (cfcs.m_ForceID!=force.m_ForceID)
							{
								nVecForces.push_back(cfcs);
							}
						}					
					}
				}
				if (nFc)
				{
					entitiy->m_VecOfForces=nVecForces;
				}
			}
			entitiy->m_LastFrameAccel=entitiy->m_LinearAccel;
			entitiy->m_LastFrameAccel+=entitiy->m_LinearForceAccum*entitiy->m_InverseMass;
			DragonXVector3 angularAccel = entitiy->inverseInertiaTensorWorld.transform(entitiy->m_AngularForceAccum);
			entitiy->m_LinearVelocity+=entitiy->m_LastFrameAccel*dt;
			entitiy->m_RotationalVelocity+=angularAccel*dt;
			entitiy->m_LinearVelocity*=pow(entitiy->m_LinearDampening,dt);
			entitiy->m_RotationalVelocity*=pow(entitiy->m_AngularDampening,dt);
			entitiy->m_Position+=entitiy->m_LinearVelocity*dt;
			entitiy->m_QuatRot.addScaledVectorDrX(entitiy->m_RotationalVelocity,dt);
			entitiy->UpdateMatricies();
			entitiy->clearAccumulators();
			if (entitiy->m_CanSleep)
			{
				real currentMotion = entitiy->m_LinearVelocity*entitiy->m_LinearVelocity+entitiy->m_RotationalVelocity*entitiy->m_RotationalVelocity;
				real bias = (real)pow(0.5, dt);
				entitiy->m_motion = bias*entitiy->m_motion + (1-bias)*currentMotion;
				if (entitiy->m_motion < sleepEpsilon)
				{
					entitiy->setAwake(false);
				}
				else if (entitiy->m_motion > 10 * sleepEpsilon)
				{
					entitiy->m_motion = 10 * sleepEpsilon;
				}
			}
			entitiy->UpdateActor();
		}
		else
		{
			entitiy->UpdatePhysXAlt();	
		}
	}
}