#include "DragonConstraintManager.h"
#include "BaseConstraintObject.h"
#include "CableConstraintObject.h"
#include "RodConstraintObject.h"
#include "CustomConstrainObject.h"
#include "DragonEnums.h"

DragonConstraintManager::DragonConstraintManager()
{
}
DragonConstraintManager::~DragonConstraintManager()
{
}
std::vector<DragonContact> DragonConstraintManager::GenConstraintContacts(const std::vector<BaseDragonPhysicsObject*>& entities)
{
	std::vector<DragonContact> mRetVal;//note probably best to iterate through the list given

	for(auto set : mConstraintsMap)
	{
		for (auto constraint : set.second)
		{
			switch(constraint->mConstraintType)
			{
			case CnT_Rod:
				{
					RodConstraintObject* rod;
					rod = static_cast<RodConstraintObject*>(constraint);
					bool first = false;
					bool second = false;
					for(const auto BaseDragonPhysicsObject : entities)
					{
						if(BaseDragonPhysicsObject->m_ID==rod->mObj1->m_ID)
						{
							first=true;
						}
						if (BaseDragonPhysicsObject->m_ID==rod->mObj2->m_ID)
						{
							second=true;
						}
					}
					if(first==false&&second==false)
					{
						break;
					}
					else
					{
						real curLength = (rod->mObj1->getPointInWorldSpace(rod->mAnchorPointOne)-rod->mObj2->getPointInWorldSpace(rod->mAnchorPointTwo)).GetMagnitude();
						if(curLength==rod->mRodLength)
						{
							break;
						}
						else
						{
							DragonContact DC = DragonContact();
							DC.m_ContactNormal=rod->mObj1->getPointInWorldSpace(rod->mAnchorPointOne)-rod->mObj2->getPointInWorldSpace(rod->mAnchorPointTwo);
							DC.m_ContactNormal.Normalize();
							DC.m_ContactPoint=(rod->mObj1->getPointInWorldSpace(rod->mAnchorPointOne)+rod->mObj2->getPointInWorldSpace(rod->mAnchorPointTwo))*0.5f;
							DC.mRestitution=0.0f;
							DC.mFriction=1.0f;
							if(curLength>rod->mRodLength)
							{
								DC.mPenitrtionDepth=curLength-rod->mRodLength;
							}
							else
							{
								DC.m_ContactNormal.invert();
								DC.mPenitrtionDepth=rod->mRodLength-curLength;
							}
							if (first)
							{
								DC.m_Entities[0]=rod->mObj1;
							}
							else
							{
								DC.m_Entities[0]=nullptr;
							}
							if (second)
							{
								DC.m_Entities[1]=rod->mObj2;
							}
							else
							{
								DC.m_Entities[1]=nullptr;
							}
							mRetVal.push_back(DC);
							break;
						}
					}
					break;
				}
			case CnT_Cable:
				{
					CableConstraintObject* cable;
					cable = static_cast<CableConstraintObject*>(constraint);
					bool first = false;
					bool second = false;
					for(const auto BaseDragonPhysicsObject : entities)
					{
						if(BaseDragonPhysicsObject->m_ID==cable->mObj1->m_ID)
						{
							first=true;
						}
						if (BaseDragonPhysicsObject->m_ID==cable->mObj2->m_ID)
						{
							second=true;
						}
					}
					if(first==false&&second==false)
					{
						break;
					}
					else
					{
						real curLength = (cable->mObj1->getPointInWorldSpace(cable->mAnchorPointOne)-cable->mObj2->getPointInWorldSpace(cable->mAnchorPointTwo)).GetMagnitude();
						if(curLength<cable->mCableLength)
						{
							break;
						}
						else
						{
							DragonContact DC = DragonContact();
							DC.m_ContactNormal=cable->mObj1->getPointInWorldSpace(cable->mAnchorPointOne)-cable->mObj2->getPointInWorldSpace(cable->mAnchorPointTwo);
							DC.m_ContactNormal.Normalize();
							DC.m_ContactPoint=(cable->mObj1->getPointInWorldSpace(cable->mAnchorPointOne)+cable->mObj2->getPointInWorldSpace(cable->mAnchorPointTwo))*0.5f;
							DC.mRestitution=0.0f;
							DC.mFriction=1.0f;
							DC.m_ContactNormal.invert();
							DC.mPenitrtionDepth=cable->mCableLength-curLength;
							if (first)
							{
								DC.m_Entities[0]=cable->mObj1;
							}
							else
							{
								DC.m_Entities[0]=nullptr;
							}
							if (second)
							{
								DC.m_Entities[1]=cable->mObj2;
							}
							else
							{
								DC.m_Entities[1]=nullptr;
							}
							mRetVal.push_back(DC);
							break;
						}
					}
					break;
				}
			case CnT_Custom:
				{
					CustomConstraintObject* cco;
					cco = static_cast<CustomConstraintObject*>(constraint);
					bool first = false;
					bool second = false;
					for(const auto BaseDragonPhysicsObject : entities)
					{
						if(BaseDragonPhysicsObject->m_ID==cco->mObj1->m_ID)
						{
							first=true;
						}
						if (BaseDragonPhysicsObject->m_ID==cco->mObj2->m_ID)
						{
							second=true;
						}
					}
					if(first==false&&second==false)
					{
						break;
					}
					else
					{
						for(auto cusSet : cco->mCustomConstraints)
						{
							switch(cusSet->mConstraintType)
							{
							case CnT_cRod:
								{
									CustomRodConstraint* rod;
									rod = static_cast<CustomRodConstraint*>(cusSet);
									real curLength = (cco->mObj1->getPointInWorldSpace(rod->mAnchorPointOne)-cco->mObj2->getPointInWorldSpace(rod->mAnchorPointTwo)).GetMagnitude();
									if(curLength==rod->mRodLength)
									{
										break;
									}
									else
									{
										DragonContact DC = DragonContact();
										DC.m_ContactNormal=cco->mObj1->getPointInWorldSpace(rod->mAnchorPointOne)-cco->mObj2->getPointInWorldSpace(rod->mAnchorPointTwo);
										DC.m_ContactNormal.Normalize();
										DC.m_ContactPoint=(cco->mObj1->getPointInWorldSpace(rod->mAnchorPointOne)+cco->mObj2->getPointInWorldSpace(rod->mAnchorPointTwo))*0.5f;
										DC.mRestitution=0.0f;
										DC.mFriction=1.0f;
										if(curLength>rod->mRodLength)
										{
											DC.mPenitrtionDepth=curLength-rod->mRodLength;
										}
										else
										{
											DC.m_ContactNormal.invert();
											DC.mPenitrtionDepth=rod->mRodLength-curLength;
										}
										if (first)
										{
											DC.m_Entities[0]=cco->mObj1;
										}
										else
										{
											DC.m_Entities[0]=nullptr;
										}
										if (second)
										{
											DC.m_Entities[1]=cco->mObj2;
										}
										else
										{
											DC.m_Entities[1]=nullptr;
										}
										mRetVal.push_back(DC);
										break;
									}
									break;
								}
							case CnT_cCable:
								{
									CustomCableConstraint* cable;
									cable = static_cast<CustomCableConstraint*>(cusSet);
									real curLength = (cco->mObj1->getPointInWorldSpace(cable->mAnchorPointOne)-cco->mObj2->getPointInWorldSpace(cable->mAnchorPointTwo)).GetMagnitude();
									if(curLength<cable->mCableLength)
									{
										break;
									}
									else
									{
										DragonContact DC = DragonContact();
										DC.m_ContactNormal=cco->mObj1->getPointInWorldSpace(cable->mAnchorPointOne)-cco->mObj2->getPointInWorldSpace(cable->mAnchorPointTwo);
										DC.m_ContactNormal.Normalize();
										DC.m_ContactPoint=(cco->mObj1->getPointInWorldSpace(cable->mAnchorPointOne)+cco->mObj2->getPointInWorldSpace(cable->mAnchorPointTwo))*0.5f;
										DC.mRestitution=0.0f;
										DC.mFriction=1.0f;
										DC.m_ContactNormal.invert();
										DC.mPenitrtionDepth=cable->mCableLength-curLength;
										if (first)
										{
											DC.m_Entities[0]=cco->mObj1;
										}
										else
										{
											DC.m_Entities[0]=nullptr;
										}
										if (second)
										{
											DC.m_Entities[1]=cco->mObj2;
										}
										else
										{
											DC.m_Entities[1]=nullptr;
										}
										mRetVal.push_back(DC);
										break;
									}
									break;
								}
							default:
								{
									break;
								}
							}
						}
					}
					break;
				}
			default :
				{
					break;
				}
			}
		}
	}
	return mRetVal;
}