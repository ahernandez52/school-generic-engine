#pragma once
#include <vector>
#include "DragonContact.h"

class DragonContactResolver
{
private:

public:
	float mVelocityEpsilon;
	float mPositionEpsilon;
public:
	DragonContactResolver();
	~DragonContactResolver();
	void ResolveContacts(std::vector<DragonContact>& contacts,const float dt);
};