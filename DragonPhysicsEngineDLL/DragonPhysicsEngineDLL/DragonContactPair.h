#pragma once

class BaseDragonPhysicsObject;

class DragonContactPair
{
private:

public:
	bool mVailidData;
	BaseDragonPhysicsObject* Obj1;
	BaseDragonPhysicsObject* Obj2;
	int mContactType;
public:
	DragonContactPair()
	{
		mVailidData=false;
		Obj1=nullptr;
		Obj2=nullptr;
		mContactType=-1;
	}
	DragonContactPair(BaseDragonPhysicsObject* obj1,BaseDragonPhysicsObject* obj2,int ConType)
	{
		mVailidData=true;
		Obj1=obj1;
		Obj2=obj2;
	}
	~DragonContactPair()
	{
	}
};