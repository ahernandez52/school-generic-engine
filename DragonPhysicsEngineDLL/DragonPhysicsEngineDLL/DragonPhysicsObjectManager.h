#pragma once

#include <vector>
#include <map>
#include "BaseDragonCollisionObject.h"
#include "BaseDragonPhysicsObject.h"

class DragonPhysicsObjectManager
{
private:

public:
	std::map<std::string,BaseDragonCollisionObject*> m_CollisionObjectRegistry;
	std::vector<BaseDragonPhysicsObject*> m_VectorOfPhysxOjects;
public:
	DragonPhysicsObjectManager();
	~DragonPhysicsObjectManager();
};