#pragma once
#include "DragonPhysicsAPI.h"
#include <vector>
#include <map>
#include <functional>

class DragonPhysicsObjectManager;
class BaseDragonPhysicsObject;
class DragonCollisionEngine;
class HydraObjectUpdater;
class DragonPhysicsHydraOctreePartitioner;
class DragonForceEngine;
class Actor;

struct VertexPNT
{
	VertexPNT()
		:pos(0.0f, 0.0f, 0.0f),
		normal(0.0f, 0.0f, 0.0f),
		tex0(0.0f, 0.0f){}
	VertexPNT(float x, float y, float z,
		float nx, float ny, float nz,
		float u, float v):pos(x,y,z), normal(nx,ny,nz), tex0(u,v){}
	VertexPNT(const D3DXVECTOR3& v, const D3DXVECTOR3& n, const D3DXVECTOR2& uv)
		:pos(v),normal(n), tex0(uv){}

	D3DXVECTOR3 pos;
	D3DXVECTOR3 normal;
	D3DXVECTOR2 tex0;

	static IDirect3DVertexDeclaration9* Decl;
};

class DragonPhysicEngine// : public IDragonPhysicsAPI
{
private:
	DragonPhysicsObjectManager* m_DragonPhysicsObjectManager;
	DragonCollisionEngine* m_DragonCollisionEngine;
	HydraObjectUpdater* m_HydraObjectUpdater;
	DragonPhysicsHydraOctreePartitioner* m_DragonPhysicsHydraOctreePartitioner;
	DragonForceEngine* m_DragonForceEngine;
public:

public:
	DragonPhysicEngine();
	virtual ~DragonPhysicEngine();
	void UpdatePhysics(const float dt);
	bool CreateSphereCollisionObjectSchematic(float radius,std::string SphereSchemName);
	bool CreateObbCollisionObjectSchematic(D3DXVECTOR3 halfSizes,std::string ObbSchemName);
	bool CreatePlaneCollisionObjectSchematic(D3DXVECTOR3 normalDirection,float dist, std::string PlaneSchemName);
	bool CreateMeshCollisionObject(IDirect3DDevice9* gDevice,std::string meshCollisionObjectFile,std::string MeshSchemName);
	int CreatePhysicsObject(int id,D3DXVECTOR3 pos,std::string SchemName,float mass,bool PhysxEnable,bool colResolutionOn,Actor* actr);	
	std::map<int,std::vector<int>> GetPairs(std::vector<int> IDs);
	std::vector<bool> SetPhysxEnable(std::map<int,bool> mapOfData);
	std::vector<bool> SetLinearAccel(std::map<int,D3DXVECTOR3> mapOfIdsAndLinearAccelData);
	std::vector<bool> SetCollisionObject(std::map<int,std::string> mapOfIDsAndCollisionObjectData);
	std::vector<bool> SetMass(std::map<int,float> mapOfIdsAndMasses);
	int CreateBlastForce(D3DXVECTOR3 detPoint=D3DXVECTOR3(0.0f,0.0f,0.0f),float ImplMinRad=75.0f,float inplMaxRad=150.0f,float implDur=0.5f,float implForce=8.0f,float shkWvSpd=1000.0f,float shkWvThkns=300.0f,float pkCncusFc=2000.0f,float cncusDur=1.0f,float pkcnvecFc=500.0f,float chmnRad=200.0f,float chmnHght=500.0f,float cnvecDur=2.0f);
	int CreateThrustForce(float thDur=5.5f,float decOfThst=0.5f,float magOfThst=5000.0f,D3DXVECTOR3 ThstDir=D3DXVECTOR3(0.0f,1.0f,0.0f));
	int createCustomForce(std::function<bool (BaseDragonPhysicsObject* entity, float dt)> forceEffect);
	std::map<int,std::map<int,bool>> AddForces(std::vector<int> ForceIDs,std::vector<int> ObjIDs);
	std::vector<bool> ReleaseObjects(std::vector<int> IDs);

	BaseDragonPhysicsObject* GetComponent(int id);
};

//extern "C"
//{
//	HRESULT CreateDragonPhysicsEngine(HINSTANCE hDLL,IDragonPhysicsAPI **pInterface);
//	HRESULT ReleaseDragonPhysicsEngine(IDragonPhysicsAPI **pInterface);
//}
//
//typedef HRESULT (*CREATEDRAGONPHYSICSENGINE)(HINSTANCE hDLL,IDragonPhysicsAPI **pInstance);
//typedef HRESULT (*RELEASEDRAGONPHYSICSENGINE)(IDragonPhysicsAPI **pInstance);