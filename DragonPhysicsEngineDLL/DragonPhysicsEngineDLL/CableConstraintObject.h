#pragma once
#include "BaseConstraintObject.h"
#include "DragonXMath.h"
class BaseDragonPhysicsObject;

class CableConstraintObject : public BaseConstraintObject
{
private:

public:
	BaseDragonPhysicsObject* mObj1;
	BaseDragonPhysicsObject* mObj2;
	DragonXVector3 mAnchorPointOne;
	DragonXVector3 mAnchorPointTwo;
	real mCableLength;
public:
	CableConstraintObject();
	~CableConstraintObject();
};

class CustomCableConstraint : public BaseConstraintObject
{
private:

public:
	DragonXVector3 mAnchorPointOne;
	DragonXVector3 mAnchorPointTwo;
	real mCableLength;
public:
	CustomCableConstraint();
	~CustomCableConstraint();
};