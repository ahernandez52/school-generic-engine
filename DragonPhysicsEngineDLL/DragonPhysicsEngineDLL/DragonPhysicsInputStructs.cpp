#include "DragonPhysicsInputStructs.h"
#include "DragonPhysicsEngine.h"
#include "BaseDragonPhysicsObject.h"

void PhysicsCore::Message(char* API, unsigned long param, void *in /* = nullptr */, void *out /* = nullptr */)
{
	static int componentID = 0;

	if(strcmp(API,"INIT")==0)
	{
		mPhysicsAPI=new DragonPhysicEngine();

		mPhysicsAPI->CreateSphereCollisionObjectSchematic(2.5f, "SPHERE");
		mPhysicsAPI->CreateObbCollisionObjectSchematic(D3DXVECTOR3(5.0f, 5.0f, 5.0f), "BOX");
		mPhysicsAPI->CreatePlaneCollisionObjectSchematic(D3DXVECTOR3(0.0f, 1.0f, 0.0f), -10.0f, "PLANE");

		mPhysicsAPI->CreatePhysicsObject(20, D3DXVECTOR3(0.0f, 0.0f, 0.0f), "PLANE", 99999.0f, true, true, nullptr);


		return;
	}

	if(strcmp(API,"SHUTDOWN")==0)
	{
		delete mPhysicsAPI;
		return;
	}

	if(strcmp(API,"UPDATE")==0)
	{
		mPhysicsAPI->UpdatePhysics(*reinterpret_cast<float const*>(&param));
		return;
	}

	if(strcmp(API, "CREATESPHERE") == 0)
	{
		float *paramList = (float*)in;
		mPhysicsAPI->CreatePhysicsObject(componentID, D3DXVECTOR3(paramList[0], paramList[1], paramList[2]), "SPHERE", paramList[3], true, true, nullptr);
		paramList[0] = componentID;
		componentID++;
		return;
	}

	if(strcmp(API, "CREATEBOX") == 0)
	{
		float *paramList = (float*)in;
		mPhysicsAPI->CreatePhysicsObject(componentID, D3DXVECTOR3(paramList[0], paramList[1], paramList[2]), "BOX", paramList[3], true, true, nullptr);
		paramList[0] = componentID;
		componentID++;
		return;
	}

	if(strcmp(API, "GETCOMPONENTDATA") == 0)
	{
		BaseDragonPhysicsObject* component = mPhysicsAPI->GetComponent(param);

		float *paramList = (float*)in;

		paramList[0] = component->m_Position.x;
		paramList[1] = component->m_Position.y;
		paramList[2] = component->m_Position.z;
		paramList[3] = component->m_QuatRot.w;
		paramList[4] = component->m_QuatRot.x;
		paramList[5] = component->m_QuatRot.y;
		paramList[6] = component->m_QuatRot.z;

		return;
	}

	if(strcmp(API, "SYNCCOMPONENT") == 0)
	{
		BaseDragonPhysicsObject* component = mPhysicsAPI->GetComponent(param);

		float *paramList = (float*)in;

		component->m_Position = D3DXVECTOR3(paramList[0], paramList[1], paramList[2]);
		component->m_QuatRot = D3DXQUATERNION(paramList[3], paramList[4], paramList[5], paramList[6]);

		return;
	}

	if(strcmp(API,"CREATESPHERECOLLISION")==0)
	{
		SphereCollisionObjectSchematic* schem = (SphereCollisionObjectSchematic*)in;
		auto retVal = mPhysicsAPI->CreateSphereCollisionObjectSchematic(schem->mRadius,schem->SchemName);
		out=(void*)retVal;
		return;
	}

	if (strcmp(API,"CREATEOBBCOLLISION")==0)
	{
		OBBcollisionObjectSchematic* schem = (OBBcollisionObjectSchematic*)in;
		auto retVal = mPhysicsAPI->CreateObbCollisionObjectSchematic(schem->mHalfsizes,schem->mObbSchemName);
		out=(void*)retVal;
		return;
	}

	if (strcmp(API,"CREATEPLANECOLLISION")==0)
	{
		PlaneCollisionObjectSchematic* schem = (PlaneCollisionObjectSchematic*)in;
		auto retVal = mPhysicsAPI->CreatePlaneCollisionObjectSchematic(schem->mPlaneNormal,schem->mDistFromOrigin,schem->mPlaneSchemName);
		out=(void*)retVal;
		return;
	}

	if (strcmp(API,"CREATEMESHCOLLISION")==0)
	{
		MeshCollisionObjectSchematic* schem = (MeshCollisionObjectSchematic*)in;
		auto retVal = mPhysicsAPI->CreateMeshCollisionObject(schem->mDevice,schem->mMeshCollisionObjectName,schem->mMeshCollisionSchemName);
		out=(void*)retVal;
		return;
	}

	if (strcmp(API,"CREATEPHYSICSOBJECT")==0)
	{
		PhysicsObjectSchematic* schem = (PhysicsObjectSchematic*)in;
		auto retVal = mPhysicsAPI->CreatePhysicsObject(schem->mID,schem->mPosition,schem->mCollisionObjectSchem,schem->mMass,schem->mPhysicsEnable,schem->mCollisionResolutionOn,schem->mActor);
		out=(void*)retVal;
		return;
	}

	if (strcmp(API,"CREATEBLASTFORCE")==0)
	{
		CreateBlastForceStruct* schem = (CreateBlastForceStruct*)in;
		auto retVal = mPhysicsAPI->CreateBlastForce(schem->mDetonationPoint,schem->mImplosionMinimumRadius,schem->mImplosionMaxRadius,schem->mImplosionDuration,schem->mImplosionForce,schem->mShockWaveSpeed,schem->mShockWaveThickness,schem->mPeakConcussiveForce,schem->mConcussiveDuration,schem->mPeakConvectionForce,schem->mChimneyRadius,schem->mChimneyHeight,schem->mConvectionDuration);
		out=(void*)retVal;
		return;
	}

	if (strcmp(API,"CREATETHRUSTFORCE")==0)
	{
		CreateThrustForceStruct* schem = (CreateThrustForceStruct*)in;
		auto retVal = mPhysicsAPI->CreateThrustForce(schem->mThrustDuration,schem->mDecrementOfThrust,schem->mMagnitudeOfThrust,schem->mDirectionOfThrust);
		out=(void*)retVal;
		return;
	}

	if (strcmp(API,"CREATECUSTOMFORCE")==0)
	{
		CreateCustomForce* schem = (CreateCustomForce*)in;
		auto retVal = mPhysicsAPI->createCustomForce(schem->mForceEffect);
		out=(void*)retVal;
		return;
	}

	if(strcmp(API,"ADDFORCES")==0)
	{
		AddForcesStruct* schem = (AddForcesStruct*)in;
		mPhysicsAPI->AddForces(schem->mForceIDs,schem->mObjIDs);
		return;
	}
	
	if(strcmp(API,"RELEASEOBJECTS")==0)
	{
		ReleaseObjectsStruct* schem = (ReleaseObjectsStruct*)in;
		mPhysicsAPI->ReleaseObjects(schem->mIDs);
		return;
	}
}

HRESULT CreatePhysicsCore(HINSTANCE hDLL, ISubSystem **Interface)
{
	if(!*Interface)
	{
		*Interface = new PhysicsCore();

		return S_OK;
	}

	return S_FALSE;
}

HRESULT ReleasePhysicsCore(ISubSystem **Interface)
{
	if(!*Interface)
	{
		return S_FALSE;
	}

	delete *Interface;
	*Interface = nullptr;

	return S_OK;
}