/**
 * Actor - Acts as the base object for any in-app classes
 *
 * Author - Jesse Dillon
 */

#pragma once 

#include "DragonXMath.h"

class Actor
{
private:

	static int m_NextId;

	int m_Id;

protected:

	D3DXVECTOR3		m_Position;
	D3DXQUATERNION  m_Orientation;

public:

	Actor(void) 
		: m_Position(D3DXVECTOR3(0, 0, 0)), 
		m_Orientation(D3DXQUATERNION(0, 0, 0, 1)){}//}// /*{SetId()*/;}

	//Actor(D3DXVECTOR3 pos, D3DXQUATERNION orient = D3DXQUATERNION(0, 0, 0, 1))//{SetId();}

	~Actor(void){}

	/**
	 * Accessors
	 */
	D3DXVECTOR3 GetPosition(void){return m_Position;}
	void		SetPosition(D3DXVECTOR3 p){m_Position = p;}

	D3DXQUATERNION GetOrientation(void){return m_Orientation;}
	void		   SetOrientation(D3DXQUATERNION o){m_Orientation = o;}

	int GetId(void){return m_Id;}

private:

	//void SetId(void){m_Id = m_NextId; m_NextId++;}
};
