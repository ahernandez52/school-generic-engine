#pragma once
#include <string>
#include <functional>
#include "../../TaskEngine/Actor.h"
#include "../../MathLibrary/MathLibrary.h"
#include <vector>
#include <map>
#include "../../TaskEngine/ISubSystem.h"

class DragonPhysicEngine;
class BaseDragonPhysicsObject;

struct SphereCollisionObjectSchematic
{
	float mRadius;
	std::string SchemName;
};

struct OBBcollisionObjectSchematic
{
	D3DXVECTOR3 mHalfsizes;
	std::string mObbSchemName;
};

struct PlaneCollisionObjectSchematic
{
	D3DXVECTOR3 mPlaneNormal;
	float mDistFromOrigin;
	std::string mPlaneSchemName;
};

struct MeshCollisionObjectSchematic
{
	IDirect3DDevice9* mDevice;
	std::string mMeshCollisionObjectName;
	std::string mMeshCollisionSchemName;
};

struct PhysicsObjectSchematic
{
	int mID;
	D3DXVECTOR3 mPosition;
	std::string mCollisionObjectSchem;
	float mMass;
	bool mPhysicsEnable;
	bool mCollisionResolutionOn;
	Actor* mActor;
};

struct GetCollisionPairsStruct
{
	std::vector<int> mIDs;
};

struct SetPhysicsEnableStruct
{
	std::map<int,bool> mMapOfData;
};

struct SetLinearAccelStruct
{
	std::map<int,D3DXVECTOR3> mMapOfLinAccelData;
};

struct SetCollisionObjectStruct
{
	std::map<int,std::string> mMapOfIDsAndCollisionObjNames;
};

struct SetMassStruct
{
	std::map<int,float> mMapofIDsAndMass;
};

struct CreateBlastForceStruct
{
	D3DXVECTOR3 mDetonationPoint;
	float mImplosionMinimumRadius;
	float mImplosionMaxRadius;
	float mImplosionDuration;
	float mImplosionForce;
	float mShockWaveSpeed;
	float mShockWaveThickness;
	float mPeakConcussiveForce;
	float mConcussiveDuration;
	float mPeakConvectionForce;
	float mChimneyRadius;
	float mChimneyHeight;
	float mConvectionDuration;
	CreateBlastForceStruct()
	{
		mDetonationPoint=D3DXVECTOR3(0.0f,0.0f,0.0f);
		mImplosionMinimumRadius=75.0f;
		mImplosionMaxRadius=150.0f;
		mImplosionDuration=0.5f;
		mImplosionForce=8.0f;
		mShockWaveSpeed=1000.0f;
		mShockWaveThickness=300.0f;
		mPeakConcussiveForce=2000.0f;
		mConcussiveDuration=1.0f;
		mPeakConvectionForce=500.0f;
		mChimneyRadius=200.0f;
		mChimneyHeight=500.0f;
		mConvectionDuration=2.0f;
	}
};

struct CreateThrustForceStruct
{
	float mThrustDuration;
	float mDecrementOfThrust;
	float mMagnitudeOfThrust;
	D3DXVECTOR3 mDirectionOfThrust;
	CreateThrustForceStruct()
	{
		mThrustDuration=5.5f;
		mDecrementOfThrust=0.5f;
		mMagnitudeOfThrust=5000.0f;
		mDirectionOfThrust=D3DXVECTOR3(0.0f,1.0f,0.0f);
	}
};

struct CreateCustomForce
{
	std::function<bool (BaseDragonPhysicsObject* entity,float dt)> mForceEffect;
};

struct AddForcesStruct
{
	std::vector<int> mForceIDs;
	std::vector<int> mObjIDs;
};

struct ReleaseObjectsStruct
{
	std::vector<int> mIDs;
};

class PhysicsCore : public ISubSystem
{
	DragonPhysicEngine* mPhysicsAPI;
public:
	void Message(char* API, unsigned long param, void *in = nullptr, void *out = nullptr);
};

extern "C"
{
	__declspec(dllexport) HRESULT CreatePhysicsCore(HINSTANCE hDLL, ISubSystem **Interface);
	__declspec(dllexport) HRESULT ReleasePhysicsCore(ISubSystem **Interface);
}

typedef HRESULT (*CREATECOREOBJECT)(HINSTANCE hDLL, ISubSystem **Interface);
typedef HRESULT (*RELEASECOREOBJECT)(ISubSystem **Interface);