#pragma once

#include "DragonPhysicsCollisionSphere.h"

class DragonPhysicsCollisionOBB : public DragonPhysicsCollisionSphere
{
private:

public:
	DragonXVector3 halfExtents;
public:
	DragonPhysicsCollisionOBB();
	virtual ~DragonPhysicsCollisionOBB();
	virtual DragonXMatrix ReCalcInvInrTensor(float mass)
	{
		DragonXMatrix m = DragonXMatrix();
		D3DXMatrixIdentity(&m);
		m._11=(real)0.08334*mass*((halfExtents.y*halfExtents.y)+(halfExtents.z*halfExtents.z));
		m._22=(real)0.08334*mass*((halfExtents.x*halfExtents.x)+(halfExtents.z*halfExtents.z));
		m._33=(real)0.08334*mass*((halfExtents.x*halfExtents.x)+(halfExtents.y*halfExtents.y));
		D3DXMatrixInverse(&m,NULL,&m);
		return m;
	}
};