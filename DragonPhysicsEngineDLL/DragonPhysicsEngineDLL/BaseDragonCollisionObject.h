#pragma once

class DragonXMatrix;

class BaseDragonCollisionObject
{
private:

public:
	int m_PrimTypeID;
public:
	BaseDragonCollisionObject()
	{
	}
	virtual ~BaseDragonCollisionObject()
	{
	}
	virtual DragonXMatrix ReCalcInvInrTensor(float mass)=0;
};