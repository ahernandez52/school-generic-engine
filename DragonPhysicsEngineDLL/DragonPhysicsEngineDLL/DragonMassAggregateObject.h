#pragma once
#include <vector>
#include "DragonBoneObject.h"
class BaseDragonPhysicsObject;

class DragonMassAggregateObject
{
private:

public:
	BaseDragonPhysicsObject* m_RootPhysicsObject;
	int m_RootBoneID;
	std::vector<DragonBoneObject> m_Children;
public:
	DragonMassAggregateObject();
	~DragonMassAggregateObject();
};