#pragma once
#pragma comment (lib, "d3d9.lib")
#pragma comment (lib, "d3dx9.lib")
//#pragma comment (lib, "dxerr9.lib")
#pragma comment (lib, "dinput8.lib")
#pragma comment (lib, "dxguid.lib")
#include <d3d9.h>
#include <d3dx9.h>
//#include <dxerr9.h>
#include <string>
#include <Windows.h>
#include <vector>
#include <map>
#include <functional>

class BaseDragonPhysicsObject;
class Actor;

class IDragonPhysicsAPI
{
private:

public:
	virtual ~IDragonPhysicsAPI(){}
	virtual void UpdatePhysics(const float dt)=0;
	virtual bool CreateSphereCollisionObjectSchematic(float radius,std::string SphereSchemName)=0;
	virtual bool CreateObbCollisionObjectSchematic(D3DXVECTOR3 halfSizes,std::string ObbSchemName)=0;
	virtual bool CreatePlaneCollisionObjectSchematic(D3DXVECTOR3 normalDirection,float dist, std::string PlaneSchemName)=0;
	virtual bool CreateMeshCollisionObject(IDirect3DDevice9* gDevice,std::string meshCollisionObjectFile,std::string MeshSchemName)=0;
	virtual int CreatePhysicsObject(int id,D3DXVECTOR3 pos,std::string SchemName,float mass,bool PhysxEnable,bool colResolutionOn,Actor* actr)=0;
	virtual std::map<int,std::vector<int>> GetPairs(std::vector<int> IDs)=0;
	virtual std::vector<bool> SetPhysxEnable(std::map<int,bool> mapOfData)=0;
	virtual std::vector<bool> SetLinearAccel(std::map<int,D3DXVECTOR3> mapOfIdsAndLinearAccelData)=0;
	virtual std::vector<bool> SetCollisionObject(std::map<int,std::string> mapOfIDsAndCollisionObjectData)=0;
	virtual std::vector<bool> SetMass(std::map<int,float> mapOfIdsAndMasses)=0;
	virtual int CreateBlastForce(D3DXVECTOR3 detPoint=D3DXVECTOR3(0.0f,0.0f,0.0f),float ImplMinRad=75.0f,float inplMaxRad=150.0f,float implDur=0.5f,float implForce=8.0f,float shkWvSpd=1000.0f,float shkWvThkns=300.0f,float pkCncusFc=2000.0f,float cncusDur=1.0f,float pkcnvecFc=500.0f,float chmnRad=200.0f,float chmnHght=500.0f,float cnvecDur=2.0f)=0;
	virtual int CreateThrustForce(float thDur=5.5f,float decOfThst=0.5f,float magOfThst=5000.0f,D3DXVECTOR3 ThstDir=D3DXVECTOR3(0.0f,1.0f,0.0f))=0;
	virtual int createCustomForce(std::function<bool (BaseDragonPhysicsObject* entity, float dt)> forceEffect)=0;
	virtual std::map<int,std::map<int,bool>> AddForces(std::vector<int> ForceIDs,std::vector<int> ObjIDs)=0;
	virtual std::vector<bool> ReleaseObjects(std::vector<int> IDs)=0;
};