#pragma once
#include <map>
#include <vector>
#include "BaseDragonPhysicsObject.h"
#include "DragonContact.h"
class BaseConstraintObject;
class DragonMassAggregateObject;

class DragonConstraintManager
{
private:

public:
	std::map<int,std::vector<BaseConstraintObject*>> mConstraintsMap;// int is id of mass aggregate system associateed with the constraints
	std::map<int,DragonMassAggregateObject*> mMassAggregateObjectMap;
public:
	DragonConstraintManager();
	~DragonConstraintManager();
	std::vector<DragonContact> GenConstraintContacts(const std::vector<BaseDragonPhysicsObject*>& entities);
};