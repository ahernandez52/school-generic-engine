#include "DragonContactResolver.h"
#include "BaseDragonPhysicsObject.h"
#include <algorithm>

DragonContactResolver::DragonContactResolver()
{
	mVelocityEpsilon=0.01f;
	mPositionEpsilon=0.01f;
}
DragonContactResolver::~DragonContactResolver()
{
}
void DragonContactResolver::ResolveContacts(std::vector<DragonContact>& contacts,const float dt)
{
	if (contacts.size()==0)
	{
		return;
	}
	int velIts = contacts.size()*2;
	int posIts = contacts.size()*2;
	for(auto &contact : contacts)//for loop preps contacts implementation all moved inline its not neat but should help a lil bit performance wise
	{
		if (!contact.mValidData)
		{
			break;
		}
		if(contact.m_Entities[0]==nullptr||!contact.m_Entities[0]->m_PhysicsEnabled)
		{
			contact.m_ContactNormal.invert();
			BaseDragonPhysicsObject *temp = contact.m_Entities[0];
			contact.m_Entities[0] = contact.m_Entities[1];
			contact.m_Entities[1] = temp;
		}
		DragonXVector3 DragonContactTangent[2];
		if((real)abs(contact.m_ContactNormal.x) > (real)abs(contact.m_ContactNormal.y))
		{
			const real s = (real)1.0f/(real)sqrt(contact.m_ContactNormal.z*contact.m_ContactNormal.z+contact.m_ContactNormal.x*contact.m_ContactNormal.x);
			DragonContactTangent[0].x=contact.m_ContactNormal.z*s;
			DragonContactTangent[0].y= 0.0f;
			DragonContactTangent[0].z= -contact.m_ContactNormal.x*s;
			DragonContactTangent[1].x= contact.m_ContactNormal.y*DragonContactTangent[0].x;
			DragonContactTangent[1].y=contact.m_ContactNormal.z*DragonContactTangent[0].x-contact.m_ContactNormal.x*DragonContactTangent[0].z;
			DragonContactTangent[1].z=-contact.m_ContactNormal.y*DragonContactTangent[0].x;
		}
		else
		{
			const real s = (real)1.0/(real)sqrt(contact.m_ContactNormal.z*contact.m_ContactNormal.z+contact.m_ContactNormal.y*contact.m_ContactNormal.y);
			DragonContactTangent[0].x=(0);
			DragonContactTangent[0].y=-contact.m_ContactNormal.z*s;
			DragonContactTangent[0].z=contact.m_ContactNormal.y*s;
			DragonContactTangent[1].x= contact.m_ContactNormal.y*DragonContactTangent[0].z-contact.m_ContactNormal.z*DragonContactTangent[0].y;
			DragonContactTangent[1].y=-contact.m_ContactNormal.x*DragonContactTangent[0].z;
			DragonContactTangent[1].z=contact.m_ContactNormal.x*DragonContactTangent[0].y;
		}
		contact.mContactToWorld.setComponents(contact.m_ContactNormal,DragonContactTangent[0],DragonContactTangent[1]);
		contact.mRelitiveContactPosition[0]=contact.m_ContactPoint-contact.m_Entities[0]->m_Position;
		if(contact.m_Entities[1]!=nullptr&&contact.m_Entities[1]->m_PhysicsEnabled)
		{
			contact.mRelitiveContactPosition[1]=contact.m_ContactPoint-contact.m_Entities[1]->m_Position;
		}
		DragonXVector3 velocity = contact.m_Entities[0]->TransformMatrix.TransformDirection(contact.m_Entities[0]->m_RotationalVelocity)%contact.mRelitiveContactPosition[0];
		velocity+=contact.m_Entities[0]->m_LinearVelocity;
		DragonXVector3 contactVel=contact.mContactToWorld.TransformTranspose(velocity);
		DragonXVector3 accVel=contact.m_Entities[0]->m_LastFrameAccel*dt;
		accVel=contact.mContactToWorld.TransformTranspose(accVel);
		accVel.x=0.0f;
		contactVel+=accVel;
		contact.mContactVelocity=contactVel;
		if (contact.m_Entities[1]!=nullptr&&contact.m_Entities[1]->m_PhysicsEnabled)
		{
			DragonXVector3 velocity = contact.m_Entities[1]->TransformMatrix.TransformDirection(contact.m_Entities[1]->m_RotationalVelocity)%contact.mRelitiveContactPosition[1];
			velocity+=contact.m_Entities[1]->m_LinearVelocity;
			DragonXVector3 contactVel=contact.mContactToWorld.TransformTranspose(velocity);
			DragonXVector3 accVel=contact.m_Entities[1]->m_LastFrameAccel*dt;
			accVel=contact.mContactToWorld.TransformTranspose(accVel);
			accVel.x=0.0f;
			contactVel+=accVel;
			contact.mContactVelocity-=contactVel;
		}
		const real velocityLimit = (real).5f;
		real velFromAcc = 0.0f;
		if(contact.m_Entities[0]->m_isAwake)
		{
			velFromAcc+=contact.m_Entities[0]->m_LastFrameAccel*dt*contact.m_ContactNormal;
		}
		if (contact.m_Entities[1]!=nullptr&&contact.m_Entities[1]->m_PhysicsEnabled)
		{
			if (contact.m_Entities[1]->m_isAwake)
			{
				velFromAcc-=contact.m_Entities[1]->m_LastFrameAccel*dt*contact.m_ContactNormal;
			}
		}
		real rest = contact.mRestitution;
		if ((real)abs(contact.mContactVelocity.GetMagSquared()) < velocityLimit)
		{
			rest = (real)0.0f;
		}
		contact.mDesiredDeltaVelocity=-contact.mContactVelocity.x - rest * (contact.mContactVelocity.x - velFromAcc);
	}
	// interpenetration fix loop
	DragonXVector3 linearChange[2], angularChange[2];
	DragonXVector3 deltaPosition;
	int m_iPositionIterationsUsed = 0;
	while (m_iPositionIterationsUsed < posIts)
	{
		auto itr = std::max_element(begin(contacts),end(contacts),[&](DragonContact& a, DragonContact& b){return a.mPenitrtionDepth <b.mPenitrtionDepth;});
		if (itr.operator*().mPenitrtionDepth<mPositionEpsilon)
		{
			break;
		}
		if (itr.operator*().m_Entities[1]!=nullptr&&itr.operator*().m_Entities[1]->m_PhysicsEnabled)
		{
			bool m_aEntitiesContacted0awake = itr.operator*().m_Entities[0]->m_isAwake;
			bool m_aEntitiesContacted1awake = itr.operator*().m_Entities[1]->m_isAwake;
			if (m_aEntitiesContacted0awake ^ m_aEntitiesContacted1awake)
			{
				if (m_aEntitiesContacted0awake)
				{
					itr.operator*().m_Entities[1]->setAwake(true);
				}
				else
				{
					itr.operator*().m_Entities[0]->setAwake(true);
				}
			}
		}
		const real angularLimit = (real)0.0001f;
		real angularMove[2];
		real linearMove[2];
		real totalInertia = 0;
		real linearInertia[2];
		real angularInertia[2];
		for (unsigned i = 0; i < 2; i++)
		{
			if(itr.operator*().m_Entities[i]!=nullptr&&itr.operator*().m_Entities[i]->m_PhysicsEnabled)
			{
				DragonXMatrix inverseInertiaTensor;
				inverseInertiaTensor=itr.operator*().m_Entities[i]->inverseInertiaTensorWorld;
				DragonXVector3 angularInertiaWorld = itr.operator*().mRelitiveContactPosition[i] % itr.operator*().m_ContactNormal;
				angularInertiaWorld = inverseInertiaTensor.TransformDirection(angularInertiaWorld);
				angularInertiaWorld = angularInertiaWorld % itr.operator*().mRelitiveContactPosition[i];
				angularInertia[i] = angularInertiaWorld * itr.operator*().m_ContactNormal;
				linearInertia[i] = itr.operator*().m_Entities[i]->m_InverseMass;
				totalInertia += linearInertia[i] + angularInertia[i];
			}
		}
		for (unsigned i = 0; i < 2; i++)
		{
			if(itr.operator*().m_Entities[i]!=nullptr&&itr.operator*().m_Entities[i]->m_PhysicsEnabled)
			{
				real sign =(i == 0)?(real)1:-1;
				angularMove[i] = sign * itr.operator*().mPenitrtionDepth * (angularInertia[i] / totalInertia);
				linearMove[i] = sign * itr.operator*().mPenitrtionDepth * (linearInertia[i] / totalInertia);
				DragonXVector3 proj =  itr.operator*().mRelitiveContactPosition[i];
				proj.operator+=(itr.operator*().m_ContactNormal*( itr.operator*().mRelitiveContactPosition[i]*-1.0f*itr.operator*().m_ContactNormal));
				real maxMag= angularLimit * proj.GetMagnitude();
				if(angularMove[i] < -maxMag)
				{
					real totalMove = angularMove[i] + linearMove[i];
					angularMove[i] = -maxMag;
					linearMove[i]= totalMove-angularMove[i];
				}
				if(angularMove[i] > maxMag)
				{
					real totalMove = angularMove[i] + linearMove[i];
					angularMove[i] = maxMag;
					linearMove[i]= totalMove-angularMove[i];
				}
				if (angularMove[i] == 0)
				{
					angularChange[i].clear();
				}
				else
				{
					DragonXVector3 targetAngularDirection = itr.operator*().mRelitiveContactPosition[i]%itr.operator*().m_ContactNormal;
					DragonXMatrix inverseInertiaTensor =  itr.operator*().m_Entities[i]->inverseInertiaTensorWorld;
					angularChange[i] = 	inverseInertiaTensor.TransformDirection(targetAngularDirection) * angularMove[i];
				}
				linearChange[i]=itr.operator*().m_ContactNormal*linearMove[i];
				itr.operator*().m_Entities[i]->m_Position+=linearChange[i];
				itr.operator*().m_Entities[i]->m_QuatRot.addScaledVectorDrX(itr.operator*().m_Entities[i]->TransformMatrix.TransformInverseDirection(angularChange[i]),(real)1.0f);
				itr.operator*().m_Entities[i]->m_QuatRot.normalize();
				itr.operator*().m_Entities[i]->UpdateActor();
				if (!itr.operator*().m_Entities[i]->m_isAwake)
				{
					itr.operator*().m_Entities[i]->UpdateMatricies();					
				}			
			}
		}
		for (auto &contact : contacts)
		{
			if(!contact.mValidData)
			{
				break;
			}
			for (unsigned b = 0; b < 2; b++)
			{
				if (contact.m_Entities[b]!=nullptr&&contact.m_Entities[b]->m_PhysicsEnabled)
				{
					for (unsigned d = 0; d < 2; d++)
					{
						if (contact.m_Entities[b] == itr.operator*().m_Entities[d])
						{
							deltaPosition = linearChange[d] + angularChange[d]%contact.mRelitiveContactPosition[b];
							contact.mPenitrtionDepth += deltaPosition * contact.m_ContactNormal*(b?1:-1);
						}
					}
				}
			}
		}
		m_iPositionIterationsUsed++;
	}
	// velocity fix loop
	DragonXVector3 velocityChange[2], rotationChange[2];
	DragonXVector3 deltaVel;
	int m_iVelocityIterationsUsed = 0;
	while (m_iVelocityIterationsUsed < velIts)
	{
		auto itr = std::max_element(begin(contacts),end(contacts),[&](DragonContact& a,DragonContact& b){return a.mDesiredDeltaVelocity<b.mDesiredDeltaVelocity;});
		if (itr.operator*().mDesiredDeltaVelocity<mVelocityEpsilon)
		{
			break;
		}
		if (itr.operator*().m_Entities[1]!=nullptr&&itr.operator*().m_Entities[1]->m_PhysicsEnabled)
		{
			bool m_aEntitiesContacted0awake = itr.operator*().m_Entities[0]->m_isAwake;
			bool m_aEntitiesContacted1awake = itr.operator*().m_Entities[1]->m_isAwake;
			if (m_aEntitiesContacted0awake ^ m_aEntitiesContacted1awake)
			{
				if (m_aEntitiesContacted0awake)
				{
					itr.operator*().m_Entities[1]->setAwake(true);
				}
				else
				{
					itr.operator*().m_Entities[0]->setAwake(true);
				}
			}
		}
		DragonXMatrix inverseInertiaTensor[2];
		inverseInertiaTensor[0]=itr.operator*().m_Entities[0]->inverseInertiaTensorWorld;
		if(itr.operator*().m_Entities[1]!=nullptr&&itr.operator*().m_Entities[1]->m_PhysicsEnabled)
		{
			inverseInertiaTensor[1]=itr.operator*().m_Entities[1]->inverseInertiaTensorWorld;
		}
		DragonXVector3 impulseDragonContactW;
		if(itr.operator*().mFriction>0.0f)
		{
			DragonXVector3 impulseDragonContact;
			real inverseMass = itr.operator*().m_Entities[0]->m_InverseMass;
			DragonXMatrix impulseToTorque;
			impulseToTorque.setSkewSymmetric(itr.operator*().mRelitiveContactPosition[0]);
			DragonXMatrix deltaVelWorld = impulseToTorque;
			deltaVelWorld *= inverseInertiaTensor[0];
			deltaVelWorld *= impulseToTorque;
			deltaVelWorld *= -1;
			if (itr.operator*().m_Entities[1]!=nullptr&&itr.operator*().m_Entities[1]->m_PhysicsEnabled)
			{
				impulseToTorque.setSkewSymmetric(itr.operator*().mRelitiveContactPosition[1]);
				DragonXMatrix deltaVelWorld2 = impulseToTorque;
				deltaVelWorld2 *= inverseInertiaTensor[1];
				deltaVelWorld2 *= impulseToTorque;
				deltaVelWorld2 *= -1;
				deltaVelWorld += deltaVelWorld2;
				inverseMass += itr.operator*().m_Entities[1]->m_InverseMass;
			}
			DragonXMatrix deltaVelocity = itr.operator*().mContactToWorld.transpose();
			deltaVelocity *= deltaVelWorld;
			deltaVelocity *= itr.operator*().mContactToWorld;
			deltaVelocity._11 += inverseMass;
			deltaVelocity._22 += inverseMass;
			deltaVelocity._33 += inverseMass;
			DragonXMatrix impulseMatrix = deltaVelocity.getInverse();
			DragonXVector3 velKill(itr.operator*().mDesiredDeltaVelocity,-1.0f*itr.operator*().mContactVelocity.y,-1.0f*itr.operator*().mContactVelocity.z);
			impulseDragonContact = impulseMatrix.TransformDirection(velKill);
			real planarImpulse = (real)sqrt(impulseDragonContact.y*impulseDragonContact.y+impulseDragonContact.z*impulseDragonContact.z);
			if (planarImpulse > impulseDragonContact.x * itr.operator*().mFriction)
			{
				impulseDragonContact.y=( impulseDragonContact.y/ planarImpulse);
				impulseDragonContact.z=( impulseDragonContact.z/ planarImpulse);
				impulseDragonContact.x=( deltaVelocity._11 +
					deltaVelocity._12*itr.operator*().mFriction*impulseDragonContact.y +
					deltaVelocity._13*itr.operator*().mFriction*impulseDragonContact.z);
				impulseDragonContact.x=( itr.operator*().mDesiredDeltaVelocity / impulseDragonContact.x);
				impulseDragonContact.y=( impulseDragonContact.y * itr.operator*().mFriction * impulseDragonContact.x);
				impulseDragonContact.z=( impulseDragonContact.z * itr.operator*().mFriction * impulseDragonContact.x);
			}
			impulseDragonContactW=impulseDragonContact;
		}
		else
		{
			DragonXVector3 impulseDragonContact;
			DragonXVector3 deltaVelWorld = itr.operator*().mRelitiveContactPosition[0] % itr.operator*().m_ContactNormal;
			deltaVelWorld = inverseInertiaTensor[0].TransformDirection(deltaVelWorld);
			deltaVelWorld = deltaVelWorld % itr.operator*().mRelitiveContactPosition[0];
			real deltaVelocity = deltaVelWorld * itr.operator*().m_ContactNormal;
			deltaVelocity += itr.operator*().m_Entities[0]->m_InverseMass;
			if(itr.operator*().m_Entities[1]!=nullptr&&itr.operator*().m_Entities[1]->m_PhysicsEnabled)
			{
				DragonXVector3 deltaVelWorld = itr.operator*().mRelitiveContactPosition[1] % itr.operator*().m_ContactNormal;
				deltaVelWorld = inverseInertiaTensor[1].TransformDirection(deltaVelWorld);
				deltaVelWorld = deltaVelWorld % itr.operator*().mRelitiveContactPosition[1];
				deltaVelocity += deltaVelWorld * itr.operator*().m_ContactNormal;
				deltaVelocity +=itr.operator*().m_Entities[1]->m_InverseMass;
			}
			impulseDragonContact.x=(itr.operator*().mDesiredDeltaVelocity / deltaVelocity);
			impulseDragonContact.y=(0);
			impulseDragonContact.z=(0);
			impulseDragonContactW=impulseDragonContact;
		}
		DragonXVector3 impulse = itr.operator*().mContactToWorld.TransformDirection(impulseDragonContactW);
		DragonXVector3 impulsiveTorque = itr.operator*().mRelitiveContactPosition[0]%impulse;
		rotationChange[0] = inverseInertiaTensor[0].TransformDirection(impulsiveTorque);
		velocityChange[0].clear();
		velocityChange[0] = impulse * itr.operator*().m_Entities[0]->m_InverseMass;
		itr.operator*().m_Entities[0]->m_LinearVelocity+=velocityChange[0];
		itr.operator*().m_Entities[0]->m_RotationalVelocity+= itr.operator*().m_Entities[0]->TransformMatrix.TransformInverseDirection(rotationChange[0]);
		if(itr.operator*().m_Entities[1]!=nullptr&&itr.operator*().m_Entities[1]->m_PhysicsEnabled)
		{
			impulse*=-1.0f;
			DragonXVector3 impulsiveTorque = itr.operator*().mRelitiveContactPosition[1]%impulse;
			rotationChange[1] = inverseInertiaTensor[1].TransformDirection(impulsiveTorque);
			velocityChange[1].clear();
			velocityChange[1]= impulse * itr.operator*().m_Entities[1]->m_InverseMass;
			itr.operator*().m_Entities[1]->m_LinearVelocity+=velocityChange[1];
			itr.operator*().m_Entities[1]->m_RotationalVelocity+=itr.operator*().m_Entities[1]->TransformMatrix.TransformInverseDirection(rotationChange[1]);
		}
		for (auto &contact : contacts)
		{
			if(!contact.mValidData)
			{
				break;
			}
			for (unsigned b = 0; b < 2; b++)
			{
				if (contact.m_Entities[b]!=nullptr&&contact.m_Entities[b]->m_PhysicsEnabled)
				{
					for (unsigned d = 0; d < 2; d++)
					{
						if (contact.m_Entities[b] == itr.operator*().m_Entities[d])
						{
							deltaVel = velocityChange[d] + rotationChange[d] % contact.mRelitiveContactPosition[b];
							contact.mContactVelocity +=contact.mContactToWorld.TransformTranspose(deltaVel)* (real)(b?-1:1);
							const real velocityLimit = (real).5f;
							real velFromAcc = 0.0f;
							if(contact.m_Entities[0]->m_isAwake)
							{
								velFromAcc+=contact.m_Entities[0]->m_LastFrameAccel*dt*contact.m_ContactNormal;
							}
							if (contact.m_Entities[1]!=nullptr&&contact.m_Entities[1]->m_PhysicsEnabled)
							{
								if (contact.m_Entities[1]->m_isAwake)
								{
									velFromAcc-=contact.m_Entities[1]->m_LastFrameAccel*dt*contact.m_ContactNormal;
								}
							}
							real rest = contact.mRestitution;
							if ((real)abs(contact.mContactVelocity.GetMagSquared()) < velocityLimit)
							{
								rest = (real)0.0f;
							}
							contact.mDesiredDeltaVelocity=-contact.mContactVelocity.x - rest * (contact.mContactVelocity.x - velFromAcc);
						}
					}
				}
			}
		}
		m_iVelocityIterationsUsed++;
	}
}