#include "DragonPhysicsEngine.h"
#include "DragonPhysicsObjectManager.h"
#include "DragonPhysicsHydraOctreePartitioner.h"
#include "HydraObjectUpdater.h"
#include "DragonContactRegistry.h"
#include "DragonForceEngine.h"
#include "DragonPhysicsCollisionEngine.h"
#include "Actor.h"
#include "BaseDragonCollisionObject.h"
#include "DragonPhysicsCollisionSphere.h"
#include "DragonPhysicsMeshCollisionObject.h"
#include "DragonPhysicsOBB.h"
#include "DragonPhysicsPlane.h"
#include "MeshCollisionOctreePartitioner.h"
#include "DragonContactPair.h"
#include "DragonForceGenerator.h"
#include "DragonForceRegistry.h"

IDirect3DVertexDeclaration9* VertexPNT::Decl = 0;

DragonPhysicEngine::DragonPhysicEngine()
{
	m_DragonPhysicsObjectManager = new DragonPhysicsObjectManager();
	m_DragonCollisionEngine = new DragonCollisionEngine();
	m_HydraObjectUpdater = new HydraObjectUpdater();
	m_DragonPhysicsHydraOctreePartitioner = new DragonPhysicsHydraOctreePartitioner();
	m_DragonForceEngine = new DragonForceEngine();
}
DragonPhysicEngine::~DragonPhysicEngine()
{
	delete m_DragonPhysicsObjectManager;
	delete m_DragonCollisionEngine;
	delete m_HydraObjectUpdater;
	delete m_DragonPhysicsHydraOctreePartitioner;
	delete m_DragonForceEngine;
}
void DragonPhysicEngine::UpdatePhysics(const float dt)
{
	m_DragonCollisionEngine->mContactRegistry->ResetData();
	//m_DragonPhysicsHydraOctreePartitioner->ReleaseHydra();
	m_DragonPhysicsHydraOctreePartitioner->Create(m_DragonPhysicsObjectManager->m_VectorOfPhysxOjects,*m_HydraObjectUpdater,*m_DragonCollisionEngine,dt);
	//m_DragonPhysicsHydraOctreePartitioner->UseHydra(*m_HydraObjectUpdater,*m_DragonCollisionEngine,m_DragonPhysicsHydraOctreePartitioner,dt);
}
bool DragonPhysicEngine::CreateSphereCollisionObjectSchematic(float radius,std::string SphereSchemName)
{
	auto res = m_DragonPhysicsObjectManager->m_CollisionObjectRegistry.find(SphereSchemName);
	if(res!=m_DragonPhysicsObjectManager->m_CollisionObjectRegistry.end())
	{
		return false;
	}
	else
	{
		DragonPhysicsCollisionSphere* sphere = new DragonPhysicsCollisionSphere();
		sphere->mRadius=radius;
		m_DragonPhysicsObjectManager->m_CollisionObjectRegistry.insert(std::make_pair(SphereSchemName,sphere));
		return true;
	}
}
bool DragonPhysicEngine::CreateObbCollisionObjectSchematic(D3DXVECTOR3 halfSizes,std::string ObbSchemName)
{
	auto res = m_DragonPhysicsObjectManager->m_CollisionObjectRegistry.find(ObbSchemName);
	if(res!=m_DragonPhysicsObjectManager->m_CollisionObjectRegistry.end())
	{
		return false;
	}
	else
	{
		DragonPhysicsCollisionOBB* obb = new DragonPhysicsCollisionOBB();
		obb->halfExtents=halfSizes;
		obb->mRadius=obb->halfExtents.GetMagnitude();
		m_DragonPhysicsObjectManager->m_CollisionObjectRegistry.insert(std::make_pair(ObbSchemName,obb));
		return true;
	}
}
bool DragonPhysicEngine::CreatePlaneCollisionObjectSchematic(D3DXVECTOR3 normalDirection,float dist,std::string PlaneSchemName)
{
	auto res = m_DragonPhysicsObjectManager->m_CollisionObjectRegistry.find(PlaneSchemName);
	if(res!=m_DragonPhysicsObjectManager->m_CollisionObjectRegistry.end())
	{
		return false;
	}
	else
	{
		DragonPhysicsPlane* plane = new DragonPhysicsPlane();
		plane->mNormalDirection=normalDirection;
		plane->mDistFromOrigin=dist;
		m_DragonPhysicsObjectManager->m_CollisionObjectRegistry.insert(std::make_pair(PlaneSchemName,plane));
		return true;
	}
}
bool DragonPhysicEngine::CreateMeshCollisionObject(IDirect3DDevice9* gDevice, std::string meshCollisionObjectFile,std::string MeshSchemName)
{
	auto res = m_DragonPhysicsObjectManager->m_CollisionObjectRegistry.find(MeshSchemName);
	if(res!=m_DragonPhysicsObjectManager->m_CollisionObjectRegistry.end())
	{
		return false;
	}
	else
	{
		ID3DXMesh* tempMesh;
		D3DVERTEXELEMENT9 VertexPNTElements[] =
		{
			{0, 0,  D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
			{0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0},
			{0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
			D3DDECL_END()
		};
		gDevice->CreateVertexDeclaration(VertexPNTElements, &VertexPNT::Decl);
		ID3DXMesh* meshSys      = 0;
		ID3DXBuffer* adjBuffer  = 0;
		ID3DXBuffer* mtrlBuffer = 0;
		DWORD numMtrls          = 0;	
		HRESULT hr = (D3DXLoadMeshFromX(meshCollisionObjectFile.c_str(),D3DXMESH_SYSTEMMEM,gDevice,&adjBuffer,&mtrlBuffer,NULL,NULL,&meshSys));
		D3DVERTEXELEMENT9 elements[64];
		UINT numElements = 0;
		VertexPNT::Decl->GetDeclaration(elements, &numElements);
		ID3DXMesh* temp = 0;
		meshSys->CloneMesh(D3DXMESH_SYSTEMMEM,elements, gDevice, &temp);
		meshSys->Release();
		meshSys = temp;	
		meshSys->Optimize(D3DXMESH_MANAGED|D3DXMESHOPT_COMPACT|D3DXMESHOPT_ATTRSORT|D3DXMESHOPT_VERTEXCACHE,(DWORD*)adjBuffer->GetBufferPointer(),0,0,0,&tempMesh);
		meshSys->Release(); 
		adjBuffer->Release(); 		
		std::vector<DragonXVector3*> vPos;
		LPDIRECT3DVERTEXBUFFER9 pVBuf;
		if (SUCCEEDED(tempMesh->GetVertexBuffer(&pVBuf)))
		{
			VertexPNT *pVert;
			if (SUCCEEDED(pVBuf->Lock(0,0,(void **) &pVert,D3DLOCK_DISCARD)))
			{
				DWORD numVerts=tempMesh->GetNumVertices();
				for (int i=0;(unsigned)i<numVerts;i++)
				{
					DragonXVector3* v = new DragonXVector3();
					*v=pVert[i].pos;
					vPos.push_back(v);
				}
				pVBuf->Unlock();
			}			
			pVBuf->Release();			
		}	
		else
		{
			tempMesh->Release();
			VertexPNT::Decl->Release();
			return false;
		}		
		std::vector<DragonXVector3*> vIdx;
		LPDIRECT3DINDEXBUFFER9 pIBuf;
		if (SUCCEEDED(tempMesh->GetIndexBuffer(&pIBuf)))
		{
			WORD* pInd;
			DWORD numFaces=tempMesh->GetNumFaces();
			D3DINDEXBUFFER_DESC desc;
			pIBuf->GetDesc(&desc);
			if (SUCCEEDED(pIBuf->Lock(0,0,(void **) &pInd,D3DLOCK_DISCARD)))
			{
				for(int i = 0; (unsigned)i<numFaces*3;i+=3)
				{
					DragonXVector3* p = new DragonXVector3(pInd[i],pInd[i+1],pInd[i+2]);
					vIdx.push_back(p);
				}
				pIBuf->Unlock();
			}
			pIBuf->Release();
		}	
		else
		{
			tempMesh->Release();
			VertexPNT::Decl->Release();
			return false;
		}
		std::vector<DragonTriangle*> tris;
		for (int i = 0;(unsigned)i<vIdx.size();i++)
		{
			DragonTriangle* t = new DragonTriangle(*vPos[(unsigned)vIdx[i]->x],*vPos[(unsigned)vIdx[i]->y],*vPos[(unsigned)vIdx[i]->z]);
			tris.push_back(t);
		}
		for (auto DragonXVector3 : vPos)
		{
			delete DragonXVector3;
			DragonXVector3=nullptr;
		}
		vPos.clear();
		for (auto DragonXVector3 : vIdx)
		{
			delete DragonXVector3;
			DragonXVector3=nullptr;
		}
		vIdx.clear();
		DragonMeshCollisionObject* colMesh = new DragonMeshCollisionObject();
		colMesh->m_DragonTris=tris;
		MeshOctreePartitioner octree = MeshOctreePartitioner();
		octree.Create(tris,tris.size());
		octree.GetContacts(colMesh->m_DragonSectors,&octree);
		colMesh->halfExtents=octree.m_HalfExtents;
		colMesh->mRadius=octree.m_HalfExtents.GetMagnitude();
		octree.Release();	
		m_DragonPhysicsObjectManager->m_CollisionObjectRegistry.insert(std::make_pair(MeshSchemName,colMesh));
		tempMesh->Release();
		VertexPNT::Decl->Release();
		return true;
	}
}
int DragonPhysicEngine::CreatePhysicsObject(int id,D3DXVECTOR3 pos,std::string SchemName,float mass,bool PhysxEnable,bool colResolutionOn,Actor* actr)
{
	auto colObj = m_DragonPhysicsObjectManager->m_CollisionObjectRegistry.find(SchemName);
	if(colObj==m_DragonPhysicsObjectManager->m_CollisionObjectRegistry.end())
	{
		return -1;
	}
	else
	{
		int tid = 0;
		bool isIDused=false;
		for(const auto &BaseDragonPhysicsObject: m_DragonPhysicsObjectManager->m_VectorOfPhysxOjects)
		{
			if(BaseDragonPhysicsObject->m_ID==id)
			{
				isIDused=true;
			}
			if(BaseDragonPhysicsObject->m_ID>=(unsigned)tid)
			{
				tid = BaseDragonPhysicsObject->m_ID+1;
			}
		}
		if (isIDused)
		{
			BaseDragonPhysicsObject* nDragonPhysxObj = new BaseDragonPhysicsObject();
			nDragonPhysxObj->m_ID=tid;
			nDragonPhysxObj->m_Position=pos;
			nDragonPhysxObj->m_CollisionObject=colObj->second;
			nDragonPhysxObj->mI_PrimitiveType=colObj->second->m_PrimTypeID;
			nDragonPhysxObj->ReCalcInverseTensor(mass);
			nDragonPhysxObj->m_PhysicsEnabled=PhysxEnable;
			nDragonPhysxObj->m_isCollisionResolutionOn=colResolutionOn;
			//nDragonPhysxObj->mActor=actr;
			nDragonPhysxObj->UpdatePhysX();
			nDragonPhysxObj->UpdateMatricies();
			m_DragonPhysicsObjectManager->m_VectorOfPhysxOjects.push_back(nDragonPhysxObj);
			return tid;
		}
		else
		{
			BaseDragonPhysicsObject* nDragonPhysxObj = new BaseDragonPhysicsObject();
			nDragonPhysxObj->m_ID=id;
			nDragonPhysxObj->m_Position=pos;
			nDragonPhysxObj->m_CollisionObject=colObj->second;
			nDragonPhysxObj->mI_PrimitiveType=colObj->second->m_PrimTypeID;
			nDragonPhysxObj->ReCalcInverseTensor(mass);
			nDragonPhysxObj->m_PhysicsEnabled=PhysxEnable;
			nDragonPhysxObj->m_isCollisionResolutionOn=colResolutionOn;
			//nDragonPhysxObj->mActor=actr;
			nDragonPhysxObj->UpdatePhysX();
			nDragonPhysxObj->UpdateMatricies();
			m_DragonPhysicsObjectManager->m_VectorOfPhysxOjects.push_back(nDragonPhysxObj);
			return id;
		}
	}
}
std::map<int,std::vector<int>> DragonPhysicEngine::GetPairs(std::vector<int> IDs)
{
	std::map<int,std::vector<int>> retVal;
	for(const auto ID : IDs)
	{
		std::vector<int> vCnPsFrID;
		for(const auto &DragonContactPair : m_DragonCollisionEngine->mContactRegistry->m_VecOfPairRegistrations)
		{
			if(DragonContactPair.mObj1ID==ID)
			{
				vCnPsFrID.push_back(DragonContactPair.mObj2ID);
			}
			else if(DragonContactPair.mObj2ID==ID)
			{
				vCnPsFrID.push_back(DragonContactPair.mObj1ID);
			}
		}
		retVal.insert(std::make_pair(ID,vCnPsFrID));
	}
	return retVal;
}
std::vector<bool> DragonPhysicEngine::SetPhysxEnable(std::map<int,bool> mapOfData)
{
	std::vector<bool> retVal;
	for(auto &mPair : mapOfData)
	{
		bool worked=false;
		for(auto &entities : m_DragonPhysicsObjectManager->m_VectorOfPhysxOjects)
		{
			if(entities->m_ID==mPair.first)
			{
				entities->m_PhysicsEnabled=mPair.second;
				worked=true;
				break;
			}
		}
		retVal.push_back(worked);
	}
	return retVal;
}
std::vector<bool> DragonPhysicEngine::SetLinearAccel(std::map<int,D3DXVECTOR3> mapOfIdsAndLinearAccelData)
{
	std::vector<bool> retVal;
	for(auto &mPair : mapOfIdsAndLinearAccelData)
	{
		bool worked=false;
		for(auto &entities : m_DragonPhysicsObjectManager->m_VectorOfPhysxOjects)
		{
			if(entities->m_ID==mPair.first)
			{
				entities->m_LinearAccel=mPair.second;
				worked=true;
				break;
			}
		}
		retVal.push_back(worked);
	}
	return retVal;
}
std::vector<bool> DragonPhysicEngine::SetCollisionObject(std::map<int,std::string> mapOfIDsAndCollisionObjectData)
{
	std::vector<bool> retVal;
	for(auto &mPair : mapOfIDsAndCollisionObjectData)
	{
		bool worked=false;
		auto colObj = m_DragonPhysicsObjectManager->m_CollisionObjectRegistry.find(mPair.second);
		if (colObj==m_DragonPhysicsObjectManager->m_CollisionObjectRegistry.end())
		{
			retVal.push_back(worked);
			break;
		}
		else
		{
			for(auto &entities : m_DragonPhysicsObjectManager->m_VectorOfPhysxOjects)
			{
				if(entities->m_ID==mPair.first)
				{
					entities->m_CollisionObject=colObj->second;
					worked=true;
					break;
				}
			}
			retVal.push_back(worked);
		}		
	}
	return retVal;
}
std::vector<bool> DragonPhysicEngine::SetMass(std::map<int,float> mapOfIdsAndMasses)
{
	std::vector<bool> retVal;
	for(auto &mPair : mapOfIdsAndMasses)
	{
		bool worked=false;
		for(auto &entities : m_DragonPhysicsObjectManager->m_VectorOfPhysxOjects)
		{
			if(entities->m_ID==mPair.first)
			{
				entities->setMass(mPair.second);
				worked=true;
				break;
			}
		}
		retVal.push_back(worked);
	}
	return retVal;
}
int DragonPhysicEngine::CreateBlastForce(D3DXVECTOR3 detPoint,float ImplMinRad,float inplMaxRad,float implDur,float implForce,float shkWvSpd,float shkWvThkns,float pkCncusFc,float cncusDur,float pkcnvecFc,float chmnRad,float chmnHght,float cnvecDur)
{
	DragonForceGenerator nForceGen = DragonForceGenerator();
	nForceGen.m_ForceID=m_DragonForceEngine->mDragonForceRegistry->mNxtID;
	m_DragonForceEngine->mDragonForceRegistry->mNxtID++;	
	float m_timePassed=0.0f;
	DragonXVector3 m_pointOfDetonation = detPoint;
	float m_implosionMaxRadius = inplMaxRad;
	float m_implosionMinRadius=ImplMinRad;	
	float m_implosionDuration = implDur;	
	float m_implosionForce = implForce;
	float m_shockWaveSpeed=shkWvSpd;
	float m_shockWaveThickness=shkWvThkns;	
	float m_peakConcussiveForce=pkCncusFc;	
	float m_concussionDuration=cncusDur;	
	float m_peakConvectionForce=pkcnvecFc;
	float m_chmineyRadius=chmnRad;	
	float m_chimneyHeight=chmnHght;
	float m_convectionDuration=cnvecDur;
	nForceGen.m_ForceEffect=[=](BaseDragonPhysicsObject* entity,float dt)mutable->bool
	{
		if(!entity->m_PhysicsEnabled)
		{
			return true;
		}
		m_timePassed+=dt;
		if(m_implosionDuration>0.0f)
		{
			m_implosionDuration-=dt;
			DragonXVector3 dir = entity->m_Position-m_pointOfDetonation;
			real dist=dir.GetMagnitude();
			if(dist<=m_implosionMaxRadius&&dist>=m_implosionMinRadius)
			{
				dir.Normalize();
				dir*=m_implosionForce;
				entity->addForce(dir);
			}
			return false;
		}
		else if(m_concussionDuration>0.0f)
		{
			m_concussionDuration-=dt;
			DragonXVector3 dir = entity->m_Position-m_pointOfDetonation;
			real dist=dir.GetMagnitude();
			real st=m_shockWaveSpeed*dt;
			real force =0.0f;
			if((st-(0.5f*m_shockWaveThickness))<=dist&&(dist<st))
			{
				force=1.0f-(st-dist)/(0.5f*m_shockWaveThickness);
				dir.Normalize();
				dir*=force*m_peakConcussiveForce;
				entity->addForce(dir);
			}
			else if((st<=dist)&&(dist<(st+m_shockWaveThickness)))
			{
				force = m_peakConcussiveForce;
				dir.Normalize();
				dir*=force*m_peakConcussiveForce;
				entity->addForce(dir);
			}
			else if(((st+m_shockWaveThickness)<=dist)&& (dist < (st+1.5f*m_shockWaveThickness)))
			{
				force = (dist-st-m_shockWaveThickness)/m_shockWaveThickness*0.5f;
				dir.Normalize();
				dir*=force*m_peakConcussiveForce;
				entity->addForce(dir);
			}
			else
			{
				force = 0.0f;
				dir.Normalize();
				dir*=force*m_peakConcussiveForce;
				entity->addForce(dir);
			}
			return false;
		}
		else if(m_convectionDuration>0.0f)
		{
			m_convectionDuration-=dt;
			DragonXVector3 dir = entity->m_Position-m_pointOfDetonation;
			real distxz= sqrt(dir.x*dir.x+dir.z*dir.z);
			if((distxz<m_chmineyRadius*2)&&dir.y<m_chimneyHeight)
			{
				real force = m_peakConvectionForce*distxz/m_chmineyRadius;
				dir.Normalize();
				dir*=force;
				entity->addForce(dir);
			}
			return false;
		}
		else
		{
			return true;
		}
	};
	m_DragonForceEngine->mDragonForceRegistry->m_ForceGenRegistry.insert(std::make_pair(nForceGen.m_ForceID,nForceGen));
	return nForceGen.m_ForceID;
}
int DragonPhysicEngine::CreateThrustForce(float thDur,float decOfThst,float magOfThst,D3DXVECTOR3 ThstDir)
{
	DragonForceGenerator nForceGen = DragonForceGenerator();
	nForceGen.m_ForceID=m_DragonForceEngine->mDragonForceRegistry->mNxtID;
	m_DragonForceEngine->mDragonForceRegistry->mNxtID++;
	float m_durationOfThrust=thDur;
	float m_decrement=decOfThst;
	float m_magnitudeOfTrust=magOfThst;
	DragonXVector3 m_direction=ThstDir;
	m_direction.Normalize();
	nForceGen.m_ForceEffect=[=](BaseDragonPhysicsObject* entity,float dt)mutable->bool
	{
		if(!entity->m_PhysicsEnabled)
		{
			return true;
		}
		if(m_durationOfThrust>0.0f)
		{
			m_durationOfThrust-=dt;
			m_magnitudeOfTrust-=m_decrement;
			m_direction.Normalize();
			m_direction*=m_magnitudeOfTrust;
			entity->addForce(m_direction);
			return false;
		}
		else if (m_durationOfThrust==0.0f)
		{
			m_direction.Normalize();
			m_direction*=m_magnitudeOfTrust;
			entity->addForce(m_direction);
			return true;
		}
		else if(m_durationOfThrust==-1.0f)
		{
			m_magnitudeOfTrust-=m_decrement;
			if (m_magnitudeOfTrust>0.0f)
			{
				m_direction.Normalize();
				m_direction*=m_magnitudeOfTrust;
				entity->addForce(m_direction);
				return false;
			}
			else
			{
				return true;
			}
		}
		else
		{
			return true;
		}
	};
	m_DragonForceEngine->mDragonForceRegistry->m_ForceGenRegistry.insert(std::make_pair(nForceGen.m_ForceID,nForceGen));
	return nForceGen.m_ForceID;
}
int DragonPhysicEngine::createCustomForce(std::function<bool (BaseDragonPhysicsObject* entity, float dt)> forceEffect)
{
	DragonForceGenerator nForceGen = DragonForceGenerator();
	nForceGen.m_ForceID=m_DragonForceEngine->mDragonForceRegistry->mNxtID;
	m_DragonForceEngine->mDragonForceRegistry->mNxtID++;
	nForceGen.m_ForceEffect=forceEffect;
	m_DragonForceEngine->mDragonForceRegistry->m_ForceGenRegistry.insert(std::make_pair(nForceGen.m_ForceID,nForceGen));
	return nForceGen.m_ForceID;
}
std::map<int,std::map<int,bool>> DragonPhysicEngine::AddForces(std::vector<int> ForceIDs,std::vector<int> ObjIDs)
{
	std::map<int,std::map<int,bool>> retVal;

	for(const auto &FcID : ForceIDs)
	{
		std::map<int,bool> objWm;
		auto fc = m_DragonForceEngine->mDragonForceRegistry->m_ForceGenRegistry.find(FcID);
		if(fc==m_DragonForceEngine->mDragonForceRegistry->m_ForceGenRegistry.end())
		{
			for(const auto &objID : ObjIDs)
			{
				objWm.insert(std::make_pair(objID,false));
			}
		}
		else
		{
			for(const auto &objID : ObjIDs)
			{
				bool worked=false;
				for(auto &entity : m_DragonPhysicsObjectManager->m_VectorOfPhysxOjects)
				{
					if(entity->m_ID==objID)
					{
						entity->m_VecOfForces.push_back(fc->second);
						worked=true;
						auto fcUsed = m_DragonForceEngine->mDragonForceRegistry->m_ForceApplicationRegistry.find(fc->first);
						if (fcUsed==m_DragonForceEngine->mDragonForceRegistry->m_ForceApplicationRegistry.end())
						{
							// force not yet in use
							std::vector<int> fcRgs;// so new registration vector for that force is required
							fcRgs.push_back(entity->m_ID);
							m_DragonForceEngine->mDragonForceRegistry->m_ForceApplicationRegistry.insert(std::make_pair(fc->first,fcRgs));
						}
						else
						{
							m_DragonForceEngine->mDragonForceRegistry->m_ForceApplicationRegistry.at(fc->first).push_back(entity->m_ID);// it is in use add it to its registration
						}
					}
				}
				objWm.insert(std::make_pair(objID,worked));
			}
		}
		retVal.insert(std::make_pair(FcID,objWm));
	}
	return retVal;
}
std::vector<bool> DragonPhysicEngine::ReleaseObjects(std::vector<int> IDs)
{	
	std::vector<bool> retVal;
	for(const auto &rIDs : IDs)
	{
		std::vector<BaseDragonPhysicsObject*> nVecObjs;
		bool worked = false;
		for(auto &entities : m_DragonPhysicsObjectManager->m_VectorOfPhysxOjects)
		{
			if (entities->m_ID!=rIDs)
			{
				nVecObjs.push_back(entities);
				worked=true;
			}
			else
			{
				delete entities;
			}
		}
		retVal.push_back(worked);
		m_DragonPhysicsObjectManager->m_VectorOfPhysxOjects=nVecObjs;
	}
	return retVal;
}

BaseDragonPhysicsObject* DragonPhysicEngine::GetComponent(int id)
{
	for(auto component : m_DragonPhysicsObjectManager->m_VectorOfPhysxOjects)
	{
		if(component->m_ID == id)
			return component;
	}
}

//HRESULT CreateDragonPhysicsEngine(HINSTANCE hDLL,IDragonPhysicsAPI **pInterface)
//{
//	if(!*pInterface)
//	{
//		*pInterface = new DragonPhysicEngine();
//		return 1;
//	}
//	else
//	{
//		return 0;
//	}
//}
//
//HRESULT ReleaseDragonPhysicsEngine(IDragonPhysicsAPI **pInterface)
//{
//	if(!*pInterface)
//	{
//		return 0;
//	}
//	else
//	{
//		delete *pInterface;
//		*pInterface=NULL;
//		return 1;
//	}
//}