#pragma once

#include "BaseDragonCollisionObject.h"
#include "DragonXMath.h"

class DragonPhysicsCollisionSphere : public BaseDragonCollisionObject
{
private:

public:
	float mRadius;
public:
	DragonPhysicsCollisionSphere();
	virtual ~DragonPhysicsCollisionSphere();
	virtual DragonXMatrix ReCalcInvInrTensor(float mass)
	{
		DragonXMatrix m = DragonXMatrix();
		D3DXMatrixIdentity(&m);
		real i = mass*0.4f*(mRadius*mRadius);
		m._11=i;
		m._22=i;
		m._33=i;
		D3DXMatrixInverse(&m,NULL,&m);
		return m;
	}
};