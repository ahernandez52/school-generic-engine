#include "DragonPhysicsObjectManager.h"

DragonPhysicsObjectManager::DragonPhysicsObjectManager()
{

}
DragonPhysicsObjectManager::~DragonPhysicsObjectManager()
{
	for(auto colObjs : m_CollisionObjectRegistry)
	{
		delete colObjs.second;
	}
	m_CollisionObjectRegistry.clear();
	for(auto physXobj : m_VectorOfPhysxOjects)
	{
		delete physXobj;
	}
	m_VectorOfPhysxOjects.clear();
}