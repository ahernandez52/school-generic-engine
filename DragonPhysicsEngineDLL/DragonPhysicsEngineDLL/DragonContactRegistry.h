#pragma once
#include "DragonContactRegistration.h"
#include "DragonContactPair.h"
#include "DragonContact.h"
#include <Windows.h>
#include <vector>

class DragonContactRegistry
{
private:
	CRITICAL_SECTION mContactCS;
	CRITICAL_SECTION mPairCS;
public:
	std::vector<DragonContactRegistration> m_VecOfContactRegistrations;
	std::vector<DragonPairRegistration> m_VecOfPairRegistrations;
public:
	DragonContactRegistry();
	~DragonContactRegistry();
	void UpdatePairData(std::vector<DragonContactPair> pairs);
	void UpdateContactData(std::vector<DragonContact> contacts);
	void ResetData();
};